/*****************************************************************************
 * y[i] = (float) x[i]
 *****************************************************************************/

#include <cfemdef.h>

void vsngl (int n, const double *x, int incx, float *y, int incy)
{
  int i;

  x += (incx<0) ? (-n+1)*incx : 0;
  y += (incy<0) ? (-n+1)*incy : 0;

  for (i = 0; i < n; i++) y[i*incy] = (float) x[i*incx];
}
