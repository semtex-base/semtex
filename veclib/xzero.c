/*****************************************************************************
 * xzero:  x[i] = 0.
 *****************************************************************************/

#include <string.h>
#include <cfemdef.h>

#if defined(__uxp__)
#pragma global novrec
#pragma global noalias
#endif

void dzero (int n, double* x, int incx)
{
#if defined(DEBUG)
  int i;

  x += (incx < 0) ? (-n+1)*incx : 0;

  for (i = 0; i < n; i++) x[i*incx] = 0.0;
#else
  if (incx == 1)
    memset (x, '\0', n * sizeof (double));

  else {
     int i;

    x += (incx < 0) ? (-n+1)*incx : 0;

    for (i = 0; i < n; i++) x[i*incx] = 0.0;
  }
#endif
}


void izero (int n, int* x, int incx)
{
  if (incx == 1)
    memset(x, '\0', n * sizeof (int));

  else {
     int i;

    x += (incx < 0) ? (-n+1)*incx : 0;

    for (i = 0; i < n; i++) x[i*incx] = 0;
  }
}


void szero (int n, float* x, int incx)
{
  if (incx == 1)
    memset(x, '\0', n * sizeof (float));

  else {
     int i;

    x += (incx < 0) ? (-n+1)*incx : 0;

    for (i = 0; i < n; i++) x[i*incx] = 0.0F;
  }
}
