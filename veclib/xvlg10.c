/*****************************************************************************
 * xvlg10:  y[i] = log10(x[i]).
 *****************************************************************************/

#include <math.h>
#include <cfemdef.h>

#if defined(__uxp__)
#pragma global novrec
#pragma global noalias
#endif

void dvlg10 (int n, const double* x, int incx,
                          double* y, int incy)
{
  int i;

  x += (incx<0) ? (-n+1)*incx : 0;
  y += (incy<0) ? (-n+1)*incy : 0;

  for (i = 0; i < n; i++) y[i*incy] = log10 (x[i*incx]);
}


void svlg10 (int n, const float* x, int incx,
                          float* y, int incy)
{
  int i;

  x += (incx<0) ? (-n+1)*incx : 0;
  y += (incy<0) ? (-n+1)*incy : 0;

#if  defined(__uxp__)
  for (i = 0; i < n; i++) y[i*incy] = (float) log10  (x[i*incx]);
#else
  for (i = 0; i < n; i++) y[i*incy] =         log10f (x[i*incx]);
#endif
}
