#ifndef UTILITY_H
#define UTILITY_H
///////////////////////////////////////////////////////////////////////////////
// Utility.h:  C++ wrapper for veclib utility and memory routines.
//
// Copyright (c) 1994+, Hugh M Blackburn
///////////////////////////////////////////////////////////////////////////////

#include <cfemdef.h>

template<class T> inline T sqr(T x)             { return x * x;            }
template<class T> inline T sgn(T x)             { return (x < 0) ? -1 : 1; }
template<class T> inline T clamp(T t, T a, T b) { return max(min(t,b),a);  }

// Max & Min are part of STL now, so have been removed from the above.

#ifndef M_PI
const double M_PI   = 3.14159265358979323846;
#endif

#ifndef TWOPI
const double TWOPI  = 6.28318530717958647692;
#endif

#ifndef PI_180
const double PI_180 = 0.01745329251994329576;
#endif

const double EPSm3  = 1.0e-3;
const double EPSm4  = 1.0e-4;
const double EPSm5  = 1.0e-5;
const double EPSm6  = 1.0e-6;
const double EPSSP  = 6.0e-7;
const double EPSm7  = 1.0e-7;
const double EPSm8  = 1.0e-8;
const double EPSm12 = 1.0e-12;
const double EPSDP  = 6.0e-14;
const double EPSm14 = 1.0e-14;
const double EPSm20 = 1.0e-20;
const double EPSm30 = 1.0e-30;

const int StrMax    = STR_MAX;

enum lev {WARNING, ERROR, REMARK};

extern "C" {
  void       message (const char *routine, const char *txt, int level);
  
  void       stringSubst (char * const       buffptr,
			  const char * const pattstr,
			  const char * const replstr);

  double     dclock  ();
  float      sclock  ();

  double   *dvector  (int nl, int nh);
  double  **dmatrix  (int rl, int rh, int cl, int ch);
  double ***d3matrix (int rl, int rh, int cl, int ch,
		      int dl, int dh);

  float    *svector  (int nl, int nh);
  float   **smatrix  (int rl, int rh, int cl, int ch);
  float  ***s3matrix (int rl, int rh, int cl, int ch,
		      int dl, int dh);

  int   *ivector  (int nl, int nh);
  int  **imatrix  (int rl, int rh, int cl, int ch);
  int ***i3matrix (int rl, int rh, int cl, int ch,
		     int dl, int dh);
  
  void freeDvector  (double    *v, int nl);
  void freeDmatrix  (double   **m, int nrl, int ncl);
  void freeD3matrix (double  ***t, int nrl, int ncl, int ndl);

  void freeSvector  (float     *v, int nl);
  void freeSmatrix  (float    **m, int nrl, int ncl);
  void freeS3matrix (float   ***t, int nrl, int ncl, int ndl);

  void freeIvector  (int   *v, int nl);
  void freeImatrix  (int  **m, int nrl, int ncl);
  void freeI3matrix (int ***t, int nrl, int ncl, int ndl);
}

#endif

