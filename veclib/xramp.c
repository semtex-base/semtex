/*****************************************************************************
 * xramp:  ramp function:  x[i] = alpha + i*beta.
 *****************************************************************************/

#include <cfemdef.h>

#if defined(__uxp__)
#pragma global novrec
#pragma global noalias
#endif

void dramp (int n, double alpha, double beta, double* x, int incx)
{
   int i;

  x += (incx<0) ? (-n+1)*incx : 0;

  for (i = 0; i < n; i++) x[i*incx] = alpha + i * beta;
}


void iramp (int n, int alpha, int beta, int* x, int incx)
{
   int i;

  x += (incx<0) ? (-n+1)*incx : 0;

  for (i = 0; i < n; i++) x[i*incx] = alpha + i * beta;
}


void sramp (int n, float alpha, float beta, float* x, int incx)
{
   int i;
  
  x += (incx<0) ? (-n+1)*incx : 0;

  for (i = 0; i < n; i++) x[i*incx] = alpha + i * beta;
}
