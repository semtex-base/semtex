/*****************************************************************************
 * xvpow:  y[i] = pow(x[i], alpha).
 *****************************************************************************/

#include <math.h>
#include <cfemdef.h>

#if defined(__uxp__)
#pragma global novrec
#pragma global noalias
#endif

void dspow (const int n, const double alpha,
	    const double* x, int incx,
	          double* y, int incy)
{
  int i;

  x += (incx<0) ? (-n+1)*incx : 0;
  y += (incy<0) ? (-n+1)*incy : 0;
  
  for (i = 0; i < n; i++) y[i*incy] = pow (x[i*incx], alpha);
}


void sspow (const int n, const float alpha,
	    const float* x, int incx, 
	          float* y, int incy)
{
  int i;

  x += (incx<0) ? (-n+1)*incx : 0;
  y += (incy<0) ? (-n+1)*incy : 0;
  
  for (i = 0; i < n; i++)
#if  defined(__uxp__)
    y[i*incy] = (float) pow  (x[i*incx], alpha);
#else
    y[i*incy] =         powf (x[i*incx], alpha);
#endif
}
