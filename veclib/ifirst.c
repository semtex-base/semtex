/*****************************************************************************
 * ifirst:  index of first non-zero value in x.
 *****************************************************************************/

#include <cfemdef.h>


int ifirst (int n, const int* x, int incx)
{ 
  int i;

  x += (incx<0) ? (-n+1)*incx : 0;

  for (i = 0; i < n; i++) if (x[i*incx]) return i;
  
  return -1;
}
