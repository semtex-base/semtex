/*****************************************************************************
 * ixmin: index of minimum value in x.
 *****************************************************************************/

#include <cfemdef.h>


int idmin (int n, const double* x, int incx)
{
  int    i, imin;
  double xmin;

  x += (incx<0) ? (-n+1)*incx : 0;
  xmin = x[0];
  imin = 0;

  for (i = 1; i < n; i++)
   if (x[i*incx] < xmin) {
      xmin = x[i*incx];
      imin = i;
    }

  return imin;
}


int iimin (int n, const int* x, int incx)
{
  int i, xmin, imin;

  x += (incx<0) ? (-n+1)*incx : 0;
  xmin = x[0];
  imin = 0;

  for (i = 1; i < n; i++)
    if (x[i*incx] < xmin) {
      xmin = x[i*incx];
      imin = i;
    }

  return imin;
}


int ismin (int n, const float* x, int incx)
{
  int   i, imin;
  float xmin;

  x += (incx<0) ? (-n+1)*incx : 0;
  xmin = x[0];
  imin = 0;

  for (i = 1; i < n; i++)
    if (x[i*incx] < xmin) {
      xmin = x[i*incx];
      imin = i;
    }

  return imin;
}
