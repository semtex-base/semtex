/*****************************************************************************
 * xvsgn:  y[i] = sign(x[i])  --  sign = -1 if x<0, else +1.
 *****************************************************************************/

#include <cfemdef.h>

#if defined(__uxp__)
#pragma global novrec
#pragma global noalias
#endif

void dvsgn (int n, const double* x, int incx,
                         double* y, int incy)
{
  int i;

  x += (incx<0) ? (-n+1)*incx : 0;
  y += (incy<0) ? (-n+1)*incy : 0;

  for (i = 0; i < n; i++) y[i*incy] = (x[i*incx] < 0.0) ? -1.0 : 1.0;
}


void ivsgn (int n, const int* x, int incx,
                         int* y, int incy)
{
  int i;

  x += (incx<0) ? (-n+1)*incx : 0;
  y += (incy<0) ? (-n+1)*incy : 0;

  for (i = 0; i < n; i++) y[i*incy] = (x[i*incx] < 0) ? -1 : 1;
}


void svsgn (int n, const float* x, int incx,
                         float* y, int incy)
{
  int i;
  
  x += (incx<0) ? (-n+1)*incx : 0;
  y += (incy<0) ? (-n+1)*incy : 0;

  for (i = 0; i < n; i++) y[i*incy] = (x[i*incx] < 0.0F) ? -1.0F : 1.0F;
}
