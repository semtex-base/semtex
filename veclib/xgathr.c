/*****************************************************************************
 * xgathr:  vector gather:  z[i] = x[y[i]].
 *****************************************************************************/

#include <cfemdef.h>

#if defined(__uxp__)
#pragma global novrec
#pragma global noalias
#endif

void dgathr (int n, const double* x, const int* y, double* z)
{
  int i;
  
  for (i = 0; i < n; i++) z[i] = x[y[i]];
}


void igathr (int n, const int* x, const int* y, int* z)
{
  int i;
  
  for (i = 0; i < n; i++) z[i] = x[y[i]];
}


void sgathr (int n, const float* x, const int* y, float* z)
{
  int i;
  
  for (i = 0; i < n; i++) z[i] = x[y[i]];
}
