/*****************************************************************************
 *                   FILE, STRING, I-O, TIMING UTILITIES
 *****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>

#include <cfemdef.h>
#include <cveclib.h>

int     _vecIreg[NVREG];	/* For FORTRAN linkage - these are not in  */
char    _vecCreg[NVREG];	/* a header file since they should only be */
float   _vecSreg[NVREG];        /* declared once. Used e.g. in cveclib.h.  */
double  _vecDreg[NVREG];


void message (const char *routine, const char *text, int level)
/* ------------------------------------------------------------------------- *
 * A simple error handler.
 * ------------------------------------------------------------------------- */
{
  switch (level) {
  case WARNING:
    fprintf (stderr, "-- WARNING: %s: %s\n",  routine, text); 
    break;
  case ERROR:
    fprintf (stderr, "-- ERROR: %s: %s\n",   routine, text); 
    break;
  case REMARK:
    fprintf (stderr, "-- REMARK: %s: %s\n",  routine, text);
    break;
  default:
    fprintf (stderr, "bad error level in message: %d\n", level);
    exit (EXIT_FAILURE);
    break;
  }

  if (level == ERROR) exit (EXIT_FAILURE);
}


void semheaderstr (char*       tgt    ,
		   const char* session,
		   const char* created,
		   int         nr     ,
		   int         ns     ,
		   int         nz     ,
		   int         nel    ,
		   int         step   ,
		   double      simtime,
		   double      deltat ,
		   double      kinvis ,
		   double      beta   ,
		   const char* fields ,
		   const char* wformat)
/* --------------------------------------------------------------------------
 * Install standard Semtex file header information into input string
 * tgt, which must be at least HDR_LEN + 1 characters long.  HDR_LEN
 * is defined in cfemdef.h.  (The "extra" character "+ 1" is a
 * terminating NULL.)
 *
 * If input "created" is an empty string, use the current wall clock
 * time.
 *
 * If input "wformat" contains "binary", the output format indication
 * is over-ridden to be whatever binary double type the present
 * machine supports (little- or big-endian), regardless of the stated
 * input type.
 *
 * NB: Presently, the "Fields written" string is blank-padded to allow
 * the list of fields to be quite long -- it can reach a maximum of 49
 * characters; once it is longer than 25, "Fields written" will be
 * pushed to the right and truncated until it eventually disappears,
 * leaving the line always 49 characters long.
 *
 * Explanation of this padding: owing to use by AMTEC Tecplot of the
 * characters "XYZ" to denote spatial coordinates, these are
 * disallowed field name characters.  Numeric character names are
 * disallowed by Tecplot.  Hence this leaves 2*26-3=49 upper/lower
 * case characters available as field names.
 * ------------------------------------------------------------------------- */
{
  const char routine [] = "semheaderstr";
  const char *hdr_fmt[] = { 
    "%-25s "                  "Session\n",
    "%-25s "                  "Created\n",
    "%-4d %-4d %-6d %-7d  "   "Nr, Ns, Nz, Elements\n",
    "%-25d "                  "Step\n",
    "%-25.6g "                "Time\n",
    "%-25.6g "                "Time step\n",
    "%-25.6g "                "Kinvis\n",
    "%-25.6g "                "Beta\n",
 #if 1
    "%-25s "                  "Fields written         ",    
 #else
    "%-25s "                  "Fields written\n",
 #endif
    "%-25s "                  "Format\n"
  };

  char* tgtp = tgt;          /* -- Save a copy of start location. */
  char  work[STR_MAX];

  if (strlen (fields) > 49)
    message (routine, "number of fields can be at most 49", ERROR);

  if (created) { /* -- Creation time is supplied, use it. */
    strncpy (work, created, 25);
    work[26] = '\0';
  } else {
    time_t tp = time (NULL);
    strftime (work, 25, "%a %b %d %H:%M:%S %Y", localtime (&tp));
  }
 
  tgt += sprintf (tgt, hdr_fmt[0], session);
  tgt += sprintf (tgt, hdr_fmt[1], work);
  tgt += sprintf (tgt, hdr_fmt[2], nr, ns, nz, nel);
  tgt += sprintf (tgt, hdr_fmt[3], step);
  tgt += sprintf (tgt, hdr_fmt[4], simtime);
  tgt += sprintf (tgt, hdr_fmt[5], deltat);
  tgt += sprintf (tgt, hdr_fmt[6], kinvis);
  tgt += sprintf (tgt, hdr_fmt[7], beta);
#if 1
  snprintf (tgt, 50, hdr_fmt[8], fields);
  sprintf  (tgt + strlen(tgt), "%c", '\n');
  tgt += strlen (tgt);

  if (strstr (wformat, "binary")) {
    sprintf (work, "%s", "binary "); format (work + strlen (work));
    /* -- See veclib/xbrev.c for format(). */
    sprintf (tgt, hdr_fmt[9], work);
  } else if (strstr (wformat, "ASCII")) {
    sprintf (tgt, hdr_fmt[9], "ASCII");
  } else
    message (routine, "unknown write format supplied", ERROR);

#else
  tgt += sprintf  (tgt, hdr_fmt[8], fields);
  
  sprintf  (work, "%s", "binary "); format (work + strlen (work));
  sprintf  (tgt, hdr_fmt[9], work);
#endif

  if (strlen (tgtp) != HDR_LEN) { /* -- Sanity check. */
    sprintf (work,
	     "output is %d bytes long, need %d", strlen(tgtp), HDR_LEN);
    message (routine, work, ERROR);
  }
}


int needbyteswap (const char* desc)
/* ------------------------------------------------------------------------- *
 * Utility routine to facilitate dealing with binary data files
 * produced on a machine whose floating point format does not match
 * that of the current machine.  Generally, input desc will be obtained
 * from a semtex file header during file processing.
 *
 * Return 1 if the floating point format of current machine does not
 * match desc (flagging that byte swapping of input will be needed)
 * else 0.
 *
 * Check desc is a valid descriptor (e.g. "binary IEEE
 * little-endian").
 *
 * Some (very old) machines did not support IEEE binary types so IEEE
 * conformity not required in desc -- we still want to know if byte
 * swapping is/not required if we are on one of those machines
 * (warning issued), but since byte swapping isn't flagged by this
 * routine in that case, transferring files from such a machine may
 * cause difficulty.
 * ------------------------------------------------------------------------- */
{
  const char routine[] = "needbyteswap";
  char       work[32];
  int        swab = 0;

  format (work); /* -- format() defined in xbrev.c. */
  
  if (!strstr (desc, "binary"))
    message (routine, "input format description not binary",     ERROR);
  else if (!strstr (desc, "endian"))
    message (routine, "input binary format type not recognized", WARNING);
  else {
    swab = ((strstr (desc, "big") && strstr (work, "little"))
	 || (strstr (work, "big") && strstr (desc, "little")) );
  }

  return swab;
}


FILE *efopen (const char *file, const char *mode)
/* ------------------------------------------------------------------------- *
 * fopen file, die if can't.
 * ------------------------------------------------------------------------- */
{
  FILE *fp;
  char work[128];

  if (fp = fopen (file, mode)) return fp;

  sprintf (work, "can't open %s mode %s", file, mode);
  message ("efopen", work, ERROR);
  
  return (FILE*) 0;
}


#if !defined(i860) && !defined(dclock)

double dclock (void)
/* ------------------------------------------------------------------------- *
 * Double-precision timing routine.
 * ------------------------------------------------------------------------- */
{
  static double tps = 1.0 / CLOCKS_PER_SEC;
  return (double) clock() * tps;
}


float sclock (void)
/* ------------------------------------------------------------------------- *
 * Single-precision timing routine.
 * ------------------------------------------------------------------------- */
{
  static float tps = 1.0F / CLOCKS_PER_SEC;
  return (float) clock() * tps;
}

#endif


static void stringSubst2 (char * const       bsrcptr,
			  char * const       bdstptr,
			  const char * const pattstr,
			  const char * const replstr,
			  const int          pattsiz,
			  const int          replsiz)
/* ------------------------------------------------------------------------- *
 * String substitution routine, borrowed from Scotch. This part is hidden.
 * ------------------------------------------------------------------------- */
{
  char *pattptr;
  int   pattidx;

  /* Search for the pattern in the remaining source string   */
  pattptr = strstr (bsrcptr, pattstr);

  /* Get length of unchanged part */
  pattidx = (pattptr == NULL) ? (strlen (bsrcptr) + 1): (pattptr - bsrcptr);

  /* If replacement is smaller, pre-move unchanged part */
  if (replsiz < pattsiz)
    memmove (bdstptr, bsrcptr, pattidx * sizeof (char));

  /* If remaining part of string has to be processed */
  if (pattptr != NULL)
    stringSubst2 (pattptr + pattsiz,
		  bdstptr + pattidx + replsiz,
		  pattstr, replstr,
		  pattsiz, replsiz);

  /* If replacement is longer, post-move unchanged part */
  if (replsiz > pattsiz)
    memmove (bdstptr, bsrcptr, pattidx * sizeof (char));
  
  /* If there is something to replace         */
  if (pattptr != NULL)
    /* Write replacement string */
    memcpy (bdstptr + pattidx, replstr, replsiz * sizeof (char));

  return;
}


void stringSubst (char * const       buffptr, /* String to search into */
		  const char * const pattstr, /* Pattern to search for */
		  const char * const replstr) /* Replacement string    */
/* ------------------------------------------------------------------------- *
 * String substitution routine, borrowed from Scotch.  This part is visible.
 * ------------------------------------------------------------------------- */ 
{
  int pattsiz;
  int replsiz;

  pattsiz = strlen (pattstr);
  replsiz = strlen (replstr);

  stringSubst2 (buffptr, buffptr, pattstr, replstr, pattsiz, replsiz);
}


void printDvector (FILE  *fp     ,
		   int   width  ,
		   int   prec   ,
		   int   ntot   ,
		   int   inc    ,
		   int   nfield , ...)
/* ------------------------------------------------------------------------- *
 * Print up a variable number of dvectors on fp, in columns.
 * ------------------------------------------------------------------------- */
{
  char    routine[] = "printDvector";
  int     i, j, k;
  double  **u;
  va_list ap;

  u = (double **) calloc (nfield, sizeof (double*));
  va_start (ap, nfield);
  for (i = 0; i < nfield; i++) u[i] = va_arg (ap, double*); 
  va_end (ap);

  k = (inc < 0) ? (-ntot + 1)*inc : 0;

  for (i = 0; i < ntot; i++) {
    for (j = 0; j < nfield; j++)
      if (fprintf (fp, "%*.*f", width, prec, u[j][k]) < 0)
	message (routine, "unable to write to file", ERROR);
    k += inc;
    fprintf(fp, "\n");
  }

  free (u);
}


void printIvector (FILE  *fp     ,
		   int    width  ,
		   int    ntot   ,
		   int    inc    ,
		   int    nfield , ...)
/* ------------------------------------------------------------------------- *
 * Print up a variable number of ivectors on fp, in columns.
 * ------------------------------------------------------------------------- */
{
  char    routine[] = "printIvector";
  int i, j, k;
  int **u;
  va_list ap;

  u = (int **) calloc (nfield, sizeof (int*));
  va_start (ap, nfield);
  for (i = 0; i < nfield; i++) u[i] = va_arg (ap, int*); 
  va_end (ap);

  k = (inc < 0) ? (-ntot + 1)*inc : 0;

  for (i = 0; i < ntot; i++) {
    for (j = 0; j < nfield; j++)
      if (fprintf (fp, "%*d", width, u[j][k]) < 0)
	message (routine, "couldn't write to file", ERROR);
    k += inc;
    fprintf (fp, "\n");
  }

  free (u);
}


void printSvector (FILE *fp     ,
		   int  width  ,
		   int  prec   ,
		   int  ntot   ,
		   int  inc    ,
		   int  nfield , ...)
/* ------------------------------------------------------------------------- *
 * Write (ASCII) a variable number of svectors on fp, in columns.
 * ------------------------------------------------------------------------- */
{
  char    routine[] = "printSvector";
  int     i, j, k;
  float   **u;
  va_list ap;

  u = (float **) calloc (nfield, sizeof (float*));
  va_start (ap, nfield);
  for (i = 0; i < nfield; i++) u[i] = va_arg (ap, float*); 
  va_end (ap);

  k = (inc < 0) ? (-ntot + 1)*inc : 0;

  for (i = 0; i < ntot; i++) {
    for (j = 0; j < nfield; j++)
      if (fprintf (fp, "%*.*f", width, prec, u[j][k]) < 0)
	message (routine, "unable to write to file", ERROR);
    k += inc;
    fprintf (fp, "\n");
  }

  free (u);
}
