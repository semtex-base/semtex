/*****************************************************************************
 * xsum:  sum = 0;  sum += x[i];
 *****************************************************************************/

#include <cfemdef.h>


double dsum (int n, const double* x, int incx)
{
  int    i;
  double sum = 0.0;

  x += (incx<0) ? (-n+1)*incx : 0;

  for (i = 0; i < n; i++) sum += x[i*incx];
  
  return sum;
}


int isum (int n, const int* x, int incx)
{
  int i, sum = 0;

  x += (incx<0) ? (-n+1)*incx : 0;

  for (i = 0; i < n; i++) sum += x[i*incx];
  
  return sum;
}


float ssum (int n, const float* x, int incx)
{
  int   i;
  float sum = 0.0F;

  x += (incx<0) ? (-n+1)*incx : 0;

  for (i = 0; i < n; i++) sum += x[i*incx];
  
  return sum;
}
