/*****************************************************************************
 * ixmax: index of maximum value in x.
 *****************************************************************************/

#include <cfemdef.h>


int idmax (int n, const double* x, int incx)
{
  int    i, imax;
  double xmax;

  x += (incx<0) ? (-n+1)*incx : 0;
  xmax = x[0];
  imax = 0;

  for (i = 1; i < n; i++)
   if (x[i*incx] > xmax) {
      xmax = x[i*incx];
      imax = i;
    }

  return imax;
}


int iimax (int n, const int* x, int incx)
{
  int i, xmax, imax;

  x += (incx<0) ? (-n+1)*incx : 0;
  xmax = x[0];
  imax = 0;

  for (i = 1; i < n; i++)
    if (x[i*incx] > xmax) {
      xmax = x[i*incx];
      imax = i;
    }

  return imax;
}


int ismax (int n, const float* x, int incx)
{
  int   i, imax;
  float xmax;

  x += (incx<0) ? (-n+1)*incx : 0;
  xmax = x[0];
  imax = 0;

  for (i = 1; i < n; i++)
    if (x[i*incx] > xmax) {
      xmax = x[i*incx];
      imax = i;
    }

  return imax;
}
