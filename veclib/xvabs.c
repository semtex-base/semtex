/*****************************************************************************
 * xvabs:  y[i] = abs(x[i]).
 *****************************************************************************/

#include <math.h>
#include <cfemdef.h>

#if defined(__uxp__)
#pragma global novrec
#pragma global noalias
#endif

void dvabs (int n, const double* x, int incx,
                           double* y, int incy)
{
   int i;

  x += (incx<0) ? (-n+1)*incx : 0;
  y += (incy<0) ? (-n+1)*incy : 0;

  for (i = 0; i < n; i++) y[i*incy] = fabs (x[i*incx]);
}


void ivabs (int n, const int* x, int incx,
                           int* y, int incy)
{
   int i;

  x += (incx<0) ? (-n+1)*incx : 0;
  y += (incy<0) ? (-n+1)*incy : 0;

  for (i = 0; i < n; i++) y[i*incy] = (x[i*incx] < 0) ? -x[i*incx] : x[i*incx];
}


void svabs (int n, const float* x, int incx,
                             float* y, int incy)
{
   int i;

  x += (incx<0) ? (-n+1)*incx : 0;
  y += (incy<0) ? (-n+1)*incy : 0;

#if  defined(__uxp__)
  for (i = 0; i < n; i++) y[i*incy] = (float) fabs  (x[i*incx]);
#else
  for (i = 0; i < n; i++) y[i*incy] =         fabsf (x[i*incx]);
#endif
}
