/*****************************************************************************
 * xiclip: inverted clip to interval [alpha,beta]:
 *   if x[i] < (alpha+beta)/2 y[i] = MIN(x[i],alpha) else y[i]=MAX(x[i],beta)
 *****************************************************************************/

#include <cfemdef.h>

#if defined(__uxp__)
#pragma global novrec
#pragma global noalias
#endif

#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define MAX(a, b) ((a) > (b) ? (a) : (b))

void diclip (int n, const double alpha, const double beta,
	     const double* x, int incx,
	           double* y, int incy)
{
  int          i;
  double       xtmp;
  const double mval = 0.5*(alpha + beta);

  x += (incx<0) ? (-n+1)*incx : 0;
  y += (incy<0) ? (-n+1)*incy : 0;

  for (i = 0; i < n; i++) {
    xtmp = x[i*incx];
    y[i*incy] = (xtmp < mval) ? MIN(xtmp, alpha) : MAX(xtmp, beta);
  }
}


void iiclip (int n, const int alpha, const int beta,
	     const int* x, int incx,
	           int* y, int incy)
{
  int       i, xtmp;
  const int mval = (alpha + beta)/2;

  x += (incx<0) ? (-n+1)*incx : 0;
  y += (incy<0) ? (-n+1)*incy : 0;

  for (i = 0; i < n; i++) {
    xtmp = x[i*incx];
    y[i*incy] = (xtmp <= mval) ? MIN(xtmp, alpha) : MAX(xtmp, beta);
  }
}


void siclip (int n, const float alpha, const float beta,
	     const float* x, int incx,
	           float* y, int incy)
{
  int         i;
  float       xtmp;
  const float mval=0.5*(alpha + beta);

  x += (incx<0) ? (-n+1)*incx : 0;
  y += (incy<0) ? (-n+1)*incy : 0;

  for (i = 0; i < n; i++) {
    xtmp = x[i*incx];
    y[i*incy] = (xtmp < mval) ? MIN(xtmp, alpha) : MAX(xtmp, beta);
  }
}

#undef MIN
#undef MAX
