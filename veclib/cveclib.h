#ifndef CVECLIB_H
#define CVECLIB_H
/*****************************************************************************
 *                         C V E C L I B . H
 *****************************************************************************/

#include <stdarg.h>

#include <cfemdef.h>

#ifndef BLAS			/* Assume vendor-supplied FORTRAN BLAS       */
#define BLAS 3			/* library will be available.  If not,       */
#endif                          /* define BLAS to be 0 before this point.    */

#ifndef LAPACK			/* Assume vendor-supplied FORTRAN LAPACK     */
#define LAPACK 1	        /* library will be available.  If not,       */
#endif                          /* define LAPACK to be 0 before this point.  */

#define NVREG  8		/* Registers reserved for FORTRAN linkage.   */

extern int    _vecIreg[NVREG]; /* These are declared in util.c              */
extern char   _vecCreg[NVREG];
extern float  _vecSreg[NVREG];
extern double _vecDreg[NVREG];

#define SQR(a)     ((a) * (a))
#define SGN(a)     ((a) < 0.0 ?    -1.0 : 1.0)
#define MIN(a, b)  ((a) < (b) ?     (a) : (b))
#define MAX(a, b)  ((a) > (b) ?     (a) : (b))
#define SIGN(a, b) ((b) > 0.0 ? fabs(a) : -fabs(a))

#ifndef STR_MAX
#define STR_MAX 2048
#endif

#ifndef M_PI
#define M_PI   3.14159265358979323846
#endif

#ifndef TWOPI
#define TWOPI  6.28318530717958647692
#endif

#ifndef PI_180
#define PI_180 0.01745329251994329576
#endif

#define EPSm3   1.0e-3
#define EPSm4   1.0e-4
#define EPSm5   1.0e-5
#define EPSm6   1.0e-6
#define EPSSP   6.0e-7
#define EPSm7   1.0e-7
#define EPSm8   1.0e-8
#define EPSm12  1.0e-12
#define EPSDP   6.0e-14
#define EPSm14  1.0e-14
#define EPSm20  1.0e-20
#define EPSm30  1.0e-30
 
typedef struct  {float  Re, Im;} complex;
typedef struct  {double Re, Im;} zomplex;
enum    err_lev {WARNING, ERROR, REMARK};


/* ------------------------------------------------------------------------- *
 * UTILITIES (util.c).
 * ------------------------------------------------------------------------- */

void    message (const char *routine, const char *txt, int level);
FILE*   efopen  (const char *file,    const char *mode);
double  dclock  (void);
float   sclock  (void);

void    printDvector (FILE *fp,  int width, int prec,
		      int ntot,  int inc, int nfield, ...);
void    printIvector (FILE *fp,  int width,           
		      int ntot,  int inc, int nfield, ...);
void    printSvector (FILE *fp,  int width, int prec,
		      int ntot,  int inc, int nfield, ...);

void    semheaderstr (char*       tgt    ,
		      const char* session,
		      const char* created,
		      int         nr     ,
		      int         ns     ,
		      int         nz     ,
		      int         nel    ,
		      int         step   ,
		      double      simtime,
		      double      dt     ,
		      double      kinvis ,
		      double      beta   ,
		      const char* fields ,
		      const char* wformat);

int    needbyteswap  (const char*);


/* ------------------------------------------------------------------------- *
 * MEMORY MANAGEMENT (memory.c)
 * ------------------------------------------------------------------------- */

#define tempVector(v, n) double *v = dvector (0, n-1)
#define freeVector(v)    free (v)

complex   *cvector  (int nl, int nh);
complex  **cmatrix  (int rl, int rh, int cl, int ch);
complex ***c3matrix (int rl, int rh, int cl, int ch,
		     int dl, int dh);

double    *dvector  (int nl, int nh);
double   **dmatrix  (int rl, int rh, int cl, int ch);
double  ***d3matrix (int rl, int rh, int cl, int ch,
		     int dl, int dh);

float     *svector  (int nl, int nh);
float    **smatrix  (int rl, int rh, int cl, int ch);
float   ***s3matrix (int rl, int rh, int cl, int ch,
		     int dl, int dh);

int   *ivector  (int nl, int nh);
int  **imatrix  (int rl, int rh, int cl, int ch);
int ***i3matrix (int rl, int rh, int cl, int ch,
		     int dl, int dh);

zomplex   *zvector  (int nl, int nh);
zomplex  **zmatrix  (int rl, int rh, int cl, int ch);
zomplex ***z3matrix (int rl, int rh, int cl, int ch,
		     int dl, int dh);

void freeCvector  (complex   *v, int nl);
void freeCmatrix  (complex  **m, int nrl, int ncl);
void freeC3matrix (complex ***t, int nrl, int ncl, int ndl);

void freeDvector  (double    *v, int nl);
void freeDmatrix  (double   **m, int nrl, int ncl);
void freeD3matrix (double  ***t, int nrl, int ncl, int ndl);

void freeSvector  (float     *v, int nl);
void freeSmatrix  (float    **m, int nrl, int ncl);
void freeS3matrix (float   ***t, int nrl, int ncl, int ndl);

void freeIvector  (int   *v, int nl);
void freeImatrix  (int  **m, int nrl, int ncl);
void freeI3matrix (int ***t, int nrl, int ncl, int ndl);

void freeZvector  (zomplex   *v, int nl);
void freeZmatrix  (zomplex  **m, int nrl, int ncl);
void freeZ3matrix (zomplex ***t, int nrl, int ncl, int ndl);


/* ------------------------------------------------------------------------- *
 * MATHEMATICAL PRIMITIVES:
 * ------------------------------------------------------------------------- */

void dcopy (int n, const double* x, int incx, double* y, int incy);
void icopy (int n, const int*    x, int incx, int*    y, int incy);
void scopy (int n, const float*  x, int incx, float*  y, int incy);

void dfill (int n, double  alpha, double* x, int incx);
void ifill (int n, int     alpha, int*    x, int incx);
void sfill (int n, float   alpha, float*  x, int incx);

void dznan (int n, double* x, int incx);
void sznan (int n, float*  x, int incx);

void dneg (int n, double* x, int incx);
void ineg (int n, int*    x, int incx);
void sneg (int n, float*  x, int incx);

void dvneg (int n, const double* x, int incx, double* y, int incy);
void ivneg (int n, const int*    x, int incx, int*    y, int incy);
void svneg (int n, const float*  x, int incx, float*  y, int incy);

void dvsgn (int n, const double* x, int incx, double* y, int incy);
void ivsgn (int n, const int*    x, int incx, int*    y, int incy);
void svsgn (int n, const float*  x, int incx, float*  y, int incy);

void dsadd (int n, double alpha,
	    const double* x, int incx, double* y, int incy);
void isadd (int n, int    alpha,
	    const int*    x, int incx, int*    y, int incy);
void ssadd (int n, float  alpha,
	    const float*  x, int incx, float*  y, int incy);

void dspow (const int n, const double alpha,
	    const double* x, int incx, double* y, int incy);
void sspow (const int n, const float  alpha,
	    const float*  x, int incx, float*  y, int incy);

void dvadd (int n, const double* x, int incx,
	    const double* y, int incy, double* z, int incz);
void ivadd (int n, const int*    x, int incx,
	    const int*    y, int incy, int*    z, int incz);
void svadd (int n, const float*  x, int incx,
	    const float*  y, int incy, float*  z, int incz);

void dssub (int n, double alpha, const double* x, int incx, 
	                               double* y, int incy);
void issub (int n, int    alpha, const int*    x, int incx,
	                               int*    y, int incy);
void sssub (int n, float  alpha, const float*  x, int incx,
	                               float*  y, int incy);

void dvsub (int n, const double* x, int incx,
	           const double* y, int incy, double* z, int incz);
void ivsub (int n, const int*    x, int incx,
	           const int*    y, int incy, int*    z, int incz);
void svsub (int n, const float*  x, int incx,
	           const float*  y, int incy, float*  z, int incz);

void dsmul (int n, double alpha, const double* x, int incx,
	                               double* y, int incy);
void ismul (int n, int    alpha, const int*    x, int incx,
	                               int*    y, int incy);
void ssmul (int n, float  alpha, const float*  x, int incx,
	                               float*  y, int incy);

void dvmul (int n, const double* x, int incx,
	           const double* y, int incy, double* z, int incz);
void ivmul (int n, const int*    x, int incx,
	           const int*    y, int incy, int*    z, int incz);
void svmul (int n, const float*  x, int incx,
	           const float*  y, int incy, float*  z, int incz);

void dsdiv (int n, double alpha, const double* x, int incx,
	                               double* y, int incy);
void isdiv (int n, int    alpha, const int*    x, int incx,
	                               int*    y, int incy);
void ssdiv (int n, float  alpha, const float*  x, int incx,
	                               float*  y, int incy);

void dvrecp (int n, const double* x, int incx, double* y, int incy);
void svrecp (int n, const float*  x, int incx, float*  y, int incy);

void dvdiv (int n, const double* x, int incx,
	           const double* y, int incy, double* z, int incz);
void svdiv (int n, const float*  x, int incx,
	           const float*  y, int incy, float*  z, int incz);

void dzero (int n, double* x, int incx);
void izero (int n, int*    x, int incx);
void szero (int n, float*  x, int incx);


/* ------------------------------------------------------------------------- *
 * OTHER MATHEMATICAL FUNCTIONS:
 * ------------------------------------------------------------------------- */

void dvabs (int n, const double* x, int incx,
                         double* y, int incy);
void ivabs (int n, const int*    x, int incx,
	                 int*    y, int incy);
void svabs (int n, const float*  x, int incx,
	                 float*  y, int incy);

void dvamax (int n, const double* x, int incx,
	            const double* y, int incy, double* z, int incz);
void ivamax (int n, const int*    x, int incx,
	            const int*    y, int incy, int*    z, int incz);
void svamax (int n, const float*  x, int incx,
	            const float*  y, int incy, float*  z, int incz);

void dvexp (int n, const double* x, int incx, double* y, int incy);
void svexp (int n, const float*  x, int incx, float*  y, int incy);

void dvlg10 (int n, const double* x, int incx, double* y, int incy);
void svlg10 (int n, const float*  x, int incx, float*  y, int incy);

void dvlog (int n, const double* x, int incx, double* y, int incy);
void svlog (int n, const float*  x, int incx, float*  y, int incy);

void dvatan (int n, const double* x, int incx, double* y, int incy);
void svatan (int n, const float*  x, int incx, float*  y, int incy);

void dvatn2 (int n, const double* x, int incx,
	            const double* y, int incy, double* z, int incz);
void svatn2 (int n, const float*  x, int incx,
	            const float*  y, int incy, float*  z, int incz);

void dvcos (int n, const double* x, int incx, double* y, int incy);
void svcos (int n, const float*  x, int incx, float*  y, int incy);

void dvsin (int n, const double* x, int incx, double* y, int incy);
void svsin (int n, const float*  x, int incx, float*  y, int incy);

void dvsqrt (int n, const double* x, int incx, double* y, int incy);
void svsqrt (int n, const float*  x, int incx, float*  y, int incy);

void dvtanh (int n, const double* x, int incx, double* y, int incy);
void svtanh (int n, const float*  x, int incx, float*  y, int incy);

void   raninit  (int flag);
double dranu    (void);
float  sranu    (void);
double drang    (void);
float  srang    (void);
double dnormal  (double mean, double sdev);
float  snormal  (float  mean, float  sdev);
void   dvrandom (int n, double* x, int incx);
void   svrandom (int n, float*  x, int incx);
void   dvgauss  (int n, double* x, int incx);
void   dsgauss  (int n, float*  x, int incx);
void   dvnormal (int n, double mean, double sdev, double* x, int incx);
void   svnormal (int n, float  mean, float  sdev, float*  x, int incx);

int ispow2  (int k);
int irpow2  (int k);
void zpreft (int k, zomplex *wtab, int sign);
void cpreft (int k, complex *wtab, int sign);
void zfft   (int n, zomplex *x, int l, const zomplex *wtab, int dir);
void cfft   (int n, complex *x, int l, const complex *wtab, int dir);
void dzfft  (int n, zomplex *x, int l, const zomplex *wtab, int dir);
void scfft  (int n, complex *x, int l, const complex *wtab, int dir);
void zpfft  (int n, zomplex *x, int l, const zomplex *wtab, int dir);
void spfft  (int n, complex *x, int l, const complex *wtab, int dir);

void dvhypot (int n, const double* x, int incx,
	             const double* y, int incy, double* z, int incz);
void svhypot (int n, const float*  x, int incx,
	             const float*  y, int incy, float*  z, int incz);
void dvmag   (int n, const double* w, int incw, 
	             const double* x, int incx,
	             const double* y, int incy,
	                   double* z, int incz);
void svmag   (int n, const float*  w, int incw, 
	             const float*  x, int incx,
	             const float*  y, int incy,
	                   float*  z, int incz);

void dvpow (int n, const double* x, int incx,
	           const double* y, int incy, double* z, int incz);
void svpow (int n, const float*  x, int incx,
	           const float*  y, int incy, float*  z, int incz);


/* ------------------------------------------------------------------------- *
 * TRIAD OPERATIONS:
 * ------------------------------------------------------------------------- */

void dsvmvt (int n, double alpha, const double* x, int incx,
                                  const double* y, int incy,
	                                double* z, int incz);
void ssvmvt (int n, float  alpha, const float*  x, int incx,
                                  const float*  y, int incy,
	                                float*  z, int incz);

void dsvpvt (int n, double alpha, const double* x, int incx,
                                  const double* y, int incy,
	                                double* z, int incz);
void ssvpvt (int n, float  alpha, const float*  x, int incx,
                                  const float*  y, int incy,
	                                float*  z, int incz);

void dsvtsp (int n, double alpha, double beta,
	     const double* x, int incx, double* y, int incy);
void ssvtsp (int n, float  alpha, float  beta,
	     const float*  x, int incx, float*  y, int incy);

void dsvtvm (int n, double alpha, const double* x, int incx,
                                  const double* y, int incy,
	                                double* z, int incz);
void ssvtvm (int n, float  alpha, const float*  x, int incx,
                                  const float*  y, int incy,
	                                float*  z, int incz);

void dsvtvp (int n, double alpha, const double* x, int incx,
                                  const double* y, int incy,
	                                double* z, int incz);
void ssvtvp (int n, float  alpha, const float*  x, int incx,
                                  const float*  y, int incy,
	                                float*  z, int incz);

void dsvvmt (int n, double alpha, const double* x, int incx,
                                    const double* y, int incy,
	                                  double* z, int incz);
void ssvvmt (int n, float  alpha, const float*  x, int incx,
                                    const float*  y, int incy,
	                                  float*  z, int incz);

void dsvvpt (int n, double alpha, const double* x, int incx,
                                    const double* y, int incy,
	                                  double* z, int incz);
void ssvvpt (int n, float  alpha, const float*  x, int incx,
                                    const float*  y, int incy,
	                                  float*  z, int incz);

void dsvvtm (int n, double alpha, const double* x, int incx,
                                  const double* y, int incy,
	                                double* z, int incz);
void ssvvtm (int n, float  alpha, const float*  x, int incx,
                                  const float*  y, int incy,
	                                float*  z, int incz);

void dsvvtp (int n, double alpha, const double* x, int incx,
                                  const double* y, int incy,
	                                double* z, int incz);
void ssvvtp (int n, float  alpha, const float*  x, int incx,
                                  const float*  y, int incy,
	                                float*  z, int incz);

void dsvvtt (int n, double alpha, const double* x, int incx,
                                  const double* y, int incy,
	                                double* z, int incz);
void ssvvtt (int n, float  alpha, const float*  x, int incx,
                                  const float*  y, int incy,
	                                float*  z, int incz);

void dvvmvt (int n, const double* w, int incw, 
	            const double* x, int incx,
	            const double* y, int incy,
	                  double* z, int incz);
void svvmvt (int n, const float*  w, int incw,
                    const float*  x, int incx,
                    const float*  y, int incy,
	                  float*  z, int incz);

void dvvpvt (int n, const double* w, int incw,
	            const double* x, int incx,
                    const double* y, int incy,
	                  double* z, int incz);
void svvpvt (int n, const float*  w, int incw,
	            const float*  x, int incx,
                    const float*  y, int incy,
	                  float*  z, int incz);

void dvvpvt (int n, const double* w, int incw,
	            const double* x, int incx,
                    const double* y, int incy,
	                  double* z, int incz);
void svvpvt (int n, const float*  w, int incw,
	            const float*  x, int incx,
                    const float*  y, int incy,
	                  float*  z, int incz);

void dvvtvp (int n, const double* w, int incw,
	            const double* x, int incx,
                    const double* y, int incy,
	                  double* z, int incz);
void svvtvp (int n, const float*  w, int incw,
	            const float*  x, int incx,
                    const float*  y, int incy,
	                  float*  z, int incz);

void dvvtvm (int n, const double* w, int incw,
	            const double* x, int incx,
                    const double* y, int incy,
	                  double* z, int incz);
void svvtvm (int n, const float*  w, int incw,
	              const float*  x, int incx,
                      const float*  y, int incy,
	                    float*  z, int incz);

void dvvvtm (int n, const double* w, int incw,
	              const double* x, int incx,
                      const double* y, int incy,
	                    double* z, int incz);
void svvvtm (int n, const float*  w, int incw,
	              const float*  x, int incx,
                      const float*  y, int incy,
	                    float*  z, int incz);

void dvvvtt (int n, const double* w, int incw,
	              const double* x, int incx,
                      const double* y, int incy,
	                    double* z, int incz);
void svvvtt (int n, const float*  w, int incw,
	              const float*  x, int incx,
                      const float*  y, int incy,
	                    float*  z, int incz);


/* ------------------------------------------------------------------------- *
 * RELATIONAL PRIMITIVE OPERATIONS:
 * ------------------------------------------------------------------------- */

void iseq (int n, int alpha,
	   const int* x, int incx, int *y, int incy);
void dsge (int n, double  alpha,
	   const double*  x, int incx, int *y, int incy);
void isge (int n, int alpha,
	   const int* x, int incx, int *y, int incy);
void ssge (int n, float   alpha,
	   const float*   x, int incx, int *y, int incy);
void dsle (int n, double  alpha,
	   const double*  x, int incx, int *y, int incy);
void isle (int n, int alpha,
	   const int* x, int incx, int *y, int incy);
void ssle (int n, float   alpha,
	   const float*   x, int incx, int *y, int incy);
void dslt (int n, double  alpha,
	   const double*  x, int incx, int *y, int incy);
void islt (int n, int alpha,
	   const int* x, int incx, int *y, int incy);
void sslt (int n, float   alpha,
	   const float*   x, int incx, int *y, int incy);
void dsne (int n, double  alpha,
	   const double*  x, int incx, int *y, int incy);
void isne (int n, int alpha,
	   const int* x, int incx, int *y, int incy);
void ssne (int n, float   alpha,
	   const float*   x, int incx, int *y, int incy);


/* ------------------------------------------------------------------------- *
 * REDUCTION FUNCTIONS:
 * ------------------------------------------------------------------------- */

double dsum   (int n, const double* x, int incx);
int  isum   (int n, const int*  x, int incx);
float  ssum   (int n, const float*  x, int incx);
int  idmax  (int n, const double* x, int incx);
int  iimax  (int n, const int*  x, int incx);
int  ismax  (int n, const float*  x, int incx);
int  idmin  (int n, const double* x, int incx);
int  iimin  (int n, const int*  x, int incx);
int  ismin  (int n, const float*  x, int incx);
int  icount (int n, const int*  x, int incx);
int  imatch (int n, const int   alpha, const int* x, int incx);
int  ifirst (int n, const int*  x, int incx);
int  lany   (int n, const int*  x, int incx);
int  lisame (int n, const int*  x, int incx,
  	                const int*  y, int incy);
int  ldsame (int n, const double* x, int incx,
	                const double* y, int incy);
int  lssame (int n, const float*  x, int incx,
		        const float*  y, int incy);

/* ------------------------------------------------------------------------- *
 * CONVERSION PRIMITIVES:
 * ------------------------------------------------------------------------- */

void vdble  (int n, const float*   x, int incx,
	                    double*  y, int incy);
void vsngl  (int n, const double*  x, int incx,
	                    float*   y, int incy);

void dvfloa (int n, const int*   x, int incx,
	                    double*  y, int incy);
void svfloa (int n, const int*   x, int incx,
	                    float*   y, int incy);

int iformat(void);
void format (char*);
void dbrev  (int n, const double* x, int incx,
	                    double* y, int incy);
void ibrev  (int n, const int*  x, int incx,
	                    int*  y, int incy);
void sbrev  (int n, const float*  x, int incx,
	                    float*  y, int incy);


/* ------------------------------------------------------------------------- *
 * MISCELLANEOUS FUNCTIONS:
 *
 * NB: xmxm & xmxv operations are replaced by macros that call BLAS xgemm,
 * xgemv routines (generally faster).
 * ------------------------------------------------------------------------- */

void dscatr (int n, double*  x, int *y, double*  z);
void iscatr (int n, int* x, int *y, int* z);
void sscatr (int n, float*   x, int *y, float*   z);

void dgathr (int n, double*  x, int *y, double*  z);
void igathr (int n, int*  x, int *y, int* z);
void sgathr (int n, float*   x, int *y, float*   z);

void dramp (int n, double  alpha, double  beta, double*  x, int incx);
void iramp (int n, int alpha, int beta, int* x, int incx);
void sramp (int n, float   alpha, float   beta, float*   x, int incx);

void dclip (int n, const  double alpha,  const double   beta,
	    const double* x,  int incx,  double*  y, int incy);
void iclip (int n, const  int alpha, const int  beta,
	    const int* x, int incx,  int* y, int incy);
void sclip (int n, const  float alpha,   const float    beta,
	    const float* x,  int incx,   float*   y, int incy);

void dclipup (int n, const  double alpha,
	      const double* x,  int incx,  double*  y, int incy);
void iclipup (int n, const  int alpha,
	      const int* x, int incx,  int* y, int incy);
void sclipup (int n, const  float alpha,
	      const float* x,  int incx,   float*   y, int incy);

void dclipdn (int n, const  double alpha,
	      const double* x,  int incx,  double*  y, int incy);
void iclipdn (int n, const  int alpha,
	      const int* x, int incx,  int* y, int incy);
void sclipdn (int n, const  float alpha,
	      const float* x,  int incx,   float*   y, int incy);

void diclip (int n, const  double alpha,  const double   beta,
	     const double* x,  int incx,  double*  y, int incy);
void iiclip (int n, const  int alpha, const int  beta,
	     const int* x, int incx,  int* y, int incy);
void siclip (int n, const  float alpha,   const float    beta,
	     const float* x,  int incx,   float*   y, int incy);

void dcndst (int n, const  double*  x, int incx,
	     const int* y, int incy, double*  z, int incz);
void icndst (int n, const  int* x, int incx,
	     const int* y, int incy, int* z, int incz);
void scndst (int n, const  float*   x, int incx,
	     const int* y, int incy, float*   z, int incz);

void dmask (int n, const double*  w, int incw,
	               const double*  x, int incx,
	               const int* y, int incy,
	                     double*  z, int incz);
void imask (int n, const int* w, int incw,
	               const int* x, int incx,
	               const int* y, int incy,
	                     int* z, int incz);
void smask (int n, const float*   w, int incw,
	               const float*   x, int incx,
	               const int* y, int incy,
	                     float*   z, int incz);

void dvpoly (int n, const double* x, int incx, int m,
	                const double* c, int incc, 
		              double* y, int incy);
void svpoly (int n, const float*  x, int incx, int m,
	                const float*  c, int incc, 
		              float*  y, int incy);

double dpoly   (int n, double x, const double* xp, const double* yp);
float  spoly   (int n, float  x, const float*  xp, const float*  yp);
void   dpolint (const double* xa, const double* ya, int n,
	              double x,        double* y,  double* dy);
void   spolint (const float*  xa, const float*  ya, int n,
	              float   x,        float*  y, float*  dy);

void   dspline  (int n, double yp1, double ypn,
	         const double* x, const double* y,  double* y2);
void   sspline  (int n, float yp1, float ypn,
	         const float*  x, const float*  y,  float*  y2);
double dsplint  (int n, double x,
	         const  double* xa, const double* ya, const double* y2a);
float  ssplint  (int n, float  x,
	         const  float*  xa, const float*  ya, const float*  y2a);
double dsplquad (const double* xa, const double* ya,   const double* y2a,
		 const int n,      const double xmin,  const double xmax);
float  ssplquad (const float*  xa, const float*  ya,   const float*  y2a,
		 const int n,      const float  xmin,  const float  xmax);

void dmxm (double* A, int nra, double* B, int nca,
	   double* C, int ncb);
void smxm (float*  A, int nra, float*  B, int nca,
	   float*  C, int ncb);

void dmxv (double* A, int nra, double* B, int nca, double* C);
void smxv (float*  A, int nra, float*  B, int nca, float*  C);

void dmxva (double* A, int iac, int iar, double* B, int ib,
	    double* C, int ic,  int nra, int nca);
void smxva (float*  A, int iac, int iar, float*  B, int ib,
	    float*  C, int ic,  int nra, int nca);

void dvvtvvtp (int n, const double* v, int incv,
          	          const double* w, int incw,
	                  const double* x, int incx,
	                  const double* y, int incy,
	                        double* z, int incz);
void svvtvvtp (int n, const float*  v, int incv,
	                  const float*  w, int incw,
	                  const float*  x, int incx,
	                  const float*  y, int incy,
	                        float*  z, int incz);
void dvvtvvtm (int n, const double* v, int incv,
          	          const double* w, int incw,
	                  const double* x, int incx,
	                  const double* y, int incy,
	                        double* z, int incz);
void svvtvvtm (int n, const float*  v, int incv,
	                  const float*  w, int incw,
	                  const float*  x, int incx,
	                  const float*  y, int incy,
	                        float*  z, int incz);

void dsvvttvp (int n, const double  alpha,
		          const double* w, int incw,
		          const double* x, int incx,
		          const double* y, int incy,
		                double* z, int incz);
void ssvvttvp (int n, const float   alpha,
	                  const float*  w, int incw,
		          const float*  x, int incx,
		          const float*  y, int incy,
		                float*  z, int incz);  

/*****************************************************************************
 * Prototypes and macros for vendor-supplied FORTRAN libraries follow.
 *****************************************************************************/


/* ------------------------------------------------------------------------- *
 * BLAS level 1 selection.
 *
 * Note that the regular (FORTRAN) BLAS routines idamax & isamax have their
 * return values reduced by 1 to conform with usual C practice.
 * ------------------------------------------------------------------------- */

#if BLAS >= 1

double ddot_  (int* n, double* x, int* incx, double* y, int* incy);
double dasum_ (int* n, double* x, int* incx);
double dnrm2_ (int* n, double* x, int* incx);

void drotg_  (double* a, double* b, double* c, double* s);
void drot_   (int* n, double* x, int* incx, double* y, int* incy,
	      double* c, double* s);
void dswap_  (int* n, double* x, int* incx, double* y, int* incy);
void dscal_  (int* n, double* alpha, double* x, int* incx);
void daxpy_  (int* n, double* alpha, double* x, int* incx, 
		                         double* y, int* incy);
int idamax_ (int* n, double* x, int* incx);

float sdot_  (int* n, float*  x, int* incx, float*  y, int* incy);
float sasum_ (int* n, float*  x, int* incx);
float snrm2_ (int* n, float*  x, int* incx);

void srotg_  (float*  a, float*  b, float*  c, float*  s);
void srot_   (int* n, float *x, int* incx, float *y, int* incy,
	      float *c, float *s);
void sswap_  (int* n, float *x, int* incx, float *y, int* incy);
void sscal_  (int* n, float *alpha, float *x, int* incx);
void saxpy_  (int* n, float *alpha, float *x, int* incx, 
		                        float *y, int* incy);
int isamax_ (int* n, float *x, int* incx);


#define drotg(a, b, c, s) ( drotg_(a, b, c, s) )
#define dswap(n, x, incx, y, incy) \
  (_vecIreg[0]=n, _vecIreg[1]=incx, _vecIreg[2]=incy, \
   dswap_(_vecIreg, x ,_vecIreg+1, y, _vecIreg+2) )
#define dscal(n, alpha, x, incx) \
  (_vecIreg[0]=n, _vecIreg[1]=incx, _vecDreg[0]=alpha, \
   dscal_(_vecIreg, _vecDreg, x, _vecIreg+1) )
#define daxpy(n, alpha, x, incx, y, incy) \
  (_vecIreg[0]=n, _vecIreg[1]=incx, _vecIreg[2]=incy, _vecDreg[0]=alpha, \
   daxpy_(_vecIreg, _vecDreg, x, _vecIreg+1, y, _vecIreg+2) )
#define ddot(n, x, incx, y, incy) \
  (_vecIreg[0]=n,_vecIreg[1]=incx,_vecIreg[2]=incy,\
   ddot_ (_vecIreg, x, _vecIreg+1, y, _vecIreg+2) )
#define dasum(n, x, incx) \
  (_vecIreg[0]=n, _vecIreg[1]=incx, dasum_(_vecIreg, x, _vecIreg+1) )
#define dnrm2(n, x, incx) \
  (_vecIreg[0]=n, _vecIreg[1]=incx, dnrm2_(_vecIreg, x, _vecIreg+1) )
#define drot(n, x, incx, y, incy, c, s) \
  (_vecIreg[0]=n, _vecIreg[1]=incx, _vecIreg[2]=incy, \
   _vecDreg[0]=c, _vecDreg[1]=s, \
   drot_(_vecIreg, x, _vecIreg+1, y, _vecIreg+2, _vecDreg, _vecDreg+1) )
#define idamax(n, x, incx) \
  (_vecIreg[0]=n, _vecIreg[1]=incx, idamax_(_vecIreg, x, _vecIreg+1)-1 )

#define srotg(a, b, c, s) ( srotg_(a, b, c, s) )
#define sswap(n, x, incx, y, incy) \
  (_vecIreg[0]=n, _vecIreg[1]=incx, _vecIreg[2]=incy, \
   sswap_(_vecIreg, x ,_vecIreg+1, y, _vecIreg+2) )
#define sscal(n, alpha, x, incx) \
  (_vecIreg[0]=n, _vecIreg[1]=incx, _vecSreg[0]=alpha, \
   sscal_(_vecIreg, _vecSreg, x, _vecIreg+1) )
#define saxpy(n, alpha, x, incx, y, incy) \
  (_vecIreg[0]=n, _vecIreg[1]=incx, _vecIreg[2]=incy, _vecSreg[0]=alpha, \
   saxpy_(_vecIreg, _vecSreg, x, _vecIreg+1, y, _vecIreg+2) )
#define sdot(n, x, incx, y, incy) \
  (_vecIreg[0]=n,_vecIreg[1]=incx,_vecIreg[2]=incy,\
   sdot_ (_vecIreg, x, _vecIreg+1, y, _vecIreg+2) )
#define sasum(n, x, incx) \
  (_vecIreg[0]=n, _vecIreg[1]=incx, sasum_(_vecIreg, x, _vecIreg+1) )
#define snrm2(n, x, incx) \
  (_vecIreg[0]=n, _vecIreg[1]=incx, snrm2_(_vecIreg, x, _vecIreg+1) )
#define srot(n, x, incx, y, incy, c, s) \
  (_vecIreg[0]=n, _vecIreg[1]=incx, _vecIreg[2]=incy, \
   _vecSreg[0]=c, _vecSreg[1]=s,                      \
   srot_(_vecIreg, x, _vecIreg+1, y, _vecIreg+2, _vecSreg, _vecSreg+1) )
#define isamax(n, x, incx) \
  (_vecIreg[0]=n, _vecIreg[1]=incx, isamax_(_vecIreg, x, _vecIreg+1)-1 )

#endif

/* ------------------------------------------------------------------------- *
 * BLAS level 2 selection.
 * ------------------------------------------------------------------------- */

#if BLAS >= 2

void dgemv_ (char *trans, int* m, int* n, double* alpha,
	     double* a, int* lda,
	     double* x, int* incx, double* beta, double* y, int* incy);
void sgemv_ (char *trans, int* m, int* n, float*  alpha,
	     float*  a, int* lda,
	     float*  x, int* incx, float*  beta, float*  y, int* incy);

void dger_ (int* m, int* n, double* alpha, double* x, int* incx,
	    double* y, int* incy, double* a, int* lda);
void sger_ (int* m, int* n, float*  alpha, float*  x, int* incx,
	    float*  y, int* incy, float*  a, int* lda);

void dspmv_(char *uplo, int* n, double* alpha, double* ap, double* x,
	    int* incx, double* beta, double* y, int* incy);
void sspmv_(char *uplo, int* n, float*  alpha, float*  ap, float*  x,
	    int* incx, float*  beta, float*  y, int* incy);

#define dgemv(trans,m,n,alpha,a,lda,x,incx,beta,y,incy)           \
  (_vecCreg[0]=trans,_vecIreg[0]=m,_vecIreg[1]=n,_vecIreg[2]=lda, \
   _vecIreg[3]=incx,_vecIreg[4]=incy,_vecDreg[0]=alpha,           \
   _vecDreg[1]=beta,                                              \
   dgemv_(_vecCreg,_vecIreg,_vecIreg+1,_vecDreg,a,_vecIreg+2,x,   \
	  _vecIreg+3,_vecDreg+1,y,_vecIreg+4))
#define sgemv(trans,m,n,alpha,a,lda,x,incx,beta,y,incy)           \
  (_vecCreg[0]=trans,_vecIreg[0]=m,_vecIreg[1]=n,_vecIreg[2]=lda, \
   _vecIreg[3]=incx,_vecIreg[4]=incy,_vecSreg[0]=alpha,           \
   _vecSreg[1]=beta,                                              \
   sgemv_(_vecCreg,_vecIreg,_vecIreg+1,_vecSreg,a,_vecIreg+2,x,   \
	  _vecIreg+3,_vecSreg+1,y,_vecIreg+4))

#define dspmv(uplo,n,alpha,a,x,incx,beta,y,incy)        \
  (_vecCreg[0]=uplo,_vecIreg[0]=n,_vecIreg[1]=incx,     \
   _vecIreg[2]=incy,_vecDreg[0]=alpha,_vecDreg[1]=beta, \
   dspmv_(_vecCreg,_vecIreg,_vecDreg,a,x,               \
	  _vecIreg+1,_vecDreg+1,y,_vecIreg+2))
#define sspmv(uplo,n,alpha,a,x,incx,beta,y,incy)        \
  (_vecCreg[0]=uplo,_vecIreg[0]=n,_vecIreg[1]=incx,     \
   _vecIreg[2]=incy,_vecSreg[0]=alpha,_vecSreg[1]=beta, \
   sspmv_(_vecCreg,_vecIreg,_vecSreg,a,x,               \
	  _vecIreg+1,_vecSreg+1,y,_vecIreg+2))

#define dger(m,n,alpha,x,incx,y,incy,a,lda)           \
  (_vecIreg[0]=m,_vecIreg[1]=n,_vecDreg[0]=alpha,     \
   _vecIreg[2]=incx,_vecIreg[3]=incy,_vecIreg[4]=lda, \
   dger_(_vecIreg,_vecIreg+1,_vecDreg,x,_vecIreg+2,y,_vecIreg+3,a,_vecIreg+4))
#define sger(m,n,alpha,x,incx,y,incy,a,lda)           \
  (_vecIreg[0]=m,_vecIreg[1]=n,_vecSreg[0]=alpha,     \
   _vecIreg[2]=incx,_vecIreg[3]=incy,_vecIreg[4]=lda, \
   dger_(_vecIreg,_vecIreg+1,_vecSreg,x,_vecIreg+2,y,_vecIreg+3,a,_vecIreg+4))

#define dmxv(A,nra,x,nca,y)                                       \
  (_vecCreg[0]='T',_vecIreg[0]=nra,_vecIreg[1]=nca,_vecIreg[2]=1, \
   _vecDreg[0]=1.0,_vecDreg[1]=0.0,                               \
   dgemv_(_vecCreg,_vecIreg+1,_vecIreg,_vecDreg,A,_vecIreg+1,     \
	  x,_vecIreg+2,_vecDreg+1,y,_vecIreg+2))
#define smxv(A,nra,x,nca,y)\
  (_vecCreg[0]='T',_vecIreg[0]=nra,_vecIreg[1]=nca,_vecIreg[2]=1, \
   _vecSreg[0]=1.0,_vecSreg[1]=0.0,                               \
   sgemv_(_vecCreg,_vecIreg+1,_vecIreg,_vecSreg,A,_vecIreg+1,     \
	  x,_vecIreg+2,_vecSreg+1,y,_vecIreg+2))

#endif

/* ------------------------------------------------------------------------- *
 * BLAS level 3 selection.
 * ------------------------------------------------------------------------- */

#if BLAS == 3

void dgemm_ (char *ta, char *tb, int* m, int* n, int* k,
	     double* alpha,
	     double* a, int* lda,
	     double* b, int* ldb, double* beta,
	     double* c, int* ldc);
void sgemm_ (char *ta, char *tb, int* m, int* n, int* k,
	     float *alpha,
	     float *a, int* lda, float *b,
	     int* ldb, float *beta,
	     float *c, int* ldc);
 
#define dgemm(ta,tb,m,n,k,alpha,a,lda,b,ldb,beta,c,ldc)           \
  (_vecCreg[0]=ta,_vecCreg[1]=tb,_vecIreg[0]=m,_vecIreg[1]=n,     \
   _vecIreg[2]=k,_vecIreg[3]=lda,_vecIreg[4]=ldb,_vecIreg[5]=ldc, \
   _vecDreg[0]=alpha,_vecDreg[1]=beta,                            \
   dgemm_(_vecCreg,_vecCreg+1,_vecIreg,_vecIreg+1,_vecIreg+2,     \
	  _vecDreg,a,_vecIreg+3,b,_vecIreg+4,_vecDreg+1,c,        \
	  _vecIreg+5))
#define sgemm(ta,tb,m,n,k,alpha,a,lda,b,ldb,beta,c,ldc)           \
  (_vecCreg[0]=ta,_vecCreg[1]=tb,_vecIreg[0]=m,_vecIreg[1]=n,     \
   _vecIreg[2]=k,_vecIreg[3]=lda,_vecIreg[4]=ldb,_vecIreg[5]=ldc, \
   _vecSreg[0]=alpha,_vecSreg[1]=beta,                            \
   sgemm_(_vecCreg,_vecCreg+1,_vecIreg,_vecIreg+1,_vecIreg+2,     \
	  _vecSreg,a,_vecIreg+3,b,_vecIreg+4,_vecSreg+1,c,        \
	  _vecIreg+5))

#define dmxm(A,nra,B,nca,C,ncb)                                     \
  (_vecCreg[0]='N',_vecCreg[1]='N',_vecIreg[0]=nra,_vecIreg[1]=nca, \
   _vecIreg[2]=ncb,_vecDreg[0]=1.0,_vecDreg[1]=0.0,                 \
   dgemm_(_vecCreg,_vecCreg+1,_vecIreg+2,_vecIreg,_vecIreg+1,       \
	  _vecDreg,B,_vecIreg+2,A,_vecIreg+1,_vecDreg+1,C,_vecIreg+2))
#define smxm(A,nra,B,nca,C,ncb)                                     \
  (_vecCreg[0]='N',_vecCreg[1]='N',_vecIreg[0]=nra,_vecIreg[1]=nca, \
   _vecIreg[2]=ncb,_vecSreg[0]=1.0,_vecSreg[1]=0.0,                 \
   dgemm_(_vecCreg,_vecCreg+1,_vecIreg+2,_vecIreg,_vecIreg+1,       \
	  _vecSreg,B,_vecIreg+2,A,_vecIreg+1,_vecSreg+1,C,_vecIreg+2))


#endif

/* ------------------------------------------------------------------------- *
 * LAPACK selection.
 * ------------------------------------------------------------------------- */

#if LAPACK == 1

/* Factor a general matrix. */

void dgetrf_ (int* m, int* n, double* a, int* lda,
	      int* ipiv, int* info);
void sgetrf_ (int* m, int* n, float*  a, int* lda,
	      int* ipiv, int* info);

#define dgetrf(m,n,a,lda,ipiv,info)               \
  (_vecIreg[0]=m, _vecIreg[1]=n, _vecIreg[2]=lda, \
   dgetrf_(_vecIreg, _vecIreg+1, a, _vecIreg+2, ipiv, info))
#define sgetrf(m,n,a,lda,ipiv,info)               \
  (_vecIreg[0]=m, _vecIreg[1]=n, _vecIreg[2]=lda, \
   sgetrf_(_vecIreg, _vecIreg+1, a, _vecIreg+2, ipiv, info))

/* Invert a general matrix from factors. */

void dgetri_ (int* n,    double* a,     int* lda,   int* ipiv,  
	      double* work, int* lwork, int* info);
void sgetri_ (int* n,    float*  a,     int* lda,   int* ipiv,
	      float*  work, int* lwork, int* info);

#define dgetri(n,a,lda,ipiv,work,lwork,info)          \
  (_vecIreg[0]=n, _vecIreg[1]=lda, _vecIreg[2]=lwork, \
   dgetri_(_vecIreg, a, _vecIreg+1, ipiv, work, _vecIreg+2, info))
#define sgetri(n,a,lda,ipiv,work,lwork,info)          \
  (_vecIreg[0]=n, _vecIreg[1]=lda, _vecIreg[2]=lwork, \
   sgetri_(_vecIreg, a, _vecIreg+1, ipiv, work, _vecIreg+2, info))

/* Factor and solve band-symmetric matrix problem. */

void dpbtrf_ (char *uplo, int* n, int* kd,
	      double* ab, int* ldab, int* info);
void spbtrf_ (char *uplo, int* n, int* kd,
	      float*  ab, int* ldab, int* info);

#define dpbtrf(uplo,n,kd,ab,ldab,info)                                \
  (_vecCreg[0]=uplo, _vecIreg[0]=n, _vecIreg[1]=kd, _vecIreg[2]=ldab, \
   dpbtrf_(_vecCreg, _vecIreg, _vecIreg+1, ab, _vecIreg+2, info))
#define spbtrf(uplo,n,kd,ab,ldab,info)                                \
  (_vecCreg[0]=uplo, _vecIreg[0]=n, _vecIreg[1]=kd, _vecIreg[2]=ldab, \
   spbtrf_(_vecCreg, _vecIreg, _vecIreg+1, ab, _vecIreg+2, info))

void dpbtrs_ (char *uplo, int* n, int* kd, int* nrhs, double* ab,
	      int* ldab, double* b, int* ldb, int* info);
void spbtrs_ (char *uplo, int* n, int* kd, int* nrhs, float*  ab,
	      int* ldab, float*  b, int* ldb, int* info);

#define dpbtrs(uplo,n,kd,nrhs,ab,ldab,b,ldb,info)                      \
  (_vecCreg[0]=uplo, _vecIreg[0]=n, _vecIreg[1]=kd, _vecIreg[2]=nrhs,  \
   _vecIreg[3]=ldab, _vecIreg[4]=ldb,                                  \
   dpbtrs_(_vecCreg, _vecIreg, _vecIreg+1, _vecIreg+2, ab, _vecIreg+3, \
	   b, _vecIreg+4, info))
#define spbtrs(uplo,n,kd,nrhs,ab,ldab,b,ldb,info)                      \
  (_vecCreg[0]=uplo, _vecIreg[0]=n, _vecIreg[1]=kd, _vecIreg[2]=nrhs,  \
   _vecIreg[3]=ldab, _vecIreg[4]=ldb,                                  \
   spbtrs_(_vecCreg, _vecIreg, _vecIreg+1, _vecIreg+2, ab, _vecIreg+3, \
	   b, _vecIreg+4, info))

/* Factor and solve packed-symmetric matrix problem. */

void dpptrf_(char *uplo, int* n, double* ap, int* info);
void spptrf_(char *uplo, int* n, double* ap, int* info);

#define dpptrf(uplo,n,ap,info) \
  (_vecCreg[0]=uplo, _vecIreg[0]=n, dpptrf_(_vecCreg, _vecIreg, ap, info))
#define spptrf(uplo,n,ap,info) \
  (_vecCreg[0]=uplo, _vecIreg[0]=n, spptrf_(_vecCreg, _vecIreg, ap, info))

void dpptrs_(char *uplo, int* n, int* nrhs, double* ap,
	     double* b, int* ldb, int* info);
void spptrs_(char *uplo, int* n, int* nrhs, float*  ap,
	     float*  b, int* ldb, int* info);

#define dpptrs(uplo,n,nrhs,ap,b,ldb,info)                              \
  (_vecCreg[0]=uplo, _vecIreg[0]=n, _vecIreg[1]=nrhs, _vecIreg[2]=ldb, \
   dpptrs_(_vecCreg, _vecIreg, _vecIreg+1, ap, b, _vecIreg+2, info))
#define spptrs(uplo,n,nrhs,ap,b,ldb,info)                              \
  (_vecCreg[0]=uplo, _vecIreg[0]=n, _vecIreg[1]=nrhs, _vecIreg[2]=ldb, \
   spptrs_(_vecCreg, _vecIreg, _vecIreg+1, ap, b, _vecIreg+2, info))

#endif

#endif

