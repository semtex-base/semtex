/*****************************************************************************
 * xznan:  x[i] = isnan(x[i]) ? 0.0 : x[i]
 *****************************************************************************/

#include <math.h>
#include <cfemdef.h>

#if defined(__uxp__)
#pragma global novrec
#pragma global noalias
#endif

void dznan (int n, double* x, int incx)
{
  int i;

  x += (incx<0) ? (-n+1)*incx : 0;

  for (i = 0; i < n; i++) x[i*incx] = isnan (x[i*incx]) ? 0.0 : x[i*incx];
}


void sznan (int n, float* x, int incx)
{
  int i;

  x += (incx<0) ? (-n+1)*incx : 0;

  for (i = 0; i < n; i++) x[i*incx] = isnan (x[i*incx]) ? 0.0 : x[i*incx];
}
