/*****************************************************************************
 * lxsame: return 1 if all elements of x and y match, else zero.
 *
 * Floating point versions use absolute tolerances on the allowable difference.
 *****************************************************************************/

#include <math.h>
#include <cfemdef.h>

#define EPSSP   6.0e-7
#define EPSDP   6.0e-14
#define EPSm30  1.0e-30

#if defined(__uxp__)
#pragma global novrec
#pragma global noalias
#endif


int lisame (int n,
	    const int* x, int incx,
	    const int* y, int incy)
{ 
  int i;

  x += (incx < 0) ? (-n + 1) * incx : 0;
  y += (incy < 0) ? (-n + 1) * incy : 0;

  for (i = 0; i < n; i++) if (x[i*incx] != y[i*incy]) return 0;
  
  return 1;
}


int ldsame (int n,
	    const double* x, int incx,
	    const double* y, int incy)
{ 
  int i;

  x += (incx < 0) ? (-n + 1) * incx : 0;
  y += (incy < 0) ? (-n + 1) * incy : 0;

  for (i = 0; i < n; i++) if (fabs (x[i*incx] - y[i*incy]) > EPSDP) return 0;

  return 1;
}


int lssame (int n,
	    const float* x, int incx,
	    const float* y, int incy)
{ 
  int i;

  x += (incx < 0) ? (-n + 1) * incx : 0;
  y += (incy < 0) ? (-n + 1) * incy : 0;

#if  defined(__uxp__)
  for (i = 0; i < n; i++) if (fabs  (x[i*incx] - y[i*incy]) > EPSSP) return 0;
#else
  for (i = 0; i < n; i++) if (fabsf (x[i*incx] - y[i*incy]) > EPSSP) return 0;
#endif
  
  return 1;
}
