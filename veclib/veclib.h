#ifndef VECLIB_H
#define VECLIB_H
///////////////////////////////////////////////////////////////////////////////
// veclib.h:  C++ wrappers for veclib subroutine calls.
//
// Veclib was originally described in the iPSC/2 Programmer's
// Reference Manual.  The semtex version expands on that.
//
// Copyright (c) 1994+, Hugh M Blackburn
///////////////////////////////////////////////////////////////////////////////

#include <cfemdef.h>

extern "C" {
  
  /* -- UTILITY FUNCTIONS */

  double dclock       (void);
  float  sclock       (void);
  void   message      (const char*, const char*, int);
  void   semheaderstr (char*, const char*, const char*,
		       int, int, int, int, int,
		       double, double, double, double,
		       const char*, const char*);
 
  int    needbyteswap (const char*);

  /* -- MATHEMATICAL PRIMITIVES */

  void  dcopy  (int n,
		const double* x, int incx, double* y, int incy);
  void  icopy  (int n,
		const int*  x,   int incx, int*    y, int incy);
  void  scopy  (int n,
		const float*  x, int incx, float*  y, int incy);
  
  void  dfill  (int n, double alpha, double* x, int incx);
  void  ifill  (int n, int    alpha, int*    x, int incx);
  void  sfill  (int n, float  alpha, float*  x, int incx);
  
  void  dznan  (int n, double* x, int incx);
  void  sznan  (int n, float*  x, int incx);
  
  void  dneg   (int n, double* x, int incx);
  void  ineg   (int n, int*    x, int incx);
  void  sneg   (int n, float*  x, int incx);
  
  void  dvneg  (int n, const double* x, int incx, double* y, int incy);
  void  ivneg  (int n, const int*    x, int incx, int*    y, int incy);
  void  svneg  (int n, const float*  x, int incx, float*  y, int incy);
  
  void  dvsgn  (int n, const double* x, int incx, double* y, int incy);
  void  ivsgn  (int n, const int*    x, int incx, int*    y, int incy);
  void  svsgn  (int n, const float*  x, int incx, float*  y, int incy);
  
  void  dsadd  (int n, double alpha,
		const double* x, int incx, double*  y, int incy);
  void  isadd  (int n, int    alpha,
		const int*    x, int incx, int* y, int incy);
  void  ssadd  (int n, float  alpha,
		const float*  x, int incx, float*   y, int incy);
  
  void  dspow  (const int n, const double alpha,
		const double* x, int incx, double* y, int incy);
  void  sspow  (const int n, const float  alpha,
		const float*  x, int incx, float*  y, int incy);
  
  void  dvadd  (int n, const double* x, int incx,
		const double* y, int incy, double* z, int incz);
  void  ivadd  (int n, const int*  x, int incx, 
		const int*  y, int incy, int*  z, int incz);
  void  svadd  (int n, const float*  x, int incx,
		const float*  y, int incy, float*  z, int incz);
  
  void  dssub  (int n, double alpha,
		const double* x, int incx, double* y, int incy);
  void  issub  (int n, int alpha,
		const int*  x, int incx, int*  y, int incy);
  void  sssub  (int n, float   alpha,
		const float*  x, int incx, float*  y, int incy);
  
  void  dvsub  (int n, const double* x, int incx,
		const double* y, int incy, double* z, int incz);
  void  ivsub  (int n, const int*  x, int incx,
		const int*  y, int incy, int*  z, int incz);
  void  svsub  (int n, const float*  x, int incx,
		const float*  y, int incy, float*  z, int incz);
  
  void  dsmul  (int n, double alpha,
		const double* x, int incx, double* y, int incy);
  void  ismul  (int n, int alpha,
		const int*  x, int incx, int*  y, int incy);
  void  ssmul  (int n, float  alpha,
		const float*  x, int incx, float*  y, int incy);
  
  void  dvmul  (int n, const double* x, int incx,
		const double* y, int incy, double* z, int incz);
  void  ivmul  (int n, const int*  x, int incx,
		const int*  y, int incy, int*  z, int incz);
  void  svmul  (int n, const float*  x, int incx,
		const float*  y, int incy, float*  z, int incz);
  
  void  dsdiv  (int n, double alpha,
		const double* x, int incx, double* y, int incy);
  void  isdiv  (int n, int  alpha,
		const int*  x, int incx, int*  y, int incy);
  void  ssdiv  (int n, float  alpha,
		const float*  x, int incx, float*  y, int incy);
  
  void  dvrecp (int n,
		const double* x, int incx, double* y, int incy);
  void  svrecp (int n,
		const float*  x, int incx, float*  y, int incy);
  
  void  dvdiv  (int n, const double* x, int incx,
		const double* y, int incy, double* z, int incz);
  void  svdiv  (int n, const float*  x, int incx,
		const float*  y, int incy, float*  z, int incz);
  
  void  dzero  (int n, double* x, int incx);
  void  izero  (int n, int*  x, int incx);
  void  szero  (int n, float*  x, int incx);
  
  /* -- OTHER MATHEMATICAL FUNCTIONS */
  
  void    dvabs    (int n,
		    const double*  x, int incx, double*  y, int incy);
  void    ivabs    (int n,
		    const int* x, int incx, int* y, int incy);
  void    svabs    (int n,
		    const float*   x, int incx, float*   y, int incy);
  
  void    dvamax   (int n, const double*  x, int incx,
		    const double*  y, int incy, double*  z, int incz);
  void    ivamax   (int n, const int* x, int incx,
		    const int* y, int incy, int* z, int incz);
  void    svamax   (int n, const float*   x, int incx,
		    const float*   y, int incy, float*   z, int incz);
  
  void    dvexp    (int n,
		    const double* x, int incx, double* y, int incy);
  void    svexp    (int n,
		    const float*  x, int incx, float*  y, int incy);
  
  void    dvlg10   (int n,
		    const double* x, int incx, double* y, int incy);
  void    svlg10   (int n,
		    const float*  x, int incx, float*  y, int incy);
  
  void    dvlog    (int n,
		    const double* x, int incx, double* y, int incy);
  void    svlog    (int n,
		    const float*  x, int incx, float*  y, int incy);
  
  void    dvatan   (int n,
		    const double* x, int incx, double* y, int incy);
  void    svatan   (int n,
		    const float*  x, int incx, float*  y, int incy);
  
  void    dvatn2   (int n, const double* x, int incx,
		    const double* y, int incy, double* z, int incz);
  void    svatn2   (int n, const float*  x, int incx,
		    const float*  y, int incy, float*  z, int incz);
  
  void    dvcos    (int n,
		    const double* x, int incx, double* y, int incy);
  void    svcos    (int n,
		    const float*  x, int incx, float*  y, int incy);
  
  void    dvsin    (int n,
		    const double* x, int incx, double* y, int incy);
  void    svsin    (int n,
		    const float*  x, int incx, float*  y, int incy);
  
  void    dvsqrt   (int n,
		    const double* x, int incx, double* y, int incy);
  void    svsqrt   (int n,
		    const float*  x, int incx, float*  y, int incy);
  
  void    dvtanh   (int n,
		    const double* x, int incx, double* y, int incy);
  void    svtanh   (int n,
		    const float*  x, int incx, float*  y, int incy);
  
  void    raninit  (int flag);
  double  dranu    (void);
  float   sranu    (void);
  double  drang    (void);
  float   srang    (void);
  double  dnormal  (double mean, double sdev);
  float   snormal  (float  mean, float  sdev);
  void    dvrandom (int  n, double* x, int incx);
  void    svrandom (int  n, float*  x, int incx);
  void    dvgauss  (int  n, double* x, int incx);
  void    svgauss  (int  n, float*  x, int incx);
  void    dvnormal (int  n, double mean, double sdev,
		    double* x, int incx);
  void    svnormal (int  n, float  mean, float  sdev, 
		    float*  x, int incx);

  void    dvhypot  (int n, const double* x, int incx,
		    const double* y, int incy, double* z, int incz);
  void    svhypot  (int n, const float*  x, int incx,
		    const float*  y, int incy, float*  z, int incz);
  void    dvmag    (int n, const double* w, int incw, 
		    const double* x, int incx, const double* y,
		    int incy, double* z, int incz);
  void    svmag    (int n, const float* w, int incw, 
		    const float*  x, int incx, const float*  y,
		    int incy, float*  z, int incz);

  void    dvpow    (int n, const double* x, int incx,
		    const double* y, int incy, double* z, int incz);
  void    svpow    (int n, const float*  x, int incx,
		    const float*  y, int incy, float*  z, int incz);

  
  /* -- TRIAD OPERATIONS */

  void   dsvmvt (int n, double alpha, const double* x, int incx,
		 const double* y, int incy, double* z, int incz);
  void   ssvmvt (int n, float  alpha, const float*  x, int incx,
		 const float*  y, int incy, float*  z, int incz);
  
  void   dsvpvt (int n, double alpha, const double* x, int incx,
		 const double* y, int incy, double* z, int incz);
  void   ssvpvt (int n, float  alpha, const float*  x, int incx,
		 const float*  y, int incy, float*  z, int incz);
  
  void   dsvtsp (int n, double alpha, double beta,
		 const double* x, int incx, double* y, int incy);
  void   ssvtsp (int n, float  alpha, float  beta,
		 const float*  x, int incx, float*  y, int incy);
  
  void   dsvtvm (int n, double alpha, const double* x, int incx,
		 const double* y, int incy, double* z, int incz);
  void   ssvtvm (int n, float  alpha, const float*  x, int incx,
		 const float*  y, int incy, float*  z, int incz);
  
  void   dsvtvp (int n, double alpha, const double* x, int incx,
		 const double* y, int incy, double* z, int incz);
  void   ssvtvp (int n, float  alpha, const float*  x, int incx,
		 const float*  y, int incy, float*  z, int incz);
  
  void   dsvvmt (int n, double alpha, const double* x, int incx,
		 const double* y, int incy, double* z, int incz);
  void   ssvvmt (int n, float  alpha, const float*  x, int incx,
		 const float*  y, int incy, float*  z, int incz);
  
  void   dsvvpt (int n, double alpha, const double* x, int incx,
		 const double* y, int incy, double* z, int incz);
  void   ssvvpt (int n, float  alpha, const float*  x, int incx,
		 const float*  y, int incy, float*  z, int incz);
  
  void   dsvvtm (int n, double alpha, const double* x, int incx,
		 const double* y, int incy, double* z, int incz);
  void   ssvvtm (int n, float  alpha, const float*  x, int incx,
		 const float*  y, int incy, float*  z, int incz);
  
  void   dsvvtp (int n, double alpha, const double* x, int incx,
		 const double* y, int incy, double* z, int incz);
  void   ssvvtp (int n, float  alpha, const float*  x, int incx,
		 const float*  y, int incy, float*  z, int incz);

  void   dsvvtt (int n, double alpha, const double* x, int incx,
		 const double* y, int incy, double* z, int incz);
  void   ssvvtt (int n, float  alpha, const float*  x, int incx,
		 const float*  y, int incy, float*  z, int incz);
  
  void   dvvmvt (int n, const double* w, int incw, const double* x,
		 int incx, const double* y, int incy,
		 double* z, int incz);
  void   svvmvt (int n, const float*  w, int incw, const float*  x,
		 int incx, const float*  y, int incy,
		 float*  z, int incz);
  
  void   dvvpvt (int n, const double* w, int incw, const double* x,
		 int incx, const double* y, int incy,
		 double* z, int incz);
  void   svvpvt (int n, const float*  w, int incw, const float*  x,
		 int incx, const float*  y, int incy,
		 float*  z, int incz);

  void   dvvtvm (int n, const double* w, int incw, const double* x,
		 int incx, const double* y, int incy,
		 double* z, int incz);
  void   svvtvm (int n, const float*  w, int incw, const float*  x,
		 int incx, const float*  y, int incy,
		 float*  z, int incz);
  
  void   dvvtvp (int n, const double* w, int incw, const double* x,
		 int incx, const double* y, int incy,
		 double* z, int incz);
  void   svvtvp (int n, const float*  w, int incw, const float*  x,
		 int incx, const float*  y, int incy,
		 float*  z, int incz);
  
  void   dvvvtm (int n, const double* w, int incw, const double* x,
		 int incx, const double* y, int incy,
		 double* z, int incz);
  void   svvvtm (int n, const float*  w, int incw, const float*  x,
		 int incx, const float*  y, int incy,
		 float*  z, int incz);

  void   dvvvtt (int n,    const double* w, int incw, const double* x,
		 int incx, const double* y, int incy,
		 double* z,  int incz);
  void   svvvtt (int n,    const float*  w, int incw, const float*  x,
		 int incx, const float*  y, int incy,
		 float*  z,  int incz);
  
  /* -- RELATIONAL PRIMITIVE OPERATIONS */

  void   iseq (int n, int  alpha,
	       const  int*  x, int incx, int *y, int incy);
  void   dsge (int n, double alpha,
	       const  double* x, int incx, int *y, int incy);
  void   isge (int n, int  alpha,
	       const  int*  x, int incx, int *y, int incy);
  void   ssge (int n, float  alpha,
	       const  float*  x, int incx, int *y, int incy);
  void   dsle (int n, double alpha,
	       const  double* x, int incx, int *y, int incy);
  void   isle (int n, int  alpha,
	       const  int*  x, int incx, int *y, int incy);
  void   ssle (int n, float  alpha,
	       const  float*  x, int incx, int *y, int incy);
  void   dslt (int n, double alpha,
	       const  double* x, int incx, int *y, int incy);
  void   islt (int n, int  alpha,
	       const  int*  x, int incx, int *y, int incy);
  void   sslt (int n, float  alpha,
	       const  float*  x, int incx, int *y, int incy);
  void   dsne (int n, double alpha,
	       const  double* x, int incx, int *y, int incy);
  void   isne (int n, int  alpha,
	       const  int*  x, int incx, int *y, int incy);
  void   ssne (int n, float  alpha,
	       const  float*  x, int incx, int *y, int incy);
  
  /* -- REDUCTION FUNCTIONS */

  double dsum   (int n, const double* x, int incx);
  int    isum   (int n, const int*    x, int incx);
  float  ssum   (int n, const float*  x, int incx);
  int    idmax  (int n, const double* x, int incx);
  int    iimax  (int n, const int*    x, int incx);
  int    ismax  (int n, const float*  x, int incx);
  int    idmin  (int n, const double* x, int incx);
  int    iimin  (int n, const int*    x, int incx);
  int    ismin  (int n, const float*  x, int incx);
  int    icount (int n, const int*    x, int incx);
  int    imatch (int n, const int alpha, const int*  x, int incx);  
  int    ifirst (int n, const int*    x, int incx);
  int    lany   (int n, const int*    x, int incx);  
  int    lisame (int n, const int*    x, int incx,
	                const int*    y, int incy);
  int    ldsame (int n, const double* x, int incx,
		        const double* y, int incy);
  int    lssame (int n, const float*  x, int incx,
		        const float*  y, int incy);

  /* -- CONVERSION PRIMITIVES */
  
  void   vdble   (int n,
		  const float*  x, int incx, double* y, int incy);
  void   vsngl   (int n,
		  const double* x, int incx, float*  y, int incy);
  void   dvfloa  (int n,
		  const int*  x, int incx, double* y, int incy);
  void   svfloa  (int n,
		  const int*  x, int incx, float*  y, int incy);


  int     iformat (void);
  void    format  (char*);
  void    dbrev   (int n,
		   const double* x, int incx, double* y, int incy);
  void    ibrev   (int n,
		   const int*  x, int incx, int*  y, int incy);
  void    sbrev   (int n,
		   const float*  x, int incx, float*  y, int incy);
  
  /* -- MISCELLANEOUS FUNCTIONS */
 
  void   dscatr  (int n, const double* x, const int* y, double* z);
  void   iscatr  (int n, const int*  x, const int* y, int*  z);
  void   sscatr  (int n, const float*  x, const int* y, float*  z);
  
  void   dgathr  (int n, const double* x, const int* y, double* z);
  void   igathr  (int n, const int*  x, const int* y, int*  z);
  void   sgathr  (int n, const float*  x, const int* y, float*  z);

  void   dgathr_scatr (int n, const double*  w, 
		       const int* x, const int* y, double* z);
  void   igathr_scatr (int n, const int* w,
		       const int* x, const int* y, int*  z);
  void   sgathr_scatr (int n, const float*   w,
		       const int* x, const int* y,  float* z);
  
  void   dscatr_sum (int n,
		     const double* x, const int *y, double* z);
  void   iscatr_sum (int n,
		     const int*  x, const int* y, int*  z);
  void   sscatr_sum (int n,
		     const float*  x, const int *y, float*  z);
  
  void   dgathr_sum (int n,
		     const double* x, const int* y, double* z);
  void   igathr_sum (int n,
		     const int*  x, const int* y, int*  z);
  void   sgathr_sum (int n,
		     const float*  x, const int* y, float*  z);

  void   dgathr_scatr_sum (int n, const double*  w, 
			   const int* x, const int* y, double*  z);
  void   igathr_scatr_sum (int n, const int* w,
			   const int* x, const int* y, int* z);
  void   sgathr_scatr_sum (int n, const float*   w,
			   const int* x, const int* y, float*   z);
  
  void   dramp   (int n, double  alpha, double  beta,
		  double*  x, int incx);
  void   iramp   (int n, int alpha, int beta,
		  int* x, int incx);
  void   sramp   (int n, float   alpha, float   beta,
		  float*   x, int incx);
  
  void   dcndst  (int n, const  double*  x, int incx,
		  const int* y, int incy, double*  z, int incz);
  void   icndst  (int n, const  int* x, int incx,
		  const int* y, int incy, int* z, int incz);
  void   scndst  (int n, const  float*   x, int incx,
		  const int* y, int incy, float*   z, int incz);
  
  void   dmask   (int n,
		  const double* w, int incw,
		  const double* x, int incx,
		  const int*  y, int incy,
		        double* z, int incz);
  void   imask   (int n,
		  const int*  w, int incw,
		  const int*  x, int incx,
		  const int*  y, int incy,
		        int*  z, int incz);
  void   smask   (int n,
		  const float*  w, int incw,
		  const float*  x, int incx,
		  const int*  y, int incy,
		        float*  z, int incz);

  void dclip (int n, const  double alpha,  const double   beta,
	      const double* x,  int incx,  const double*  y, int incy);
  void iclip (int n, const  int alpha, const int  beta,
	      const int* x, int incx,  const int* y, int incy);
  void sclip (int n, const  float alpha,   const float    beta,
	      const float* x,  int incx,   const float*   y, int incy);

  void dclipup (int n, const double alpha, const double* x,
		int incx,  const double* y, int incy);
  void iclipup (int n, const int alpha, const int* x,
		int incx, const int* y, int incy);
  void sclipup (int n, const float alpha, const float* x,
		int incx, const float* y, int incy);

  void dclipdn (int n, const double alpha, const double* x,
		int incx,  const double* y, int incy);
  void iclipdn (int n, const int alpha, const int* x,
		int incx, const int* y, int incy);
  void sclipdn (int n, const float alpha, const float* x,
		int incx, const float* y, int incy);

  void diclip (int n, const  double alpha,  const double   beta,
	       const double* x,  int incx, const double*  y, int incy);
  void iiclip (int n, const  int alpha, const int  beta,
	       const int* x, int incx, const int* y, int incy);
  void siclip (int n, const  float alpha,   const float    beta,
	       const float* x,  int incx,  const float*   y, int incy);
  
  void   dvpoly  (int n, const double* x, int incx, int m,
		           const double* c, int incc, 
		                 double* y, int incy);
  void   svpoly  (int n, const float*  x, int incx, int m,
		           const float*  c, int incc, 
		                 float*  y, int incy);
  
  double dpoly   (int n, double x, const double* xp, const double* yp);
  float  spoly   (int n, float  x, const float*  xp, const float*  yp);
  void   dpolint (const double* xa, const double* ya, int n,
		  double x,        double* y,  double* dy);
  void   spolint (const float*  xa, const float*  ya, int n,
		  float  x,        float*  y, float*  dy);
  
  void   dspline  (int n, double yp1, double ypn,
		   const double* x, const double* y,  double* y2);
  void   sspline  (int n, float yp1, float ypn,
		   const float*  x, const float*  y,  float*  y2);
  double dsplint  (int n, double x,
		   const double* xa, const double* ya, const double* y2a);
  float  ssplint  (int n, float  x,
		   const float*  xa, const float*  ya,  const float*  y2a);
  double dsplquad (const double* xa, const double* ya,  const double* y2a,
		   const int n,    const double xmin, const double  xmax);
  float  ssplquad (const float*  xa, const float*  ya,  const float*  y2a,
		   const int   n,  const float  xmin, const float   xmax);

  void   dvvtvvtp (int n, const double* v, int incv,
          	            const double* w, int incw,
	                    const double* x, int incx,
	                    const double* y, int incy,
	                          double* z, int incz);
  void   svvtvvtp (int n, const float*  v, int incv,
	                    const float*  w, int incw,
	                    const float*  x, int incx,
	                    const float*  y, int incy,
	                          float*  z, int incz); 

  void   dvvtvvtm (int n, const double* v, int incv,
          	            const double* w, int incw,
	                    const double* x, int incx,
	                    const double* y, int incy,
	                          double* z, int incz);
  void   svvtvvtm (int n, const float*  v, int incv,
	                    const float*  w, int incw,
	                    const float*  x, int incx,
	                    const float*  y, int incy,
	                          float*  z, int incz); 

  void   dsvvttvp (int n, const double  alpha,
          	            const double* w, int incw,
	                    const double* x, int incx,
	                    const double* y, int incy,
	                          double* z, int incz);
  void   ssvvttvp (int n, const float   alpha,
	                    const float*  w, int incw,
	                    const float*  x, int incx,
	                    const float*  y, int incy,
	                          float*  z, int incz);  
}


class Veclib {
 public:

   // -- UTILITY FUNCTIONS:

  static double clock ()
  { return dclock () ; }

  static void alert (const char *routine, const char *txt, int level)
  { message (routine, txt, level); }

  static void headerStr (char*       tgt    ,
			 const char* session,
			 const char* created,
			 int         nr     ,
			 int         ns     ,
			 int         nz     ,
			 int         nel    ,
			 int         step   ,
			 double      time   ,
			 double      deltat ,
			 double      kinvis ,
			 double      beta   ,
			 const char* fields ,
			 const char* wformat) {
    semheaderstr  (tgt, session, created,
		   nr, ns, nz, nel,
		   step, time, deltat,
		   kinvis, beta, fields, wformat);
  }

  static int byteSwap (const char* inputformat) {
    return needbyteswap (inputformat);
  }

  // -- ZERO-OFFSET 2D MATRIX ADDRESSES:

  static inline int row_major (int i, int j, int n)
  { return j + i * n; }
  static inline int col_major (int i, int j, int n)
  { return i + j * n; }


  // -- MATHEMATICAL PRIMITIVES:

  static void copy (int n, const double* x, int incx,
                                   double* y, int incy)
  { dcopy (n, x, incx, y, incy); }
  static void copy (int n, const int*  x, int incx,
                                   int*  y, int incy)
  { icopy (n, x, incx, y, incy); }
  static void copy (int n, const float*  x, int incx,
                                   float*  y, int incy)
  { scopy (n, x, incx, y, incy); } 

  
  static void fill (int n, double alpha, double* x, int incx)
  { dfill (n, alpha, x, incx); }
  static void fill (int n, int  alpha, int*  x, int incx)
  { ifill (n, alpha, x, incx); }
  static void fill (int n, float  alpha, float*  x, int incx)
  { sfill (n, alpha, x, incx); }


  static void znan (int n, double* x, int incx)
  { dznan (n, x, incx); }
  static void znan (int n, float*  x, int incx)
  { sznan (n, x, incx); }

  static void neg (int n, double* x, int incx)
  { dneg (n, x, incx); }
  static void neg (int n, int*  x, int incx)
  { ineg (n, x, incx); }
  static void neg (int n, float*  x, int incx)
  { sneg (n, x, incx); }


  static void vneg (int n,
		    const double* x, int incx, double* y, int incy)
  { dvneg (n, x, incx, y, incy); }
  static void vneg (int n,
		    const int*  x, int incx, int*  y, int incy)
  { ivneg (n, x, incx, y, incy); }
  static void vneg (int n,
		    const float*  x, int incx, float*  y, int incy)
  { svneg (n, x, incx, y, incy); }


  static void vsgn (int n,
		    const double* x, int incx, double* y, int incy)
  { dvsgn (n, x, incx, y, incy); }
  static void vsgn (int n,
		    const int*  x, int incx, int*  y, int incy)
  { ivsgn (n, x, incx, y, incy); }
  static void vsgn (int n,
		    const float*  x, int incx, float*  y, int incy)
  { svsgn (n, x, incx, y, incy); }

  
  static void sadd (int n, double alpha, const double* x, int incx, 
		                                 double* y, int incy)
  { dsadd (n, alpha, x, incx, y, incy); }
  static void sadd (int n, int  alpha, const int*  x, int incx,
		                                 int*  y, int incy)
  { isadd (n, alpha, x, incx, y, incy); }
  static void sadd (int n, float  alpha, const float*  x, int incx,
		                                 float*  y, int incy)
  { ssadd (n, alpha, x, incx, y, incy); }


  static void spow (const int n, const double alpha,
		    const double* x, int incx, double* y, int incy)
  { dspow (n, alpha, x, incx, y, incy); }
  static void spow (const int n, const float alpha,
		    const float *x, int incx, float *y, int incy)
  { sspow (n, alpha, x, incx, y, incy); }


  static void vadd (int n, const double* x, int incx,
		             const double* y, int incy,
                                   double* z, int incz)
  { dvadd (n, x, incx, y, incy, z, incz); }
  static void vadd (int n, const int*  x, int incx,
		             const int*  y, int incy,
                                   int*  z, int incz)
  { ivadd (n, x, incx, y, incy, z, incz); }
  static void vadd (int n, const float*  x, int incx,
		             const float*  y, int incy,
                                   float*  z, int incz)
  { svadd (n, x, incx, y, incy, z, incz); }

 
  static void ssub (int n, double alpha, const double* x, int incx, 
		                                 double* y, int incy)
  { dssub (n, alpha, x, incx, y, incy); }
  static void ssub (int n, int  alpha, const int*  x, int incx,
		                                 int*  y, int incy)
  { issub (n, alpha, x, incx, y, incy); }
  static void ssub (int n, float  alpha, const float*  x, int incx,
		                                 float*  y, int incy)
  { sssub (n, alpha, x, incx, y, incy); }

  
  static void vsub (int n, const double* x, int incx,
		             const double* y, int incy,
                                   double* z, int incz)
  { dvsub (n, x, incx, y, incy, z, incz); }
  static void vsub (int n, const int*  x, int incx,
		             const int*  y, int incy,
                                   int*  z, int incz)
  { ivsub (n, x, incx, y, incy, z, incz); }
  static void vsub (int n, const float*  x, int incx,
		             const float*  y, int incy,
                                   float*  z, int incz)
  { svsub (n, x, incx, y, incy, z, incz); }

  
  static void  smul  (int n, double alpha, const double* x, int incx,
		                                 double* y, int incy)
  { dsmul (n, alpha, x, incx, y, incy); }
  static void  smul  (int n, int  alpha, const int*  x, int incx,
		                               int*  y, int incy)
  { ismul (n, alpha, x, incx, y, incy); }
  static void  smul  (int n, float  alpha,  const float* x, int incx,
		                                 float*  y, int incy)
  { ssmul (n, alpha, x, incx, y, incy); }

  
  static void vmul (int n, const double* x, int incx,
		           const double* y, int incy,
                                 double* z, int incz)
  { dvmul (n, x, incx, y, incy, z, incz); } 
  static void vmul (int n, const int*  x, int incx,
		           const int*  y, int incy,
                                 int*  z, int incz)
  { ivmul (n, x, incx, y, incy, z, incz); }
  static void vmul (int n, const float*  x, int incx,
		           const float*  y, int incy,
                                 float*  z, int incz)
  { svmul (n, x, incx, y, incy, z, incz); }


  static void sdiv (int n, double alpha, const double* x, int incx,
		                               double* y, int incy)
  { dsdiv (n, alpha, x, incx, y, incy); }
  static void sdiv (int n, int  alpha, const int*  x, int incx,
		                             int*  y, int incy)
  { isdiv (n, alpha, x, incx, y, incy); }
  static void  sdiv (int n, float alpha, const float*  x, int incx,
		                               float*  y, int incy)
  { ssdiv (n, alpha, x, incx, y, incy); }

  
  static void vrecp (int n, const double* x, int incx,
                                  double* y, int incy)
  { dvrecp (n, x, incx, y, incy); }
  static void vrecp (int n, const float*  x, int incx,
                                  float*  y, int incy)
  { svrecp (n, x, incx, y, incy); }
  

  static void vdiv (int n, const double* x, int incx,
		             const double* y, int incy,
		                   double* z, int incz)
  { dvdiv (n, x, incx, y, incy, z, incz); }
  static void vdiv (int n, const float*  x, int incx,
		             const float*  y, int incy,
		                   float*  z, int incz)
  { svdiv (n, x, incx, y, incy, z, incz); }

  
  static void zero (int n, double* x, int incx)
  { dzero (n, x, incx); }
  static void zero (int n, int*    x, int incx)
  { izero (n, x, incx); }
  static void zero (int n, float*  x, int incx)
  { szero (n, x, incx); }


  // -- OTHER MATHEMATICAL FUNCTIONS:
  
  static void vabs (int n, const double*  x, int incx,
                                 double*  y, int incy)
  { dvabs (n, x, incx, y, incy); }
  static void vabs (int n, const int* x, int incx,
                                 int* y, int incy)
  { ivabs (n, x, incx, y, incy); }
  static void vabs (int n, const float*   x, int incx,
                                 float*   y, int incy)
  { svabs (n, x, incx, y, incy); }


  static void vamax (int n, const double*  x, int incx,
		            const double*  y, int incy,
		                  double*  z, int incz)
  { dvamax (n, x, incx, y, incy, z, incz); }
  static void vamax (int n, const int* x, int incx,
		            const int* y, int incy,
                                  int* z, int incz)
  { ivamax (n, x, incx, y, incy, z, incz); }
  static void vamax (int n, const float*   x, int incx,
		            const float*   y, int incy,
                                  float*   z, int incz)
  { svamax (n, x, incx, y, incy, z, incz); }

  
  static void vexp (int n, const double* x, int incx,
                                 double* y, int incy)
  { dvexp (n, x, incx, y, incy); }
  static void vexp (int n, const float*  x, int incx,
		                 float*  y, int incy)
  { svexp (n, x, incx, y, incy); }

  
  static void vlg10 (int n, const double* x, int incx,
		                  double* y, int incy)
  { dvlg10 (n, x, incx, y, incy); }
  static void vlg10 (int n, const float*  x, int incx,
		                  float*  y, int incy)
  { svlg10 (n, x, incx, y, incy); }


  static void vlog (int n, const double* x, int incx,
                                 double* y, int incy)
  { dvlog (n, x, incx, y, incy); }
  static void vlog (int n, const float*  x, int incx,
                                 float*  y, int incy)
  { svlog (n, x, incx, y, incy); }


  static void vatan (int n, const double* x, int incx,
	                          double* y, int incy)
  { dvatan (n, x, incx, y, incy); }
  static void vatan (int n, const float*  x, int incx,
                                  float*  y, int incy)
  { svatan (n, x, incx, y, incy); } 


  static void vatn2 (int n, const double* x, int incx,
		            const double* y, int incy,
                                  double* z, int incz)
  { dvatn2 (n, x, incx, y, incy, z, incz); }
  static void vatn2 (int n, const float*  x, int incx,
		            const float*  y, int incy,
                                  float*  z, int incz)
  { svatn2 (n, x, incx, y, incy, z, incz); }


  static void vcos (int n, const double* x, int incx,
		                 double* y, int incy)
  { dvcos (n, x, incx, y, incy); }
  static void vcos (int n, const float*  x, int incx,
		                 float*  y, int incy)
  { svcos (n, x, incx, y, incy); }


  static void vsin (int n, const double* x, int incx,
		                 double* y, int incy)
  { dvsin (n, x, incx, y, incy); }
  static void vsin (int n, const float*  x, int incx,
		                 float*  y, int incy)
  { svsin (n, x, incx, y, incy); }


  static void vsqrt (int n, const double* x, int incx,
		                  double* y, int incy)
  { dvsqrt (n, x, incx, y, incy); }
  static void vsqrt (int n, const float*  x, int incx,
		                  float*  y, int incy)
  { svsqrt (n, x, incx, y, incy); }


  static void vtanh (int n, const double* x, int incx,
		                  double* y, int incy)
  { dvtanh (n, x, incx, y, incy); }
  static void vtanh (int n, const float*  x, int incx,
		                  float*  y, int incy)
  { svtanh (n, x, incx, y, incy); }


  static void ranInit (int flag)
  { raninit (flag); }
  static double dranu ()         
  { return dranu (); }
  static float  sranu ()         
  { return sranu (); }
  static double drang ()         
  { return drang (); }
  static float  srang ()         
  { return srang (); }
  static double normal (double mean, double sdev)
  { return dnormal (mean, sdev); }
  static float  normal (float  mean, float  sdev)
  { return snormal (mean, sdev); }
  static void  vrandom (int n, double* x, int incx)
  { dvrandom (n, x, incx); }
  static void  vrandom (int n, float*  x, int incx)
  { svrandom (n, x, incx); }
  static void  vgauss  (int n, double* x, int incx)
  { dvgauss (n, x, incx); }
  static void  vgauss (int n, float*   x, int incx)
  { svgauss (n, x, incx); }
  static void  vnormal (int n, double mean, double sdev,
			double* x, int incx)
  { dvnormal (n, mean, sdev, x, incx); }
  static void  vnormal (int n, float  mean, float  sdev,
			float*  x, int incx)
  { svnormal (n, mean, sdev, x, incx); }


  static void vhypot (int n, const double* x, int incx,
		             const double* y, int incy,
                                   double* z, int incz)
  { dvhypot (n, x, incx, y, incy, z, incz); }
  static void vhypot (int n, const float*  x, int incx,
		             const float*  y, int incy,
                                   float*  z, int incz)
  { svhypot (n, x, incx, y, incy, z, incz); }
  static void vmag (int n, const double* w, int incw,
		           const double* x, int incx,
		           const double* y, int incy,
		                 double* z, int incz)
  { dvmag (n, w, incw, x, incx, y, incy, z, incz); }
  static void vmag (int n, const float*  w, int incw,
		           const float*  x, int incx,
		           const float*  y, int incy,
		                 float*  z, int incz)
  { svmag (n, w, incw, x, incx, y, incy, z, incz); }

  static void vpow (int n, const double* x, int incx,
		    const double* y, int incy, double* z, int incz)
  { dvpow (n, x, incx, y, incy, z, incz); } 
  static void vpow (int n, const float*  x, int incx,
		    const float*  y, int incy, float*  z, int incz)
  { svpow (n, x, incx, y, incy, z, incz); } 


  // -- TRIAD OPERATIONS:
  
  static void svmvt (int n, double alpha, const double*  x, int incx,
		                          const double*  y, int incy,
		                                double*  z, int incz)
  { dsvmvt (n, alpha, x, incx, y, incy, z, incz); }
  static void svmvt (int n, float  alpha, const float*  x, int incx,
		                          const float*  y, int incy,
                                                float*  z, int incz)
  { ssvmvt (n, alpha, x, incx, y, incy, z, incz); }

 
  static void svpvt (int n, double alpha, const double* x, int incx,
		                          const double* y, int incy,
                                            double* z, int incz)
  { dsvpvt (n, alpha, x, incx, y, incy, z, incz); }
  static void svpvt (int n, float  alpha, const float*  x, int incx,
		                          const float*  y, int incy,
                                                float*  z, int incz)
  { ssvpvt (n, alpha, x, incx, y, incy, z, incz); }


  static void svtsp (int n, double alpha, double beta,
		     const double* x, int incx,
		           double* y, int incy)
  { dsvtsp (n, alpha, beta, x, incx, y, incy); }
  static void svtsp (int n, float  alpha, float  beta,
		     const float*  x, int incx,
		           float*  y, int incy)
  { ssvtsp (n, alpha, beta, x, incx, y, incy); }
 
 
  static void svtvm (int n, double alpha, const double* x, int incx,
		                          const double* y, int incy,
		                                double* z, int incz)
  { dsvtvm (n, alpha, x, incx, y, incy, z, incz); }
  static void svtvm (int n, float  alpha, const float*  x, int incx,
		                          const float*  y, int incy,
                                                float*  z, int incz)
  { ssvtvm (n, alpha, x, incx, y, incy, z, incz); }


  static void svtvp (int n, double alpha, const double* x, int incx,
		                          const double* y, int incy,
                                                double* z, int incz)
  { dsvtvp (n, alpha, x, incx, y, incy, z, incz); }
  static void svtvp (int n, float  alpha, const float*  x, int incx,
		                          const float*  y, int incy,
                                                float*  z, int incz)
  { ssvtvp (n, alpha, x, incx, y, incy, z, incz); }

  
  static void svvmt (int n, double alpha, const double* x, int incx,
		                          const double* y, int incy,
                                                double* z, int incz)
  { dsvvmt (n, alpha, x, incx, y, incy, z, incz); }
  static void svvmt (int n, float  alpha, const float*  x, int incx,
		                          const float*  y, int incy,
		                                float*  z, int incz)
  { ssvvmt (n, alpha, x, incx, y, incy, z, incz); }

 
  static void svvpt (int n, double alpha, const double* x, int incx,
		                          const double* y, int incy,
		                                double* z, int incz)
  { dsvvpt (n, alpha, x, incx, y, incy, z, incz); }
  static void svvpt (int n, float  alpha, const float*  x, int incx,
		                          const float*  y, int incy,
                                                float*  z, int incz)
  { ssvvpt (n, alpha, x, incx, y, incy, z, incz); }


  static void svvtm (int n, double alpha, const double* x, int incx,
		                          const double* y, int incy,
		                                double* z, int incz)
  { dsvvtm (n, alpha, x, incx, y, incy, z, incz); }
  static void svvtm (int n, float  alpha, const float*  x, int incx,
		                          const float*  y, int incy,
		                                float*  z, int incz)
  { ssvvtm (n, alpha, x, incx, y, incy, z, incz); }

 
  static void svvtp (int n, double alpha, const double* x, int incx,
		                          const double* y, int incy,
		                                double* z, int incz)
  { dsvvtp (n, alpha, x, incx, y, incy, z, incz); }
  static void svvtp (int n, float  alpha, const float*  x, int incx,
		                          const float*  y, int incy,
		                                float*  z, int incz)
  { ssvvtp (n, alpha, x, incx, y, incy, z, incz); }


  static void svvtt (int n, double alpha, const double* x, int incx,
		                          const double* y, int incy,
		                                double* z, int incz)
  { dsvvtt (n, alpha, x, incx, y, incy, z, incz); }
  static void svvtt (int n, float  alpha, const float*  x, int incx,
		                          const float*  y, int incy,
		                                float*  z, int incz)
  { ssvvtt (n, alpha, x, incx, y, incy, z, incz); }


  static void vvmvt (int n, const double* w, int incw,
		            const double* x, int incx,
		            const double* y, int incy,
		                  double* z, int incz)
  { dvvmvt (n, w, incw, x, incx, y, incy, z, incz); }
  static void vvmvt (int n, const float*  w, int incw,
		            const float*  x, int incx,
		            const float*  y, int incy,
		                  float*  z, int incz)
  { svvmvt (n, w, incw, x, incx, y, incy, z, incz); }

  
  static void vvpvt (int n, const double* w, int incw,
		            const double* x, int incx,
		            const double* y, int incy,
                                  double* z, int incz)
  { dvvpvt (n, w, incw, x, incx, y, incy, z, incz); }
  static void vvpvt (int n, const float*  w, int incw,
		            const float*  x, int incx,
		            const float*  y, int incy,
		                  float*  z, int incz)
  { svvpvt (n, w, incw, x, incx, y, incy, z, incz); }

 
  static void vvtvp (int n, const double* w, int incw,
		            const double* x, int incx,
		            const double* y, int incy,
		                  double* z, int incz)
  { dvvtvp (n, w, incw, x, incx, y, incy, z, incz); }
  static void vvtvp (int n, const float*  w, int incw,
		            const float*  x, int incx,
		            const float*  y, int incy,
		                  float*  z, int incz)
  { svvtvp (n, w, incw, x, incx, y, incy, z, incz); }

 
  static void vvtvm (int n, const double* w, int incw,
		            const double* x, int incx,
		            const double* y, int incy,
		                  double* z, int incz)
  { dvvtvm (n, w, incw, x, incx, y, incy, z, incz); }
  static void vvtvm (int n, const float*  w, int incw,
		            const float*  x, int incx,
		            const float*  y, int incy,
		                  float*  z, int incz)
  { svvtvm (n, w, incw, x, incx, y, incy, z, incz); }

  
  static void vvvtm (int n, const double* w, int incw,
		            const double* x, int incx,
		            const double* y, int incy,
		                    double* z, int incz)
  { dvvvtm (n, w, incw, x, incx, y, incy, z, incz); }
  static void vvvtm (int n, const float*  w, int incw,
		            const float*  x, int incx,
		            const float*  y, int incy,
		                  float*  z, int incz)
  { svvvtm (n, w, incw, x, incx, y, incy, z, incz); }
  

  static void vvvtt (int n, const double* w, int incw,
		            const double* x, int incx,
		            const double* y, int incy,
		                  double* z, int incz)
  { dvvvtt (n, w, incw, x, incx, y, incy, z, incz); }
  static void vvvtt (int n, const float*  w, int incw,
		            const float*  x, int incx,
		            const float*  y, int incy,
		                  float*  z, int incz)
  { svvvtt (n, w, incw, x, incx, y, incy, z, incz); }

 
  // -- RELATIONAL PRIMITIVE OPERATIONS:
  
  static void seq (int n, int alpha, const int* x, int incx,
                                               int* y, int incy)
  { iseq (n, alpha, x, incx, y, incy); }


  static void sge (int n, double alpha, const double* x, int incx,
		                                int*  y, int incy)
  { dsge (n, alpha, x, incx, y, incy); }
  static void sge (int n, int    alpha, const int*  x, int incx,
                                              int*  y, int incy)
  { isge (n, alpha, x, incx, y, incy); }
  static void sge (int n, float  alpha, const float*  x, int incx,
		                                int*  y, int incy)
  { ssge (n, alpha, x, incx, y, incy); }


  static void sle (int n, double alpha, const double* x, int incx,
		                                int*  y, int incy)
  { dsle (n, alpha, x, incx, y, incy); }
  static void sle (int n, int    alpha, const int*  x, int incx,
                                              int*  y, int incy)
  { isle (n, alpha, x, incx, y, incy); }
  static void sle (int n, float  alpha, const float*  x, int incx,
		                                int*  y, int incy)
  { ssle (n, alpha, x, incx, y, incy); }


  static void slt (int n, double alpha, const double* x, int incx,
		                                int*  y, int incy)
  { dslt (n, alpha, x, incx, y, incy); }
  static void slt (int n, int    alpha, const int*  x, int incx,
		                              int*  y, int incy)
  { islt (n, alpha, x, incx, y, incy); }
  static void slt (int n, float  alpha, const float*  x, int incx,
		                                int*  y, int incy)
  { sslt (n, alpha, x, incx, y, incy); }


  static void sne (int n, double alpha, const double* x, int incx,
		                                int*  y, int incy)
  { dsne (n, alpha, x, incx, y, incy); }
  static void sne (int n, int    alpha, const int*  x, int incx,
		                              int*  y, int incy)
  { isne (n, alpha, x, incx, y, incy); }
  static void sne (int n, float  alpha, const float*  x, int incx,
		                                int*  y, int incy)
  { ssne (n, alpha, x, incx, y, incy); } 
 

  // -- REDUCTION FUNCTIONS:
  
  static double sum (int n, const double* x, int incx)
  { return dsum (n, x, incx); }
  static int  sum (int n, const int*  x, int incx)
  { return isum (n, x, incx); }
  static float  sum (int n, const float*  x, int incx)
  { return ssum (n, x, incx); }


  static int imax (int n, const double* x, int incx)
  { return idmax (n, x, incx); }
  static int imax (int n, const int*  x, int incx)
  { return iimax (n, x, incx); }
  static int imax (int n, const float*  x, int incx)
  { return ismax (n, x, incx); }


  static int imin (int n, const double* x, int incx)
  { return idmin (n, x, incx); }
  static int imin (int n, const int*  x, int incx)
  { return iimin (n, x, incx); }
  static int imin (int n, const float*  x, int incx)
  { return ismin (n, x, incx); }


  static int count (int n, const int *x, int incx)
  { return icount (n, x, incx); }
  static int match (int n, const int alpha, const int *x, int incx)
  { return imatch (n, alpha, x, incx); }
  static int first (int n, const int *x, int incx)
  { return ifirst (n, x, incx); }
  static int any   (int n, const int *x, int incx)
  { return lany  (n, x, incx); }


  static int same (int n, const int*    x, int incx,
                          const int*    y, int incy)
  { return lisame (n, x, incx, y, incy); }
  static int same (int n, const double* x, int incx,
                          const double* y, int incy)
  { return ldsame (n, x, incx, y, incy); }
  static int same (int n, const float*  x, int incx,
                          const float*  y, int incy)
  { return lssame (n, x, incx, y, incy); }

    
  // -- CONVERSION PRIMITIVES:
  
  static void vdble (int n, const float*  x, int incx,
		                  double* y, int incy)
  { vdble (n, x, incx, y, incy); }
  static void vsngl (int n, const double* x, int incx,
		                  float*  y, int incy)
  { vsngl (n, x, incx, y, incy); }


  static void vfloa (int n, const int*  x, int incx,
                                double* y, int incy)
  { dvfloa (n, x, incx, y, incy); }
  static void vfloa (int n, const int* x, int incx,
                                    float* y, int incy)
  { svfloa (n, x, incx, y, incy); }


  static int testFormat ()
  { return iformat (); }
  static void describeFormat (char* s)
  { format (s); }
  static void brev (int n, const double* x, int incx,
		                 double* y, int incy)
  { dbrev (n, x, incx, y, incy); }
  static void brev (int n, const int*    x, int incx,
                                 int*    y, int incy)
  { ibrev (n, x, incx, y, incy); }
  static void brev (int n, const float*  x, int incx,
                                 float*  y, int incy)
  { sbrev (n, x, incx, y, incy); }

   // -- MISCELLANEOUS FUNCTIONS:
 
  static void scatr (int n, const double* x, const int* y, double* z)
  { dscatr (n, x, y, z); }
  static void scatr (int n, const int*    x, const int* y, int*    z)
  { iscatr (n, x, y, z); }
  static void scatr (int n, const float*  x, const int* y, float*  z)
  { sscatr (n, x, y, z); }


  static void gathr (int n, const double* x, const int* y, double* z)
  { dgathr (n, x, y, z); }
  static void gathr (int n, const int*    x, const int* y, int*    z)
  { igathr (n, x, y, z); }
  static void gathr (int n, const float*  x, const int* y, float*  z)
  { sgathr (n, x, y, z); }


  static void gathr_scatr (int n, const double* w, const int*    x,
			          const int*    y,       double* z)
  { dgathr_scatr (n, w, x, y, z); }
  static void gathr_scatr (int n, const int*    w, const int*    x,
			          const int*    y,       int*    z)
  { igathr_scatr (n, w, x, y, z); }
  static void gathr_scatr (int n, const float* w, const int*     x,
 			          const int* y,         float*   z)
  { sgathr_scatr (n, w, x, y, z); }

  
  static void scatr_sum (int n, const double* x, const int *y, double* z)
  { dscatr_sum (n, x, y, z); }
  static void scatr_sum (int n, const int*    x, const int *y, int*    z)
  { iscatr_sum (n, x, y, z); }
  static void scatr_sum (int n, const float*  x, const int *y, float*  z)
  { sscatr_sum (n, x, y, z); }


  static void gathr_sum (int n, const double* x, const int *y, double* z)
  { dgathr_sum (n, x, y, z); }
  static void gathr_sum (int n, const int*    x, const int *y, int*    z)
  { igathr_sum (n, x, y, z); }
  static void gathr_sum (int n, const float*  x, const int *y, float*  z)
  { sgathr_sum (n, x, y, z); }


  static void gathr_scatr_sum (int n, const double* w, const int*    x,
			              const int*    y,       double* z)
  { dgathr_scatr_sum (n, w, x, y, z); }
  static void gathr_scatr_sum (int n, const int*    w, const int*    x,
			              const int*    y,       int*    z)
  { igathr_scatr_sum (n, w, x, y, z); }
  static void gathr_scatr_sum (int n, const float*  w, const int*    x,
			              const int*    y,       float*  z)
  { sgathr_scatr_sum (n, w, x, y, z); }


  static void ramp (int n, double  alpha, double beta, double* x, int incx)
  { dramp (n, alpha, beta, x, incx); }
  static void ramp (int n, int     alpha, int    beta, int*    x, int incx)
  { iramp (n, alpha, beta, x, incx); }
  static void ramp (int n, float   alpha, float  beta, float*  x, int incx)
  { sramp (n, alpha, beta, x, incx); }

  static void clip (int n, const double alpha, const double beta,
		    const double* x, int incx,
 		          double* y, int incy)
  { dclip (n, alpha, beta, x, incx, y, incy); }
  static void clip (int n, const  int   alpha, const int    beta,
		    const int* x, int incx,
		          int* y, int incy)
  { iclip (n, alpha, beta, x, incx, y, incy); }
  static void clip (int n, const  float alpha, const float  beta,
		    const float* x,  int incx,
		          float* y, int incy)
  { sclip (n, alpha, beta, x, incx, y, incy); }

  static void clipup (int n, const double alpha,
		      const double* x, int incx,
		            double* y, int incy)
  { dclipup (n, alpha, x, incx, y, incy); }
  static void clipup (int n, const int alpha,
		      const int*   x, int incx,
		            int*   y, int incy)
  { iclipup (n, alpha, x, incx, y, incy); }
  static void clipup (int n, const float alpha,
		      const float* x, int incx,
		            float* y, int incy)
  { sclipup (n, alpha, x, incx, y, incy); }

  static void clipdn (int n, const double alpha,
		      const double* x, int incx,
		            double* y, int incy)
  { dclipdn (n, alpha, x, incx, y, incy); }
  static void clipdn (int n, const int    alpha,
		      const int*    x, int incx,
		            int*    y, int incy)
  { iclipdn (n, alpha, x, incx, y, incy); }
  static void clipdn (int n, const float   alpha,
		      const float*  x,  int incx,
		            float*   y, int incy)
  { sclipdn (n, alpha, x, incx, y, incy); }

  static void iclip (int n, const double alpha,  const double beta,
		     const double* x, int incx,
		           double* y, int incy)
  { diclip (n, alpha, beta, x, incx, y, incy); }
  static void iclip (int n, const  int alpha,    const int    beta,
		     const int* x, int incx,
		           int* y, int incy)
  { iiclip (n, alpha, beta, x, incx, y, incy); }
  static void iclip (int n, const float alpha,   const float  beta,
		     const float* x,  int incx,
		           float* y,  int incy)
  { siclip (n, alpha, beta, x, incx, y, incy); }

  static void cndst (int n, const double* x, int incx,
		            const int*    y, int incy,
		                  double* z, int incz)
  { dcndst (n, x, incx, y, incy, z, incz); }
  static void cndst (int n, const int*    x, int incx,
		            const int*    y, int incy,
		                  int*    z, int incz)
  { icndst (n, x, incx, y, incy, z, incz); }
  static void cndst (int n, const float*  x, int incx,
		            const int*    y, int incy,
		                 float*   z, int incz)
  { scndst (n, x, incx, y, incy, z, incz); }
 

  static void mask (int n, const double* w, int incw,
		           const double* x, int incx,
		           const int*    y, int incy,
		                 double* z, int incz)
  { dmask (n, w, incw, x, incx, y, incy, z, incz); }
  static void mask (int n, const int*    w, int incw,
		           const int*    x, int incx,
		           const int*    y, int incy,
		                 int*    z, int incz)
  { imask (n, w, incw, x, incx, y, incy, z, incz); }
  static void mask (int n, const float*  w, int incw,
		           const float*  x, int incx,
		           const int*    y, int incy, 
		                 float*  z, int incz)
  { smask (n, w, incw, x, incx, y, incy, z, incz); }

  
  static void vpoly (int n, const double* x, int incx, int m,
		            const double* c, int incc, 
		                  double* y, int incy)
  { dvpoly (n, x, incx, m, c, incc, y, incy); }
  static void vpoly (int n, const float*  x, int incx, int m,
		            const float*  c, int incc, 
		                  float*  y, int incy)
  { svpoly (n, x, incx, m, c, incc, y, incy); }  


  static double poly (int n, double x, const double* xp, const double* yp)
  { return dpoly (n, x, xp, yp); }
  static float  poly (int n, float  x, const float*  xp, const float*  yp)
  { return spoly (n, x, xp, yp); }


  static void polint (const double* xa, const double* ya, int n,
		            double  x,        double* y,  double* dy)
  { dpolint (xa, ya, n, x, y, dy); }
  static void polint (const float*  xa, const float*  ya, int n,
		            float   x,        float*  y, float*  dy)
  { spolint (xa, ya, n, x, y, dy); }
  

  static void spline (int n, double yp1, double ypn,
		      const double* x,  const double* y, double* y2)
  { dspline (n, yp1, ypn, x,  y, y2); }
  static void spline (int n, float yp1, float ypn,
		      const float*  x, const float*  y,  float*  y2)
  { sspline (n, yp1, ypn, x,  y, y2); }


  static double splint (int n, double x,
			const  double* xa, const double* ya, const double* y2a)
  { return dsplint (n, x, xa, ya, y2a); }
  static float  splint (int n, float  x,
			const  float*  xa, const float*  ya, const float*  y2a)
  { return ssplint (n, x, xa, ya, y2a); }


  static double splquad (const double* xa, const double* ya, const double* y2a,
			 const int   n,  const double xmn, const double  xmx)
  { return dsplquad (xa, ya, y2a, n, xmn, xmx); }
  static float  splquad (const float*  xa, const float*  ya, const float*  y2a,
			 const int   n,  const float  xmn, const float   xmx)
  { return ssplquad (xa, ya, y2a, n, xmn, xmx); }


  static void vvtvvtp (int n, const double* v, int incv,
          	              const double* w, int incw,
	                      const double* x, int incx,
	                      const double* y, int incy,
	                            double* z, int incz)
  { dvvtvvtp (n, v, incv, w, incw, x, incx, y, incy, z, incz); }
  static void vvtvvtp (int n, const float*  v, int incv,
	                      const float*  w, int incw,
	                      const float*  x, int incx,
	                      const float*  y, int incy,
		                    float*  z, int incz)
  { svvtvvtp (n, v, incv, w, incw, x, incx, y, incy, z, incz); }

  static void vvtvvtm (int n, const double* v, int incv,
          	              const double* w, int incw,
	                      const double* x, int incx,
	                      const double* y, int incy,
	                            double* z, int incz)
  { dvvtvvtm (n, v, incv, w, incw, x, incx, y, incy, z, incz); }
  static void vvtvvtm (int n, const float*  v, int incv,
	                      const float*  w, int incw,
	                      const float*  x, int incx,
	                      const float*  y, int incy,
		                    float*  z, int incz)
  { svvtvvtm (n, v, incv, w, incw, x, incx, y, incy, z, incz); }


  static void svvttvp (int n, const double  alpha,
          	              const double* w, int incw,
	                      const double* x, int incx,
	                      const double* y, int incy,
	                            double* z, int incz)
  { dsvvttvp (n, alpha, w, incw, x, incx, y, incy, z, incz); }
  static void svvttvp (int n, const float   alpha,
		              const float*  w, int incw,
		              const float*  x, int incx,
		              const float*  y, int incy,
		                    float*  z, int incz)
  { ssvvttvp (n, alpha, w, incw, x, incx, y, incy, z, incz); }

};

#endif
