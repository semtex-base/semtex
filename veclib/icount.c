/*****************************************************************************
 * icount:  number of non-zero values in x.
 * imatch:  number of occurences of integer value in x.
 *****************************************************************************/

#include <cfemdef.h>


int icount (int n, const int* x, int incx)
{
  int i, sum = 0;

  x += (incx<0) ? (-n+1)*incx : 0;

  for (i = 0; i < n; i++ ) sum += (x[i*incx]) ? 1 : 0;

  return sum;
}


int imatch (int n, const int alpha, const int* x, int incx)
{
  int i, sum = 0;

  x += (incx<0) ? (-n+1)*incx : 0;

  for (i = 0; i < n; i++ ) sum += (x[i*incx] == alpha) ? 1 : 0;

  return sum;
}
