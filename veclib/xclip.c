/*****************************************************************************
 * xclip: clip to interval [alpha,beta]: y[i] = MIN(MAX(x[i],alpha),beta).
 *
 * xclipup: clip on lower limit alpha:   y[i] = MAX(x[i],alpha).
 * xclipdn: clip on upper limit alpha:   y[i] = MIN(x[i],alpha).
 *****************************************************************************/

#include <cfemdef.h>

#if defined(__uxp__)
#pragma global novrec
#pragma global noalias
#endif

#define MIN(a, b)  ((a) < (b) ? (a) : (b))
#define MAX(a, b)  ((a) > (b) ? (a) : (b))

void dclip (int n,  const double alpha, const double beta,
	    const double* x, int incx,
	          double* y, int incy)
{
  int i;

  x += (incx<0) ? (-n+1)*incx : 0;
  y += (incy<0) ? (-n+1)*incy : 0;

  for (i = 0; i < n; i++) y[i*incy] = MIN(MAX(x[i*incx],alpha),beta);
}


void iclip (int n,  const int alpha, const int beta,
	    const int* x, int incx,
	          int* y, int incy)
{
  int i;

  x += (incx<0) ? (-n+1)*incx : 0;
  y += (incy<0) ? (-n+1)*incy : 0;

  for (i = 0; i < n; i++) y[i*incy] = MIN(MAX(x[i*incx],alpha),beta);
}


void sclip (int n,  const float alpha, const float beta,
	    const float* x, int incx,
	          float* y, int incy)
{
  int i;

  x += (incx<0) ? (-n+1)*incx : 0;
  y += (incy<0) ? (-n+1)*incy : 0;

  for (i = 0; i < n; i++) y[i*incy] = MIN(MAX(x[i*incx],alpha),beta);
}


void dclipup (int n,  const double alpha,
	      const double* x, int incx,
	            double* y, int incy)
{
  int i;

  x += (incx<0) ? (-n+1)*incx : 0;
  y += (incy<0) ? (-n+1)*incy : 0;

  for (i = 0; i < n; i++) y[i*incy] = MAX(x[i*incx], alpha);
}


void iclipup (int n,  const int alpha,
	      const int* x, int incx,
	            int* y, int incy)
{
   int i;

  x += (incx<0) ? (-n+1)*incx : 0;
  y += (incy<0) ? (-n+1)*incy : 0;

  for (i = 0; i < n; i++) y[i*incy] = MAX(x[i*incx], alpha);
}


void sclipup (int n,  const float alpha,
	      const float* x, int incx,
	            float* y, int incy)
{
   int i;

  x += (incx<0) ? (-n+1)*incx : 0;
  y += (incy<0) ? (-n+1)*incy : 0;

  for (i = 0; i < n; i++) y[i*incy] = MAX(x[i*incx], alpha);
}


void dclipdn (int n,  const double alpha,
	      const double* x, int incx,
	            double* y, int incy)
{
   int i;

  x += (incx<0) ? (-n+1)*incx : 0;
  y += (incy<0) ? (-n+1)*incy : 0;

  for (i = 0; i < n; i++) y[i*incy] = MIN(x[i*incx], alpha);
}


void iclipdn (int n,  const int alpha,
	      const int* x, int incx,
	            int* y, int incy)
{
   int i;

  x += (incx<0) ? (-n+1)*incx : 0;
  y += (incy<0) ? (-n+1)*incy : 0;

  for (i = 0; i < n; i++) y[i*incy] = MIN(x[i*incx], alpha);
}


void sclipdn (int n,  const float alpha,
	      const float* x, int incx,
	            float* y, int incy)
{
   int i;

  x += (incx<0) ? (-n+1)*incx : 0;
  y += (incy<0) ? (-n+1)*incy : 0;

  for (i = 0; i < n; i++) y[i*incy] = MIN(x[i*incx], alpha);
}
