/*****************************************************************************
 * xsle:  y[i] = alpha <= x[i].
 *****************************************************************************/

#include <cfemdef.h>

#if defined(__uxp__)
#pragma global novrec
#pragma global noalias
#endif

void dsle (int n, double alpha,
	   const double* x, int incx,
	         double* y, int incy)
{
  int i;

  x += (incx<0) ? (-n+1)*incx : 0;
  y += (incy<0) ? (-n+1)*incy : 0;

  for (i = 0; i < n; i++) y[i*incy] = (alpha <= x[i*incx]) ? 1 : 0;
}


void isle (int n, int alpha,
	   const int* x, int incx,
	         int* y, int incy)
{
  int i;

  x += (incx<0) ? (-n+1)*incx : 0;
  y += (incy<0) ? (-n+1)*incy : 0;

  for (i = 0; i < n; i++) y[i*incy] = (alpha <= x[i*incx]) ? 1 : 0;
}


void ssle (int n, float alpha,
	   const float* x, int incx,
	         float* y, int incy)
{
  int i;

  x += (incx<0) ? (-n+1)*incx : 0;
  y += (incy<0) ? (-n+1)*incy : 0;

  for (i = 0; i < n; i++) y[i*incy] = (alpha <= x[i*incx]) ? 1 : 0;
}
