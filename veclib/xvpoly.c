/*****************************************************************************
 * xvpoly: Hoerner polynomial evaluation of a vector.
 *
 * m is the order of the polynomial and its coefficients are stored in c in
 * descending order: c[0] is the constant term.
 *****************************************************************************/

#include <cfemdef.h>

#if defined(__uxp__)
#pragma global novrec
#pragma global noalias
#endif

void dvpoly (int n,
	     const double* x, int incx, int m,
	     const double* c, int incc, 
	           double* y, int incy)
{
  int          i, j;
  double       sum, xval;
  const double *csave, *cp;

  csave = c;
  x += (incx<0) ? (-n+1)*incx : 0;
  y += (incy<0) ? (-n+1)*incx : 0;

  for (i = 0; i < n; i++) {
    c    = (incc<0) ? csave+(-n+1)*incc : csave;
    cp   = c + incc;
    sum  = c[0];
    xval = x[i*incx];
    for (j = 0; j < m; j++) sum = sum * xval + cp[j*incc];
    y[i*incy] = sum;
  }
}


void svpoly (int n,
	     const float* x, int incx, int m,
	     const float* c, int incc, 
	           float* y, int incy)
{
  int         i, j;
  float       sum, xval;
  const float *csave, *cp;

  csave = c;
  x += (incx<0) ? (-n+1)*incx : 0;
  y += (incy<0) ? (-n+1)*incx : 0;

  for (i = 0; i < n; i++) {
    c    = (incc<0) ? csave+(-n+1)*incc : csave;
    cp   = c + incc;
    sum  = c[0];
    xval = x[i*incx];
    for (j = 0; j < m; j++) sum = sum * xval + cp[j*incc];
    y[i*incy] = sum;
  }
}
