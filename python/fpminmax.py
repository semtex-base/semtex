#!/usr/bin/env python
"""
print min max per field, optionally also per plane
"""
#
# by Thomas Albrecht and Hugh M Blackburn

import numpy as np
import argparse
import fieldfile

def fourier_name(z):
    if (z % 2) == 0:
        return "%3i Re" % (z/2)
    else:
        return "%3i Im" % (z/2)

if __name__ == "__main__":
    """
    """
    parser = argparse.ArgumentParser \
        (description="fpminfmax.py -- print min max per field per plane")
    parser.add_argument("-e", "--eps", type=float, \
                        help="print 0 if value below EPS", default=1e-15)
    parser.add_argument("-c", "--complete", action="store_true", \
                        help="plane-by-plane min/max")
    parser.add_argument('infile', metavar='file', type=str, nargs='+',
                   help='the file')
    args = parser.parse_args()

    ff = fieldfile.Fieldfile(args.infile[0], "r")
    data = ff.read()
    elmt_wise = data.reshape((ff.nflds, ff.nz, ff.nel, ff.ns, ff.nr))
    
    if np.isnan(data).any():
        print ("NaN")
    if np.isinf(data).any():
        print ("Inf")

    if args.complete:
        
        print ("#  z     FM ")
        for field in ff.fields:
            print(("%9s_min %8s_max " % (field, field)))
        print ()
        for z in range(ff.nz):
            print(("%4i %s" % (z, fourier_name(z))))
            for field in range(ff.nflds):
                fmin = elmt_wise[field,z].flatten().min()
                fmax = elmt_wise[field,z].flatten().max()
                if abs(fmin) < args.eps: fmin = 0.
                if abs(fmax) < args.eps: fmax = 0.
                print(("  % 12g % 12g" % (fmin, fmax)))
            print ()
        
    else:
        print ("           MIN          MAX")
        for i in range(ff.nflds):
            print (("%s %12g %12g" % \
                    (ff.fields[i], data[i].min(), data[i].max())))

