python README
=============

Synopsis
--------

Various semtex-related and other utilities written in python.  We
don't yet have a good set of python binders for semtex internal
routines; the utilities here are relatively simple-minded, but useful
nonetheless.

Fieldfile-class based utilities
-------------------------------

[**fieldfile.py**](./fieldfile.py) contains classes that deal with I/O
of semtex field files and is used by various other utilities.  Among
other things it provides the ability to step into binary data on the
basis of field name, element, and z-plane.  Here is a list of example 
utilities that depend on *fieldfile.py*:

 * [**addlinear.py**](./addlinear.py) A simple script that adds one
   variable to another.
    
 * [**c2w.py**](./c2w.py) A very simple script that just renames one
   variable.
   
 * [**compose.py**](./compose.py) Choose selected variables from two
   field files, write new file.
    
 * [**coristress.py**](./coristress.py) Post-process Reynolds stress
   files to produce body forces for average flow. A custom utility
   used in work for Albrecht et al. (2021), JFM 910, A51.
    
 * [**ff.py**](./ff.py) Utilities for dealing with field files:
   rename, delete, scale fields.
    
 * [**fpminmax.py**](./fpminmax.py) Display minimum and maximum values
   of fields contained in a field file, with option to show these on a
   plane-by-plane basis.
    
 * [**zero_plane.py**](./zero_plane.py) Set nominated data z-planes of
   data to zero.


Other utilities
---------------

[**gmesh2sem.py**](./gmsh2sem.py) From a *gmsh* (and all-quad) mesh
file, produce a semtex session file skeleton.  (Visit the
[**gmsh**](http://www.geuz.org/gmsh/) web site for more information on
the *gmsh* open-source mesh generator.)

[**findaroot.py**](./findaroot.py) contains various routines that can
be used to find a zero of a scalar function of one variable (example
usage included).

[**wavy.py**](./wavy.py) example that uses semtex utilities rectmesh and
mapmesh to generate a semtex session file with a (sinusoidal) wavy
boundary on one bondary, with internal vertices and element sides
mapped using the same schema.

Notes
-----

For dealing with semtex field files you could use the classes in
*fieldfile.py*, which permits to read/write the ASCII header and
binary data in field files.  After the data has been read in, one
could manipulate it in simple ways such as finding velocity magnitude,
or to access the data elementwise.  See e.g. the *ff.py* utility,
which does some simple manipulations of this kind.  If you need to
deal with fieldfile data in ways that do not need element shape
functions, using *fieldfile.py* could be simpler than adapting C
programs in the utility directory, or writing shell scripts. See
e.g. *addlinear.py*, *c2w.py*, *compose.py* *coristress.py*,
*fpminmax.py*, *zero_plane.py*.  (If you *really do* need to take
gradients etc., you will likely start adapting C++ codes from the
utility directory, if you can't already find there something to do
what you want.)  *Fieldfile.py* was created by Thomas Albrecht.

(Sometimes, however, a "quick and dirty" shell script that uses other
utilites such as semtex's *convert*, along with standard Unix
utilities such as head, sed and awk, may do the manipulations you
require.  Check out *utility/addquick* for an example.)

For dealing with quad meshes produced using gmsh, see *gmsh2sem.py*,
which produces a boilerplate semtex session file that you could use as
a starting point.

Sometimes you need a wrapper script for finding a zero of a function
(perhaps a zero eigenvalue from analysis using dog; you could use
other python code to run dog, modify your session file, extract
eigenvalues, ... or, just a simple scalar function and you don't have
a matlab licence).  Various root finders are collected in
*findaroot.py*.
  
To reshape logically rectangular meshes such as those produced by the
*rectmesh* utility into something more general, we could use the
semtex *mapmesh* utility to alter session files.  But you might also
need to change curved element boundaries.  Obviously you'd want to
automate such a task: see *wavy.py* for an example.

------------------------------------------------------------------------------
