///////////////////////////////////////////////////////////////////////////////
// This version of analysis.cpp is specialized so that it computes and
// prints out forces exerted on "wall" boundary group in non-Newtonian
// flows.  The viscosity is a function of space and time.
//
// Copyright (c) 1999+, Hugh M Blackburn
///////////////////////////////////////////////////////////////////////////////

#include <gnewt.h>

static bool WALLED;

gnewtAnalyser::gnewtAnalyser (Domain* D   ,
			      BCmgr*  B   ,
			      FEML*   feml) : 
// ---------------------------------------------------------------------------
// Extensions to Analyser class.
// ---------------------------------------------------------------------------
  Analyser (D, feml)
{
  const char routine[] = "gnewtAnalyser::gnewtAnalyser";
  char       str[StrMax];

  WALLED = B -> nWall() > 0;	// -- Only true if we have a "wall" group.

  if (WALLED) {
    ROOTONLY {

      // -- Open state-variable file.
      
      flx_strm.open (strcat (strcpy (str, _src -> name), ".flx"));
      if (!flx_strm) Veclib::alert (routine, "can't open flux file",  ERROR);

      flx_strm << "# gnewt wall state information file"      << endl;
      flx_strm << "# Step Time [Fpre Fvis Ftot]-axis" << endl;
      flx_strm << "# -------------------------------" << endl;
    }
  }
}


void gnewtAnalyser::analyse (AuxField** worka,
			     AuxField** workb,
			     AuxField*  NNV  )
// ---------------------------------------------------------------------------
// Step-by-step processing.
//
// Computation of viscous tractive forces differs from dnsanalyais
// because viscosity is a variable rather than a constant.
// ---------------------------------------------------------------------------
{
  const char routine[] = "gnewtAnalyser::analyse";
  const int  NVEL = _src -> nVelCmpt();
  const int  NDIM = Geometry::nDim();
  int        i;

  const bool periodic = !(_src->step %  Femlib::ivalue("IO_HIS")) ||
                        !(_src->step %  Femlib::ivalue("IO_FLD"));
  const bool final    =   _src->step == Femlib::ivalue("N_STEP");
  const bool state    = periodic || final;

  // -- First make the viscosity the true computed value (add nu_ref back).
  //    Done every step because it might be used by standard analysis code.

  ROOTONLY NNV -> addToPlane (0, Femlib::value ("KINVIS"));

  if (WALLED) {
    if (state) {

      // -- We are going to work out loads on walls:

      Vector pfor, vfor, tfor;
      char   s[StrMax];

      // -- Push NNV into physical space in preparation for multiplication.
  
      NNV -> transform (INVERSE);

      // -- First do the 2-D components:

      *(worka[0]) = *(_src -> u[0]);
      *(workb[0]) = *(_src -> u[1]);
      *(worka[1]) = *(worka[0]);
      *(workb[1]) = *(workb[0]);

      for (i = 0; i < 2; i++) {
	(worka[i] -> gradient (i) . transform (INVERSE)) *= *NNV;
	worka[i] -> transform (FORWARD);
	
	(workb[i] -> gradient (i) . transform (INVERSE)) *= *NNV;
	workb[i] -> transform (FORWARD);
      }
    
      ROOTONLY {
	tfor.z = pfor.z = vfor.z = 0.0;
	pfor   = Field::normTraction (_src -> u[NVEL]);
	vfor   = Field::xytangTractionNN (_src -> u[0], worka, workb);
	tfor.x = pfor.x + vfor.x;
	tfor.y = pfor.y + vfor.y;
	tfor.z = pfor.z;
      }

      // -- Now do the Z-component if needed. 

      if (NVEL == 3) {
	*(worka[0]) = *(_src -> u[2]);
	*(worka[1]) = *(worka[0]);
	*(workb[0]) = *(_src -> u[2]);

	// -- These are to hold viscosity * (x, y) gradients of w:

	for (i = 0; i < 2; i++) {
	  (worka[i] -> gradient (i) . transform (INVERSE)) *= *NNV;
	  worka[i] -> transform (FORWARD);
	}

	// -- This is to hold viscosity * w:
	
	(workb[0] -> transform (INVERSE) *= *NNV) . transform (FORWARD);
    
	ROOTONLY {
	  vfor.z  = (Field::ztangTractionNN (_src -> u[2], worka, workb[0])).z;
	  tfor.z += vfor.z;
	}
      }
  
      ROOTONLY {
	sprintf (s,
		 "%6d %#10.6g "
		 "%#10.6g %#10.6g %#10.6g "
		 "%#10.6g %#10.6g %#10.6g "
		 "%#10.6g %#10.6g %#10.6g",
		 _src -> step, _src -> time,
		 pfor.x,   vfor.x,   tfor.x,
		 pfor.y,   vfor.y,   tfor.y,
		 pfor.z,   vfor.z,   tfor.z);
    
	flx_strm << s << endl;
      }

      // -- Push NNV back into Fourier space.
      
      NNV -> transform (FORWARD);
    }
  }

  // -- This is done after all the wall computations in gnewt because for
  //    AVERAGE = 3 the pressure (needed for wall loads) will get destroyed.
  //    However, any HISTORY POINT DATA FOR PRESSURE would then also be wrong.

  Analyser::analyse (worka, workb);
}
