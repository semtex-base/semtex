Synopsis
--------

Gnewt is an adaptation of semtex "dns" that incorporates spatially
varying generalized Newtonian viscosity.  This is handled within the
framework of the DNS solver by splitting the viscous stress into two
parts: one which can be dealt with implicitly as diffusion of momentum
with a spatially-constant kinematic viscosity REFVIS, the other dealt
with explicitly as the divergence of a stress.  This way the velocity
BCs are still set correctly in the viscous substep.  The viscosity
splitting technique was originally discussed in [1] in the context of
large eddy simulations with eddy viscosity.  For further discussion
and simulation results, see [2--5].  The only formulation of nonlinear
terms implemented in gnewt to date is skew-symmetric.

The standard semtex viscosity measure KINVIS has no direct
significance within gnewt --- except when NOMODEL is defined for
compilation (see discussion about testing below).  Instead, the user
needs to define the token REFVIS (which when the rheology is close to
Newtonian is typically a small multiple (>=1) of the equivalent
KINVIS), or more generally a representative average value within the
domain.  For example, if one can work out the wall viscosity using the
rheology model and an imposed body force/pressure drop, one would
generally set REVIS to be greater than this value if the rheology is
shear-thinning, and vice-versa if shear-thickening.  The stability of
timestepping is affected by the size of REFVIS (in addition to
standard CFL-type timestep limits); the user may need to vary REFVIS
in order to obtain a non-divergent simulation - but once it is in the
right ballpark, the outcomes are rather insensitive to further
variation.

However, to allow the standard token KINVIS be used as normal within
base-level semtex timestepping routines, in fact the values of the
session file tokens REFVIS and KINVIS are swapped around near the
commencement of execution.  Thus the input token REFVIS becomes the
viscosity in the implicit part-treatment of viscous terms, without
modification of that part of the code, while the token KINVIS, if set,
is ignored.

For testing purposes, one can define NOMODEL during compilation (see
e.g. within gnewt/CmakeLists.txt file: set NOMODEL ON). This overrides
any declared non-Newtonian viscosity models, will set the reference
kinematic viscosity REFVIS to twice the KINVIS declared in the session
fil) and formulate the viscous stresses as those supplied by the
product of the strain rate tensor with a spatially-constant NEGATIVE
viscosity -KINVIS: the results should be almost exactly the same as
obtained using dns with kinematic viscosity KINVIS.  In this case, any
rheology tokens, including REFVIS, are ignored (and the usage prompt
will announce that the executable was compiled with NOMODEL defined).
The outcomes should be very close to those obtained with the "dns"
code.  And in fact, because dns uses KINVIS but not REFVIS, you should
be able to use the same session file to run and compare outcomes from
dns and gnewt.  The benefit is that gnewt computes the divergence of
viscous stresses in the same way as it does for non-Newtonian
rheologies (albeit with constant viscosity), thus exercising that part
of the code in addition to the standard Newtonian nonlinear terms
computed by dns.

Caveat: for rheologies that permit the viscosity to become
infinite for zero strain rate (e.g. power law model), the rate of
convergence may be significantly less than would be obtained with
Newtonian rheologies if the strain rate falls to zero in a significant
region - this can be compensated for to some extent by using smaller
element sizes.

At present, gnewt does not handle scalar transport.

Usage
-----
	>$ gnewt -h  
	Usage: gnewt [options] session-file  
	  [options]:  
	   -h       ... print this message  
  	   -i       ... use iterative solver for viscous step  
	   -v[v...] ... increase verbosity level  
	   -chk     ... disable checkpointing field dumps  

Viscosity models
----------------

The following generalized Newtonian viscosity models for kinematic
viscosity $\nu$ are implemented, where $\dot{\gamma}$ is the magnitude
of the rate of strain tensor:

Power Law: $\nu=K\dot{\gamma}^{n-1}$.
Need to define tokens PowerLaw=1, PL_K, PL_N (optional: PL_ZERO)

Herschel--Bulkley: $\nu= \tau_y/\dot{\gamma}+K\dot{\gamma}^{n-1}$.
Need to define tokens HB=1, YIELD_STRESS, HB_K, HB_N, (optional: HB_ZERO)

Carreau--Yasuda: $\nu = \nu_\infty + (\nu_0-\nu_\infty)[1 +
(\lambda\dot{\gamma})^a)]^{(n-1)/a}$.
Need to define tokens CAR_YAS=1, CY_LAMBDA,
CY_A, CY_N, VISC_ZERO, VISC_INF (While terms used in some publications
may vary in detail they should all be functionally equivalent.)

Cross: $\nu=\nu_\infty +
(\nu_0-\nu_\infty)[1+(\lambda\dot{\gamma})^n]^{-1}$. Need to define
tokens CROSS=1, CROSS_LAMBDA, CROSS_N, VISC_ZERO, VISC_INF

See file viscosity.cpp for implementation details.  For the power law
and Herschel-Bulkley models, optional lower shear rate limit values
(PL_ZERO, HB_ZERO) may be defined in order to avoid potential
divide-by-zero problems where the shear rate approaches zero.  If
sufficiently small, the value chosen does not greatly affect outcomes.

References
----------

[1] Leslie DC & Gao S. (1988) The stability of spectral schemes for
the large eddy simulation of channel flows.  I J Num Meth Fluids
V8N9:1107--1116.

[2] Rudman M & Blackburn HM (2006) Direct numerical simulation of
turbulent non-Newtonian flow using a spectral element method. Appl
Math Mod 30:1229--48.

[3] Rudman M, Blackburn HM, Graham LJW and Pullum L (2004) Turbulent
pipe flow of shear-thinning fluids. J Non-Newt Fluid Mech 118:33--48.

[4] Singh J, Rudman M, Blackburn HM, Chryss A, Pullum L & Graham LJW
(2016) The importance of rheology characterization in predicting
turbulent pipe flow of generalized Newtonian fluids.  J Non-Newt Fluid
Mech 232:11--21.

[5] Singh J, Rudman M & Blackburn HM (2017) The influence of
shear-dependent rheology on turbulent pipe flow.  J Fluid Mech
822:849-79.
