///////////////////////////////////////////////////////////////////////////////
// NS.cpp: Unsteady Navier--Stokes solver, using "stiffly-stable" integration.
//
// Copyright (c) 1998+,  Murray Rudman & Hugh M Blackburn
///////////////////////////////////////////////////////////////////////////////

#include <gnewt.h>

// -- This triggers computation of nonlinear terms for diagnostics.

#define _DIAGNOSTIC_ 0


typedef ModalMatrixSys Msys;
static  int            NDIM, NCOM, NADV, NORD;
static  bool           C3D;

static void   nonLinear (Domain*, BCmgr*, AuxField**, AuxField**,
			 AuxField*, AuxField*, FieldForce*);
static void   waveProp  (Domain*, const AuxField***, const AuxField***);
static void   setPForce (const AuxField**, AuxField**);
static void   project   (const Domain*, AuxField**, AuxField**);
static Msys** preSolve  (const Domain*);
static void   Solve     (Domain*, const int, AuxField*, Msys*);


void NavierStokes (Domain*        D,
		   BCmgr*         B,
		   gnewtAnalyser* A,
 		   FieldForce*    F)
// ---------------------------------------------------------------------------
// On entry, D contains storage for velocity Fields 'u', 'v' ('w') and
// constraint Field 'p'.
//
// Us is multi-level auxillary Field storage for velocities and 
// Uf is multi-level auxillary Field storage for nonlinear forcing terms.
// ---------------------------------------------------------------------------
{
  const char routine[] = "NavierStokes";
  
  NCOM = D -> nVelCmpt();              // -- Number of velocity components.
  NADV = D -> nAdvect();               // -- Number of advected fields.
  NDIM = Geometry::nDim();	       // -- Number of space dimensions.
  NORD = Femlib::ivalue ("N_TIME");    // -- Time integration order.
  C3D  = Geometry::cylindrical() && NDIM == 3;
  
  int                i, j, k;
  const double       dt    = Femlib:: value ("D_T");
  const int          nStep = Femlib::ivalue ("N_STEP");
  const int          nZ    = Geometry::nZProc();
  static Msys**      MMS;
  static AuxField*** Us;
  static AuxField*** Uf;
  static AuxField*   work;
  Field*             Pressure = D -> u[NADV];

  // -- Set pointer to non-Newtonian viscosity auxfield (follows pressure).
  
  AuxField*          NNV = D -> ua[NADV + 1];

  if (!MMS) {			// -- Initialise static storage.

    // -- Create global matrix systems: rename viscosities beforehand.

#if defined (NOMODEL)
    Femlib::value ("REFVIS", Femlib::value ("2.*KINVIS"));
#endif

    if (Femlib::value ("REFVIS") > 0.0) {
      double kinVis = Femlib::value ("REFVIS");
      double refVis = Femlib::value ("KINVIS");
      Femlib::value ("KINVIS", kinVis);
      Femlib::value ("REFVIS", refVis);
    } else
      Veclib::alert (routine, "REFVIS must be positive", ERROR);

    // -- Create multi-level storage for velocities (Us) and forcing (Uf).

    const int ntot  = Geometry::nTotProc();
    double*   alloc = new double [static_cast<size_t>(2 * NADV*NORD * ntot)];
    Us              = new AuxField** [static_cast<size_t>(2 * NORD)];
    Uf              = Us + NORD;

    for (k = 0, i = 0; i < NORD; i++) {
      Us[i] = new AuxField* [static_cast<size_t>(2 * NADV)];
      Uf[i] = Us[i] + NADV;
      for (j = 0; j < NADV; j++) {
        Us[i][j] = new AuxField (alloc + k++ * ntot, nZ, D -> elmt);
        Uf[i][j] = new AuxField (alloc + k++ * ntot, nZ, D -> elmt);
      }
    }

    // -- Create global matrix systems.

    MMS = preSolve (D);

    // -- Create multi-level storage for pressure BCS.

    B -> buildComputedBCs (Pressure, D -> hasScalar());

    // -- Apply coupling to radial & azimuthal velocity BCs.

    if (C3D) Field::coupleBCs (D -> u[1], D -> u[2], FORWARD);

    // -- Make temp storage needed to preserve viscosity in special cases.

    if (Geometry::cylindrical() && NCOM == 3)
      work = new AuxField
	(new double[Geometry::nTotProc()], Geometry::nZProc(), D -> elmt);
  }

  // -- Because we may restart from scratch on each call, zero these:

  *Pressure = 0.0;

  for (i = 0; i < NORD; i++)
    for (j = 0; j < NADV; j++) {
      *Us[i][j] = 0.0;
      *Uf[i][j] = 0.0;
    }

#if _DIAGNOSTIC_

  // -- Process input to generate desired diagnostic terms and quit.

  viscosity (D, Us[0], Uf[0], NNV);
  nonLinear (D, B, Us[0], Uf[0], NNV, work, F);
  D -> step += 1; D -> time += dt;
  for (i = 0; i < NADV; i++) *D -> u[i] = *Uf[0][i];
  A -> analyse (Us[0], Uf[0], NNV);

#else  // -- Normal timestepping.

  // -- Timestepping loop.

  while (D -> step < nStep) {

    D -> step += 1; 
    D -> time += dt;
    Femlib::value ("t", D -> time);

    // -- Compute spatially-varying kinematic eddy viscosity.

    viscosity (D, Us[0], Uf[0], NNV);

    // -- Unconstrained forcing substep.

    nonLinear (D, B, Us[0], Uf[0], NNV, work, F);

    // -- Update high-order pressure BC storage.

    B -> maintainFourier (D -> step, Pressure, 
			  const_cast<const AuxField**> (Us[0]),
			  const_cast<const AuxField**> (Uf[0]),
			  NCOM, NADV);
    Pressure -> evaluateBoundaries (Pressure, D -> step);

    // -- Complete unconstrained advective substep.

    if (Geometry::cylindrical()) { Us[0][0] -> mulY(); Us[0][1] -> mulY(); }
    waveProp (D, const_cast<const AuxField***> (Us),
	         const_cast<const AuxField***> (Uf));
    for (i = 0; i < NADV; i++) AuxField::swapData (D -> u[i], Us[0][i]);
    rollm     (Uf, NORD, NADV);

    // -- Compute pressure.

    setPForce (const_cast<const AuxField**> (Us[0]), Uf[0]);
    Solve     (D, NADV,  Uf[0][0], MMS[NADV]);
    
    // -- Correct velocities for pressure gradient.

    project   (D, Us[0], Uf[0]);

    // -- Update multilevel velocity storage.

    for (i = 0; i < NADV; i++) *Us[0][i] = *D -> u[i];
    rollm (Us, NORD, NADV);

    // -- Re-evaluate velocity (possibly time-dependent) BCs.

    for (i = 0; i < NADV; i++)  {
      D -> u[i] -> evaluateBoundaries (NULL,     D -> step, false);
      D -> u[i] -> bTransform         (FORWARD);
      D -> u[i] -> evaluateBoundaries (Pressure, D -> step, true);
    }
    if (C3D) Field::coupleBCs (D -> u[1], D -> u[2], FORWARD);

    // -- Viscous correction substep to complete computation of
    //    velocity components (and, if relevant, scalar) for this time
    //    step.

    if (C3D) {
      AuxField::couple (Uf [0][1], Uf [0][2], FORWARD);
      AuxField::couple (D -> u[1], D -> u[2], FORWARD);
    }

    for (i = 0; i < NADV; i++) Solve (D, i, Uf[0][i], MMS[i]);
    if (C3D)
      AuxField::couple (D -> u[1], D -> u[2], INVERSE);

    // -- Process results of this step.

    A -> analyse (Us[0], Uf[0], NNV);
  }
#endif
}


static void nonLinear (Domain*     D ,
		       BCmgr*      B ,
		       AuxField**  Us,
		       AuxField**  Uf,
		       AuxField*   GV,
		       AuxField*   wk,
		       FieldForce* FF)
// ---------------------------------------------------------------------------
// Compute nonlinear + forcing terms in Navier--Stokes equations
//
//                       N(u) + div(2*GV*S) + f.
//
// Here N(u) represents the nonlinear advection terms in the N--S
// equations transposed to the RHS, GV is generalized Newtonian
// viscosity (more correctly: minus REFVIS, i.e. it is the
// non-constant part thereof), while div(2*GV*S), where S is the
// rate-of-strain tensor, is the divergence of the non-constant
// component of generalised Newtonian viscous terms.  FF describes
// field force mer unit mass.
//
// On entry, D contains the old velocity (and pressure) fields, the
// lowest levels of Us & Uf contain the components of the old
// strain-rate tensor and GV contains the spatially-varying viscosity.
// On exit, the velocity field storage areas of D are free, the zeroth
// level of Us contains the old velocities and the zeroth level of Uf
// contains the most recent explicit forcing terms.  Velocity field
// data areas of D and first level of Us are swapped, then the next
// stage of nonlinear forcing terms N(u) - a are computed from
// velocity fields and left in the first level of Uf.
//
// Nonlinear terms N(u) are computed in skew-symmetric form (Zang 1991)
//                 ~ ~
//           N  = -0.5 ( u . grad u + div uu )
//           ~           ~        ~       ~~
//
// i.e. in Cartesian component form
//
//           N  = -0.5 ( u  d(u ) / dx  + d(u u ) / dx ),
//            i           j    i      j      i j      j
//
// in cylindrical coordinates
//
//           Nx = -0.5 {ud(u)/dx + vd(u)/dy +  d(uu)/dx + d(vu)/dy +
//                 1/y [wd(u)/dz + d(uw)/dz + vu      ]}
//           Ny = -0.5 {ud(v)/dx + vd(v)/dy +  d(uv)/dx + d(vv)/dy +
//                 1/y [wd(v)/dz + d(vw)/dz + vv - 2ww]}
//           Nz = -0.5 {ud(w)/dx + vd(w)/dy +  d(uw)/dx + d(vw)/dy +
//                 1/y [wd(w)/dz + d(ww)/dz + 3wv     ]}
//
// NB: for the cylindrical coordinate formulation we actually here
// compute y*Nx, y*Ny, Nz, as outlined in Blackburn & Sherwin JCP
// 2004.
//
// Data are transformed to physical space for most of the operations, with
// the Fourier transform extended using zero padding for dealiasing.  For
// gradients in the Fourier direction however, the data must be transferred
// back to Fourier space.
//
// Refer to Bird, Stewart & Lightfoot (1960) Appendix A, or BSL 2ed
// (2002) Appendix B, for stress-divergence operations in cylindrical &
// Cartesian coordinates.  Also p117 of Semtex handwritten notes.
// ---------------------------------------------------------------------------
{
  const int NDIM = Geometry::nDim();
  const int NADV = D -> nAdvect();
  const int NCOM = D -> nVelCmpt();

  vector<AuxField*> U (NADV), N (NADV), Uphys (NADV);
  AuxField*         tmp = D -> u[NADV]; // -- Pressure is used for scratch.
  int               i, j;

  // -- Commence by computing viscous stress-divergence terms from
  //    inputs GV (viscosity), Us (diagonal of S) and Uf (off-diagonal
  //    of S), eventually placing that into N (a.k.a. Uf), but
  //    initially back into Us.  After this, Us is considered free for
  //    use, and the computation of N has begun.

  GV -> transform (INVERSE) *= 4.0;  // -- Skew symmetric; we will halve
                                     //    all these terms later.

  // -- Probably we could economize by having had viscosity() return
  //    values in physical space, and multiply S by GV.  Check later.
  
  if (Geometry::cylindrical()) {	// -- Cylindrical coordinates.
    
    //  (Recall that the x and y components get multiplied by y.)

    if (NDIM == 2 && NCOM == 2) {
      
      *Us[0] *= *GV; *Us[1] *= *GV;
      *Uf[0] *= *GV; *Uf[1] *= *GV;

      // -- x.
      
      (*tmp = *Uf[0])  . gradient (1);
      (*Us[0])         . gradient (0);
      (*Us[0] += *tmp) . mulY ();
      *Us[0] += *Uf[0];

      // -- y.

      (*tmp = *Us[1])  . gradient (1);
      (*Uf[0]       )  . gradient (0);
      (*Uf[0] += *tmp) . mulY ();
      *Us[1] += *Uf[0];
      *Us[1] -= *Uf[1]; // -- Uf[1] is the "oddball" term (called Us[2] below).
      
    } else if (NDIM == 2 && NCOM == 3) {

      *Us[0] *= *GV; *Us[1] *= *GV; *Us[2] *= *GV;
      *Uf[0] *= *GV; *Uf[1] *= *GV; *Uf[2] *= *GV;

      // -- x.
      
      (*tmp = *Uf[0])  . gradient (1);
      (*Us[0])         . gradient (0);
      (*Us[0] += *tmp) . mulY ();
      *Us[0]  += *Uf[0];

      // -- y.

      (*tmp = *Us[1])  . gradient (1);
      (*Uf[0]       )  . gradient (0);
      (*Uf[0] += *tmp) . mulY ();
      *Us[1] += *Uf[0];
      *Us[1] -= *Us[2];

      // -- z.

      (*tmp = *Uf[2]) *= 2.0;
      (*Us[2]  = *tmp) . divY ();
      (*Uf[2]) . gradient (1);
      (*Uf[1]) . gradient (0);
      *Us[2] += *Uf[1];
      *Us[2] += *Uf[2];

    } else { // -- 3D3C: first transform everything else to physical space.
                  
      Us[0] -> transform (INVERSE) *= *GV;
      Us[1] -> transform (INVERSE) *= *GV;
      Us[2] -> transform (INVERSE) *= *GV;
                  
      Uf[0] -> transform (INVERSE) *= *GV;
      Uf[1] -> transform (INVERSE) *= *GV;
      Uf[2] -> transform (INVERSE) *= *GV;

      // -- x.
      
      (*tmp = *Uf[0])  . gradient (1);
      (*Us[0])         . gradient (0);
      (*Us[0] += *tmp) . mulY ();
      *Us[0] += *Uf[0];
      Uf[1] -> transform (FORWARD) . gradient (2) . transform (INVERSE);
      *Us[0] += *Uf[1];

      // -- y.

      (*tmp = *Us[1])  . gradient (1);
      (*Uf[0]       )  . gradient (0);
      (*Uf[0] += *tmp) . mulY ();
      *Us[1] += *Uf[0];
      *Us[1] -= *Us[2];
      (*tmp   = *Uf[2]);	// -- Preserve Uf[2] for next operation block.
      tmp -> transform (FORWARD) . gradient (2) . transform (INVERSE);
      *Us[1] += *tmp;

      // -- z.

      Us[2] -> transform (FORWARD) . gradient (2) . transform (INVERSE);
      (*tmp = *Uf[2]) *= 2.0;
      (*Us[2] += *tmp) . divY ();
      (*Uf[2]) . gradient (1);
      (*Uf[1]) . gradient (0);
      *Us[2] += *Uf[1];
      *Us[2] += *Uf[2];
    }

  } else {			// -- Cartesian coordinates.

    // -- Diagonal stress-divergence terms.

    // -- Deal with the first two terms, which always contribute.

    (Us[0] -> transform (INVERSE) *= *GV) . gradient (0);
    (Us[1] -> transform (INVERSE) *= *GV) . gradient (1);

    // -- Now the final term.

    if (NCOM == 3) {
      if (NDIM == 3) {	        // -- z derivative required.
	Us[2] -> transform (INVERSE) *= *GV;
	Us[2] -> transform (FORWARD) . gradient (2) . transform (INVERSE);
      } else 			// -- 2D/3C: final diagonal term is zero.
	*Us[2] = 0.0;
    }
    
    // -- Off-diagonal stress-divergence terms.
    //    Start at Uf[0] (i,e. Sxy, Syx).  These always contribute.
    
    *tmp = Uf[0] -> transform (INVERSE) *= *GV;

    *Us[0] += Uf[0] -> gradient (1);
    *Us[1] += tmp   -> gradient (0);

    // -- Other contributions depend on components/dimensions.

    if (NDIM == 2 && NCOM == 3) {
      
      *Us[2] += (Uf[1] -> transform (INVERSE) *= *GV) . gradient (0);
      *Us[2] += (Uf[2] -> transform (INVERSE) *= *GV) . gradient (1);
      
    } else if (NDIM == 3 && NCOM == 3) {
      
      *tmp = Uf[1] -> transform (INVERSE) *= *GV;
      Uf[1] -> transform (FORWARD) . gradient (2) . transform (INVERSE);
      *Us[0] += *Uf[1];
      tmp -> gradient (0);
      *Us[2] += *tmp;
      
      *tmp = Uf[2] -> transform (INVERSE) *= *GV;
      Uf[2] -> transform (FORWARD) . gradient (2) . transform (INVERSE);
      *Us[1] += *Uf[2];
      tmp -> gradient (1);
      *Us[2] += *tmp;
      
    }
  }

  GV -> transform (FORWARD) *= 0.25; // -- Undo former scaling+transform.
  // -- (This operation enables storage/output of viscosity in physical space.)

  // -- Done divergence of viscous stress.  Set up pointers and copy
  //    the just-computed divergence terms in Us into (physical-space)
  //    nonlinear storage N (a local alias for Uf).
  
  for (i = 0; i < NADV; i++) {
    Uphys[i] = D -> u[i];
    N[i]     = Uf[i];
    U[i]     = Us[i];
    *N[i]    = *Us[i];		// -- Move the just-computed divergence terms.
    *U[i]    = *Uphys[i];
    Uphys[i] -> transform (INVERSE);
  }

  if (NADV > NCOM) *N[NCOM] = 0.0; // -- Cleanup for scalar advection.

  // -- Housekeeping for computed BCs.

  B -> maintainPhysical (D -> u[0], Uphys, NCOM, NADV);

  // --  Now for nonlinear terms proper.
  
  if (Geometry::cylindrical()) {

    if (NCOM == 3) *wk = *N[2]; // -- Save for inelegant work-around.

    for (i = 0; i < NADV; i++) {

      if (i == 0) N[0] -> timesMinus (*Uphys[0], *Uphys[1]);
      if (i == 1) N[1] -> timesMinus (*Uphys[1], *Uphys[1]);

      // -- Terms involving azimuthal derivatives and frame components.

      if (NCOM == 3) {
        if (i == 1) {
          tmp -> times (*Uphys[2], *Uphys[2]);
          N[1] -> axpy ( 2.0, *tmp);
        }

        if (i == 2) {
          tmp -> times (*Uphys[2], *Uphys[1]);
          N[2] -> axpy (-3.0, *tmp);
        }

	if (i == 3)
	  N[3] -> timesMinus (*Uphys[3], *Uphys[1]);
	  
        if (NDIM == 3) {
          (*tmp = *U[i]) . gradient (2) . transform (INVERSE);
          N[i] -> timesMinus (*Uphys[2], *tmp);

          tmp -> times (*Uphys[i], *Uphys[2]);
          (*tmp) . transform (FORWARD). gradient (2). transform (INVERSE);
          *N[i] -= *tmp;
        }
      } else if (i == 2 && (NADV > NCOM))
	N[2] -> timesMinus (*Uphys[1], *Uphys[2]);

      if (i >= 2) {
#if 1
	// -- Ugly workaround for i==2 (w).
	if (i == 2) { *N[2] -= *wk; N[2] -> divY (); *N[2] += *wk; }
	else N[i] -> divY ();
#else
	N[i] -> divY ();
#endif
      }
      
      // -- 2D convective derivatives.

      for (j = 0; j < 2; j++) {
        (*tmp = *Uphys[i]) . gradient (j);
        if (i < 2) tmp -> mulY ();
        N[i] -> timesMinus (*Uphys[j], *tmp);
      }

      // -- 2D conservative derivatives.

      for (j = 0; j < 2; j++) {
        (*tmp) . times (*Uphys[j], *Uphys[i]) . gradient (j);
        if (i < 2) tmp -> mulY ();
        *N[i] -= *tmp;
      }

      *N[i] *= 0.5;      // -- Average the two forms to get skew-symmetric.

      if (i < NCOM) FF -> addPhysical (N[i], tmp, i, Uphys);
    }

  } else {			// -- Cartesian coordinates.

    for (i = 0; i < NADV; i++) {
      for (j = 0; j < NDIM; j++) {

        // -- Perform n_i -= u_j d(u_i) / dx_j.

        if (j == 2) (*tmp = *U[i]) . gradient (j) . transform (INVERSE);
        else    (*tmp = *Uphys[i]) . gradient (j);
        N[i] -> timesMinus (*Uphys[j], *tmp);

        // -- Perform n_i -= d(u_i u_j) / dx_j.

        tmp -> times (*Uphys[i], *Uphys[j]);
        if (j == 2) tmp -> transform (FORWARD);
        tmp -> gradient (j);
        if (j == 2) tmp -> transform (INVERSE);
        *N[i] -= *tmp;
      }

      *N[i] *= 0.5;      // -- Average the two forms to get skew-symmetric.

      if (i < NCOM) FF -> addPhysical (N[i], tmp, i, Uphys);
    }
  }

  // -- Omitted: LMA-type buoyancy treatment present in standard dns.
  
  // -- Finally, take nonlinear terms to Fourier space.

  for (i = 0; i < NADV; i++) {
    N[i] -> transform (FORWARD);
    N[i] -> smooth (D -> nGlobal(), D -> assemblyNaive(), D -> invMassNaive());
  }
}


static void waveProp (Domain*           D ,
		      const AuxField*** Us,
		      const AuxField*** Uf)
// ---------------------------------------------------------------------------
// Compute the first substep of stiffly-stable timestepping scheme.
//
// On entry, the most recent velocity fields are in Us, and the most
// recent nonlinear terms in Uf.  The intermediate velocity field u^ is
// computed and left in D's velocity areas.
//
// This is the only routine that makes explicit use of the multi time
// level structure of Us & Uf.
// ---------------------------------------------------------------------------
{
  int               i, q;
  vector<AuxField*> H (NADV);	// -- Mnemonic for u^{Hat}.

  for (i = 0; i < NADV; i++) {
     H[i] = D -> u[i];
    *H[i] = 0.0;
  }

  const int    Je = min (D -> step, NORD);
  vector<double> alpha (Integration::OrderMax + 1);
  vector<double> beta  (Integration::OrderMax);
  
  Integration::StifflyStable (Je, &alpha[0]);
  Integration::Extrapolation (Je, &beta [0]);
  Blas::scal (Je, Femlib::value ("D_T"), &beta[0],  1);

  for (i = 0; i < NCOM; i++)
    for (q = 0; q < Je; q++) {
      H[i] -> axpy (-alpha[q + 1], *Us[q][i]);
      H[i] -> axpy ( beta [q]    , *Uf[q][i]);
    }
}


static void setPForce (const AuxField** Us,
		       AuxField**       Uf)
// ---------------------------------------------------------------------------
// On input, intermediate velocity storage u^ is in Us.  Create div u^ / D_T
// in the first dimension of Uf as a forcing field for discrete PPE.
// ---------------------------------------------------------------------------
{
  int          i;
  const double dt = Femlib::value ("D_T");

  for (i = 0; i < NDIM; i++) (*Uf[i] = *Us[i]) . gradient (i);

  for (i = 1; i < NDIM; i++) *Uf[0] += *Uf[i];

  *Uf[0] /= dt;
}


// ** NNEWT ** FIXME ** The following needs fixing for scalar transport
// diffusivity.

static void project (const Domain* D ,
		     AuxField**    Us,
		     AuxField**    Uf)
// ---------------------------------------------------------------------------
// On input, new pressure field is stored in D and intermediate velocity
// level u^ is stored in Us.  Constrain velocity field:
//
//                    u^^ = u^ - D_T * grad P,
//
// then scale by -1.0 / (D_T * KINVIS) to create forcing for viscous step
// (this is -1.0 / (D_T  * diffusivity) in the case of a scalar field).
//
// u^^ is left in Uf.
// ---------------------------------------------------------------------------
{
  int          i;
  const double alpha = -1.0 / Femlib::value ("D_T * KINVIS");
  const double beta  =  1.0 / Femlib::value ("KINVIS");
  const double Pr    =        Femlib::value ("PRANDTL");

  for (i = 0; i < NADV; i++) {
    Field::swapData (Us[i], Uf[i]);
    if (Geometry::cylindrical() && i >= 2) Uf[i] -> mulY();
    *Uf[i] *= alpha;
  }

  // -- For scalar, use diffusivity instead of viscosity.
  if (NADV > NCOM) *Uf[NCOM] *= Pr;

  for (i = 0; i < NDIM; i++) {
    (*Us[0] = *D -> u[NADV]) . gradient (i);
    if (Geometry::cylindrical() && i <  2) Us[0] -> mulY();
    Uf[i] -> axpy (beta, *Us[0]);
  }
}


static Msys** preSolve (const Domain* D)
// ---------------------------------------------------------------------------
// Set up ModalMatrixSystems for each Field of D.  If iterative solution
// is selected for any Field, the corresponding ModalMatrixSystem pointer
// is set to zero.
//
// ITERATIVE >= 1 selects iterative solver for velocity components,
// ITERATIVE >= 2 selects iterative solver for non-zero pressure Fourier modes.
// ---------------------------------------------------------------------------
{
  const int               nmodes = Geometry::nModeProc();
  const int               base   = Geometry::baseMode();
  const int               itLev  = Femlib::ivalue ("ITERATIVE");
  const double            beta   = Femlib:: value ("BETA");
  const vector<Element*>& E = D -> elmt;
  Msys**                  M = new Msys* [static_cast<size_t>(NADV + 1)];
  int                     i;

  vector<double> alpha (Integration::OrderMax + 1);
  Integration::StifflyStable (NORD, &alpha[0]);
  double         lambda2 = alpha[0] / Femlib::value ("D_T * KINVIS");

  // -- Velocity systems.

  for (i = 0; i < NCOM; i++)
    M[i] = new Msys
      (lambda2, beta, base, nmodes, E, D -> b[i], D -> n[i],
       (itLev) ? JACPCG : DIRECT);

  // -- Scalar system.

  if (NADV != NCOM) {
    lambda2 = alpha[0] / Femlib::value ("D_T * KINVIS / PRANDTL");
    M[NCOM] = new Msys
      (lambda2, beta, base, nmodes, E, D -> b[NCOM], D -> n[NCOM],
       (itLev < 1)?DIRECT:JACPCG);
  }

  // -- Pressure system.

  if (itLev > 1)
    M[NADV] = new Msys
      (0.0, beta, base, nmodes, E, D -> b[NADV], D -> n[NADV], MIXED);
  else
    M[NADV] = new Msys
      (0.0, beta, base, nmodes, E, D -> b[NADV], D -> n[NADV], DIRECT);

  return M;
}


static void Solve (Domain*   D,
		   const int i,
		   AuxField* F,
		   Msys*     M)
// ---------------------------------------------------------------------------
// Solve Helmholtz problem for D -> u[i], using F as a forcing Field.
// Iterative or direct solver selected on basis of field type, step,
// time order and command-line arguments.
// ---------------------------------------------------------------------------
{
  const int step = D -> step;

  if (i < NADV && step < NORD) { // -- We need a temporary matrix system.
    const int Je     = min (step, NORD);
    const int base   = Geometry::baseMode();
    const int nmodes = Geometry::nModeProc();

    vector<double> alpha (Je + 1);
    Integration::StifflyStable (Je, &alpha[0]);
    const double   lambda2 = (i == NCOM) ? // -- True for scalar diffusion.
      alpha[0] / Femlib::value ("D_T * KINVIS / PRANDTL") :
      alpha[0] / Femlib::value ("D_T * KINVIS");
    const double   beta    = Femlib::value ("BETA");

    Msys* tmp = new Msys
      (lambda2, beta, base, nmodes, D -> elmt, D -> b[i], D -> n[i], JACPCG);

    D -> u[i] -> solve (F, tmp);
    delete tmp;

  } else D -> u[i] -> solve (F, M);
}
