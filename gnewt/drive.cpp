//////////////////////////////////////////////////////////////////////////////
// drive.cpp: control spectral element non-Newtonian DNS for incompressible
// flows, with power-law viscosity.
//
// Copyright (c) 1999+, Hugh M Blackburn & Murray Rudman.
//
// USAGE:
// -----
// gnewt [options] session
//   options:
//   -h       ... print usage prompt
//   -i       ... use iterative solver for viscous steps
//   -v[v...] ... increase verbosity level
//   -chk     ... disable checkpointed field dumps
// 
// SYNOPSIS
// --------
//
// Nnewt is an adaptation of semtex "dns" that incorporates spatially
// varying generalized Newtonian viscosity.  This is handled within
// the framework of the DNS solver by splitting the viscous stress
// into two parts: one which can be dealt with implicitly as diffusion
// of momentum with a spatially-constant kinematic viscosity REFVIS,
// the other dealt with explicitly as the divergence of a stress.
// This way the velocity BCs are still set correctly in the viscous
// substep.  The viscosity splitting technique was originally
// discussed in [4] in the context of large eddy simulations with eddy
// viscosity.  For some further discussion and simulation results, see
// [1--3].  The only formulation of nonlinear terms implemented to
// date is skew-symmetric.
//
// VISCOSITY MODELS
// ----------------
//
// The following generalised Newtonian viscosity models for kinematic
// viscosity NU are implemented, where S is the magnitude of the rate
// of strain tensor:
//
// Power Law: NU = rho^{-1}[K * S^(N-1)]. Need to define tokens
// PowerLaw, PL_K, PL_N (NB: PL_K \equiv K/rho).
//
// Herschel--Bulkley: NU = rho^{-1}[Tau_Y*S^(-1) + K*S^(N-1)]. Need to
// define tokens HB, YIELD_STRESS, HB_K, HB_N, HB_ZERO (NB:
// YIELD_STRESS \equiv Tau_Y/rho, HB_K \equiv K/rho).
//
// Carreau--Yasuda: NU = NU_infty + (NU_0 - NU_infty) * (1 +
// (LAMBDA*S)^N))^(-A). Need to define tokens CAR_YAS, CY_K, CY_N,
// CY_A, VISC_ZERO, VISC_INF.
//
// Cross: NU = NU_infty + (NU_0 - NU_infty) * (1 +
// (LAMBDA*S)^N))^(-1). Need to define tokens CROSS, CROSS_LAMBDA,
// CROSS_N, VISC_ZERO, VISC_INF.
//
// See file viscosity.cpp for implementation details.  For both power
// law and Herschel-Bulkley models, an additional lower shear rate
// limit value is required to avoid potential divide-by-zero problems
// where the shear rate approaches zero.  If sufficiently small, the
// value chosen does not greatly affect outcomes, at least if the flow
// is turbulent.
//
// For testing purposes, we can define NOMODEL during compilation.
// This overrides any declared non-Newtonian viscosity models and will
// set the kinematic viscosity equal to twice the KINVIS value
// declared in the session file and formulate the viscous stresses as
// those supplied by the product of the strain rate tensor with a
// spatially-constant NEGATIVE viscosity -KINVIS: the results should
// be almost exactly the same as obtained using dns with kinematic
// viscosity KINVIS.
//
// References
// ----------
//
// [1] Rudman M & Blackburn HM (2006) Direct numerical simulation of
// turbulent non-Newtonian flow using a spectral element method. Appl
// Math Mod 30:1229--48.
//
// [2] Rudman M, Blackburn HM, Graham LJW and Pullum L (2004)
// Turbulent pipe flow of shear-thinning fluids. J Non-Newt Fluid Mech
// 118:33--48.
//
// [3] Singh J, Rudman M & Blackburn HM (2017) The influence of
// shear-dependent rheology on turbulent pipe flow.  J Fluid Mech
// 822:849-79.
//
// [4] Leslie DC & Gao S. (1988) The stability of spectral schemes for
// the large eddy simulation of channel flows.  I J Num Meth Fluids
// V8N9: 1107--1116.
//
// AUTHOR:
// ------
// Hugh M Blackburn, Murray Rudman
// Department of Mechanical and Aerospace Engineering
// Monash University
// Vic 3800
// Australia
// mailto:hugh.blackburn@monash.edu
//////////////////////////////////////////////////////////////////////////////

#include <gnewt.h>

#if defined(MPI_EX)
  static char prog[] = "gnewt_mp";
#else
  static char prog[] = "gnewt";
#endif

static void getargs    (int, char**, char*&);
static void preprocess (const char*, FEML*&, Mesh*&, vector<Element*>&,
			BCmgr*&, Domain*&, FieldForce*&);

void NavierStokes (Domain*, BCmgr*, gnewtAnalyser*, FieldForce*);


int main (int    argc,
	  char** argv)
// ---------------------------------------------------------------------------
// Driver.
// ---------------------------------------------------------------------------
{
#ifdef _GNU_SOURCE
  feenableexcept (FE_OVERFLOW);    // -- Force SIG8 crash on FP overflow.
#endif

  char             *session;
  int              nproc = 1, iproc = 0, npart2d = 1;    
  vector<Element*> elmt;
  FEML*            file;
  Mesh*            mesh;
  BCmgr*           bman;
  Domain*          domain;
  gnewtAnalyser*   analyst;
  FieldForce*      FF;
  
  Femlib::init  ();
  Message::init (&argc, &argv, nproc, iproc);

  Femlib::ivalue ("I_PROC", iproc);
  Femlib::ivalue ("N_PROC", nproc);

  getargs (argc, argv, session);

  preprocess (session, file, mesh, elmt, bman, domain, FF);

  analyst = new gnewtAnalyser (domain, bman, file);

  domain -> restart();

  ROOTONLY domain -> report();
  
  NavierStokes (domain, bman, analyst, FF);

  Message::stop ();
  
  return EXIT_SUCCESS;
}


static void getargs (int    argc   ,
		     char** argv   ,
		     char*& session)
// ---------------------------------------------------------------------------
// Install default parameters and options, parse command-line for optional
// arguments.  Last argument is name of a session file, not dealt with here.
// ---------------------------------------------------------------------------
{
  char       buf[StrMax];
  const char routine[] = "getargs";
  const char usage[] =
    "Usage: %s [options] session-file\n"
    "  [options]:\n"
    "  -h       ... print this message\n"
    "  -i       ... use iterative solver for viscous step\n"
    "  -v[v...] ... increase verbosity level\n"
    "  -chk     ... disable checkpointing field dumps\n";

  Femlib::ivalue ("PowerLaw", 0);
  Femlib::ivalue ("HB",       0);
  Femlib::ivalue ("CAR_YAS",  0);
  Femlib::ivalue ("Cross",    0);
  Femlib::value  ("PL_ZERO",  EPSm30);
  Femlib::value  ("HB_ZERO",  EPSm30);
    
  while (--argc && **++argv == '-')
    switch (*++argv[0]) {
    case 'h':
      sprintf (buf, usage, prog);
      cout << buf;
#if defined (NOMODEL)
      cout << "NB: This executable has non-Newtonian disabled" << endl;
#endif
      exit (EXIT_SUCCESS);
      break;
    case 'i':
      do
	Femlib::ivalue ("ITERATIVE", Femlib::ivalue ("ITERATIVE") + 1);
      while (*++argv[0] == 'i');
      break;
    case 'v':
      do
	Femlib::ivalue ("VERBOSE",   Femlib::ivalue ("VERBOSE")   + 1);
      while (*++argv[0] == 'v');
      break;
    case 'c':
      if (strstr ("chk", *argv)) Femlib::ivalue ("CHKPOINT",  0);
      else { fprintf (stdout, usage, prog); exit (EXIT_FAILURE); }
      break;
    default:
      sprintf (buf, usage, prog);
      cout << buf;
      exit (EXIT_FAILURE);
      break;
    }

  if   (argc != 1) Veclib::alert (routine, "no session definition file", ERROR);
  else             session = *argv;
}


static void preprocess (const char*       session,
			FEML*&            file   ,
			Mesh*&            mesh   ,
			vector<Element*>& elmt   ,
			BCmgr*&           bman   ,
			Domain*&          domain ,
			FieldForce*&      FF     )
// ---------------------------------------------------------------------------
// Create objects needed for execution, given the session file name.
// They are listed in order of creation.
// ---------------------------------------------------------------------------
{
  const char         routine[] = "preprocess";  
  const int          verbose = Femlib::ivalue ("VERBOSE");
  Geometry::CoordSys space;
  int                i, np, nz, nel, procid, seed;
  int                npart2d = 1, ipart2d, npartz, ipartz;
  
  // -- Initialise problem and set up mesh geometry.

  VERBOSE cout << "Building mesh ..." << endl;

  file = new FEML (session);
  mesh = new Mesh (file);

  Message::grid ((int) npart2d, ipart2d, npartz, ipartz);

  VERBOSE cout << "done" << endl;

  // -- Set up global geometry variables.

  VERBOSE cout << "Setting geometry ... ";

  nel   =  mesh -> nEl();
  np    =  Femlib::ivalue ("N_P");
  nz    =  Femlib::ivalue ("N_Z");
  space = (Femlib::ivalue ("CYLINDRICAL")) ? 
    Geometry::Cylindrical : Geometry::Cartesian;

  Geometry::set (np, nz, nel, space);

  VERBOSE cout << "done" << endl;

  // -- If token RANSEED > 0 then initialize the random number
  //    generator based on wall clock time and process ID (i.e. a "truly"
  //    pseudo-random number).  NB: it is important to have done this
  //    here, before any other possible call to random number routines.

  if (Femlib::ivalue ("RANSEED") > 0) {
    procid = Geometry::procID();
    seed   = -abs((procid + 1) * (char) time(NULL));
  } else seed = -1;
  Veclib::ranInit (seed);

  // -- Build all the elements.

  VERBOSE cout << "Building elements ... ";

  elmt.resize (nel);
  for (i = 0; i < nel; i++) elmt[i] = new Element (i, np, mesh);

  VERBOSE cout << "done" << endl;

  // -- Build all the boundary condition applicators.

  VERBOSE cout << "Building boundary condition manager ..." << endl;

  bman = new BCmgr (file, elmt);

  VERBOSE cout << "done" << endl;

  // -- Build the solution domain.

  VERBOSE cout << "Building domain ..." << endl;

  domain = new Domain (file, mesh, elmt, bman);

  if (domain -> nAdvect() > domain -> nVelCmpt())
    Veclib::alert (routine, "can only advect velocity components", ERROR);

  VERBOSE cout << "done" << endl;

  // -- Parse Field Force info from FEML file.

  VERBOSE cout << "Building field forcing ..." << endl;

  FF = new FieldForce (domain, file);

  VERBOSE cout << "done" << endl;

  // -- Sanity checks on standard installed tokens.  Could be more extensive.

  if (Femlib::ivalue ("SVV_MN") > Geometry::nP())
    Veclib::alert (routine, "SVV_MN exceeds N_P", ERROR);
  if (Femlib::ivalue ("SVV_MZ") > Geometry::nMode())
    Veclib::alert (routine, "SVV_MZ exceeds N_Z/2", ERROR);

#if !defined (NOMODEL)
  // -- We need one of the implemented non-Newtonian models set.
  //    The test below could also be more extensive.
  
  if (! (Femlib::ivalue ("PowerLaw") || Femlib::ivalue ("HB")    ||
	 Femlib::ivalue ("CAR_YAS")  || Femlib::ivalue ("Cross")) )
    Veclib::alert (routine, "no viscosity model has been defined", ERROR);

  if (Femlib::value ("REFVIS") < EPSm30)
    Veclib::alert (routine, "a positive value of REFVIS is needed", ERROR);
	
#endif
}
