//////////////////////////////////////////////////////////////////////////////
// gnewt.h: header file for gnewt (generalized Newtonian) dns solver.
//
// Copyright (c) 1998+,  Murray Rudman & Hugh M Blackburn
//////////////////////////////////////////////////////////////////////////////

#include <sem.h>
#include <fieldforce.h>


class gnewtAnalyser : public Analyser
// ===========================================================================
// Implement step-by-step processing and output control for flow solver.
// ===========================================================================
{
public:
  gnewtAnalyser (Domain*, BCmgr*, FEML*);
  void analyse  (AuxField**, AuxField**, AuxField*);

private:
  ofstream flx_strm;
};


// -- In viscosity.cpp:

void viscosity (const Domain*, AuxField**, AuxField**, AuxField*);
