//////////////////////////////////////////////////////////////////////////////
// eneq_gn.cpp: from a field file containing correlations, compute
// terms of either the mean or the fluctuating flow energy equations
// for generalized Newtonian flows in Cartesian coordinates, see Ref
// [2] eqs (5.1, 5.2).  This file is an adaption of utility/eneq.cpp
// which was written for Newtonian fluids.
//
// NB: the code is thus far restricted for use in Cartesian coords.
// For cylindrical, divergence-related terms need alteration.
//
// Copyright (c) 2004+, Jagmohan Singh, Murray Rudman & Hugh M Blackburn
//
// Usage:
// -----
// eneq_gn [options] session session.avg
//   options:
//   -h        ... print this message
//   -m        ... output terms for mean flow energy instead of TKE
//
// Output to standard output.
//
// Synopsis
// --------
//
// From a field file containing correlations, compute terms of either
// the fluctuating flow energy equation, Tennekes & Lumley (3.2.1)
// which is the default action, or the mean flow energy equation, T&L
// (3.1.11). Output values have dimensions of kinetic energy per unit
// mass per unit time, i.e. power per unit mass.
//
// The input field file is required to contain only velocity and
// appropriate correlation data, made by running gnewt with
// AVERAGE=3. This requirement is checked based on named fields it
// contains. The input/output naming conventions employed for example in
// addfield.cpp are broken (e.g. output field names a, b, ..., h are here
// used for different variables).  Averages/correlations required for
// the analysis described herein are collected when running dns by
// setting token AVERAGE=3.
//
// The program works for both 2C (uvp) 3C (uvwp) input files.
//
// NB: for the mean flow energy equation, any mean pressure gradient
// should be included.  Since that is very often replaced by a
// constant body force in our simulations (as would be supplied to the
// simulation in the <FORCE> section), one should define CONST_Z
// forcing.  If you *haven't* defined CONST_Z in the session file, a
// warning will be issued if the mean flow analysis is run -- see
// inside the meanflo subroutine below.  More thought is required in
// cases where there may be both mean pressure gradients and body
// forces.
//
// NB: Ref[1] (3.1.11) contains an error: the term +2\nu S_ijS_ij
// should read -2\nu S_ijS_ij.  The equivalent in Ref [2] (eq. 5.1) has
// the sign correct. So does the code that follows!
//
// Input field names (cf. statistics.cpp):
// ---------------------------------------
//
// (As written from gnewt with AVERAGE=3 into <session>.avg.)
//
// u -- x velocity component (cylindrical: axial)
// v -- y velocity component (cylindrical: radial)
// w -- z velocity component (cylindrical: azimuthal)
// p -- pressure/density
//
// Names for components of the symmetric "Reynolds stress" correlations:
//
//                      / uu uv uw \     / A  B  D \
//                      | .  vv vw |  =  | .  C  E |
//                      \ .  .  ww /     \ .  .  F /
//
// Names for correlations specific to the energy equation:
// -------------------------------------------------------
// a) Scalar: 
//    i) q = 0.5 [u^2 + v^2 (+ w^2)]
//   ii) d = SijSij
/// iii) e = l SijSij
//
// b) Vector: Naming:
//    i) p u_i
//                      / pu \   / m \
//                      | pv | = | n |
//                      \ pw /   \ o /
//   ii) q u_i
//                      / qu \   / r \
//                      | qv | = | s |
//                      \ qw /   \ t /
//
//   iii) Sij u_j       / SxxU + SxyV + SxzW \   / x \   / x \ 
//                      | SyxU + SyyV + SxzW | = | y | = \ y / -- if 2C.
//                      \ SzxU + SzyV + SzzW /   \ z / 
//
//    iv) l u_i         / lu \   / f \
//                      | lv | = | g |
//                      \ lw /   \ h /
//
//     v) l Sij u_i     / la \   / i \
//                      | lb | = | j |
//                      \ lc /   \ k /
// 
// c) Tensor: Naming:
//     i) Sij           / xx xy xz \     / K  L  N \
//                      | .  yy yz |  =  | .  M  O |
//                      \ .  .  zz /     \ .  .  P /
//
//
//    ii) l Sij         / xx xy xz \     / R  S  U \
//                      | .  yy yz |  =  | .  T  V |
//                      \ .  .  zz /     \ .  .  W /
//
// Names for (output) terms in the fluctutating energy (TKE) equation:
// -------------------------------------------------------------------
// In the case that we consider non-Newtonian flow, there are ten
// terms to compute.  We will use arabic numeric characters to name
// these terms.  Compared to equivalent Newtonian TKE equation,
// CHANGED terms have variable viscosity, NEW terms are new.
//
// 0: -ui.uj.Sij                   turbulent production
// 1: Uj dq/dxj                    mean flow advection
// 2: -d(q.uj)/dxj                 turbulent velocity transport
// 3: -d(1/rho p.uj)/dxj           pressure related transport
// 4: 2 d(L sij.ui)/dxj ** CHANGED mean viscous transport
// 5: 2 d(l ui Sij)/dxj ** NEW     mean shear turbulent viscous transport
// 6: 2 d(l sij ui)/dxj ** NEW     turbulent viscous transport
// 7: -2 L sij.sij      ** CHANGED mean viscous dissipation
// 8: -2 l sij.Sij      ** NEW     mean shear turbulent viscous dissipation
// 9: -2 l sij.sij      ** NEW     turbulent viscous dissipation
//
// Note: names 5,6,8,9 are for non-Newtonian flow.  The
// above terms should sum to zero for stationary turbulence, so we
// also write out
//
// S: 0 - 1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9
//
// NB: new OUTPUT NAMES (to get around tecplot's problems with numeric
// names) -- NOTE they conflict with input names.  Sorry.
//
// S: a - b + c + d + e + f + g + h + i + j
//
// which should approach zero pointwise, and when integrated over domain.
//
// Names for (output) terms in the MEAN energy equation (MKE):
// -----------------------------------------------------------
//
// '0': ui.uj.Sij                    (turbulent) production
// '1': Uj dQ/dxj                    mean flow advection, Q = 0.5*UiUi
// '2': -d(ui.uj.Ui)/dxj             turbulent Reynolds stress transport        
// '3': -1/rho*d(P.Uj)/dxj           mean flow production (pressure work)
// '4': 2 d(l Sij.Ui)/dxj ** CHANGED mean viscous transport
// '5': 2 d(Ui l sij)/dxj ** NEW     turbulent viscous stress transport
// '6': -2 l sij.Sij      ** NEW     mean shear turbulent viscous dissipation
// '7': -2 l Sij.Sij      ** CHANGED mean flow dissipation
//
// Again, the sum of terms below should equal zero for stationary
// turbulence, so we also write out
//
// S: 0 - 1 + 2 + 3 + 4 + 5 + 6 + 7
//
// New OUTPUT NAMES (to get around tecplot's problems with numeric names);
//
// S: a - b + c + d + e + f + g + h
//
// which should approach zero pointwise, and when integrated over
// domain. For steady flow, terms 2 and 0 are also zero.
//
// References
// ----------
//
// [1] Tennekes H & Lumley JH (1972) A First Course in Turbulence.
// MIT Press.
//
// [2] Singh J, Rudman M & Blackburn HM (2017) The influence of
// shear-dependent rheology on turbulent pipe flow.  J Fluid Mech
// 822:849-79.
//////////////////////////////////////////////////////////////////////////////

#include <sem.h>

static char prog[] = "eneq_gn";
static void getargs (int,char**,bool&,const char*&,const char*&);
static void getMesh (const char*,vector<Element*>&);
static void makeBuf (map<char,AuxField*>&,vector<AuxField*>&,
		     vector<Element*>&, const bool&);
static bool getDump (ifstream&,map<char, AuxField*>&,vector<Element*>&);
static void covary  (map<char,AuxField*>&,map<char,AuxField*>&,
		     vector<AuxField*>&);
static void meanflo (map<char,AuxField*>&,map<char,AuxField*>&,
		     vector<AuxField*>&);

static const char* fieldNames(map<char, AuxField*>&);


int main (int    argc,
	  char** argv)
// ---------------------------------------------------------------------------
// Driver -- adapted from eneq.cpp.
// ---------------------------------------------------------------------------
{
  const char           *session, *dump;
  ifstream             avgfile;
  vector<Element*>     elmt;
  map<char, AuxField*> input, output;
  vector<AuxField*>    work,  outbuf;
  bool                 MKE = false;
  int                  nproc = 1, iproc = 0, npart2d = 1;

  Femlib::init  ();
  Message::init (&argc, &argv, nproc, iproc);
  getargs       (argc, argv, MKE, session, dump);

  avgfile.open (dump, ios::in);
  if (!avgfile) Veclib::alert (prog, "no field file", ERROR);
  
  getMesh (session, elmt);
  makeBuf (output, work, elmt, MKE);

  // -- Need to link outbuf to output so we can use writeField.
  //    Maybe in the longer term we should overload writeField.

  outbuf.resize (output.size()); int i = 0;
  for (map<char,AuxField*>::iterator k = output.begin();
       k != output.end(); k++, i++) outbuf[i] = k -> second;
  
  while (getDump (avgfile, input, elmt)) {
    covary     (input, output, work);
    writeField (cout, session, 0, 0.0, outbuf);
  }
  
  Message::stop ();
  return EXIT_SUCCESS;
}


static void getargs (int          argc   ,
		     char**       argv   ,
		     bool&        MKE    ,
		     const char*& session,
		     const char*& dump   )
// ---------------------------------------------------------------------------
// Deal with command-line arguments.
// ---------------------------------------------------------------------------
{
  char usage[] =
    "Usage: %s [options] session dump.avg\n"
    "options:\n"
    "  -h ... print this message \n"
    "  -m ... output terms for mean flow energy instead of TKE\n";
    
  char buf[StrMax];
 
  while (--argc && **++argv == '-')
    switch (*++argv[0]) {
    case 'h':
      sprintf (buf, usage, prog); cerr << buf; exit (EXIT_SUCCESS);
      break;
    case 'm':
      MKE = true;
    default:
      sprintf (buf, usage, prog); cerr << buf; exit (EXIT_FAILURE);
      break;
    }

  if (argc != 2) {
    sprintf (buf, usage, prog); cerr << buf; exit (EXIT_FAILURE);
  } else {
    --argc; session = *argv;
    --argc; dump    = *++argv;
  }
}


static void getMesh (const char*       session,
		     vector<Element*>& elmt   )
// ---------------------------------------------------------------------------
// Set up 2D mesh information. Note that parser tokens and Geometry
// are set here, too.
// ---------------------------------------------------------------------------
{
  FEML* F = new FEML (session);
  Mesh* M = new Mesh (F);
  
  const int nel = M -> nEl();  
  const int np  = Femlib::ivalue ("N_P");
  const int nz  = Femlib::ivalue ("N_Z");

  Geometry::CoordSys space = (Femlib::ivalue ("CYLINDRICAL")) ?
    Geometry::Cylindrical : Geometry::Cartesian;
  
  Geometry::set (np, nz, nel, space);
  elmt.resize   (nel);

  for (int k = 0; k < nel; k++) elmt[k] = new Element (k, np, M);
}


static void makeBuf (map<char, AuxField*>& output,
		     vector<AuxField*>&    work  ,
		     vector<Element*>&     elmt  ,
		     const bool&           MKE   )
// ---------------------------------------------------------------------------
// Note that we only set up the output and work buffers here. The
// input buffers get created in getDump.
//
// Unlike the Newtonian case, there are different numbers of output
// fields for MKE and TKE analyses, hence the test on MKE.
// ---------------------------------------------------------------------------
{
  const int nz   = Geometry::nZ();
  const int ntot = Geometry::nTotal();

  work.resize (2);

  if (MKE) {
    output['0'] = new AuxField (new double[ntot], nz, elmt, 'a');
    output['1'] = new AuxField (new double[ntot], nz, elmt, 'b');
    output['2'] = new AuxField (new double[ntot], nz, elmt, 'c');
    output['3'] = new AuxField (new double[ntot], nz, elmt, 'd');
    output['4'] = new AuxField (new double[ntot], nz, elmt, 'e');
    output['5'] = new AuxField (new double[ntot], nz, elmt, 'f');
    output['6'] = new AuxField (new double[ntot], nz, elmt, 'g');
    output['7'] = new AuxField (new double[ntot], nz, elmt, 'h');
    output['S'] = new AuxField (new double[ntot], nz, elmt, 'S');
  } else { 
    output['0'] = new AuxField (new double[ntot], nz, elmt, 'a');
    output['1'] = new AuxField (new double[ntot], nz, elmt, 'b');
    output['2'] = new AuxField (new double[ntot], nz, elmt, 'c');
    output['3'] = new AuxField (new double[ntot], nz, elmt, 'd');
    output['4'] = new AuxField (new double[ntot], nz, elmt, 'e');
    output['5'] = new AuxField (new double[ntot], nz, elmt, 'f');
    output['6'] = new AuxField (new double[ntot], nz, elmt, 'g');
    output['7'] = new AuxField (new double[ntot], nz, elmt, 'h');
    output['8'] = new AuxField (new double[ntot], nz, elmt, 'i');
    output['9'] = new AuxField (new double[ntot], nz, elmt, 'j');
    output['S'] = new AuxField (new double[ntot], nz, elmt, 'S');
  }

  work[0]     = new AuxField (new double[ntot], nz, elmt, '\0');
  work[1]     = new AuxField (new double[ntot], nz, elmt, '\0');
}


static const char* fieldNames (map<char, AuxField*>& u)
// ---------------------------------------------------------------------------
// Return string containing single-character names of fields.
// ---------------------------------------------------------------------------
{
  int         i;
  static char buf[StrMax];
  map<char, AuxField*>::iterator k;

  for (i = 0, k = u.begin(); k != u.end(); k++, i++) buf[i] = k -> first;
  buf[i] = '\0';

  return buf;
}


static bool getDump (ifstream&             file,
		     map<char, AuxField*>& u   ,
		     vector<Element*>&     elmt)
// ---------------------------------------------------------------------------
// Load data from field dump, with byte-swapping if required.
// If there is more than one dump in file, it is required that the
// structure of each dump is the same as the first.
// ---------------------------------------------------------------------------
{
  const int ntot = Geometry::nTotal();
  char      buf[StrMax], fields[StrMax];
  int       i, nf, np, nz, nel;
  bool      swab;

  if (file.getline(buf, StrMax).eof()) return false;
  
  if (!strstr (buf, "Session")) Veclib::alert (prog, "not a field file", ERROR);
  file.getline (buf, StrMax);

  // -- Input numerical description of field sizes.

  file >> np >> nz >> nz >> nel;
  file.getline (buf, StrMax);
  
  if (np != Geometry::nP() || nz != Geometry::nZ() || nel != Geometry::nElmt())
    Veclib::alert (prog, "size of dump mismatch with session file", ERROR);

  file.getline (buf, StrMax);
  file.getline (buf, StrMax);
  file.getline (buf, StrMax);
  file.getline (buf, StrMax);
  file.getline (buf, StrMax);

  // -- Input field names, assumed to be written without intervening spaces.

  file >> fields;
  nf = strlen  (fields);
  file.getline (buf, StrMax);

  // -- Arrange for byte-swapping if required.

  file.getline  (buf, StrMax);
  swab = Veclib::byteSwap (buf);

  // -- Create AuxFields on first pass.

  if (u.size() == 0) {
    for (i = 0; i < nf; i++)
      u[fields[i]] = new AuxField (new double[ntot], nz, elmt, fields[i]);
  } else if (strcmp (fieldNames (u), fields) != 0)
    Veclib::alert (prog, "fields mismatch with first dump in file", ERROR);

  // -- Read binary field data.

  for (map<char, AuxField*>::iterator k = u.begin(); k != u.end(); k++) {
    file >> *(k -> second);
    if (swab) k -> second -> reverse();
  }

  return file.good();
}


static void meanflo (map<char, AuxField*>& in  ,
		     map<char, AuxField*>& out ,
		     vector<AuxField*>&    work)
// ---------------------------------------------------------------------------
// This does the actual work of building MKE equation terms.
// ---------------------------------------------------------------------------
{
  const char  list2d[]  = "ABCKLMRSTdefgijlmnpqrsuvxy";
  const char  list3d[]  = "ABCDEFKLMNOPRSTUVWdefghijklmnopqrstuvwxyz";
  const char  list2dc[] = "ABCGHJKLMRSTcdefgijlmnpqrsuvxy";
  const char  list3dc[] = "ABCDEFGHIJKLMNOPRSTUVWcdefghijklmnopqrstuvwxyz";
  
  const char* names     = fieldNames (in);
  const int   nvel      = (strchr (names, 'w')) ? 3 : 2;
  const int   scalar    = (strchr (names, 'c')) ? 1 : 0;  
  char        err[StrMax];

  const double FFZ = Femlib::value ("CONST_Z");  // -- See NB in synopsis above.
  
  if (nvel == 2) {
    if (strcmp (names, list2d) != 0) {
      sprintf
	(err, "input field names should match %s: have %s", list2d, names);
      Veclib::alert (prog, err, ERROR);
    } 
  } else if (strcmp (names, list3d) != 0) {
    sprintf
      (err, "input field names should match %s: have %s", list3d, names);
    Veclib::alert (prog, err, ERROR);
  }

  if (scalar) {
    if (nvel == 2) {
        if (strcmp (names, list2dc) != 0) {
          sprintf
	    (err, "input field names should match %s: have %s", list2dc, names);
          Veclib::alert (prog, err, ERROR);
        } 
      } else if (strcmp (names, list3dc) != 0) {
        sprintf
	  (err, "input field names should match %s: have %s", list3dc, names);
        Veclib::alert (prog, err, ERROR);
      }
  } else {
    if (nvel == 2) {
      if (strcmp (names, list2d) != 0) {
	sprintf
	  (err, "input field names should match %s: have %s", list2d, names);
	Veclib::alert (prog, err, ERROR);
      } 
    } else if (strcmp (names, list3d) != 0) {
      sprintf
	(err, "input field names should match %s: have %s", list3d, names);
      Veclib::alert (prog, err, ERROR);
    }
  }

  // -- Turn ABC... into Reynolds stresses.

  in['A'] -> timesMinus (*in['u'], *in['u']);
  in['B'] -> timesMinus (*in['u'], *in['v']);
  in['C'] -> timesMinus (*in['v'], *in['v']);
  if (nvel == 3) {
    in['D'] -> timesMinus (*in['u'], *in['w']);
    in['E'] -> timesMinus (*in['v'], *in['w']);
    in['F'] -> timesMinus (*in['w'], *in['w']);
  }

  // -- At this stage, in['q'] still holds the total kinetic energy.
  //    Replace it with the mean flow kinetic energy.
 
  in['q'] -> times     (*in['u'], *in['u']);
  in['q'] -> timesPlus (*in['v'], *in['v']);
  if (nvel == 3)
    in['q'] -> timesPlus (*in['w'], *in['w']);
  *in['q'] *= 0.5;

  // -- Next we build the mean rate-of-strain tensor for further work.
  //    The '2d' components get stored in 'm', 'n', 'r' while the '3d'
  //    ones, if needed, go in 'o', 's', 't'. (We can use these input
  //    terms for storage since they are not needed for MKE.)

  (*in['m']  = *in['u']) . gradient (0);
  (*in['r']  = *in['v']) . gradient (1);
  (*in['n']  = *in['u']) . gradient (1);
  (*work[0]  = *in['v']) . gradient (0);
  (*in['n'] += *work[0]) *= 0.5;

  if (nvel == 3) {
    (*in['o']  = *in['u']).transform(FORWARD).gradient(2).transform(INVERSE);
    (*work[0]  = *in['w']).gradient(0);
    (*in['o'] += *work[0]) *= 0.5;
    (*in['s']  = *in['v']).transform(FORWARD).gradient(2).transform(INVERSE);
    (*work[0]  = *in['w']).gradient(1);
    (*in['s'] += *work[0]) *= 0.5;
    (*in['t']  = *in['w']).transform(FORWARD).gradient(2).transform(INVERSE);
  }

  // -- So let's get started by making term '1' (advection):

  *work[0]  = *in['q']; work[0]   -> gradient (0);
  *work[0] *= *in['u']; *out['1']  = *work[0];
  *work[0]  = *in['q']; work[0]   -> gradient (1);
  *work[0] *= *in['v']; *out['1'] += *work[0];
  if (nvel == 3) {
   (*work[0]  = *in['q']).transform(FORWARD).gradient(2).transform(INVERSE);
    *work[0] *= *in['w']; *out['1'] += *work[0];
  }

  // -- Term '2' (turbulent transport, zero for steady flow).  Destroy
  //    'x', 'y', 'z' (again, unused for MKE) along the way:

  in['x'] -> times     (*in['A'], *in['u']);
  in['x'] -> timesPlus (*in['B'], *in['v']);
  in['y'] -> times     (*in['B'], *in['u']);
  in['y'] -> timesPlus (*in['C'], *in['v']);
  if (nvel == 3) {
    in['x'] -> timesPlus (*in['D'], *in['w']);
    in['y'] -> timesPlus (*in['E'], *in['w']);
    in['z'] -> times     (*in['D'], *in['u']);
    in['z'] -> timesPlus (*in['E'], *in['v']);
    in['z'] -> timesPlus (*in['F'], *in['w']);
  }

  in['x'] -> gradient (0); *out['2']  = *in['x'];
  in['y'] -> gradient (1); *out['2'] += *in['y'];
  if (nvel == 3) {
    (in['z']  -> transform (FORWARD)) . gradient (2) . transform (INVERSE);
    *out['2'] += *in['z'];
  }
  
  *out['2'] *= -1.0;

  // -- Term '3' (pressure work).

  out['3'] -> times (*in['p'], *in['u']);
  out['3'] -> gradient (0);
  work[0]  -> times (*in['p'], *in['v']);
  work[0]  -> gradient (1); *out['3'] += *work[0];
  if (nvel == 3) {
    work[0]  -> times (*in['p'], *in['w']);
    (work[0] ->  transform (FORWARD)) . gradient (2) . transform (INVERSE);
    *out['3'] += *work[0];
  }

  // -- NB: what Jagmohan had below for '3' over-rides the above and
  //    is only suitable for cases like pipe or channel flows with z
  //    as the axial direction.  More thought required.
  
  *out['3'] = *in['w'];
  *out['3'] *= -FFZ;  // -- Pressure gradient is negative.
  *out['3'] *= -1.0; 

  // -- Compute term '4' (viscous transport), somewhat like term '2'.
  //    Again we can use 'x', 'y', 'z' as scratch.  Recall that 'm',
  //    'n', 'o', 'r', 's', 't' now contain mean flow RoS tensor.

  in['x'] -> times     (*in['m'], *in['u']);
  in['x'] -> timesPlus (*in['n'], *in['v']);
  in['y'] -> times     (*in['n'], *in['u']);
  in['y'] -> timesPlus (*in['r'], *in['v']);
  if (nvel == 3) {
    in['x'] -> timesPlus (*in['o'], *in['w']);
    in['y'] -> timesPlus (*in['s'], *in['w']);
    in['z'] -> times     (*in['o'], *in['u']);
    in['z'] -> timesPlus (*in['s'], *in['v']);
    in['z'] -> timesPlus (*in['t'], *in['w']);
  }
  *in['x'] *= *in['l'];
  *in['y'] *= *in['l'];
  in['x'] -> gradient (0); *out['4']  = *in['x'];
  in['y'] -> gradient (1); *out['4'] += *in['y'];
  if (nvel == 3) {
    *in['z'] *= *in['l'];
    (in['z']  -> transform (FORWARD)) . gradient (2) . transform (INVERSE);
    *out['4'] += *in['z'];
  }
  
  *out['4'] *= 2.0;
   
  // -- Compute term '5' (turbulent viscous transport), somewhat like
  //    term '4'.  Again recall that 'm', 'n', 'o', 'r', 's', 't' now
  //    contain mean flow RoS tensor. Scratch: 'x', 'y', 'z'.

     
  in['R'] -> timesMinus (*in['l'], *in['m']);
  in['S'] -> timesMinus (*in['l'], *in['n']);
  in['T'] -> timesMinus (*in['l'], *in['r']);
  if (nvel == 3) {
    in['U'] -> timesMinus (*in['l'], *in['o']);
    in['V'] -> timesMinus (*in['l'], *in['s']);
    in['W'] -> timesMinus (*in['l'], *in['t']);
  }
  
  in['x'] -> times     (*in['R'], *in['u']);
  in['x'] -> timesPlus (*in['S'], *in['v']);
  in['y'] -> times     (*in['S'], *in['u']);
  in['y'] -> timesPlus (*in['T'], *in['v']);
  if (nvel == 3) {
    in['x'] -> timesPlus (*in['U'], *in['w']);
    in['y'] -> timesPlus (*in['V'], *in['w']);
    in['z'] -> times     (*in['U'], *in['u']);
    in['z'] -> timesPlus (*in['V'], *in['v']);
    in['z'] -> timesPlus (*in['W'], *in['w']);
  }

  in['x'] -> gradient (0); *out['5']  = *in['x'];
  in['y'] -> gradient (1); *out['5'] += *in['y'];
  if (nvel == 3) {
    (in['z']  -> transform (FORWARD)) . gradient (2) . transform (INVERSE);
    *out['5'] += *in['z'];
  }
  
  *out['5'] *= 2.0;

  // -- Compute term '6' (turbulent viscous stress dissipation):

  *in['S'] *= sqrt (2.0);
  if (nvel == 3) {
    *in['U'] *= sqrt (2.0);
    *in['V'] *= sqrt (2.0);
  }

  out['6'] -> times     (*in['R'], *in['m']);
  out['6'] -> timesPlus (*in['S'], *in['n']);
  out['6'] -> timesPlus (*in['T'], *in['r']);
  if (nvel == 3) {
    out['6'] -> timesPlus (*in['U'], *in['o']);
    out['6'] -> timesPlus (*in['V'], *in['s']);
    out['6'] -> timesPlus (*in['W'], *in['t']);
  }
  
  *out['6'] *= -2.0;
  
  // -- Compute term '7' (mean flow dissipation):

  *in['n'] *= sqrt (2.0);
  if (nvel == 3) {
    *in['o'] *= sqrt (2.0);
    *in['s'] *= sqrt (2.0);
  }

  out['7'] -> times     (*in['m'], *in['m']);
  out['7'] -> timesPlus (*in['n'], *in['n']);
  out['7'] -> timesPlus (*in['r'], *in['r']);
  if (nvel == 3) {
    out['7'] -> timesPlus (*in['o'], *in['o']);
    out['7'] -> timesPlus (*in['s'], *in['s']);
    out['7'] -> timesPlus (*in['t'], *in['t']);
  }
  *out['7'] *= *in['l'];
  *out['7'] *= -2.0;
  
  // -- Compute term '0' (turbulence production, zero for steady flow):

  *in['n'] *= sqrt (2.0);
  if (nvel == 3) {
    *in['o'] *= sqrt (2.0);
    *in['s'] *= sqrt (2.0);
  }
 
  out['0'] -> times     (*in['A'], *in['m']);
  out['0'] -> timesPlus (*in['B'], *in['n']);
  out['0'] -> timesPlus (*in['C'], *in['r']);
  if (nvel == 3) {
    out['0'] -> timesPlus (*in['D'], *in['o']);
    out['0'] -> timesPlus (*in['E'], *in['s']);
    out['0'] -> timesPlus (*in['F'], *in['t']);
  }

  // -- Finally, compute the sum, 'S' (should converge to zero):

 (*out['S']  = *out['1']) *= -1.0;  // -- Term 1 is on the left of MKE equation.
  *out['S'] += *out['2'];
  *out['S'] += *out['3'];
  *out['S'] += *out['4'];
  *out['S'] += *out['5'];
  *out['S'] += *out['6'];
  *out['S'] += *out['7'];
  *out['S'] += *out['0'];
}


static void covary  (map<char, AuxField*>& in  ,
		     map<char, AuxField*>& out ,
		     vector<AuxField*>&    work)
// ---------------------------------------------------------------------------
// This does the actual work of building TKE equation terms.
// ---------------------------------------------------------------------------
{
  const char  list2d[]  = "ABCKLMRSTdefgijlmnpqrsuvxy";
  const char  list3d[]  = "ABCDEFKLMNOPRSTUVWdefghijklmnopqrstuvwxyz";
  const char  list2dc[] = "ABCGHJKLMRSTcdefgijlmnpqrsuvxy";
  const char  list3dc[] = "ABCDEFGHIJKLMNOPRSTUVWcdefghijklmnopqrstuvwxyz";
  
  const char* names     = fieldNames (in);
  const int   nvel      = (strchr (names, 'w')) ? 3 : 2;
  const int   scalar    = (strchr (names, 'c')) ? 1 : 0;  
  char        err[StrMax];

  if (nvel == 2) {
    if (strcmp (names, list2d) != 0) {
      sprintf (err,"list of names should match %s: have %s", list2d, names);
      Veclib::alert (prog, err, ERROR);
    } 
  } else if (strcmp (names, list3d) != 0) {
    sprintf (err,"list of names should match %s: have %s", list3d, names);
    Veclib::alert (prog, err, ERROR);
  }

  // -- Deal with all the correlations first, before any differentiation.

  // -- Turn ABC... into Reynolds stresses.

  in['A'] -> timesMinus (*in['u'], *in['u']);
  in['B'] -> timesMinus (*in['u'], *in['v']);
  in['C'] -> timesMinus (*in['v'], *in['v']);
  if (nvel == 3) {
    in['D'] -> timesMinus (*in['u'], *in['w']);
    in['E'] -> timesMinus (*in['v'], *in['w']);
    in['F'] -> timesMinus (*in['w'], *in['w']);
  }

  // -- At this stage, in['q'] still holds the total kinetic energy.

  in['r'] -> timesMinus (*in['A'], *in['u']);
  in['r'] -> timesMinus (*in['B'], *in['v']);
  in['r'] -> timesMinus (*in['q'], *in['u']);
  in['s'] -> timesMinus (*in['B'], *in['u']);
  in['s'] -> timesMinus (*in['C'], *in['v']);
  in['s'] -> timesMinus (*in['q'], *in['v']);
  if (nvel == 3) {
    in['r'] -> timesMinus (*in['D'], *in['w']);
    in['s'] -> timesMinus (*in['E'], *in['w']);
    in['t'] -> timesMinus (*in['D'], *in['u']);
    in['t'] -> timesMinus (*in['E'], *in['v']);
    in['t'] -> timesMinus (*in['F'], *in['w']);
    in['t'] -> timesMinus (*in['q'], *in['w']);
  }

  // -- Rework in['q'] so it holds the fluctuating KE.

  *in['q'] = *in['A']; *in['q'] += *in['C'];
  if (nvel == 3) *in['q'] += *in['F'];
  *in['q'] *= 0.5;

  // -- The pressure-velocity covariances: m, n (o).

  in['m'] -> timesMinus (*in['p'], *in['u']);
  in['n'] -> timesMinus (*in['p'], *in['v']);
  if (nvel == 3) in['o'] -> timesMinus (*in['p'], *in['w']);

  // -- Everything from now on involves derivatives in one way or another.

  // -- So let's get started by making term '1' (advection):

  *work[0]  = *in['q']; work[0]   -> gradient (0);
  *work[0] *= *in['u']; *out['1']  = *work[0];
  *work[0]  = *in['q']; work[0]   -> gradient (1);
  *work[0] *= *in['v']; *out['1'] += *work[0];
  if (nvel == 3) {
   (*work[0]  = *in['q']).transform(FORWARD).gradient(2).transform(INVERSE);
    *work[0] *= *in['w']; *out['1'] += *work[0];
  }

  // -- Term '2' (turbulent transport). Destroy 'r', 's', 't' along the way:
  
  in['r'] -> gradient (0); *out['2']  = *in['r'];
  in['s'] -> gradient (1); *out['2'] += *in['s'];
  if (nvel == 3) {
    (in['t']  -> transform (FORWARD)) . gradient (2) . transform (INVERSE);
    *out['2'] += *in['t'];
  }

  *out['2'] *= -1.0;

  // -- Term '3' (pressure work), destroying 'm', 'n', 'o' as we go:

  in['m'] -> gradient (0); *out['3']  = *in['m'];
  in['n'] -> gradient (1); *out['3'] += *in['n'];
  if (nvel == 3) {
    (in['o'] ->  transform (FORWARD)) . gradient (2) . transform (INVERSE);
    *out['3'] += *in['o'];
  }

  *out['3'] *= -1.0;

  // -- Next we build the mean rate-of-strain tensor for further work.
  //    The '2d' components get stored in 'm', 'n', 'o' while 
  //    the '3d' ones, if needed,   go in 'r', 's', 't'.

  (*in['m']  = *in['u']) . gradient (0);
  (*in['o']  = *in['v']) . gradient (1);
  (*in['n']  = *in['u']) . gradient (1);
  (*work[0]  = *in['v']) . gradient (0);
  (*in['n'] += *work[0]) *= 0.5;

  if (nvel == 3) {
    (*in['r']  = *in['u']).transform(FORWARD).gradient(2).transform(INVERSE);
    (*work[0]  = *in['w']).gradient(0);
    (*in['r'] += *work[0]) *= 0.5;
    (*in['s']  = *in['v']).transform(FORWARD).gradient(2).transform(INVERSE);
    (*work[0]  = *in['w']).gradient(1);
    (*in['s'] += *work[0]) *= 0.5;
    (*in['t']  = *in['w']).transform(FORWARD).gradient(2).transform(INVERSE);
  }

  // -- Reduce the rate-of-strain correlations to covariances.

  *in['K'] -= *in['m'];
  *in['L'] -= *in['n'];
  *in['M'] -= *in['o'];

  if (nvel == 3) {
    *in['N'] -= *in['r'];
    *in['O'] -= *in['s'];
    *in['P'] -= *in['t'];
  }

  // -- Compute term '4' (viscous transport):

  work[0] -> times (*in['m'], *in['u']);
  work[1] -> times (*in['n'], *in['v']);
  *work[0] += *work[1];
  if (nvel == 3) {
    work[1] -> times (*in['r'], *in['w']);
    *work[0] += *work[1];
  }
  (*in['x'] -= *work[0]) *= *in['l'];
  (*work[0]  = *in['x']) . gradient (0);
  *out['4']  = *work[0];

  work[0] -> times (*in['n'], *in['u']);
  work[1] -> times (*in['o'], *in['v']);
  *work[0] += *work[1];
  if (nvel == 3) {
    work[1] -> times (*in['s'], *in['w']);
    *work[0] += *work[1];
  }
  (*in['z'] -= *work[0]) *= *in['l'];
  (*work[0]  = *in['z']) . gradient (1);
  *out['4'] += *work[0];

  if (nvel == 3) {
    work[0] -> times (*in['r'], *in['u']);
    work[1] -> times (*in['s'], *in['v']);
    *work[0] += *work[1];
    work[1] -> times (*in['t'], *in['w']);
    *work[0] += *work[1];
    (*in['z'] -= *work[0]) *= *in['l'];
    (*work[0]  = *in['z']).transform(FORWARD).gradient(2).transform(INVERSE);
    *out['4'] += *work[0];
  }
  
  *out['4'] *= 2.0;

  // -- Compute term '5':
  
  in['f'] -> timesMinus (*in['l'], *in['u']);
  in['g'] -> timesMinus (*in['l'], *in['v']);
  if (nvel == 3) in['h'] -> timesMinus (*in['l'], *in['w']);

  work[0] -> times     (*in['m'], *in['f']);
  work[0] -> timesPlus (*in['n'], *in['g']);
  if (nvel == 3) work[0] -> timesPlus (*in['r'], *in['h']);
  work[0] -> gradient (0);
  *out['5'] = *work[0];

  work[0] -> times     (*in['n'], *in['f']);
  work[0] -> timesPlus (*in['o'], *in['g']);
  if (nvel == 3) work[0] -> timesPlus (*in['s'], *in['h']);
  work[0] -> gradient (1);
  *out['5'] += *work[0];

  if (nvel == 3) {
    work[0] -> times     (*in['r'], *in['f']);
    work[0] -> timesPlus (*in['s'], *in['g']);
    work[0] -> timesPlus (*in['t'], *in['h']);
    work[0] -> transform(FORWARD).gradient(2).transform(INVERSE);
    *out['5'] += *work[0];
  }
  
  *out['5'] *= 2.0;

  // -- Now, "out of order", compute term '8':

  in['R'] -> timesMinus (*in['l'], *in['m']);
  in['S'] -> timesMinus (*in['l'], *in['n']);
  in['T'] -> timesMinus (*in['l'], *in['o']);
  if (nvel == 3) {
    in['U'] -> timesMinus (*in['l'], *in['r']);
    in['V'] -> timesMinus (*in['l'], *in['s']);
    in['W'] -> timesMinus (*in['l'], *in['t']);
  }

  out['8'] -> times     (*in['R'], *in['m']);
  out['8'] -> timesPlus (*in['T'], *in['o']);
  if (nvel == 3) out['8'] -> timesPlus (*in['W'], *in['t']);
  work[0] -> times (*in['S'], *in['n']);
  if (nvel == 3) {
    work[0] -> timesPlus (*in['U'], *in['r']);
    work[0] -> timesPlus (*in['V'], *in['s']);
  }
  *work[0] *= 2.0;
  (*out['8'] += *work[0]) *= -2.0;

  // -- Compute term '6':

  *in['i'] -= *in['x'];
  *in['j'] -= *in['y'];
  if (nvel == 3) *in['k'] -= *in ['z'];
  in['i'] -> timesMinus (*in['R'], *in['u']);
  in['i'] -> timesMinus (*in['S'], *in['v']);
  if (nvel == 3) in['i'] -> timesMinus (*in['U'], *in['w']);
  in['j'] -> timesMinus (*in['S'], *in['u']);
  in['j'] -> timesMinus (*in['T'], *in['v']);
  if (nvel == 3) in['j'] -> timesMinus (*in['V'], *in['w']);
  if (nvel == 3) {
    in['k'] -> timesMinus (*in['P'], *in['u']);
    in['k'] -> timesMinus (*in['Q'], *in['v']);
    in['k'] -> timesMinus (*in['R'], *in['w']);
  }
  in['i'] -> timesMinus (*in['m'], *in['f']);
  in['i'] -> timesMinus (*in['n'], *in['g']);
  if (nvel == 3) in['i'] -> timesMinus (*in['r'], *in['h']);
  in['j'] -> timesMinus (*in['n'], *in['f']);
  in['j'] -> timesMinus (*in['o'], *in['g']);
  if (nvel == 3) in['j'] -> timesMinus (*in['s'], *in['h']);
  if (nvel == 3) {
    in['k'] -> timesMinus (*in['r'], *in['f']);
    in['k'] -> timesMinus (*in['s'], *in['g']);
    in['k'] -> timesMinus (*in['t'], *in['h']);
  }
  work[0] -> times     (*in['m'], *in['u']);
  work[0] -> timesPlus (*in['n'], *in['v']);
  if (nvel == 3) work[0] -> timesPlus (*in['r'], *in['w']);
  *work[0] *= *in['l'];
  *in['i'] -= *work[0];
  work[0] -> times     (*in['n'], *in['u']);
  work[0] -> timesPlus (*in['o'], *in['v']);
  if (nvel == 3) work[0] -> timesPlus (*in['s'], *in['w']);
  *work[0] *= *in['l'];
  *in['j'] -= *work[0];
  if (nvel == 3) {
      work[0] -> times     (*in['r'], *in['u']);
      work[0] -> timesPlus (*in['s'], *in['v']);
      work[0] -> timesPlus (*in['t'], *in['w']);
      *work[0] *= *in['l'];
      *in['k'] -= *work[0];
  }
  in['i'] -> gradient (0);
  in['j'] -> gradient (1);
  if (nvel == 3) in['k'] -> transform(FORWARD).gradient(2).transform(INVERSE);
  *out['6']  = *in['i'];
  *out['6'] += *in['j'];
  if (nvel == 3) *out['6'] += *in['k'];
  
  *out['6'] *= 2.0;
  
  // -- Compute term '7' (dissipation):

  *in['n'] *= sqrt (2.0);
  if (nvel == 3) {
    *in['r'] *= sqrt (2.0);
    *in['s'] *= sqrt (2.0);
  }

  in['d'] -> timesMinus (*in['m'], *in['m']);
  in['d'] -> timesMinus (*in['n'], *in['n']);
  in['d'] -> timesMinus (*in['o'], *in['o']);
  if (nvel == 3) {
    in['d'] -> timesMinus (*in['r'], *in['r']);
    in['d'] -> timesMinus (*in['s'], *in['s']);
    in['d'] -> timesMinus (*in['t'], *in['t']);
  }
  (*out['7'] = *in['d']) *= *in['l'];
  *out['7'] *= -2.0;

  // -- Compute term 9:
  //    Note we have already premultiplied 'n', 'r', 's' by sqrt(2).

  work[0] -> times (*in['N'], *in['n']);
  if (nvel == 3) {
    work[0] -> timesPlus (*in['U'], *in['r']);
    work[0] -> timesPlus (*in['V'], *in['s']);
  }
  *work[0] *= sqrt (2.0);
  work[0] -> timesPlus (*in['R'], *in['m']);
  work[0] -> timesPlus (*in['T'], *in['o']);
  if (nvel == 3) work[0] -> timesPlus (*in['W'], *in['t']);
  *work[0] *= 2.0;
  *in['e'] -= *work[0];
  
  in['e'] -> timesMinus (*in['d'], *in['l']);
  
  work[0] -> times (*in['n'], *in['n']);
  if (nvel == 3) {
    work[0] -> timesPlus (*in['r'], *in['r']);
    work[0] -> timesPlus (*in['s'], *in['s']);
  }
  work[0] -> timesPlus (*in['m'], *in['m']);
  work[0] -> timesPlus (*in['o'], *in['o']);
  if (nvel == 3) work[0] -> timesPlus (*in['t'], *in['t']);
  *work[0] *= *in['l'];

  *in['e'] -= *work[0];

  (*out['9'] = *in['e']) *= -2.0;
  
  // -- Compute term '0' (production):

  *in['n'] *= sqrt (2.0);
  if (nvel == 3) {
    *in['r'] *= sqrt (2.0);
    *in['s'] *= sqrt (2.0);
  }
 
  out['0'] -> times     (*in['A'], *in['m']);
  out['0'] -> timesPlus (*in['B'], *in['n']);
  out['0'] -> timesPlus (*in['C'], *in['o']);
  if (nvel == 3) {
    out['0'] -> timesPlus (*in['D'], *in['r']);
    out['0'] -> timesPlus (*in['E'], *in['s']);
    out['0'] -> timesPlus (*in['F'], *in['t']);
  }

  *out['0'] *= -1.0;

  // -- Finally, compute the sum, 'S':

 (*out['S']  = *out['1']) *= -1.0;
  *out['S'] += *out['2'];
  *out['S'] += *out['3'];
  *out['S'] += *out['4'];
  *out['S'] += *out['5'];
  *out['S'] += *out['6'];
  *out['S'] += *out['7'];
  *out['S'] += *out['8'];
  *out['S'] += *out['9'];
  *out['S'] += *out['0'];
}

