///////////////////////////////////////////////////////////////////////////////
// viscosity.cpp: calculate non-Newtonian viscosity for gnewt.
//
// Copyright (c) 1998+,  Murray Rudman & Hugh M Blackburn
///////////////////////////////////////////////////////////////////////////////

#include <gnewt.h>

static int NCOM, NDIM;

static void strainRate  (const Domain*, AuxField**, AuxField**);
static void viscoModel  (const Domain*, AuxField**, AuxField**, AuxField*);


void viscosity (const Domain* D  ,
		AuxField**    Us ,
		AuxField**    Uf ,
		AuxField*     GNV)
// ---------------------------------------------------------------------------
// Compute the strain-rate field and GNV, the generalized Newtonian
// viscosity field from it.  (Actually, the GN viscosity minus
// REFVIS.)
//
// On entry, D contains velocity (and pressure) fields, and the first
// levels of Us & Uf are free.  The diagonal entries of the rate of
// strain tensor are left in Us, and the off-diagonal entries are left
// in Uf (here for 3D):
//
//                      / Us[0]  Uf[0]  Uf[1] \
//                S =   |    .   Us[1]  Uf[2] |
//                ~     \    .       .  Us[2] /
//
// N.B.: all inputs and outputs are in Fourier-transformed state (if 3D).
// ---------------------------------------------------------------------------
{
  NDIM = Geometry::nDim();
  NCOM = D -> nVelCmpt();
 
  strainRate  (D, Us, Uf);
#if 1
#if defined (NOMODEL)
  *GNV = 0.0;
  ROOTONLY GNV -> addToPlane (0, Femlib::value ("-REFVIS"));
#else
  viscoModel  (D, Us, Uf, GNV);
  ROOTONLY GNV -> addToPlane (0, Femlib::value ("-KINVIS"));
#endif  
#endif
}


static void strainRate (const Domain* D ,
			AuxField**    Us,
			AuxField**    Uf)
// ---------------------------------------------------------------------------
// On entry D contains the velocity fields Ui (in Fourier space) and
// the first-level areas of Us and Uf are free.  Construct the
// symmetric strain-rate tensor terms.
// 
// For Cartesian coordinates
//           
//             / du/dx  1/2(du/dy + dv/dx)  1/2(du/dz + dw/dx) \
//       S =   |    .           dv/dy       1/2(dv/dz + dw/dy) |,
//       ~     \    .             .                 dw/dz      /
//
// Cartesian is straightforward: leave the diagonal terms in Us and
// the off-diagonal terms in Uf.  This works fine for 2D/2C, 2D/3C, 3D/3C.
//  
// 3D/3C stored like:
//             / Us[0]  Uf[0]  Uf[1] \
//       S =   |    .   Us[1]  Uf[2] |,
//       ~     \    .    .     Us[2] /
//
// 2D/3C stored like:
//             / Us[0]  Uf[0]  Uf[1] \
//       S =   |    .   Us[1]  Uf[2] |,
//       ~     \    .    .      0    /
//
// 2D/2C stored like:
//             / Us[0]  Uf[0] \
//       S =   \    .   Us[1] /,        (Uf[1] not used),
//       ~    
//
// while for cylindrical coordinates (e.g. Batchelor A2)
//           
//         / du/dx  1/2(du/dy + dv/dx)  1/2(1/y*du/dz + dw/dx)       \
//   S =   |    .           dv/dy       1/2(1/y*dv/dz + dw/dy - w/y) |.
//   ~     \    .             .             1/y*dw/dz         + v/y  /
//
//
// Cylindrical is more complicated owing to the cross-coupling terms,
// especially v/y.  Always there is enough total storage in Us and Uf,
// we just have to pack it in different ways.
//
// 3D/3C stored like (as for Cartesian):
//             / Us[0]  Uf[0]  Uf[1] \
//       S =   |    .   Us[1]  Uf[2] |,
//       ~     \    .    .     Us[2] /
//
// 2D/3C stored like (same):
//             / Us[0]  Uf[0]  Uf[1] \
//       S =   |    .   Us[1]  Uf[2] |,
//       ~     \    .    .     Us[2] /
//
// 2D/2C stored like (Us is only 2 long -- the diagonal term v/y goes to Uf[1]):
//             / Us[0]  Uf[0]   0    \
//       S =   |    .   Us[1]   0    |,
//       ~     \    .    .     Uf[1] /
//
// ---------------------------------------------------------------------------
{
  int       i, j;
  AuxField* tp1 = Us[0];
  AuxField* tp2 = Us[1];

  if (Geometry::cylindrical()) {

    if (NDIM == 2 && NCOM == 2) {

      (*Uf[0] = *D -> u[0]) . gradient (1);
      (*tp1   = *D -> u[1]) . gradient (0);
      (*Uf[0] += *tp1) *= 0.5;

      (*Us[0] = *D -> u[0]) . gradient (0);
      (*Us[1] = *D -> u[1]) . gradient (1);

      // -- Diagonal "odd-ball" hoop-strain term v/y gets placed into Uf[1].

      (*Uf[1] = *D -> u[1]) . divY();

      // -- Then any on-axis terms are replaced using l'Hopital's rule.

      D -> u[0] -> overwriteForGroup ("axis", Us[1], Uf[1]);

    } else if (NDIM == 2 && NCOM == 3) {

      (*Uf[0] = *D -> u[0]) . gradient (1);
      (*tp1   = *D -> u[1]) . gradient (0);
      (*Uf[0] += *tp1) *= 0.5;
      
      (*Uf[1] = *D -> u[2]) . gradient (0) *= 0.5;

      (*Uf[2] = *D -> u[2]) . gradient (1);
      (*tp1 = *D -> u[2]) . divY ();
      D -> u[0] -> overwriteForGroup ("axis", Uf[2], tp1);
      (*Uf[2] -= *tp1) *= 0.5;	

      (*Us[0] = *D -> u[0]) . gradient (0);
      (*Us[1] = *D -> u[1]) . gradient (1);
      
      // -- Note Us[2] gets the "off-diagonal" term v/y in 2D3C.
      
      (*Us[2] = *D -> u[1]) . divY ();
      D -> u[0] -> overwriteForGroup ("axis", Us[1], Us[2]);

    } else {
      
      // -- The "standard" 3D/3C case.

      for (i = 0; i < NCOM; i++)
	for (j = 0; j < NDIM; j++) {
	  if (j == i) continue;
	  if (i == 2 && j == 1) {
	    (*tp1 = *D -> u[2]) . gradient (1);
	    (*tp2 = *D -> u[2]) . divY();
	    D -> u[0] -> overwriteForGroup ("axis", tp1, tp2);
	    *tp1 -= *tp2;
	  } else {
	    (*tp1 = *D -> u[i]) . gradient (j);
	    if (j == 2) tp1 -> divY();
	  }
	  if   (j > i) *Uf[i + j - 1]  = *tp1;
	  else         *Uf[i + j - 1] += *tp1;
	}
  
      for (i = 0; i < NCOM; i++) *Uf[i] *= 0.5;
  
      // -- Diagonal.

      for (i = 0; i < NDIM; i++) {
	(*Us[i] = *D -> u[i]) . gradient (i);
	if (i == 2) {
	  Us[2] -> divY();
	  (*tp1 = *D -> u[1]) . divY();
	  D -> u[0] -> overwriteForGroup ("axis", Us[1], tp1);
	  *Us[2] += *tp1;
	}
      }
    }

  } else {			// -- Cartesian geometry.

    if (NDIM == 2 && NCOM == 2) {

      (*Uf[0] = *D -> u[0]) . gradient (1);
      (*tp1   = *D -> u[1]) . gradient (0);
      (*Uf[0] += *tp1) *= 0.5;

      (*Us[0] = *D -> u[0]) . gradient (0);
      (*Us[1] = *D -> u[1]) . gradient (1);

    } else if (NDIM == 2 && NCOM == 3) {

      (*Uf[0] = *D -> u[0]) . gradient (1);
      (*tp1   = *D -> u[1]) . gradient (0);
      (*Uf[0] += *tp1) *= 0.5;
      
      (*Uf[1] = *D -> u[2]) . gradient (0) *= 0.5;
      (*Uf[2] = *D -> u[2]) . gradient (1) *= 0.5;

      (*Us[0] = *D -> u[0]) . gradient (0);
      (*Us[1] = *D -> u[1]) . gradient (1);
      *Us[2] = 0.0;

    } else {  // -- The "standard" 3D/3C case.

      // -- Off-diagonal.

      for (i = 0; i < NCOM; i++) {
	for (j = 0; j < NCOM; j++) {
	  if (i == j) continue;
	  (*tp1 = *D -> u[j]) . gradient (i);
	  if (j > i) *Uf[i + j - 1]  = *tp1;
	  else       *Uf[i + j - 1] += *tp1;
	}
      }
      
      for (i = 0; i < NCOM; i++) *Uf[i] *= 0.5;

      // -- Diagonal.

      for (i = 0; i < NCOM; i++) (*Us[i] = *D -> u[i]) . gradient (i);
    }
  }
}


static void viscoModel (const Domain* D  ,
			AuxField**    Us ,
			AuxField**    Uf ,
			AuxField*     GNV)
// ---------------------------------------------------------------------------

// On entry the first-level areas of Us & Uf contain the components of
// the strain-rate tensor S (in Fourier space) and GNV contains the
// old values of non-Newtonian viscosity (gets overwritten).  On exit,
// by GNV contains the (Fourier transform of the) non-Newtonian
// viscosity field, which e.g. for the Cross model is:
//
// GNV = (\nu_0 + \nu_\inf (K|S|)^N)/ (1+(K|S|)^N), where
//
// |S| = sqrt{2[(S11)^2 + (S22)^2 + (S33)^2 + 2(S12)^2 + 2(S13)^2 + 2(S23)^2]},
//     = sqrt{2[Us[0]^2 + Us[1]^2 + Us[2]^2 + 2Uf[0]^2 + 2Uf[1]^2 + 2Uf[3]^2]}
//
// \nu_0 = KINVIS from session file (i.e. REFVIS below), \nu_\inf is
// the infinite shear rate viscosity and K and N are the cross model
// parameters stored in CROSS_K and CROSS_N.  Other non-Newtonian
// models can be defined as desired.
//
// As noted in the header to NS.cpp, GNV = -KINVIS for debugging (NB: Fourier!).
//
// The pressure storage area of D is free for use.  Us and Uf should be left
// unaltered.  GNV is the output.
//
// In forming |S|, the only distinction between Cartesian and cylindrical is
// the location of the "oddball" hoop strain term in Uf[1] for cylindrical
// 2D2C case.
// ---------------------------------------------------------------------------
{
  int       i, j;
  AuxField* SR = D -> u[D -> nAdvect()]; // -- SR = |S|, Strain Rate, gamma'.

  // -- Construct |S|, can use GNV as workspace.

  if (Geometry::cylindrical()) {

    if (NDIM == 2 && NCOM == 2) {

      (*GNV = *Uf[0]) . transform (INVERSE);
      (SR -> times (*GNV, *GNV)) *= 2.0;

      (*GNV = *Uf[1]) . transform (INVERSE); // -- "Oddball" hoop-strain term.
      *SR  += GNV -> times (*GNV, *GNV);     //    Is a diagonal element of S.

      (*GNV = *Us[1]) . transform (INVERSE);
      *SR  += GNV -> times (*GNV, *GNV);
 
      (*GNV = *Us[0]) . transform (INVERSE);
      *SR  += GNV -> times (*GNV, *GNV);

      *SR *= 2.0;

    } else {			// -- Other two cases (2D/3C and 3D/3C).

      *SR = 0.0;
      
      for (i = 0; i < NCOM; i++) {
	(*GNV = *Uf[i]) . transform (INVERSE);
	*SR  += GNV -> times (*GNV, *GNV);
      }
      *SR *= 2.0;

      for (i = 0; i < NCOM; i++) {
	(*GNV = *Us[i]) . transform (INVERSE);
	*SR  += GNV -> times (*GNV, *GNV);
      }
      *SR *= 2.0;      

    }

  } else {			// -- Cartesian.

    *SR = 0.0;

    if (NCOM == 2) {
      (*GNV = *Uf[0]) . transform (INVERSE);
      *SR  += GNV -> times (*GNV, *GNV);
    } else {
      for (i = 0; i < NCOM; i++) {
	(*GNV = *Uf[i]) . transform (INVERSE);
	*SR  += GNV -> times (*GNV, *GNV);
      }
    }
    *SR *= 2.0;

    for (i = 0; i < NCOM; i++) {
      (*GNV = *Us[i]) . transform (INVERSE);
      *SR  += GNV -> times (*GNV, *GNV);
    }
    *SR *= 2.0;
  }
  
  SR -> sqroot();		// -- SR = |S|; by construction, non-negative.

  // -- Smooth shear rate.

  SR -> smooth (D -> nGlobal(), D -> assemblyNaive(), D -> invMassNaive());
  
  // -- Now compute viscosity according to model, leave in GNV.

  if (Femlib::ivalue ("PowerLaw")) {

    // -- Power law model: visc = K*S**(N-1).

    static const double K = Femlib::value ("PL_K");
    static const double N = Femlib::value ("PL_N");
    static const double Gamma_min = Femlib::value ("PL_ZERO"); // Limit shear.

    if (Gamma_min > EPSDP) SR -> clipUp (Gamma_min);    //  Option for > 6e-14.
    
    *GNV = (SR -> pow (N - 1.0)) *= K;
    
  } else if (Femlib::ivalue ("HB")) {

    // -- Herschel--Bulkley model: visc = Tau_Y/S + K*S**(N-1).

    static const double Tau_Y     = Femlib::value ("YIELD_STRESS");
    static const double K         = Femlib::value ("HB_K");
    static const double N         = Femlib::value ("HB_N");
    static const double Gamma_min = Femlib::value ("HB_ZERO"); // Limit shear.
    
    if (Gamma_min > EPSDP) SR -> clipUp (Gamma_min);    // Option for > 6E-14.

    if (Tau_Y > EPSDP)	{	// -- OK, use HB formula.
      (*GNV = Tau_Y)      /= *SR;
      SR -> pow (N - 1.0) *= K;
      *GNV                += *SR;
    } else			// -- Effectively it's Power Law; ignore yield.
      *GNV = (SR -> pow (N - 1.0)) *= K;

  } else if (Femlib::ivalue ("CAR_YAS")) {

    // -- Carreau--Yasuda model:
    //    visc = nu_inf + (nu_0-nu_inf)(1+(lambda*S)**A))**((N-1)/A)

    static const double L      = Femlib::value ("CY_LAMBDA");
    static const double A      = Femlib::value ("CY_A");
    static const double EXP    = Femlib::value ("(CY_N-1.0)/CY_A");
    static const double mu_0   = Femlib::value ("VISC_ZERO");
    static const double mu_inf = Femlib::value ("VISC_INF");
    static const double mu_dif = mu_0 - mu_inf;

    *GNV = (((((*SR *= L) . pow (A)) += 1.0) . pow (EXP)) *= mu_dif) += mu_inf;

  } else {

    // -- Cross model: visc = nu_inf + (nu_0-nu_inf)/(1 + (lambda*S)^N)).

    static const double L      = Femlib::value ("CROSS_LAMBDA");
    static const double N      = Femlib::value ("CROSS_N");
    static const double mu_0   = Femlib::value ("VISC_ZERO");
    static const double mu_inf = Femlib::value ("VISC_INF");
    static const double mu_dif = mu_0 - mu_inf;

    ((*SR *= L ) . pow (N)) += 1.0;
    ((*GNV = mu_dif) /= *SR) += mu_inf;

  }

  // -- Transform back to Fourier space.
  
  GNV -> transform (FORWARD);
}


