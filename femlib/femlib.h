#ifndef FEMLIB_H
#define FEMLIB_H
///////////////////////////////////////////////////////////////////////////////
// C++ function prototypes for routines in library libfem.a
///////////////////////////////////////////////////////////////////////////////

#include <cstdio>
#include <cmath>

#include "cfemdef.h"

#if 0
#include "polylib.h"
#endif

extern "C" {

#if 1
  /* This gets used below for parser calls, declared in initial.y. */
  extern char _yy_vecbuf[STR_MAX];
#endif

// -- Routines from initial.y:

void   yy_initialize (void);
double yy_interpret  (const char*);

void   yy_vec_init   (const char*, const char*);
void   yy_vec_interp (const int, ...);

void   yy_help       (void);
void   yy_show       (void);

// -- Routines from polyops.c:

void   dermat_g (const int, const double*, const int,
		 const double*, double*, double*);
void   intmat_g (const int, const double*, const int,
		 const double*, double*, double*);
void   uniknot  (const int, double*);

double PNLEG    (const double, const int);
double PNMOD    (const double, const int);	
void   ZWGLL    (double*, double*, const int);

// -- Routines from operators.c:

void zquad (const double** point ,  // Quadrature points.
	    const double** weight,  // Quadrature weights.
	    const double** dv    ,  // Derivative operator at points.
	    const double** dt    ,  // (Transpose) derivative operator.
	    const int      np    ,  // Input: Number of quadrature points.
	    const char     rule  ,  // Input: 'G'auss, 'R'adau, or 'L'obatto.
	    const double   alpha ,  // Input: Jacobi polynomial constant.
	    const double   beta  ); // Input: Jacobi polynomial constant.

void proj  (const double** IN    ,  // Interpolant operator matrix
	    const double** IT    ,  // Transposed interpolant operator matrix
	    const int      nfrom ,  // Input: Number of "from" points
	    const char     rulefr,  // Input: 'G', 'R', 'L', or 'U', "from"
	    const double   alphfr,  // Input: Jacobi constant, "from"
	    const double   betafr,  // Input: Jacobi constant, "from"
	    const int      nto   ,  // Input: Number of "to" points
	    const char     ruleto,  // Input: 'G', 'R', 'L', or 'U', "to"
	    const double   alphto,  // Input: Jacobi constant, "to"
	    const double   betato); // Input: Jacobi constant, "to"

void intp  (double*        inr   ,  // 1D shape function at r
	    double*        ins   ,  // 1D shape function at s
	    double*        dvr   ,  // 1D shape function derivative at r
	    double*        dvs   ,  // 1D shape function derivative at s
	    const int      nr    ,  // Input: number of points, r-direction
	    const char     basisr,  // Input: 'G', 'R', 'L', r-direction
	    const double   alphar,  // Input: Jacobi constant, r-direction
	    const double   betar ,  // Input: Jacobi constant, r-direction
	    const int      ns    ,  // Input: number of points, s-direction
	    const char     basiss,  // Input: 'G', 'R', 'L', s-direction
	    const double   alphas,  // Input: Jacobi constant, s-direction
	    const double   betas ,  // Input: Jacobi constant, s-direction
	    const double   r     ,  // Input: location of r in [-1, 1]
	    const double   s     ); // Input: location of s in [-1, 1]

void dglldpc (const int      np,     // input:  number of points for Leg polys
	      const double** cd);    // output: pointer to table of coeffs

void dglldpt (const int      np,     // input:  number of points for DLT
	      const double** fw,     // output: 1D forward transform matrix
	      const double** ft,     // output: transpose of fw
	      const double** bw,     // output: 1D inverse transform matrix
	      const double** bt,     // output: transpose of bw
	      const double** fu,     // output: 2D forward transform matrix
	      const double** bu);    // output: 2D inverse transform matrix

void dglmdpc (const int      np,     // input:  no. of points for expansions.
	      const double** cd);    // output: pointer to table of coeffs

void dglmdpt (const int      np,     // input:  number of points for DPT
	      const double** fw,     // output: 1D forward transform matrix
	      const double** ft,     // output: transpose of fw
	      const double** bw,     // output: 1D inverse transform matrix
	      const double** bt,     // output: transpose of bw
	      const double** fu,     // output: 2D forward transform matrix
	      const double** bu);    // output: 2D inverse transform matrix

// -- Routines from mapping.c:

void edgemaps (const int np, const int dim, int** emap, int** pmap);

// -- Routines from family.c:

void iadopt   (const int, int**);
void dadopt   (const int, double**);
void sadopt   (const int, float**);

void iabandon (int**);
void dabandon (double**);
void sabandon (float**);

int  FamilySize (int*, int*, int*);

// -- Routines from fourier.c:

void preFFT (const int);
void dDFTr  (double*, const int, const int, const int);

// -- Routines from filter.c

void bvdFilter (const int, const double, const double, const double, double*);

// -- Routines from message.c:

void message_init      (int*, char***);
void message_stop      ();
void message_sync      ();
void message_dsend     (double*, const int, const int);
void message_drecv     (double*, const int, const int);
void message_ssend     (float*,  const int, const int);
void message_srecv     (float*,  const int, const int);
void message_isend     (int*,    const int, const int);
void message_irecv     (int*,    const int, const int);
void message_dexchange (double*, const int, const int, const int);
void message_sexchange (float*,  const int, const int, const int);
void message_iexchange (int*,  const int, const int, const int);

// -- FORTRAN
// -- Routines from sparsepak.F:

   // 1) SPARSPAK

void F77NAME(fnroot) (int&,int*,int*,int*,int&,int*,int*);
void F77NAME(rcm)    (const int&,int*,int*,int*,int*,int&,int*);
void F77NAME(gennd)  (const int&,int*,int*,int*,int*,int*,int*);

// -- Routines from netlib.f:  
  
  // 2) FFTPACK

void F77NAME(drffti) (const int&,double*,int*);
void F77NAME(drfftf) (const int&,double*,double*,const double*,const int*);
void F77NAME(drfftb) (const int&,double*,double*,const double*,const int*);

  // 3) Other
  
double F77NAME(fbrent) (const double&, const double&,
			double(*)(const double&), const double&);
void   F77NAME(braket) (double&, double&, double&, double&, double&, double&,
			double(*)(const double&));

// -- Routines from canfft.f:

void F77NAME(factor) (const int&, int&, int*);

void F77NAME(dpreft) (const int&, int&, int*, double*);
void F77NAME(dmrcft) (double*, const int&, const int&, double*,
 		      const int&, int*, double*, const int&);
void F77NAME(dfft1)  (double*, double*, const int&, const int&,
		      const int*, const int&, const double*,
		      const int&);

// -- Routines from temfftd.F:

void F77NAME(prf235) (int&, int&, int&, int&, int&);

void F77NAME(dsetpf) (const double*, const int&, const int&,
		      const int&, const int&);
void F77NAME(dmpfft) (double*, double*, const int&, const int&,
		      const int&, const int&, const int&, 
		      const double*, const int&);
void F77NAME(dgpfa)  (double*, double*, const double*, const int&,
		      const int&, const int&, const int&,
		      const int&, const int&, const int&,
		      const int&);

// -- Routines from matops.F:

void F77NAME(dgrad2) (const double*, const double*, double*, double*,
		      const double*, const double*, const int&,
		      const int&, const int&);
void F77NAME(dtpr2d) (const double*, double*, double*, const double*,
		      const double*, const int&, const int&, const int&);

}

class Femlib {
public:

#if 1  
  static void init ()
    { yy_initialize (); }
  
#else
   static void initialize (int* argc, char*** argv)
    { message_init (argc, argv); }
  static void prepVec (const char* v, const char* f)
    { yy_vec_init (v, f); }
  static void finalize () 
    { message_stop (); }
  static void synchronize ()
    { message_sync(); }
#endif

  static void prepVec (const char* v, const char* f)
    { yy_vec_init (v, f); }

#if 0
  // -- This form seems to cause a problem, hence workaround:
  static void parseVec (const int n ... )
    { yy_vec_interp (n ... ); }
#else
  #define Femlib__parseVec yy_vec_interp
#endif

  static void value (const char* s, const double p)
    { sprintf (_yy_vecbuf, "%s = %.17g", s, p);     yy_interpret (_yy_vecbuf); }
  static void ivalue (const char* s, const int p)
    { sprintf (_yy_vecbuf, "%s = %1d", s, p); rint (yy_interpret(_yy_vecbuf)); }
  static double value (const char* s)
    { return yy_interpret (s); }
  static int ivalue (const char* s)
    { return rint (yy_interpret (s)); }
  
  static void equispacedMesh (const int np, double* z)
    { uniknot (np, z); }

  static void LagrangeInt (const int N,  const double* zero,
			   const int I,  const double* x   ,
			   double*   IN, double*       IT  )
    { intmat_g (N, zero, I, x, IN, IT); }
  static void LagrangeDer (const int N,  const double* zero,
			   const int I,  const double* x   ,
			   double*   IN, double*       IT  )
    { dermat_g (N, zero, I, x, IN, IT); }

  static void   GLLzw (const int N,double* z,double* w)  {ZWGLL(z,w,N);}
  static double LegendreVal (const int N,const double x) {return PNLEG(x,N);}
  static double ModalVal    (const int N,const double x) {return PNMOD(x,N);}

  static void quadrature (const double** point ,
			  const double** weight,
			  const double** dv    ,
			  const double** dt    ,
			  const int      np    ,
			  const char     rule  ,
			  const double   alpha ,
			  const double   beta  )
  { zquad (point, weight, dv, dt, np, rule, alpha, beta); }

  static void projection (const double** IN    ,
			  const double** IT    ,
			  const int      nfrom ,
			  const char     rulefr,
			  const double   alphfr,
			  const double   betafr,
			  const int      nto   ,
			  const char     ruleto,
			  const double   alphto,
			  const double   betato)
  { proj (IN, IT, nfrom,rulefr,alphfr,betafr, nto,ruleto,alphto,betato); }

  static void interpolation (double*      inr   ,
			     double*      ins   ,
			     double*      dvr   ,
			     double*      dvs   ,
			     const int    nr    ,
			     const char   basisr,
			     const double alphar,
			     const double betar ,
			     const int    ns    ,
			     const char   basiss,
			     const double alphas,
			     const double betas ,
			     const double r     ,
			     const double s     )
  { intp (inr,ins,dvr,dvs,nr,basisr,alphar,betar,ns,basiss,alphas,betas,r,s); }

  static void legCoef (const int n,
		       const double**  c)
    { dglldpc (n, c); }

  static void legTran (const int n,
		       const double** f1, const double** ft,
		       const double** i1, const double** it,
		       const double** f2, const double** i2)
    { dglldpt (n, f1, ft, i1, it, f2, i2); }

  static void modCoef (const int    n,
		       const double** c)
    { dglmdpc (n, c); }

  static void modTran (const int n,
		       const double** f1, const double** ft,
		       const double** i1, const double** it,
		       const double** f2, const double** i2)
    { dglmdpt (n, f1, ft, i1, it, f2, i2); }

  static void buildMaps (const int np, const int dim,
			 int** e, int** i)
    { edgemaps (np, dim, e, i); }

  static void adopt (const int np, int** v)
    { iadopt (np, v); }
  static void adopt (const int np, double** v)
    { dadopt (np, v); }
  static void adopt (const int np, float** v)
    { sadopt (np, v); }
  static void abandon (int** v)
    { iabandon (v); }
  static void abandon (double** v)
    { dabandon (v); }
  static void abandon (float** v)
    { sabandon (v); }
  static int fwords (int* ni, int* nd, int* ns)
    { return FamilySize (ni, nd, ns); }

  static void fnroot (int& r, int* x, int* a, int* m,
		      int& n, int* l, int* p) 
    { F77NAME(fnroot) (r, x, a, m, n, l, p); }
  static void rcm    (const int& n, int* x, int* a, int* m,
		      int* p, int& c, int* l)
    { F77NAME(rcm)   (n, x, a, m, p, c, l); }
  static void gennd  (const int& n, int* x, int* a, int* m,
		      int* p, int* c, int* l)
    { F77NAME(gennd) (n, x, a, m, p, c, l); }

  static void rffti (const int& n , double* w, int* i)
    { F77NAME(drffti) (n, w, i); }
  static void rfftf (const int& n, double* c, double* ch,
	const double* wa, const int* ifac)
    { F77NAME(drfftf) (n, c, ch, wa, ifac); }
  static void rfftb (const int& n, double* c, double* ch,
  	const double* wa, const int* ifac) 
    { F77NAME(drfftb) (n, c, ch, wa, ifac); }

  static double brent  (const double& ax, const double& bx,
			double(*f)(const double&),
			const double& tol) {
    return F77NAME(fbrent) (ax, bx, f, tol);
  }
  static void bracket   (double& ax, double& bx, double& cx,
			 double& fa, double& fb, double& fc,
			 double(*func)(const double&)) {
    F77NAME(braket) (ax, bx, cx, fa, fb, fc, func);
  }
  
  static void DFTr (double*  data, const int nz, const int np,
		    const int sign)
    { dDFTr (data, nz, np, sign); }

  static void preft  (const int& n, int& nfax, int* ifax,
		      double* trig)
    { F77NAME(dpreft) (n, nfax, ifax, trig); }
  static void mrcft  (double* v, const int& np, const int& nz,
		      double* w, const int& nfx, int* ifx,
		      double* trig, const int& sign)
    { F77NAME(dmrcft) (v, np, nz, w, nfx, ifx, trig, sign); }
  static void primes23 (const int& n, int& np, int* fact)
    { F77NAME(factor) (n, np, fact); }
  static void fft1 (double* a, double* c, const int& n,
		    const int& nfax,
                    const int* ifax, const int& isign,
		    const double* trig, const int& len)
    { F77NAME(dfft1) (a,c,n,nfax,ifax,isign,trig,len); }  

  static void primes235 (int& n,
			 int&ip, int&iq, int& ir, int& ipqr2)
    { F77NAME(prf235) (n, ip, iq, ir, ipqr2); }
  static void setpf (const double* t, const int& n, const int& ip,
		     const int& iq, const int& ir)
    { F77NAME(dsetpf) (t, n, ip, iq, ir); }
  static void mpfft (double* v, double* w, const int& np,
		     const int& nz,
		     const int& ip, const int& iq, const int& ir,
		     const double* trig, const int& isign)
    { F77NAME(dmpfft) (v, w, np, nz, ip, iq, ir, trig, isign); }
  static void gpfa (double* a, double* b, const double* trig,
		    const int& inc, const int& jump, const int& n,
		    const int& ip, const int& iq, const int& ir,
		    const int& lot, const int& isign)
    { F77NAME(dgpfa) (a, b, trig, inc, jump, n, ip, iq, ir, lot, isign); }

#if 0				// -- Moving these to src/message.cpp.
  static void initialize (int* argc, char*** argv)
    { message_init (argc, argv); }
  static void finalize () 
    { message_stop (); }
  static void synchronize ()
    { message_sync(); }
  static void send  (double* data, const int N, const int tgt)
    { message_dsend (data, N, tgt); }
  static void recv  (double* data, const int N, const int src)
    { message_drecv (data, N, src); }
  static void send  (float*  data, const int N, const int tgt)
    { message_ssend (data, N, tgt); }
  static void recv  (float*  data, const int N, const int src)
    { message_srecv (data, N, src); }
  static void send  (int*  data, const int N, const int tgt)
    { message_isend (data, N, tgt); }
  static void recv  (int*  data, const int N, const int src)
    { message_irecv (data, N, src); }
  static void exchange  (double* data, const int nZ,
			 const int nP, const int sign)
    { message_dexchange (data, nZ, nP, sign); }
  static void exchange  (float* data, const int nZ,
			 const int nP, const int sign)
    { message_sexchange (data, nZ, nP, sign); }
  static void exchange  (int* data,  const int nZ,
			 const int nP, const int sign)
    { message_iexchange (data, nZ, nP, sign); }
#else

#endif
  
  static void grad2 (const double* x, const double* y, double* xr, double* ys,
		     const double* dv, const double* dt,
		     const int& nr, const int& ns, const int& nel)
    { F77NAME(dgrad2) (x, y, xr, ys, dv, dt, nr, ns, nel); }

  static void tpr2d (const double* x, double* y, double* t,
		     const double* dv, const double* dt,
		     const int& nr, const int& ns, const int& nel)
    { F77NAME(dtpr2d) (x, y, t, dv, dt, nr, ns, nel); }

  static void erfcFilter (const int N, const double p, const double s,
			  const double a, double* f)
    { bvdFilter (N, p, s, a, f); }

};

#endif
