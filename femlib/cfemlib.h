#ifndef C_FEMLIB_H
#define C_FEMLIB_H
/*****************************************************************************
 *         FUNCTION PROTOTYPES FOR ROUTINES IN LIBRARY LIBFEM.A
 *****************************************************************************/

#include "cfemdef.h"

/* -- Routines from initial.c: */

void    yy_initialize (void);
double  yy_interpret  (const char*);

void    yy_vec_init   (const char*, const char*);
void    yy_vec_interp (const int, ...);

void    yy_help       (void);
void    yy_show       (void);

/* -- Routines from polyops.c: */

void   dermat_g (const int, const double*, const int,
		 const double*, double*, double*);
void   intmat_g (const int, const double*, const int,
		 const double*, double*, double*);
void   dermat_k (const int, const double*, double*, double*);

void   JACG     (const int, const double, const double, double*);
void   JACGR    (const int, const double, const double, double*);
void   JACGL    (const int, const double, const double, double*);

void   ZWGL     (double*, double*, const int);
void   ZWGRL    (double*, double*, const int);
void   ZWGLL    (double*, double*, const int);
void   ZWGLJ    (double*, double*, const double, const double, const int);

void   DGLL     (const int, const double*, double**, double**);

double PNLEG    (const double, const int);
double PNDLEG   (const double, const int);
double PND2LEG  (const double, const int);
double PNMOD    (const double, const int);

void   uniknot  (const int, double*);

/* -- Routines from operators.c: */

void zquad (const double** point ,  /* Quadrature points.                    */
	    const double** weight,  /* Quadrature weights.                   */
	    const double** dv    ,  /* Derivative operator at points.        */
	    const double** dt    ,  /* (Transpose) derivative operator.      */
	    const int      np    ,  /* Input: Number of quadrature points.   */
	    const char     rule  ,  /* Input: 'G'auss, 'R'adau, or 'L'obatto.*/
	    const double   alpha ,  /* Input: Jacobi polynomial constant.    */
	    const double   beta  ); /* Input: Jacobi polynomial constant.    */

void proj  (const double** IN    ,  /* Interpolant operator matrix           */
	    const double** IT    ,  /* Transposed interpolant operator matrix*/
	    const int      nfrom ,  /* Input: Number of "from" points        */
	    const char     rulefr,  /* Input: 'G', 'R', 'L', or 'U', "from"  */
	    const double   alphfr,  /* Input: Jacobi constant, "from"        */
	    const double   betafr,  /* Input: Jacobi constant, "from"        */
	    const int      nto   ,  /* Input: Number of "to" points          */
	    const char     ruleto,  /* Input: 'G', 'R', 'L', or 'U', "to"    */
	    const double   alphto,  /* Input: Jacobi constant, "to"          */
	    const double   betato); /* Input: Jacobi constant, "to"          */

void intp  (double*        inr   ,  /* 1D shape function at r                */
	    double*        ins   ,  /* 1D shape function at s                */
	    double*        dvr   ,  /* 1D shape function derivative at r     */
	    double*        dvs   ,  /* 1D shape function derivative at s     */
	    const int      nr    ,  /* Input: number of points, r-direction  */
	    const char     basisr,  /* Input: 'G', 'R', 'L', r-direction     */
	    const double   alphar,  /* Input: Jacobi constant, r-direction   */
	    const double   betar ,  /* Input: Jacobi constant, r-direction   */
	    const int      ns    ,  /* Input: number of points, s-direction  */
	    const char     basiss,  /* Input: 'G', 'R', 'L', s-direction     */
	    const double   alphas,  /* Input: Jacobi constant, s-direction   */
	    const double   betas ,  /* Input: Jacobi constant, s-direction   */
	    const double   r     ,  /* Input: location of r in [-1, 1]       */
	    const double   s     ); /* Input: location of s in [-1, 1]       */

void dglldpc (const int      np,    /* input: number of points for Leg polys */
	      const double** cd);   /* output: pointer to table of coeffs.   */

void dglldpt (const int      np,    /* input:  number of points for DLT      */
	      const double** fw,    /* output: 1D forward transform matrix   */
	      const double** ft,    /* output: transpose of fw               */
	      const double** bw,    /* output: 1D inverse transform matrix   */
	      const double** bt,    /* output: transpose of bw               */
	      const double** fu,    /* output: 2D forward transform matrix   */
	      const double** bu);   /* output: 2D inverse transform matrix   */

/* -- Routines from mapping.c: */

void edgemaps (const int np, const int dim,
	       int** emap, int** pmap);

/* -- Routines from family.c: */

void iadopt   (const int, int**);
void dadopt   (const int, double** );
void sadopt   (const int, float**  );

void iabandon (int**);
void dabandon (double**);
void sabandon (float**);

int FamilySize (int*, int*, int*);

/* -- Routines from sparsepak.F: */

void genrcm_ (int*, int*, int*, int*, int*, int*);
#define genrcm(neqns, xadj, adjncy, perm, mask, xls) \
(_vecIreg[0] = neqns, genrcm_(_vecIreg, xadj, adjncy, perm, mask, xls))

void F77NAME(fnroot) (int*, int*, int*, int*,
	      int*, int*, int*);
#define fnroot(root, xadj, adncy, mask, nlvl, xls, ls) \
(_vecIreg[0] = root, _vecIreg[1] = nlvl,                   \
 fnroot_(_vecIreg, xadj, adjncy, mask, _vecIreg + 1, xls, ls))

void rcm_    (int*, int*, int*, int*,
	      int*, int*, int*);
#define rcm(root, xadj, adjncy, mask, perm, ccsize, deg)  \
(_vecIreg[0] = root, rcm_(_vecIreg, xadj, adjncy, mask, perm, ccsize, deg))

/* -- Routines from netlib.f (NETLIB/FFTPACK): */

void drffti_ (int*, double*, int*);
void drfftf_ (int*, double*, double*, double*, int*);
void drfftb_ (int*, double*, double*, double*, int*);

#define drffti(n,wa,ifac) \
  (_vecIreg[0]=n, drffti_ (_vecIreg, wa, ifac))
#define drfftf(n,c,ch,wa,ifac) \
  (_vecIreg[0]=n, drfftf_ (_vecIreg, c, ch, wa, ifac))
#define drfftb(n,c,ch,wa,ifac) \
  (_vecIreg[0]=n, drfftb_ (_vecIreg, c, ch, wa, ifac))

/* -- Routines from canfft.f (Canuto FFT routines): */

void factor_ (int*, int*, int*);

#define factor(n, nfac, ifac) (_vecIreg[0]=n, factor_ (_vecIreg, nfac, ifac))

#define dpreft(n,nfac,ifac,trig)  \
(_vecIreg[0]=n, dpreft_ (_vecIreg, nfac, ifac, trig))
#define dmrcft(v, np, nz, w, nfac, ifac, trig, sign)  \
(_vecIreg[0]=np, _vecIreg[1]=nz, _vecIreg[2]=nfac, _vecIreg[3]=sign,  \
dmrcft_(v, _vecIreg, _vecIreg+1, w, _vecIreg+2, ifac, trig, _vecIreg+3))

/* -- Routines from temfftd.F (Temperton FFT routines): */

void prf235_ (int*, int*, int*, int*, int*);
#define prf235(n,ip,iq,ir,ipqr2) (prf235_(n,ip,iq,ir,ipqr2))

void dsetpf_ (double*, int*, int*, int*, int*);
void dmpfft_ (double*, double*,  int*, int*, int*,
	      int*, int*, double*, int*);

#define dsetpf(trig,n,ip,iq,ir)                               \
(_vecIreg[0]=n,_vecIreg[1]=ip,_vecIreg[2]=iq, _vecIreg[3]=ir, \
dsetpf_(trig,_vecIreg,_vecIreg+1,_vecIreg+2,_vecIreg+3))
#define dmpfft(v,w,np,nz,ip,iq,ir,trig,isign)                 \
(_vecIreg[0]=np,_vecIreg[1]=nz,_vecIreg[2]=ip,                \
_vecIreg[3]=iq, _vecIreg[4]=ir,_vecIreg[5]=isign,             \
dmpfft_(v,w,_vecIreg,_vecIreg+1,_vecIreg+2,                   \
_vecIreg+3,_vecIreg+4,trig,_vecIreg+5))

/* -- Routines from matops.F: */

void dgrad2_ (double*,double*,double*,double*,double*,double*,int*,int*);

#define dgrad2(u,v,ur,vs,dv,dt,np,nel)                          \
(_vecIreg[0]=np,_vecIreg[1]=nel,dgrad2_(u,v,ur,vs,dv,dt,_vecIreg,_vecIreg+1))


/* -- Routines from fourier.c */

void preFFT (const int);
void dDFTr  (double*, const int, const int, const int);

/* -- The next two are useful because the number of transforms doesn't
      need to be even.  But not as fast as Temperton FFT.  OK for
      utility routines but not intended for repetitive use.
*/

void preFFT_netlib (const int);
void dDFTr_netlib  (double*, const int, const int, const int);

/* -- Routines from filter.c */

void bvdFilter (const int,const int,const int, const double, double*);

#if 0
/* -- Routines from message.c: */

void message_init      (int*, char***);
void message_stop      ();
void message_sync      ();
void message_dsend     (double*, const int, const int);
void message_drecv     (double*, const int, const int);
void message_ssend     (float*,  const int, const int);
void message_srecv     (float*,  const int, const int);
void message_isend     (int*,    const int, const int);
void message_irecv     (int*,    const int, const int);
void message_dexchange (double*, const int, const int, const int);
void message_sexchange (float*,  const int, const int, const int);
void message_iexchange (int*,    const int, const int, const int);
#endif

#endif
