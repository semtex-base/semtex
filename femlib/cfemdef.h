#ifndef CFEMDEF_H
#define CFEMDEF_H
/*****************************************************************************
 * Common names and typedefs for C or C++ finite-element codes.
 *****************************************************************************/

#if 0
typedef int     int_t;
typedef double  real_t;
#endif

typedef struct { double x, y, z; } Point;
typedef Point                      Vector;

/*
  UNSET   is a generic value used to indicate lack of initialization, etc.
  HDR_LEN is the number of characters in a semtex field file header.
  STR_MAX is the number of characters in a typical semtex string.
  F77NAME is a macro that appends an underscore to names.  Generally,
          though not all compilers do this, that is how F77 names will
          appear in object files -- so the underscore is needed to
          resolve linkages to C and C++.
*/

#define UNSET      -1
#define HDR_LEN    360
#define STR_MAX    2048
#define F77NAME(x) x##_

/* 
   Choose Chebyshev or Legendre polynomials as the underlying basis
   for quadrature points and weights.  Legendre is the default.
*/

#if defined(CHEBYSHEV)
  #define JAC_ALFA -0.5
  #define JAC_BETA -0.5
#else
  #define JAC_ALFA  0.0
  #define JAC_BETA  0.0
#endif

typedef enum quadrature_kind {
  GJ  = 'G',	/* Gauss-Jacobi quadrature.         */
  GLJ = 'L',	/* Gauss-Lobatto-Jacobi quadrature. */
  GRJ = 'R',	/* Gauss-Radau-Jacobi quadrature.   */
  TRZ = 'U'	/* Trapezoidal (uniform mesh).      */
} QuadratureKind;

typedef enum solver_kind {
  DIRECT,	/* Cholesky back-substitution.                           */
  NESTED,       /* Nested dissection. TBD                                */
  JACPCG,	/* Conjugate gradient, Jacobi (diagonal) preconditioner. */
  MIXED         /* Direct for mode 0, iterative for all others.          */
} SolverKind;

typedef enum transform_kind {
  INVERSE = -1,	/* Basis    --> physical space. */
  FORWARD = +1	/* Physical --> basis    space. */
} TransformKind;

typedef enum exchange_kind {
  FULL,		/* Full exchange (preserves planar and element structure). */
  HALF		/* Half memory exchange across processes (planar only).    */
} ExchangeKind;

#endif
