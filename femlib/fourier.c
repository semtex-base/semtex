/*****************************************************************************
 * fourier.c
 *
 * 1D Fourier transform routines for real data fields based on
 * FFTPACK, Temperton FFT routines (default), or vendor-supplied
 * alternatives.  NB: different restrictions may apply to input args
 * depending on compile-time options/FFT selection.

 * 1. Input data is to be Fourier transformed in the direction normal
 * to the most rapid traverse through memory, with sucessive points in
 * the transform separated by ntrn.  Data has a total of tlen * ntrn
 * real points.
 *
 * 2. If input parameter sign = +1 then the transform is taken as
 * real-->complex, the outcome is normalised by 1/tlen, and a positive
 * sign is used in the complex exponential employed for the Fourier
 * coefficients. If sign = -1 then transform is taken as
 * complex-->real and a 1/2PI normalising factor is used.  In general:
 * these signs and scaling definitions match those adopted in
 * Numerical Recipes.
 *
 * 3. The number of transforms, ntrn, must be even owing to the way
 * real-complex transforms are implemented in the default FFT (two
 * real vectors treated as a single complex one).
 *
 * 4. The transform length tlen also must be even, owing to the way
 * Fourier data are assumed to be packed (with Nyquist data packed in
 * as the imaginary part of mode 0).  The transform length is assumed
 * to stay fixed during execution.
 *
 * Copyright (c) 1999+, Hugh M Blackburn
 *****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

#include <cfemdef.h>
#include <cveclib.h>
#include <cfemlib.h>

#if defined(DEBUG_FFT) /* -- FFTPACK routines (for checking). */

static double* Wtab;
static int*    ifax;


void preFFT (const int tlen)
/* -------------------------------------------------------------------------
 * Carry out FFT rule checks, set up FFT data.  FFTPACK real<-->complex
 * FFT routines do not impose any limitations on tlen but we require
 * it to be even owing to how dDFTr assumes complex data are packed.
 * ------------------------------------------------------------------------- */
{
  const char routine[] = "preFFT";
  char       err[STR_MAX];
  static int tlenLast;

  if (tlen < 1 || tlen & 1) {
    sprintf (err, "transform length (%1d) must be even, and positive", tlen);
    message (routine, err, ERROR);
  }

  if (tlen != tlenLast) {	       /* -- Setup required.      */
    if (!ifax) ifax = ivector (0, 14); /* -- Length is always 15. */

    if (Wtab) freeDvector (Wtab, 0); 
    Wtab = dvector (0, tlen - 1);

    drffti (tlen, Wtab, ifax);

    tlenLast = tlen;
  }
}


void dDFTr (double*   data,
	    const int tlen,
	    const int ntrn,
	    const int sign)
/* ------------------------------------------------------------------------- *
 * Carry out multiple 1D real--complex Fourier transforms of data.
 * ------------------------------------------------------------------------- */
{
  const char     routine[] = "dDFTr";
  char           err[STR_MAX];
  static int     tlenLast;
  static double* work;
  double*        ptr;
  int            i;

  if (tlen < 2 || !ntrn) return;

  /* -- The following restriction isn't imposed by FFTPACK. */
  
  if (ntrn & 1) {
    sprintf (err, "number of transforms (%1d) must be even", ntrn);
    message (routine, err, ERROR);
  }

  if (tlen != tlenLast) {
    preFFT (tlen);
    if (work) freeDvector (work, 0);
    work = dvector (0, 2 * tlen - 1);
  }

  if (sign == FORWARD) {
    for (i = 0, ptr = data; i < ntrn; i++, ptr++) {
      dcopy  (tlen, ptr, ntrn, work, 1);
      drfftf (tlen, work, work + tlen, Wtab, ifax);
      dcopy  (tlen - 2, work + 1, 1, ptr + 2 * ntrn, ntrn);
      ptr[0]    = work[0];
      ptr[ntrn] = work[tlen - 1];
    }
    dscal (ntrn * tlen, 1.0 / tlen, data, 1);
  } else {
    for (i = 0, ptr = data; i < ntrn; i++, ptr++) {
      work[tlen - 1] = ptr[ntrn];
      work[0]        = ptr[0];
      dcopy  (tlen - 2, ptr + 2 * ntrn, ntrn, work + 1, 1);
      drfftb (tlen, work, work + tlen, Wtab, ifax);
      dcopy  (tlen, work, 1, ptr, ntrn);
    }
  }

  tlenLast = tlen;
}


#else /* -- Temperton FFT routine, which is the default. */

static int     ip, iq, ir, ipqr2;
static double* Wtab;


void preFFT (const int tlen)
/* ------------------------------------------------------------------------- *
 * Carry out FFT rule checks, set up FFT data.
 * ------------------------------------------------------------------------- */
{
  const char routine[] = "preFFT";
  char       err[STR_MAX];
  static int tlenLast;
  int        chk;

  if (tlen < 1 || tlen & 1) {
    sprintf (err, "transform length (%1d) must be even, and positive", tlen);
    message (routine, err, ERROR);
  }

  if (tlen != tlenLast) {	/* -- Setup required. */
    chk = tlen;
    prf235 (&chk, &ip, &iq, &ir, &ipqr2);
  
    if (!chk) {
      sprintf (err, "transform length (%1d) needs prime factors 2, 3, 5", tlen);
      message (routine, err, ERROR);
    }

    if (Wtab) freeDvector (Wtab, 0); 
    Wtab = dvector (0, ipqr2 - 1);

    dsetpf (Wtab, tlen, ip, iq, ir);

    tlenLast = tlen;
  }
}


void dDFTr (double*   data,
	    const int tlen,
	    const int ntrn,
	    const int sign)
/* ------------------------------------------------------------------------- *
 * Carry out multiple 1D real--complex Fourier transforms of data.
 * ------------------------------------------------------------------------- */
{
  const char    routine[] = "dDFTr";
  char          err[STR_MAX];
  static int    tlenLast, ntotLast;
  const int     ntot = tlen * ntrn;
  static double *work;

  if (tlen < 2 || !ntrn) return;

  if (ntrn & 1) {
    sprintf (err, "number of transforms (%1d) must be even", ntrn);
    message (routine, err, ERROR);
  }

  if (tlen != tlenLast) preFFT (tlen);

  if (ntot != ntotLast) {
    if (work) freeDvector (work, 0);
    work = dvector (0, ntot - 1);
  }

  dmpfft (data, work, ntrn, tlen, ip, iq, ir, Wtab, sign);
  if (sign == FORWARD) dscal (ntot, 1.0 / tlen, data, 1);

  tlenLast = tlen;
  ntotLast = ntot;
}

#endif

/* Here are the NETLIB routines again - for these we remove the
   restraint that ntrn has to be even. Suggest you don't use these
   repetitively, because they do a lot of memory copying - they are
   supplied for use by utility routines if required (especially those
   for which the number of transforms, ntrn, may not be an even
   number).  The underyling 1D NETLIB routines are reliable.
*/

static double* Wtab_NL;
static int*    ifax_NL;


void preFFT_netlib (const int tlen)
/* -------------------------------------------------------------------------
 * Carry out FFT rule checks, set up FFT data.  FFTPACK real<-->complex
 * FFT routines do not impose any limitations on tlen but we require
 * it to be even owing to how dDFTr assumes complex data are packed.
 * ------------------------------------------------------------------------- */
{
  const char routine[] = "preFFT_netlib";
  char       err[STR_MAX];
  static int tlenLast;

  if (tlen < 1 || tlen & 1) {
    sprintf (err, "transform length (%1d) must be even, and positive", tlen);
    message (routine, err, ERROR);
  }

  if (tlen != tlenLast) {	       /* -- Setup required.      */
    if (!ifax_NL) ifax_NL = ivector (0, 14); /* -- Length is always 15. */

    if (Wtab_NL) freeDvector (Wtab_NL, 0); 
    Wtab_NL = dvector (0, tlen - 1);

    drffti (tlen, Wtab_NL, ifax_NL);

    tlenLast = tlen;
  }
}


void dDFTr_netlib (double*   data,
		   const int tlen,
		   const int ntrn,
		   const int sign)
/* ------------------------------------------------------------------------- *
 * Carry out multiple 1D real--complex Fourier transforms of data.
 * ------------------------------------------------------------------------- */
{
  const char     routine[] = "dDFTr_netlib";
  char           err[STR_MAX];
  static int     tlenLast;
  static double* work;
  double*        ptr;
  int            i;

  if (tlen < 2 || !ntrn) return;

#if 0  
  /* -- The following restriction isn't actually imposed by FFTPACK. */

  if (ntrn & 1) {
    sprintf (err, "number of transforms (%1d) must be even", ntrn);
    message (routine, err, ERROR);
  }
#endif
  
  if (tlen != tlenLast) {
    preFFT_netlib (tlen);
    if (work) freeDvector (work, 0);
    work = dvector (0, 2 * tlen - 1);
  }

  if (sign == FORWARD) {
    for (i = 0, ptr = data; i < ntrn; i++, ptr++) {
      dcopy  (tlen, ptr, ntrn, work, 1);
      drfftf (tlen, work, work + tlen, Wtab_NL, ifax_NL);
      dcopy  (tlen - 2, work + 1, 1, ptr + 2 * ntrn, ntrn);
      ptr[0]    = work[0];
      ptr[ntrn] = work[tlen - 1];
    }
    dscal (ntrn * tlen, 1.0 / tlen, data, 1);
  } else {
    for (i = 0, ptr = data; i < ntrn; i++, ptr++) {
      work[tlen - 1] = ptr[ntrn];
      work[0]        = ptr[0];
      dcopy  (tlen - 2, ptr + 2 * ntrn, ntrn, work + 1, 1);
      drfftb (tlen, work, work + tlen, Wtab_NL, ifax_NL);
      dcopy  (tlen, work, 1, ptr, ntrn);
    }
  }

  tlenLast = tlen;
}
