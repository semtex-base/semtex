#ifndef FIELD_H
#define FIELD_H
  

class Field : public AuxField
// ===========================================================================
// Field adds boundary conditions and global numbering to AuxField.
// With the boundary conditions comes knowledge of which sections of
// the boundary hold essential (known) nodes, and which contain
// natural (unknown) nodes.
//
// A Field holds global node numbers and solve masks for element
// boundaries: where mesh value is given by an essential BC, solve
// mask is 0, for all other nodes have value 1.
//
// Helmholtz solution routines are also available.
// ===========================================================================
{
friend class BCmgr;

public:
  Field  (double*, BoundarySys*, NumberSys*, const int,
	  vector<Element*>&, const char);
 ~Field  () { }

  Field& operator = (const AuxField& z) {AuxField::operator=(z); return *this;}
  Field& operator = (const double&   z) {AuxField::operator=(z); return *this;}
  Field& operator = (const char*     z) {AuxField::operator=(z); return *this;}

  Field& solve  (AuxField*, const ModalMatrixSys*);
  Field& solve  (AuxField*, const MatrixSys*);

  void evaluateBoundaries    (const Field*, const int, const bool = true);
  void evaluateM0Boundaries  (const Field*, const int);

  static void   coupleBCs    (Field*, Field*, const int);
  static double modeConstant (const char, const int, const double);

private:
  int          _nbound;		// Number of boundary edges.
  int          _nline ;		// Length of one boundary line.
  double*      _sheet ;		// Wrap-around storage for data boundary.
  double**     _line  ;		// Single plane's worth of sheet.
  BoundarySys* _bsys  ;		// Boundary system information.
  NumberSys*   _nsys  ; 	// Assembly mapping information.

  void getEssential      (const double*, double*,
			  const vector<Boundary*>&, const AssemblyMap*) const;
  void setEssential      (const double*, double*, const AssemblyMap*);
  void local2global      (const double*, double*, const AssemblyMap*)   const;
  void global2local      (const double*, double*, const AssemblyMap*)   const;
  void local2globalSum   (const double*, double*, const AssemblyMap*)   const;

  void constrain         (double*, const double, const double,
			  const double*, const AssemblyMap*, double*)   const;
  void buildRHS          (double*, const double*, double*, double*,
			  const double**, const int, const int,
			  const vector<Boundary*>&,
			  const AssemblyMap*, double*)                  const;
  void HelmholtzOperator (const double*, double*, const double,
			  const double, const int, double*)           const;
};

#endif
