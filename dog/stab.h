#ifndef STAB_H
#define STAB_H

//////////////////////////////////////////////////////////////////////////////
// stab.h: header file for linearised NS stability solver.
//////////////////////////////////////////////////////////////////////////////

#include <sem.h>
#include <data2df.h>

typedef enum { PRIMAL, ADJOINT, GROWTH, SHRINK } problem_t;


class StabAnalyser : public Analyser
// ===========================================================================
// Implement step-by-step processing and output control for flow solver.
// ===========================================================================
{
public:
  StabAnalyser (Domain*, FEML*);
  void analyse (AuxField**, AuxField** = 0);

private:
  vector<HistoryPoint*> base_history; // -- Locations, etc. of history points.
  ofstream              bhs_strm;     // -- File for base history points.
};

void integrate  (void (*)(Domain*, BCmgr*, AuxField**, AuxField**),
		 Domain*, BCmgr*, StabAnalyser*);
void linAdvect  (Domain*, BCmgr*, AuxField**, AuxField**);
void linAdvectT (Domain*, BCmgr*, AuxField**, AuxField**);

// -- Fortran interfaces to things not included in semtex.

extern "C" {
  //  -- Fortran77 linear systems routines from ARPACK and Templates.
  
  // -- ARPACK Nonsymmetric eigensystem.

  void F77NAME(dnaupd) 		// -- ARPACK reverse-communications interface.
    (int&         ido   ,
     const char*  bmat  ,
     const int&   n     ,
     const char*  which ,
     const int&   nev   ,
     double&      tol   ,
     double*      resid ,
     const int&   ncv   ,
     double*      v     ,
     const int&   ldv   ,
     int*         iparam,
     int*         ipntr ,
     double*      workd ,
     double*      workl ,
     const int&   lworkl,
     int&         info  );

  void F77NAME(dneupd)		// -- Postprocessing.
    (const int&     rvec  ,
     const char*    howmny,
     const int*     select,
     double*        dr    ,
     double*        di    ,
     double*        z     ,
     const int&     ldz   ,
     const double&  sigmar,
     const double&  sigmai,
     double*        workev,
     const char*    bmat  ,	// -- Remainder unchanged after dnaupd.
     const int&     n     ,
     const char*    which ,
     const int&     nev   ,
     double&        tol   ,
     double*        resid ,
     const int&     ncv   ,
     double*        v     ,
     const int&     ldv   ,
     int*           iparam,
     int*           ipntr ,
     double*        workd ,
     double*        workl ,
     const int&     lworkl,
     int&           info  );

  // -- ARPACK Symmetric eigensystem.

  void F77NAME(dsaupd) 		// -- ARPACK reverse-communications interface.
    (int&           ido   ,
     const char*    bmat  ,
     const int&     n     ,
     const char*    which ,
     const int&     nev   ,
     double&        tol   ,
     double*        resid ,
     const int&     ncv   ,
     double*        v     ,
     const int&     ldv   ,
     int*           iparam,
     int*           ipntr ,
     double*        workd ,
     double*        workl ,
     const int&     lworkl,
     int&           info  );

  void F77NAME(dseupd)		// -- Postprocessing.
    (const int&     rvec  ,
     const char*    howmny,
     const int*     select,
     double*        dr    ,
     double*        z     ,
     const int&     ldz   ,
     const double&  sigma ,
     const char*    bmat  ,	// -- Remainder unchanged after dsaupd.
     const int&     n     ,
     const char*    which ,
     const int&     nev   ,
     double&        tol   ,
     double*        resid ,
     const int&     ncv   ,
     double*        v     ,
     const int&     ldv   ,
     int*           iparam,
     int*           ipntr ,
     double*        workd ,
     double*        workl ,
     const int&     lworkl,
     int&           info  );

  // -- Templates iterative linear systems solvers.
  
  void F77NAME(bicgstab)	// -- Templates Bi-Conj-Grad-Stab solver.
    (const int&     N    ,
     const double*  B    ,
     double*        X    ,
     double*        WORK ,
     const int&     LDW  ,
     int&           ITER ,
     double&        RESID,
     void (*MATVEC) (const double&, const double*, const double&, double*),
     void (*PSOLVE) (double*, const double*),
     int&           INFO );

  void F77NAME(gmres)	        // -- Templates GMRES solver.
    (const int&     N    ,
     const double*  B    ,
     double*        X    ,
     const int&     RESTRT,
     double*        WORK ,
     const int&     LDW  ,
     double*        WORK2,
     const int&     LDW2 ,
     int&           ITER ,
     double&        RESID,
     void (*MATVEC) (const double&, const double*, const double&, double*),
     void (*PSOLVE) (double*, const double*),
     int&           INFO );
}

#endif
