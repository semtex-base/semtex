#ifndef AUXFIELD_H
#define AUXFIELD_H


class AuxField
// ===========================================================================
// Physical field storage class, no BCs.
//
// An AuxField has a storage area for data, physical space mesh, and a
// matching list of elements which can access this storage area on an
// element-wise basis if required.  Functions to operate on fields as
// a whole are included.  AuxFields have no boundary conditions, no
// global numbering system, and no solution routines.
// ===========================================================================
{
friend istream& operator >> (istream&, AuxField&);
friend ostream& operator << (ostream&, AuxField&);
friend class    Field;
friend class    BCmgr;

public:
  AuxField (double*, const int, vector<Element*>&, const char = 0);

  char name     ()      const { return _name; }
  void describe (char*) const;

  AuxField& operator  = (const double);
  AuxField& operator += (const double);
  AuxField& operator -= (const double);
  AuxField& operator *= (const double);
  AuxField& operator /= (const double);

  AuxField& operator  = (const AuxField&);
  AuxField& operator += (const AuxField&);
  AuxField& operator -= (const AuxField&);
  AuxField& operator *= (const AuxField&);

  AuxField& operator  = (const char*);
  AuxField& axpy        (const double, const AuxField&);

  AuxField& times       (const AuxField&, const AuxField&);
  AuxField& timesPlus   (const AuxField&, const AuxField&);
  AuxField& timesMinus  (const AuxField&, const AuxField&);

  AuxField& addToPlane  (const int, const double);
  AuxField& getPlane    (const int, double*);
  AuxField& setPlane    (const int, const double*);
  AuxField& setPlane    (const int, const double);

  AuxField& update   (const int, const double*, const double, const double); 

  AuxField& gradient (const int);
  AuxField& mulY     ();
  AuxField& divY     ();

  double mode_L2     (const int)         const;
  double CFL         (const int, int&) const;
  double integral    ()                    const;
  double area        ()                    const;
  
  double probe       (const Element*, const double, 
		      const double, const int)    const;
  double probe       (const Element*, const double,
		      const double, const double)   const;

  AuxField&   reverse   ();
  AuxField&   smooth    (const int, const int*, const double*);
  
  static void swapData  (AuxField*, AuxField*);
  static void couple    (AuxField*, AuxField*, const int);

protected:
  char              _name ;	// Identification tag.  '\0' by default.
  vector<Element*>& _elmt ;	// Quadrilateral elements.
  int               _nz   ;	// number of data planes (per process).
  int               _size ;	// _nz * Geometry::planeSize().
  double*           _data ;	// 2/3D data area, element x element x plane.
  double**          _plane;	// Pointer into data for each 2D frame.

private:
  AuxField& operator /= (const AuxField&) { return *this; }

};


// -- Related routins for I/O that don't use any private data.

void readField  (istream&, vector<AuxField*>&);
void writeField (ostream&, const char*, const int, const double,
		 vector<AuxField*>&);

#endif
