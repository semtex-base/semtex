mesh README
===========

Synopsis
--------

Various contributed semtex session files.  While they are often
specific to applications and/or publications, the geometry sections
may be re-used or adapted for other cases.

Notes
-----

* A quick way of previewing the meshes is to use the meshplot utility,
which generates encapsulated postscript from output of meshpr.  
E.g. on a Mac (now need ps2pdf since Preview no longer opens EPS):  
*meshpr <session_file> | meshplot | ps2pdf | open -a Preview.app -f*  
On Linux:  
*meshpr <session_file> | meshplot -o mesh.eps -d gv*

* Meshes can be quickly stretched in x or y directions using tokens
X_SCALE or Y_SCALE.

* It is quite easy to generate a rectangular mesh using the rectmesh
utility (see chapter DNS-101 in semtex user guide).  The outcomes
generally need some hand editing e.g. to install desired TOKENS and BCs.

* Meshes can be remapped using the mapmesh utility.


Grouping by domain geometry
===========================

(Numbers in brackets are number of elements)

Simple/canonical
----------------

* Annuli: annulus16 (16), annulus36 (36)

* Circles: circle64 (64), circle161 (161), circle300 (300), circle323
  (323)

* Rectangles: box1 (1), box2 (1), box3 (4), cav01 (108), chan1 (4),
  chan2 (2), chan3 (24), chan4 (60), chan5 (60), chan6 (96), chan7
  (120), kovas1 (4), kovas2 (4), kovas3 (4), kovas4 (4), kovas5 (4),
  laplace1 (4), laplace2 (4), laplace3 (4), laplace4 (4), laplace5
  (4), laplace7 (4), laplace8 (4), laplace9 (4), mixed1 (4), poisson1
  (4), shearlay (256), taylor2 (4), taylor3 (4), taylor4 (4), taylor5
  (4), taylor6 (16), tdrivcav1 (4), TG1600 (1024), tgreen (4),
  tgreen100 (100), tgreen1el (1), tkovas1 (4)

* Rectangles/axisymmetric: MMBL (63), can1 (21), can2 (32), can3 (50),
  cylkov1 (7), cylkov2 (6), pipe1 (60), pipe6 (150), pipe7 (35), pipe8
  (32), poisson2 (60), PreCyl03 (192), sbr (4), slr1 (100), tc0 (4),
  tc1 (4), tc2 (4), tc3 (4), tube1 (4), tube2 (4), tube3 (4), tube4
  (4), tube8 (240), vb1 (15), vb2 (60), wom1 (4)

Embedded bodies/cutouts
-----------------------

* Circular cylinders: cylinder01 (54), cylinder02 (74), cylinder03
  (88), cylinder04 (86), cylinder05 (92), cylinder06 (92), cylinder07
  (108), cylinder08 (112), cylinder09 (128), cylinder10 (196),
  cylinder11 (422), cylinder12 (444), cylinder13 (196), cylinder14
  (218), cylinder15 (502), cylinder16 (480), cylinder17 (536),
  cylinder18 (536), cylinder19 (608), web4 (164), web14 (700)

* Half cylinders (or spheres): hlf_web4 (82), semicyl01 (83),
  semicyl02 (268), sphere01 (26), sphere02 (82), sphere03 (88),
  sphere04 (94), sphere05 (25)

* Square cylinders: squarecyl01 (62), squarecyl02 (236)

* Airfoil: naca-a04-v1 (891)

Complex
-------

* Axisymmetric stenoses: Msten50_25D (648), Msten50_50D (743),
  sten_50R_50D (894), Sten50_20D (135)

* Steps: backfs1 (66), bfs01 (411), bfs02 (494), bfs03 (435), bfs04
  (425), bfs05 (419), bfs06 (435)

* Cyclonic separators: cyclone1 (100), cyclone2 (635)

* Flowmeters (ulrasonic, embedded body): flowmeter1 (93), flowmeter2 (224)

* Distorted squares: laplace6 (4)

* Jet in cavity: pjb1 (700)

* Tapered rectangle (conical bottomed tank): sfp1 (186)

* House: thouse (18)








------------------------------------------------------------------------------
