///////////////////////////////////////////////////////////////////////////////
// transform: utility to carry out Fourier and/or 2D polynomial
// transform of data.
//
// Copyright (c) 1999+, Hugh M Blackburn
//
// Usage
// -----
// transform [options] [file]
// options:
// -h       ... print this message.
// -i       ... invert transform.
// -l       ... polynomial transform is Legendre       [Default: modal]
// -P||F||B ... Carry out DPT (P), DFT (F) or both (B) [Default: both]
// 
// If file is not present, read from standard input.  Write to
// standard output.
///////////////////////////////////////////////////////////////////////////////

#include <sem.h>
#include <data2df.h>

static char prog[] = "transform";
static void getargs  (int, char**, int&, char&, char&, istream*&);
static bool getDump  (istream&, ostream&, vector<Data2DF*>&);
static void loadName (const vector<Data2DF*>&, char*);


int main (int    argc,
	  char** argv)
// ---------------------------------------------------------------------------
// Driver.
// ---------------------------------------------------------------------------
{
  int              i, dir = FORWARD;
  char             type   = 'B', basis = 'm';
  istream*         input;
  vector<Data2DF*> u;

  Femlib::init ();
  
  getargs (argc, argv, dir, type, basis, input);
  
  while (getDump (*input, cout, u))
    for (i = 0; i < u.size(); i++) {
      if (type == 'P' || type == 'B') u[i] -> DPT2D (dir, basis);
      if (type == 'F' || type == 'B') u[i] -> DFT1D (dir);
      cout << *u[i];
    }
  
  return EXIT_SUCCESS;
}


static void getargs (int       argc ,
		     char**    argv ,
		     int&      dir  ,
		     char&     type ,
		     char&     basis,
		     istream*& input)
// ---------------------------------------------------------------------------
// Deal with command-line arguments.
// ---------------------------------------------------------------------------
{
  char usage[] = "Usage: transform [options] [file]\n"
    "options:\n"
    "-h ... print this message\n"
    "-P ... Discrete Polynomial Transform (2D)\n"
    "-F ... Discrete Fourier    Transform (1D)\n"
    "-B ... do both DPT & DFT [Default]\n"
    "-i ... carry out inverse transform instead\n"
    "-l ... use Legendre basis functions instead of modal expansions\n";
    
  while (--argc && **++argv == '-')
    switch (*++argv[0]) {
    case 'h':
      cout << usage;
      exit (EXIT_SUCCESS);
      break;
    case 'i':
      dir = INVERSE;
      break;
    case 'l':
      basis = 'l';
      break;
    case 'P':
      type = 'P';
      break;
    case 'F':
      type = 'F';
      break;
    case 'B':
      type = 'B';
      break;
    default:
      cerr << usage;
      exit (EXIT_FAILURE);
      break;
    }

  if (argc == 1) {
    input = new ifstream (*argv);
    if (input -> fail())
      Veclib::alert (prog, "unable to open input file", ERROR);
  } else input = &cin;
}


static void loadName (const vector<Data2DF*>& u,
		      char*                   s)
// --------------------------------------------------------------------------
// Load a string containing the names of fields.
// ---------------------------------------------------------------------------
{
  int i, N = u.size();

  for (i = 0; i < N; i++) s[i] = u[i] -> getName();
  s[N] = '\0';
}


static bool getDump (istream&          ifile,
		     ostream&          ofile,
		     vector<Data2DF*>& u    )
// ---------------------------------------------------------------------------
// Read next set of field dumps from ifile, put headers on ofile.
// ---------------------------------------------------------------------------
{
  Header hdr;
  int    i, nf;
  bool   swab;

  if (ifile.peek () == EOF) return false;

  ifile >> hdr;

  swab = Veclib::byteSwap (hdr.frmt);
  nf   = strlen (hdr.flds);
  
  ofile << hdr;

  if (u.size() != nf) {
    u.resize (nf);
    for (i = 0; i < nf; i++)
      u[i] = new Data2DF (hdr.nr, hdr.nz, hdr.nel, hdr.flds[i]);
  }

  for (i = 0; i < nf; i++) {
    ifile >> *u[i];
    if (swab) u[i] -> reverse();
  }

  return ifile.good();
}
