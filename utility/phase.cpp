//////////////////////////////////////////////////////////////////////////////
// phase: utility to perform operations on 3D data file in
// phase/Fourier space.
//
// Copyright (c) 2002+, Hugh M Blackburn
//
// Usage
// -----
// phase [options] [file]
// options:
// -h       ... print this message.
// -c       ... perform complex conjugation.
// -f       ... data are already Fourier transformed (do not transform).
// -z       ... take mode zero as complex (e.g. it is an eigenmode).
// -r       ... enforce reflection symmetry of velocity & pressure data.
// -s <num> ... shift data a fraction <num> of the fundamental wavelength.
// 
// If file is not present, read from standard input.  Write to
// standard output.
//////////////////////////////////////////////////////////////////////////////

#include <sem.h>
#include <data2df.h>

static char prog[] = "phase";
static void getargs  (int,char**,bool&,bool&,bool&,bool&,double&,istream*&);
static int  getDump  (istream&,ostream&,vector<Data2DF*>&);
static void loadName (const vector<Data2DF*>&,char*);


int main (int    argc,
	  char** argv)
// ---------------------------------------------------------------------------
// Driver.
// ---------------------------------------------------------------------------
{
  int              i;
  bool             conj = false, cmplx = false, zero = false, symm = false;
  double           alpha;
  istream*         input;
  vector<Data2DF*> u;

  Femlib::init ();
  
  getargs (argc, argv, conj, cmplx, zero, symm, alpha, input);
  
  while (getDump (*input, cout, u))
    for (i = 0; i < u.size(); i++) {
      if (!cmplx|zero) u[i] -> DFT1D (FORWARD);

      // -- Here are the various possible actions:

      if (fabs (alpha) > EPSDP) u[i] -> F_shift (alpha, zero);
      if (conj)                 u[i] -> F_conjugate    (zero);
      if (symm)                 u[i] -> F_symmetrize   (zero);

      if (!cmplx|zero) u[i] -> DFT1D (INVERSE);
      cout << *u[i];
    }
  
  return EXIT_SUCCESS;
}


static void getargs (int       argc ,
		     char**    argv ,
		     bool&     conj ,
		     bool&     cmplx,
		     bool&     zero ,
		     bool&     symm ,
		     double&   shift,
		     istream*& input)
// ---------------------------------------------------------------------------
// Deal with command-line arguments.
// ---------------------------------------------------------------------------
{
  char usage[] = "Usage: phase [options] [file]\n"
  "options:\n"
  "-h       ... print this message\n"
  "-c       ... perform complex conjugation (reflection).\n"
  "-f       ... data are already complex (do not Fourier transform).\n"
  "-z       ... take mode zero as complex (e.g. file is an eigenmode).\n"
  "-r       ... enforce reflection symmetry of velocity & pressure data.\n"
  "-s <num> ... shift data a fraction <num> of the fundamental wavelength.\n";

  while (--argc && **++argv == '-')
    switch (*++argv[0]) {
    case 'h':
      cout << usage;
      exit (EXIT_SUCCESS);
      break;
    case 'c':
      conj = true;
      break;
    case 'f':
      cmplx = true;
      break;
    case 'z':
      zero = true;
      break;
    case 'r':
      symm = true;
      break;
    case 's':
      if   (*++argv[0]) shift = atof (*argv);
      else { --argc;    shift = atof (*++argv); }
      break;
    default:
      cerr << usage;
      exit (EXIT_FAILURE);
      break;
    }

  if (fabs(shift) < EPSDP && !(conj | cmplx | symm))
    Veclib::alert (prog, "no action signalled", ERROR);

  if (argc == 1) {
    input = new ifstream (*argv);
    if (input -> fail()) Veclib::alert (prog, "unable to open input file", ERROR);
  } else input = &cin;
}


static void loadName (const vector<Data2DF*>& u,
		      char*                    s)
// --------------------------------------------------------------------------
// Load a string containing the names of fields.
// ---------------------------------------------------------------------------
{
  int i, N = u.size();

  for (i = 0; i < N; i++) s[i] = u[i] -> getName();
  s[N] = '\0';
}


static int getDump (istream&          ifile,
		    ostream&          ofile,
		    vector<Data2DF*>& u    )
// ---------------------------------------------------------------------------
// Read next set of field dumps from ifile, put headers on ofile.
// ---------------------------------------------------------------------------
{
  char   buf[StrMax], session[StrMax], fields[StrMax], *tok;
  int    i, j, nf, np, nz, nel, simstep;
  double kinvis, beta, simtime, deltat;
  bool   swab;

  if (ifile.getline(buf, StrMax).eof()) return 0;
  
  if (!strstr (buf, "Session"))
    Veclib::alert (prog, "not a field file", ERROR);
  tok = strtok (buf, " ");
  strcpy (session, tok);

  ifile.getline (buf, StrMax); 	// -- Creation date (discard).

  // -- Get numerical description of field sizes.

  ifile >> np >> nz >> nz >> nel;
  ifile.getline (buf, StrMax);

  // -- Get runtime info.

  ifile >> simstep; ifile.getline (buf, StrMax);
  ifile >> simtime; ifile.getline (buf, StrMax);
  ifile >> deltat;  ifile.getline (buf, StrMax);
  ifile >> kinvis;  ifile.getline (buf, StrMax);
  ifile >> beta;    ifile.getline (buf, StrMax);

  // -- Input field names.

  ifile >> fields; ifile.getline (buf, StrMax);
  nf = strlen (fields);

  // -- Arrange for byte-swapping if required.

  ifile.getline (buf, StrMax);
  swab = Veclib::byteSwap (buf);

  // -- Output a standard header string.

  Veclib::headerStr (buf, session, NULL, np, np, nz, nel,
		     simstep, simtime, deltat, kinvis, beta, fields, "binary");
    
  ofile << buf;

  if (u.size() != nf) {
    u.resize (nf);
    for (i = 0; i < nf; i++) u[i] = new Data2DF (np, nz, nel, fields[i]);
  }

  for (i = 0; i < nf; i++) {
    ifile >> *u[i];
    if (swab) u[i] -> reverse();
  }

  return ifile.good();
}
