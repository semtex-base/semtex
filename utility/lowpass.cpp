//////////////////////////////////////////////////////////////////////////////
// lowpass: utility to carry out lowpass filtering of data in polynomial
// and/or Fourier space.
//
// Copyright (c) 2004+, Hugh M Blackburn
//
// Usage
// -----
// lowpass [options] [file]
// options:
// -h       ... print this message.
// -P||F||B ... carry out DPT (P), DFT (F) or both (B) [Default: both]
// -r <num> ... start of filter roll-off, real number in [0,1] [Default: 0.0]
// -o <num> ... filter order, integer [Default: 2, the minimum permitted value]
// 
// If file is not present, read from standard input.  Write to
// standard output.
//
// Synopsis
// --------
// 2D polynomial filtering is carried out in the tensor-product
// modal polynomial space. 
//
// Filters in each space are Boyd--VanDeven (erfc) shapes.
//////////////////////////////////////////////////////////////////////////////

#include <sem.h>
#include <data2df.h>

static char prog[] = "lowpass";
static void getargs  (int, char**, char&, int&, double&, istream*&);
static bool getDump  (istream&, ostream&, vector<Data2DF*>&);
static void loadName (const vector<Data2DF*>&, char*);


int main (int    argc,
	  char** argv)
// ---------------------------------------------------------------------------
// Driver.
// ---------------------------------------------------------------------------
{
  int              i, order = 2;
  double           roll = 0.0;
  char             type = 'B';
  istream*         input;
  vector<Data2DF*> u;

  Femlib::init ();
  
  getargs (argc, argv, type, order, roll, input);
 
  while (getDump (*input, cout, u))
    for (i = 0; i < u.size(); i++) {
      if (type == 'P' || type == 'B')
	u[i] -> filter2D (roll, order);
      if (type == 'F' || type == 'B')
	u[i] -> DFT1D (FORWARD) . filter1D (roll, order) . DFT1D (INVERSE);
      cout << *u[i];
    }
  
  return EXIT_SUCCESS;
}


static void getargs (int       argc ,
		     char**    argv ,
		     char&     type ,
		     int&      order,
		     double&   roll ,
		     istream*& input)
// ---------------------------------------------------------------------------
// Deal with command-line arguments.
// ---------------------------------------------------------------------------
{
  char usage[] = "Usage: lowpass [options] [file]\n"
    "options:\n"
    "-h       ... print this message\n"
    "-P       ... polynomial filter (2D)\n"
    "-F       ... Fourier    filter (1D)\n"
    "-B       ... do both P & F [Default]\n"
    "-r <num> ... start of filter roll-off, real number in [0,1]"
    " [Default: 0.0]\n"
    "-o <num> ... filter order, integer"
    " [Default: 2, the minimum permitted value]\n";
    
  while (--argc && **++argv == '-')
    switch (*++argv[0]) {
    case 'h':
      cout << usage;
      exit (EXIT_SUCCESS);
      break;
    case 'P':
      type = 'P';
      break;
    case 'F':
      type = 'F';
      break;
    case 'B':
      type = 'B';
      break;
    case 'o':
      if   (*++argv[0]) order = atoi (*argv);
      else { --argc;    order = atoi (*++argv); }
      break;
    case 'r':
      if   (*++argv[0]) roll = atof (*argv);
      else { --argc;    roll = atof (*++argv); }
      break;
    default:
      cerr << usage;
      exit (EXIT_FAILURE);
      break;
    }

  if (argc == 1) {
    input = new ifstream (*argv);
    if (input -> fail())
      Veclib::alert (prog, "unable to open input file", ERROR);
  } else input = &cin;
}


static void loadName (const vector<Data2DF*>& u,
		      char*                   s)
// --------------------------------------------------------------------------
// Load a string containing the names of fields.
// ---------------------------------------------------------------------------
{
  int i, N = u.size();

  for (i = 0; i < N; i++) s[i] = u[i] -> getName();
  s[N] = '\0';
}


static bool getDump (istream&          ifile,
		     ostream&          ofile,
		     vector<Data2DF*>& u    )
// ---------------------------------------------------------------------------
// Read next set of field dumps from ifile, write headers to ofile on
// the fly, return fields to be subsequently filtered and written.
// ---------------------------------------------------------------------------
{
  char   buf[StrMax], session[StrMax], fmt[StrMax], fields[StrMax];
  char   created[StrMax], work[StrMax], *tok;
  int    i, j, nf, np, nz, nel, runstep;
  double runtime, deltat, kinvis, beta;
  bool   swab;
  
  if (ifile.getline(buf, StrMax).eof()) return false;
  
  if (!strstr (buf, "Session"))
    Veclib::alert (prog, "not a field file", ERROR);

  tok = strtok (buf, " ");
  strcpy (session, tok);
  
  ifile.getline (buf, StrMax); // -- Creation date (discard).

  // -- Get numerical description of field sizes.

  ifile >> np >> nz >> nz >> nel;
  ifile.getline (buf, StrMax);
  
  // -- Get runtime info.
  
  ifile >> runstep; ifile.getline (buf, StrMax);
  ifile >> runtime; ifile.getline (buf, StrMax);
  ifile >> deltat;  ifile.getline (buf, StrMax);
  ifile >> kinvis;  ifile.getline (buf, StrMax);
  ifile >> beta;    ifile.getline (buf, StrMax);

  // -- Input field names.

  ifile >> fields;
  ifile.getline (buf, StrMax);

  // -- Arrange for byte-swapping if required.

  ifile.getline (buf, StrMax);

  swab = Veclib::byteSwap (buf);

  Veclib::headerStr (work, session, NULL, np, np, nz, nel,
		     runstep, runtime, deltat, kinvis, beta,
		     fields, "binary");
    
  cout << work;

  if (u.size() != nf) {
    u.resize (nf);
    for (i = 0; i < nf; i++) u[i] = new Data2DF (np, nz, nel, fields[i]);
  }

  for (i = 0; i < nf; i++) {
    ifile >> *u[i];
    if (swab) u[i] -> reverse();
  }

  return ifile.good();

}
