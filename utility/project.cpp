//////////////////////////////////////////////////////////////////////////////
// project: utility to project solution files to different
// interpolation orders.
//
// Copyright (c) 1996+, Hugh M Blackburn
//
// Usage
// -----
// project [options] [file]
// options:
// -h       ... print this message.
// -n <num> ... project elements to num x num.
// -z <num> ... project to <num> planes in the homogeneous direction.
// -w       ... Retain w components in 3D-->2D proj'n [Default: delete]
// -u       ... project elements to uniform internal grid [Default: GLL].
// -U       ... project from uniform grid to GLL.
// 
// If file is not present, read from standard input.  Write to
// standard output.
//
// Synopsis
// --------
// Process sem field file, project to new interpolation order on the
// same mesh.  Each dump in file is expected to be the same size.
// Also it is assumed that the field file represents a vector field
// dump, so that if the input file has N space dimensions, the first N
// fields represent vector components.  Input file must be binary
// format.
//////////////////////////////////////////////////////////////////////////////

#include <sem.h>

static int uniform = 0;


static int _index (const char* s, char c)
/* ------------------------------------------------------------------------- *
 * Return index of c in s, -1 if not found.
 * ------------------------------------------------------------------------- */
{
  int       i;
  const int len = strlen (s);

  for (i = 0; i < len; i++) if (s[i] == c) return i;

  return -1;
}


class Field2DF
// ============================================================================
// Canonical field class, each nr * ns element is defined on [-1,1] X [-1, 1].
// Data are arranged element-ordered in 2D planes to create a 3D scalar field.
//
// This is a slightly modified and (much) cut-down version of Data2DF,
// which could fairly easily be used instead.  This modified variant
// allows different element orders in the r and s (x/y) directions,
// and projection to/from uniform mesh spacings.
// ============================================================================
{
friend istream& operator >> (istream&, Field2DF&);
friend ostream& operator << (ostream&, Field2DF&);

public:
  Field2DF  (const int nr, const int ns, const int nZ, const int nEl,
	     const char Name='\0');
  ~Field2DF () { delete data; delete plane; }

  char getName () { return name; }

  Field2DF& operator = (const Field2DF&);
  Field2DF& operator = (const double);

  Field2DF& transform (const int);
  Field2DF& reverse   ();
  
private:
  const char name;
  const int  nr, ns, nz, nel, nrns;
  int        nplane, ntot;
  double*    data;
  double**   plane;
};


Field2DF::Field2DF (const int  nR  ,
		    const int  nS  ,
		    const int  nZ  ,
		    const int  nEl ,
		    const char Name) :

		    name       (Name),
                    nr         (nR  ),
                    ns         (nS  ),
		    nz         (nZ  ),
		    nel        (nEl ),
		    nrns       (nr * ns)
// ---------------------------------------------------------------------------
// Field2DF constructor. 
// ---------------------------------------------------------------------------
{
  int i;
  
  nplane = nrns * nel;
  if (nplane > 1 && nplane & 1) nplane++;
  ntot   = nplane * nz;

  data  = new double  [ntot];
  plane = new double* [nz];

  for (i = 0; i < nz; i++) plane[i] = data + i * nplane;
  Veclib::zero (ntot, data, 1);
}


Field2DF& Field2DF::transform (const int sign)
// ---------------------------------------------------------------------------
// Carry out Fourier transformation in z direction.
// ---------------------------------------------------------------------------
{
  if (nz > 2) Femlib::DFTr (data, nz, nplane, sign);

  return *this;
}


Field2DF& Field2DF::operator = (const Field2DF& rhs)
// ---------------------------------------------------------------------------
// If the two fields conform, copy rhs's data storage to lhs.
//
// Otherwise perform projection/interpolation of rhs's data area to lhs.
// Interpolation ASSUMES THAT FOURIER TRANSFORMATION HAS ALREADY OCCURRED
// in z direction if rhs is 3D.  Truncation of Fourier modes occurs if this
// Field2DF has less modes than rhs (to avoid aliasing).
//
// Note that we may have to pay some special attention to Fourier Nyquist data:
// (a) if the projection is to a lower  number of modes - delete it.
// (b) if the projection is to a higher number of modes - relocate it.
// (c) if the projection is to the same number of modes - copy it in place.
// ---------------------------------------------------------------------------
{
  if (rhs.nel != nel)
    Veclib::alert ("Field2DF::operator =", "fields can't conform", ERROR);

  if (rhs.nr == nr && rhs.ns == ns && rhs.nz == nz) // -- Just copy.
    Veclib::copy (ntot, rhs.data, 1, data, 1);

  else {			// -- Perform projection.

    int            i, k;
    double         *LHS, *RHS;
    const double   *IN,  *IT;
    const int      nzm = min (rhs.nz, nz);
    vector<double> work (rhs.nr * nr);
    double*        tmp = &work[0];

    if      (uniform == +1)
      Femlib::projection (&IN, &IT, rhs.nr, GLJ, JAC_ALFA, JAC_BETA,
			                nr, TRZ, 0.0, 0.0);
    else if (uniform == -1) 
      Femlib::projection (&IN, &IT, rhs.nr, TRZ, 0.0, 0.0,
			                nr, GLJ, JAC_ALFA, JAC_BETA);
    else
      Femlib::projection (&IN, &IT, rhs.nr, GLJ, JAC_ALFA, JAC_BETA,
			                nr, GLJ, JAC_ALFA, JAC_BETA);

    for (k = 0; k < nzm; k++) {	// -- 2D planar projections.
      LHS = plane[k];
      RHS = rhs.plane[k];

      if (rhs.nr == nr && rhs.ns == ns)
	Veclib::copy (nplane, RHS, 1, LHS, 1);
      else
	for (i = 0; i < nel; i++, LHS += nrns, RHS += rhs.nrns) {
	  Blas::mxm (IN, nr, RHS, rhs.nr, tmp, rhs.nr);
	  Blas::mxm (tmp, nr, IT, rhs.nr, LHS,     nr);
	}
    }

    if ((i = nz - rhs.nz) > 0) {
      // -- The new area has more Fourier modes than the old one.
      // -- Zero pad for Fourier projections.
      Veclib::zero (i * nplane, data + rhs.ntot, 1);
      // -- Copy the Nyquist data to its new location,
      Veclib::copy (nplane, data + nplane, 1, data + rhs.ntot, 1);
      // -- Zero the present Nyquist location.
      Veclib::zero (nplane, data + nplane, 1);
    } else if ((nz - rhs.nz) < 0 && nz > 1) {
      // -- Zero the new Nyquist location.
      Veclib::zero (nplane, data + nplane, 1);
    }

  }

  return *this;
}


Field2DF& Field2DF::operator = (const double val)
// ---------------------------------------------------------------------------
// Set field storage area to val.
// ---------------------------------------------------------------------------
{
  if   (val == 0.0) Veclib::zero (ntot,      data, 1);
  else              Veclib::fill (ntot, val, data, 1);

  return *this;
}


Field2DF& Field2DF::reverse ()
// ---------------------------------------------------------------------------
// Reverse order of bytes within each word of data.
// ---------------------------------------------------------------------------
{
  Veclib::brev (ntot, data, 1, data, 1);

  return *this;
}


ostream& operator << (ostream&  strm,
		      Field2DF& F   )
// ---------------------------------------------------------------------------
// Binary write of F's data area.
// ---------------------------------------------------------------------------
{
  int i;
  
  for (i = 0; i < F.nz; i++)
    strm.write ((char*) F.plane[i], F.nrns * F.nel * sizeof (double));

  return strm;
}


istream& operator >> (istream&  strm,
		      Field2DF& F   )
// ---------------------------------------------------------------------------
// Binary read of F's data area.
// ---------------------------------------------------------------------------
{
  int i;
  
  for (i = 0; i < F.nz; i++)
    strm.read ((char*) F.plane[i], F.nrns * F.nel * sizeof (double));

  return strm;
}


static char prog[] = "project";
static void getargs  (int, char**, int&, int&, bool&, istream*&);
static bool getDump  (istream&, ostream&, vector<Field2DF*>&,
		      int&, int&, int&, int&, bool&, int&, char*);


int main (int    argc,
	  char** argv)
// ---------------------------------------------------------------------------
// Driver.
// ---------------------------------------------------------------------------
{
  char              fields[StrMax];
  int               i, j, nEl, fInc;
  int               nRnew = 0, nSnew = 0, nZnew = 0;
  bool              keepW = false;
  istream*          input;
  vector<Field2DF*> Uold, Unew;

  Femlib::init ();

  getargs (argc, argv, nRnew, nZnew, keepW, input);

  // -- If a 2D projection is requested, enforce equal order in each direction.
  //    Otherwise, elemental structure (perhaps non-square) is left unchanged.
  //    This allows z-projection of non-square elements.

  nSnew = nRnew;
  
  while (getDump (*input,cout,Uold,nRnew,nSnew,nZnew,nEl,keepW,fInc,fields)) {

    Unew.resize (Uold.size() + fInc);

    switch (fInc) {

    case 0:			// -- Same number of fields out as in.
      for (i = 0; i < Uold.size(); i++) {
	Unew[i] = new Field2DF (nRnew, nSnew, nZnew, nEl,Uold[i] -> getName());
       *Unew[i] = *Uold[i];
      }
      break;

    case 1:			// -- Add a new blank field called 'w'.
      for (i = 0; i < Uold.size(); i++) {
	j = _index (fields, Uold[i] -> getName());
	Unew[j] = new Field2DF (nRnew, nSnew, nZnew, nEl,Uold[i] -> getName());
	*Unew[j] = *Uold[i];
      }
      j = _index (fields, 'w');
      Unew[j] = new Field2DF (nRnew, nSnew, nZnew, nEl, 'w');
      *Unew[j] = 0.0;
      break;

    case -1:			// -- Delete field called 'w'.
      for (j = 0, i = 0; i < Uold.size(); i++) {
	if (Uold[i] -> getName() == 'w') continue;
	Unew[j] = new Field2DF (nRnew, nSnew, nZnew, nEl,Uold[i] -> getName());
	*Unew[j] = *Uold[i];
	j++;
      }
      break;

    default:			// -- Whoops.
      Veclib::alert (prog, "unrecognized conversion", ERROR);
      break;
    }

    for (i = 0; i < Unew.size(); i++) {
      Unew[i] -> transform (INVERSE);
      cout << *Unew[i];
    }
  }
  
  return EXIT_SUCCESS;
}


static void getargs (int       argc ,
		     char**    argv ,
		     int&      np   ,
		     int&      nz   ,
		     bool&     keepW,
		     istream*& input)
// ---------------------------------------------------------------------------
// Deal with command-line arguments.
// ---------------------------------------------------------------------------
{
  char usage[] = "Usage: project [options] [file]\n"
    "  options:\n"
    "  -h       ... print this message\n"
    "  -n <num> ... 2D projection onto num X num\n"
    "  -z <num> ... 3D projection onto num planes\n"
    "  -w       ... Retain w components in 3D-->2D proj'n [Default: delete]\n"
    "  -u       ... project to uniform grid from GLL\n"
    "  -U       ... project from uniform grid to GLL\n";
 
  while (--argc  && **++argv == '-')
    switch (*++argv[0]) {
    case 'h':
      cout << usage;
      exit (EXIT_SUCCESS);
      break;
    case 'n':
      if (*++argv[0]) np = atoi (*argv);
      else { --argc;  np = atoi (*++argv); }
      break;
    case 'z':
      if (*++argv[0]) nz = atoi (*argv);
      else { --argc; nz = atoi (*++argv); }
      break;
    case 'u':
      uniform =  1;
      break;
    case 'U':
      uniform = -1;
      break;
    case 'w':
      keepW   = true;
      break;
    default:
      cerr << usage;
      exit (EXIT_FAILURE);
      break;
    }

  if (argc == 1) {
    input = new ifstream (*argv);
    if (input -> fail())
      Veclib::alert (prog, "unable to open input file", ERROR);
  } else input = &cin;
}


static bool getDump (istream&           ifile ,
		     ostream&           ofile ,
		     vector<Field2DF*>& u     ,
		     int&               nrnew ,
		     int&               nsnew ,
		     int&               nznew ,
		     int&               nel   ,
		     bool&              keepW ,
		     int&               finc  ,
		     char*              fields)
// ---------------------------------------------------------------------------
// Read next set of field dumps from ifile, put headers on ofile.
// Note that the output header matches the requested values of N_P and
// N_Z, but the input is done according to the values found in the
// original field dump.
//
// The creation time written to the new field dump header will be the
// current wall clock time.
//
// Convert to Fourier space.
// ---------------------------------------------------------------------------
{
  char   buf[StrMax], s1[StrMax], oldfields[StrMax], session[StrMax], *tok;
  int    i, j, nf, nr, ns, nz, simstep;
  double kinvis, beta, simtime, deltat;
  bool   swab;

  if (ifile.getline(buf, StrMax).eof()) return false;

  if (!strstr (buf, "Session"))
    Veclib::alert (prog, "not a field file", ERROR);

  tok = strtok (buf, " ");
  strcpy (session, tok);

  ifile.getline (buf, StrMax); 	// -- Creation date (discard).

  // -- Get numerical description of field sizes.

  ifile >> nr >> ns >> nz >> nel;
  ifile.getline (buf, StrMax);
  
  if (!nrnew) { nrnew = nr; nsnew = ns; }
  if (!nznew) nznew = nz;

  // -- Get runtime info.

  ifile >> simstep; ifile.getline (buf, StrMax);
  ifile >> simtime; ifile.getline (buf, StrMax);
  ifile >> deltat;  ifile.getline (buf, StrMax);
  ifile >> kinvis;  ifile.getline (buf, StrMax);
  ifile >> beta;    ifile.getline (buf, StrMax);

  // -- Input field names.

  ifile >> oldfields;
  nf = strlen (oldfields);

  finc = 0; 			  // -- Default: fields out = fields in.
  if (strstr (oldfields, "uv")) { // -- Velocity vectors present.
    // 2D2C --> 3D3C.
    if (nz == 1 && nznew > 1 && !strchr (oldfields, 'w')) finc = +1;
    // 3D3C --> 2D2C.
    else if (nz > 1 && nznew == 1 && !keepW)              finc = -1;
  }

  // -- Change the list of fields if required.
  
  for (j = 0, i = 0; i < nf; i++) {
    if (finc == -1 && oldfields[i] == 'w') continue;
    if (finc == +1 && oldfields[i] == 'v') {
      fields[j++] = 'v';
      fields[j++] = 'w';
    } else
      fields[j++] = oldfields[i];
  }
  fields[j] = '\0';
  ifile.getline (buf, StrMax);

  // -- Arrange for byte-swapping of input if required.

  ifile.getline (buf, StrMax);
  swab = Veclib::byteSwap (buf);

  // -- Output a standard header string.

  Veclib::headerStr (s1, session, NULL, nrnew, nsnew, nznew, nel,
		     simstep, simtime, deltat, kinvis, beta, fields, "binary");
    
  cout << s1;
  
  if (u.size() != nf) {
    u.resize (nf);
    for (i = 0; i < nf; i++) u[i] = new Field2DF (nr,ns,nz,nel,oldfields[i]);
  }

  for (i = 0; i < nf; i++) {
    ifile >> *u[i];
    if (swab) u[i] -> reverse();
    u[i] -> transform (FORWARD);
  }

  return ifile.good();
}


