/*****************************************************************************
 * repeatxy: utility to read a field file, and output the number of
 * repetitions in x-y planes indicated on the command line.
 *
 * Copyright (c) 2002+, Hugh M Blackburn
 *
 * Usage
 * -----
 * repeatxy [-h] [-n <rep>] [input[.fld]
 *
 * Synopsis
 * --------
 * This is the planar equivalent of repeatz.c.  Field must be binary
 * format.  By default, output the original file, no repeats.  Adust
 * Nel in header as appropriate.
 *****************************************************************************/

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

#include <cfemlib.h>
#include <cfemdef.h>
#include <cveclib.h>

static void getargs (int, char**, FILE**, int*);
static char prog[] = "repeatxy";


int main (int    argc,
	  char** argv)
/* ------------------------------------------------------------------------- *
 * Wrapper.
 * ------------------------------------------------------------------------- */
{
  char   buf[STR_MAX];
  int    i, j, k, np, nz, nel, nrep = 1;
  int    nfields, nplane, ntot, swab;
  FILE   *fp_in = stdin, *fp_out = stdout;
  double *data;
  char   session[STR_MAX], fields[STR_MAX];
  int    step;
  double time, timestep, kinvis, beta;

  getargs (argc, argv, &fp_in, &nrep);

  while (fgets (buf, STR_MAX, fp_in)) { 

    if (!strstr (buf, "Session"))
      message (prog, "input is not a field file", ERROR);
    sscanf (buf, "%s", session);
    
    fgets (buf, STR_MAX, fp_in); /* -- Creation date (discard). */
    
    fgets (buf, STR_MAX, fp_in);
    if (sscanf (buf, "%d%*s%d%d", &np, &nz, &nel) != 3)
      message (prog, "unable to read the file size", ERROR);

    fgets  (buf, STR_MAX, fp_in); sscanf (buf, "%d",  &step);
    fgets  (buf, STR_MAX, fp_in); sscanf (buf, "%lf", &time);
    fgets  (buf, STR_MAX, fp_in); sscanf (buf, "%lf", &timestep);
    fgets  (buf, STR_MAX, fp_in); sscanf (buf, "%lf", &kinvis);
    fgets  (buf, STR_MAX, fp_in); sscanf (buf, "%lf", &beta);
    fgets  (buf, STR_MAX, fp_in); sscanf (buf, "%s",  fields);

    nfields = strlen (fields);

    fgets (buf, STR_MAX, fp_in);
    
    if (!strstr(buf, "binary"))
      message (prog, "input file not binary format", ERROR);
    if (!strstr (buf, "endian"))
      message (prog, "input field file in unknown binary format", WARNING);
    else
      swab = needbyteswap (buf);

    semheaderstr (buf, session, NULL, np, np, nz, nel*nrep,
		  step, time, timestep, kinvis, beta, fields, "binary");
    
    fputs (buf, fp_out);

    nplane = np * np * nel;
    ntot   = nz * nplane;
    data   = dvector (0, ntot - 1);
    
    /* -- Read and write all data fields. */

    for (i = 0; i < nfields; i++) {
      if (fread (data, sizeof (double), ntot, fp_in) != ntot)
	message (prog, "an error occured while reading", ERROR);
      if (swab) dbrev (ntot, data, 1, data, 1);

      for (j = 0; j < nz; j++)
	for (k = 0; k < nrep; k++)
	  if (fwrite (data+j*nplane, sizeof(double), nplane, fp_out) != nplane)
	    message (prog, "an error occured while writing", ERROR);
    }
  }

  freeDvector (data, 0);
  
  return EXIT_SUCCESS;
}


static void getargs (int    argc ,
		     char** argv ,
		     FILE** fp_in,
		     int*   nrep )
/* ------------------------------------------------------------------------- *
 * Parse command line arguments.
 * ------------------------------------------------------------------------- */
{
  char c, fname[FILENAME_MAX];
  char usage[] = "repeatxy [-h] [-n <rep>] [input[.fld]\n";

  while (--argc && **++argv == '-')
    switch (c = *++argv[0]) {
    case 'h':
      fputs (usage, stderr);
      exit  (EXIT_SUCCESS);
      break;
    case 'n':
      if (*++argv[0]) *nrep = atoi (*argv);
      else           {*nrep = atoi (*++argv); argc--;}
      break;
    default:
      fprintf (stderr, "%s: unknown option -- %c\n", prog, c);
      break;
    }
  
  if (argc == 1)
    if ((*fp_in = fopen(*argv, "r")) == (FILE*) NULL) {
      sprintf(fname, "%s.fld", *argv);
      if ((*fp_in = fopen(fname, "r")) == (FILE*) NULL) {
	fprintf(stderr, "%s: unable to open input file -- %s or %s\n",
		prog, *argv, fname);
	exit (EXIT_FAILURE);
      }
    }
}
