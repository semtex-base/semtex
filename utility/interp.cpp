//////////////////////////////////////////////////////////////////////////////
// interp: utility to interpolate results from a field file onto a set
// of 2D points.
//
// Copyright (c) 1997+, Hugh M Blackburn
//
// Usage
// -----
// interp [-h] [-v] [-q] [-m file] -s session dump
//
// Synopsis
// --------
// This utility is mainly intended for extraction of data from one
// mesh onto another, potentially non-conforming mesh.  (This is in
// contrast to the purpose of the project utility, which generates
// data on the same spectral element mesh but at a different
// polynomial order.)
//
// Interpolation is 2D and for 3D fields the data will be output in
// plane-by-plane order.  The input field file must be in binary
// format.  Output is always ASCII format -- if required, use convert
// utility to change output field files back to binary format for
// subsequent use. Each line of output contains the values for the
// fields in the file in columns, in the order they were written to
// the field file.
//
// The set of points can either be in the form output from meshpr, e.g.
// 12 12 4 422 NR NS NZ NEL
//         1.32461       0.514135
//         1.31102       0.509459
//            ..             ..
// in which case the output data will be in field dump format with header,
// or the input can be an (unstructured) set without a header, e.g.
//         1.32461       0.514135
//         1.31102       0.509459
//            ..             ..
// in which case the output has a matching lack of structure.
//
// If a point cannot be located in the mesh, zero values are output for
// that point location.  Points can either be supplied on standard input
// or in a named file.
//
// Notes from src/element.cpp: Point tolerances can be changed by
// setting token TOL_POS, but usually it's better to increase NR_MAX
// above its default, since TOL_POS is used both as a location test at
// end of iteration, and on the N--R forcing term.
///////////////////////////////////////////////////////////////////////////////

#include <ctime>
#include <sem.h>

static char  prog[]  = "interp";
static int   verbose = 0;
static int   nreport = 100;
static void  getargs    (int, char**, bool&, char*&, char*&, char*&);
static void  loadPoints (istream&, int&, int&, int&, vector<Point*>&);
static void  findPoints (vector<Point*>&, vector<Element*>&,
			 vector<Element*>&, vector<double>&, vector<double>&,
			 bool);
static int getDump      (ifstream&, vector<AuxField*>&, vector<Element*>&,
			 const int, const int, const int,
			 int&, double&, double&, double&, double&);
static void  putHeader  (const char*, const vector<AuxField*>&, const int,
			 const int, const int, const int, const double,
			 const double, const double, const double);
static void  loadName   (const vector<AuxField*>&, char*);


int main (int    argc,
	  char** argv)
// ---------------------------------------------------------------------------
// Driver.
// ---------------------------------------------------------------------------
{
  char              *session, *dump, *points = 0;
  int               NP, NZ,  NEL;
  int               np, nel, ntot;
  int               i, j, k, nf, step;
  ifstream          fldfile;
  istream*          pntfile;
  FEML*             F;
  Mesh*             M;
  double            c, time, timestep, kinvis, beta;
  vector<double>    r, s;
  vector<Point*>    point;
  vector<Element*>  elmt;
  vector<Element*>  Esys;
  vector<AuxField*> u;
  bool              quiet = false;

  Femlib::init ();
  
  getargs (argc, argv, quiet, session, dump, points);

  fldfile.open (dump, ios::in);
  if (!fldfile) Veclib::alert (prog, "no field file", ERROR);

  // -- Set up 2D mesh information.
  
  F   = new FEML (session);
  M   = new Mesh (F);

  NEL = M -> nEl();  
  NP  = Femlib::ivalue ("N_P");
  NZ  = Femlib::ivalue ("N_Z");
  
  Geometry::set (NP, NZ, NEL, Geometry::Cartesian);
  Esys.resize   (NEL);

  for (k = 0; k < NEL; k++) Esys[k] = new Element (k, NP, M);
  
  // -- Construct the list of points, then find them in the Mesh.

  if (points) {
    pntfile = new ifstream (points);
    if (pntfile -> fail())
      Veclib::alert (prog, "unable to open point file", ERROR);
  } else 
    pntfile = &cin;

  np = nel = ntot = 0;

  loadPoints (*pntfile, np, nel, ntot, point);
  findPoints (point, Esys, elmt, r, s, quiet);

  // -- Load field file, interpolate within it.

  cout.precision (13);
  while (getDump (fldfile, u, Esys, NP, NZ, NEL,
		  step, time, timestep, kinvis, beta)) {

    if (np) putHeader (session, u,  np, NZ, nel,
		       step, time, timestep, kinvis, beta);

    nf = u.size();
    for (k = 0; k < NZ; k++)
      for (i = 0; i < ntot; i++) {
	for (j = 0; j < nf; j++) {
	  if   (elmt[i]) c = u[j] -> probe (elmt[i], r[i], s[i], k);
	  else           c = 0.0;
	  cout << setw(20) <<  c;
	}
	if (verbose && !((i + 1)% nreport))
	  cerr 
	    << "interp: plane " << k + 1 << ", "
	    << i + 1 << " points interpolated" << endl;
	cout << endl;
      }
  }

  return EXIT_SUCCESS;
}


static void getargs (int    argc   ,
		     char** argv   ,
		     bool&  quiet  ,
		     char*& session,
		     char*& dump   ,
		     char*& points )
// ---------------------------------------------------------------------------
// Deal with command-line arguments.
// ---------------------------------------------------------------------------
{
  char usage[] = "Usage: interp [options] -s session dump\n"
    "  options:\n"
    "  -h      ... print this message\n"
    "  -q      ... run quietly: omit warnings about unlocated points\n"
    "  -m file ... name file of point data [Default: stdin]\n"
    "  -v      ... verbose output\n";
 
  while (--argc  && **++argv == '-')
    switch (*++argv[0]) {
    case 'h':
      cout << usage;
      exit (EXIT_SUCCESS);
      break;
    case 'q':
      quiet = true;
      break;
    case 'm':
      if (*++argv[0])
	points = *argv;
      else {
	--argc;
	points = *++argv;
      }
      break;
    case 's':
      if (*++argv[0])
	session = *argv;
      else {
	--argc;
	session = *++argv;
      }
      break;
    case 'v':
      verbose = 1;
      break;
    default:
      cerr << usage;
      exit (EXIT_FAILURE);
      break;
    }

  if   (!session)  Veclib::alert (prog, "no session file", ERROR);
  if   (argc != 1) Veclib::alert (prog, "no field file",   ERROR);
  else             dump = *argv;
}


static void loadPoints (istream&        pfile,
			int&            np   ,
			int&            nel  ,
			int&            ntot ,
			vector<Point*>& point)
// ---------------------------------------------------------------------------
// Load data which describe location of points.
// ---------------------------------------------------------------------------
{
  char          buf[StrMax];
  int           nz = 0, num = 0;
  double        x, y;
  Point*        datum;
  stack<Point*> data;

  pfile.getline (buf, StrMax);
  if (strstr (buf, "NEL")) {	// -- This is a structured set of points.
    sscanf (buf, "%d %d %d %d", &np, &np, &nz, &nel);
    ntot    = np * np * nel;
    nreport = np * np;

  } else {
    if   (sizeof (double) == sizeof (double)) sscanf (buf, "%lf %lf", &x, &y);
    else                                      sscanf (buf, "%f  %f",  &x, &y);

    datum = new Point;
    datum -> x = x;
    datum -> y = y;
    data.push (datum);
    num++;
  }

  while (pfile >> x >> y) {
    datum = new Point;
    datum -> x = x;
    datum -> y = y;
    data.push (datum);
    num++;
    if (nz && num == ntot) break;
  }

  if (np && num != ntot) {
    sprintf (buf, "No. of points (%1d) mismatches declaration (%1d)",num,ntot);
    Veclib::alert (prog, buf, ERROR);
  }

  ntot = num;
  point.resize (ntot);

  while (num--) { point[num] = data.top(); data.pop(); }
}


static void findPoints (vector<Point*>&   point,
			vector<Element*>& Esys ,
			vector<Element*>& elmt ,
			vector<double>&   rloc ,
			vector<double>&   sloc ,
			bool              quiet)
// ---------------------------------------------------------------------------
// Locate points within elements, set Element pointer & r--s locations.
// ---------------------------------------------------------------------------
{
  int            i, k, kold;
  double         x, y, r, s;
  const bool     guess = true;
  const int      NEL   = Esys .size();
  const int      NPT   = point.size();
  vector<double> work(static_cast<size_t>
		      (max (2*Geometry::nTotElmt(), 5*Geometry::nP()+6)));

  elmt.resize (NPT);
  rloc.resize (NPT);
  sloc.resize (NPT);

  for (i = 0; i < elmt.size(); i++) elmt[i] = 0;

  cerr.precision (8);

  kold = 0;
  for (i = 0; i < NPT; i++) {
    x = point[i] -> x;
    y = point[i] -> y;
    if (Esys[kold] -> locate (x, y, r, s, &work[0], guess)) {
      elmt[i] = Esys[kold];
      rloc[i] = r;
      sloc[i] = s;
    } else {
      for (k = 0; k < NEL; k++) {
	if (k == kold) continue;
	if (Esys[k] -> locate (x, y, r, s, &work[0], guess)) {
	  elmt[i] = Esys[k];
	  rloc[i] = r;
	  sloc[i] = s;
	  kold    = k;
	  break;
	}
      }
    }

    if (!elmt[i] && !quiet)
      cerr << "interp: point (" << setw(15) << x << ","
	   << setw(15) << y << ") is not in the mesh" << endl;
    if (verbose && !((i + 1)% nreport))
	cerr << "interp: " << i + 1 << " points found" << endl;
  }
  if (verbose) cerr << endl;
}


static int getDump (ifstream&          file,
		    vector<AuxField*>& u   ,
		    vector<Element*>&  Esys,
		    const int          np  ,
		    const int          nz  ,
		    const int          nel ,
		    int&               step,
		    double&            time,
		    double&            tstp,
		    double&            kinv,
		    double&            beta)
// ---------------------------------------------------------------------------
// Load data from field dump, with byte-swapping if required.
// If there is more than one dump in file, it is required that the
// structure of each dump is the same as the first.
// ---------------------------------------------------------------------------
{
  char buf[StrMax], fields[StrMax];
  int  i, nf, npnew, nznew, nelnew;
  bool swab;

  if (file.getline(buf, StrMax).eof()) return 0;
  
  if (!strstr (buf, "Session")) Veclib::alert (prog, "not a field file", ERROR);
  file.getline (buf, StrMax);

  // -- Input numerical description of field sizes.

  file >> npnew >> nznew >> nznew >> nelnew;
  file.getline (buf, StrMax);
  
  if (np != npnew || nz != nznew || nel != nelnew)
    Veclib::alert (prog, "size of dump mismatch with session file", ERROR);

  file >> step;
  file.getline (buf, StrMax);

  file >> time;
  file.getline (buf, StrMax);

  file >> tstp;
  file.getline (buf, StrMax);

  file >> kinv;
  file.getline (buf, StrMax);

  file >> beta;
  file.getline (buf, StrMax);

  // -- Input field names, assumed to be written without intervening spaces.

  file >> fields;
  nf = strlen  (fields);
  file.getline (buf, StrMax);

  // -- Arrange for byte-swapping if required.

  file.getline  (buf, StrMax);
  swab = Veclib::byteSwap (buf);

  // -- Create AuxFields on first pass.

  if (u.size() == 0) {
    u.resize (nf);
    for (i = 0; i < nf; i++)
      u[i] = new AuxField (new double[Geometry::nTotal()], nz, Esys, fields[i]);
  } else if (u.size() != nf) 
    Veclib::alert
      (prog, "number of fields mismatch with first dump in file", ERROR);

  // -- Read binary field data.

  for (i = 0; i < nf; i++) {
    file >> *u[i];
    if (swab) u[i] -> reverse();
  }

  return file.good();
}


static void putHeader (const char*              session,
		       const vector<AuxField*>& u      ,
		       const int                np     ,
		       const int                nz     ,
		       const int                nel    ,
		       const int                step   ,
		       const double             time   ,
		       const double             tstp   ,
		       const double             kinv   ,
		       const double             beta   )
// ---------------------------------------------------------------------------
// Write header information for semtex compatible ASCII file on cout;
// ASCII because we will be interpolating and outputting data
// point-by-point instead of field by field.
// ---------------------------------------------------------------------------
{
  char fields[StrMax], work[StrMax];
  loadName (u, fields);

  Veclib::headerStr (work, session, NULL, np, np, nz, nel, step,
		     time, tstp, kinv, beta, fields, "ASCII");
    
  cout << work;
}


static void loadName (const vector<AuxField*>& u,
		      char*                    s)
// --------------------------------------------------------------------------
// Load a string containing the names of fields.
// ---------------------------------------------------------------------------
{
  int i, N = u.size();

  for (i = 0; i < N; i++) s[i] = u[i] -> name();
  s[N] = '\0';
}
