utility README
==============

Synopsis
--------

Utility programs for pre- and post-processing, coded in both C and
C++, as well as various shell scripts.  Many of the compiled utilities
will read from standard input and write to standard output.  Most will
also provide a usage prompt if run with the -h command-line flag.
Some python utilities are available in another directory, see the
[python README](../python/README.md).

Catalogue
---------

[**addfield**](./addfield.cpp) Add extra fields (e.g. kinetic energy,
  vorticity) computed from an existing field file.

[**addquick**](./addquick) Example shell script to add a
  simply-computable field to an existing field file.

[**assemble**](./assemble.cpp) Produce global element-edge assembly
  numbering from a session file, for information purposes.

[**averagec**](./averagec) Shell script to compute row-wise averages
of columns of numbers in a text file, i.e. average across columns.

[**averager**](./averager) Shell script to compute column-wise
averages of columns of numbers in a text file, i.e. average down rows.

[**avgdump**](./avgdump.c) Find (weighted) average of two field files.

[**aziavg**](./aziavg) Shell script to compute azimuthal averages of
data assumed to lie in a circular domain/session file of unit
diameter, centred at origin. Developed from *rayavg*, uses *averagec*,
*probeline*, *linesdist*, *laminate*, *slit*

[**calc**](./calc.cpp) A command-line based calculator based on the
  *semtex* function parser in femlib/initial.y.

[**chop**](./chop.c) Reproduce given lines of a text file, starting a
  given line number, and with given increment.  Often used with
  *slit*.

[**compare**](./compare.cpp) Compare results in a field file to
  functions in session file.  Alternatively, use functions in session
  file to generate a field (e.g. restart) file.

[**convert**](./convert.c) Convert binary format field file to ASCII
  format, and vice-versa. Also, extract a specific dump from a
  sequence inside a field file.

[**eneq**](./eneq.cpp) Produce terms of the MKE and TKE equations from
  averages written to an average-value field file (session.avg).
  Presently, Cartesian only.

[**index**](./index) Shell script to produce a sequence of integer
  values, with increments.

[**index00**](./index00) Like *index*, but with 0-based padding,
  e.g. 0000, 0001, etc.

[**integral**](./integral.cpp) Compute integral values of variables in
  a field file.

[**interp**](./interp.cpp) Interpolate field file values at given 2D
  spatial locations.  May be used to interpolate from one mesh to
  another.

[**laminate**](./laminate) Shell script to join up a set of
text/numeric columns held in separate files into a single file.

[**linedist**](./linedist) Shell script to produce a sequence of
  floating-point values.

[**lowpass**](./lowpass.cpp) Lowpass-filter field file values in
  either polynomial or Fourier space.

[**mapmesh**](./mapmesh.cpp) Change element-vertex NODE spatial values
  in a session file according to command-line formulae.

[**massmat**](./massmat.cpp) Produce mass-matrix values for a mesh.

[**meshplot**](./meshplot.cpp) From mesh locations output by *meshpr*,
generate a *Postcript* plot of mesh.

[**meshpr**](./meshpr.cpp) From a session file, produce x-y (and z)
  locations of mesh points.

[**moden**](./moden.c) From a 3D field file, produce a 2D field file
  with flow KE in a given Fourier mode.

[**modep**](./modep.cpp) Form scalar product of velocity data in a
  field file with a shape given in another field file, for a given
  Fourier mode.

[**noiz**](./noiz.c) Add white noise of supplied standard deviation to
  data in a field file.  If required, on a given Fourier mode.
  Optionally, filter out data on a given Fourier mode.

[**nonlin**](./nonlin.cpp) From a field file containing velocity data
**u**, output nonlinear terms **u**.grad(**u**).

[**normal**](./normal.c) Produce a sequence of normally-distributed
  random data.

[**phase**](./phase.cpp) Fourier-space data operations: phase shift,
  reflect, conjugation.

[**pline**](./pline) Shell script to generate a unformly-spaced
  sequence of points along a straight line in x,y,z space.

[**preplot**](./preplot.c) Old *AMTEC* utility to produce a
  binary-format *Tecplot* .plt file. Called by *sem2tec*.

[**probe**](./probe.cpp) Probe field file data at given 2D/3D
  locations supplied either on command line of in file.  Alternative
  interfaces *probeline* and *probeplane* (made as soft links) do
  similar things but either along a line in space or on a plane in
  space.

[**project**](./project.cpp) Project values in a field file onto
  different interpolation orders either in 2D element or 3D Fourier
  spaces.

[**rayavg**](./rayavg) Shell script to generate field-dump averages
  along 2D rays emanating from a supplied x-y line.  Uses *pline*,
  *probe*, *averager*.

[**rectmesh**](./rectmesh.cpp) From two sequences with x and y values,
  produce a session file with a rectangular (tensor-product) mesh to
  be used as a starting point for a new session file after appropriate
  editing.

[**repeatxy**](./repeatxy.c) From a field file, generate another with
  x-y repetitions.

[**repeatz**](./repeatz.c) From a field file, generate another with
  z-repetitions.

[**repmesh**](./repmesh.cpp) From a session file, generate another
  which is either a 2D refection about x or y axis, or an angular
  rotation about a given point.

[**resubmit**](./resubmit) Shell script that returns 1 (shell false)
  if time in field file exceeds a given value, otherwise 0 (shell
  true). (Is resubmission required?)

[**rstress**](./rstress.c) From a field file that contains averages of
  velocity field correlations, compute covariances (i.e. subtract
  products of averages from averages of products) a.k.a. Reynolds
  stresses.  Alternatively add, subtract, multiply two field files.

[**run_job**](./run_job) Shell script to automate the task of running
  the same job in a list of directories.  As an example job, see
  *run_job_example*, which edits a session file to replace a given
  TOKEN with the name of the current directory. Ron H, perhaps
  unkindly, referred to *run_job* as "grad student in a can".

[**save**](./save) Shell script to automate the task of saving a
  sequence of runtime-generated files, relinking things like
  session.rst to the last field file.

[**sem2nek**](./sem2nek.cpp) From a session file, generate a
  *NEKTON*-style .rea file.  Probably very out of date by now, please
  let me know.

[**sem2tec**](./sem2tec.c) From a field file, generate a
  *Tecplot*-format .plt file (can also be read by *VisIt*,
  *Paraview*).

[**sem2vtk**](./sem2vtk.c) From a field file, generate a VTK file for
*VisIt*, *Paraview*.  Very likely, *sem2tec* is more reliable.

[**slit**](./slit.c) Reproduce only nominated columns of text
  input. Can be used with *chop* to "dice and slice" ASCII data.

[**stressdiv**](./stressdiv.cpp) Compute negative divergence of
  Reynolds-stress field file data (may be used as forcing terms).

[**sem2nek**](./sem2nek.cpp) From a session file, generate a
  *NEKTON*-format .rea file.  More recent than *topre*, but probably
  also out-of-date.

[**traction**](./traction.cpp) Compute tractions at walls from field
  file data.  (The same functionality is done on-the-fly by *dns*.)

[**transform**](./transform.cpp) Either Fourier or 2D polynomial
  transform of data in field file.  Inverse also.

[**wallmesh**](./wallmesh.cpp) Extract the wall mesh locations of
  session file from *meshpr* output.  May be used with *sem2tec* to
  generate *Tecplot* file of data on wall surfaces.

[**xfield**](./xfield) Shell script to extract one data field from a
  field file.

[**xplane**](./xplane.c) Extract 2D plane of data from a 3D field
  file.

Notes
-----

* See also the brief descriptions provided in Chapter 6 of the semtex
  user guide. More information is generally available in the header
  sections of source files (follow links above).

* In general, the C programs provide actions which do not rely on
  spectral element shape function operators, but may e.g. do
  manipulations based on 2D elemental storage. Usually these are
  stand-alone pieces of code and do not link other object files to
  create an exceutable.

* On the other hand, the C++ programs can do operations which require
  knowledge of elemental shape functions etc, and which may build upon
  routines defined in src/\*.cpp: quite often, the C++ programs here
  also link object files compiled from src/\*.cpp, and/or femlib,
  routines.

------------------------------------------------------------------------------
