/*****************************************************************************
 * repeatz: utility to read a field file, and output the number of
 * repetitions in the z direction indicated on the command line.
 *
 * Copyright (c) 2002+, Hugh M Blackburn
 *
 * Usage
 * -----
 * repeatz [-h] [-n <rep>] [-f] [input[.fld]
 *
 * Synopsis
 * --------
 * Field must be binary format.  By default, output the original file,
 * no repeats.  Adust Nz and Beta in header as appropriate.
 *
 * NB: Nz must be adjusted so it has prime factors of 2,3,5 to ensure
 * the resulting field can be Fourier transformed in z.  This 'feature'
 * can be defeated with command-line flag -f.
 ****************************************************************************/

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

#include <cfemlib.h>
#include <cfemdef.h>
#include <cveclib.h>

static void getargs (int, char**, FILE**, int*, int*);
static int  roundup (const int);
static void pack    (const double*, const int, double*,
		     const int, const int, const int);

static char prog[] = "repeatz";


int main (int    argc,
	  char** argv)
/* ------------------------------------------------------------------------- *
 * Wrapper.
 * ------------------------------------------------------------------------- */
{
  char   buf[STR_MAX];
  int    i, j, k, n, np, nzin, nzout, nel, nrep = 1, force = 0;
  int    nfields, nplane, nptin, nptout, ntot, swab;
  FILE   *fp_in = stdin, *fp_out = stdout;
  double *datain, *dataout, beta;
  char   session[STR_MAX], fields[STR_MAX];
  int    step;
  double time, timestep, kinvis;

  getargs (argc, argv, &fp_in, &nrep, &force);

  while (fgets (buf, STR_MAX, fp_in)) { 

    if (!strstr (buf, "Session"))
      message (prog, "input is not a field file", ERROR);
    sscanf (buf, "%s", session);
    
    fgets (buf, STR_MAX, fp_in); /* -- Creation date (discard). */
    
    fgets (buf, STR_MAX, fp_in);
    if (sscanf (buf, "%d%*s%d%d", &np, &nzin, &nel) != 3)
      message (prog, "unable to read the file size", ERROR);

    if (!force) {
      if ((nzout = roundup (nzin)) != nzin)
	message (prog, "input nz does not have 2, 3, 5 factors", ERROR);
      nzout = roundup (nzin * nrep);
    } else
      nzout = nzin * nrep;

    fgets  (buf, STR_MAX, fp_in); sscanf (buf, "%d",  &step);
    fgets  (buf, STR_MAX, fp_in); sscanf (buf, "%lf", &time);
    fgets  (buf, STR_MAX, fp_in); sscanf (buf, "%lf", &timestep);
    fgets  (buf, STR_MAX, fp_in); sscanf (buf, "%lf", &kinvis);
    fgets  (buf, STR_MAX, fp_in); sscanf (buf, "%lf", &beta);
    fgets  (buf, STR_MAX, fp_in); sscanf (buf, "%s",  fields);

    nfields = strlen (fields);

    fgets (buf, STR_MAX, fp_in);
    
    if (!strstr(buf, "binary"))
      message (prog, "input file not binary format", ERROR);
    if (!strstr (buf, "endian"))
      message (prog, "input field file in unknown binary format", WARNING);
    else
      swab = needbyteswap (buf);

    semheaderstr (buf, session, NULL, np, np, nzout, nel,
		  step, time, timestep, kinvis, beta, fields, "binary");
    
    fputs (buf, fp_out);

    /* -- Set sizes, allocate storage. */

    nplane  = np * np * nel;
    ntot    = nplane + (nplane & 1);
    nptin   = nzin  * ntot;
    nptout  = nzout * ntot;
    datain  = dvector (0, nptin  - 1);
    dataout = dvector (0, nptout - 1);
    
    /* -- Read and write all data fields. */

    for (i = 0; i < nfields; i++) {
      dzero (nptin,  datain,  1);
      dzero (nptout, dataout, 1);

      for (j = 0; j < nzin; j++) {
	if (fread (datain+j*ntot, sizeof (double), nplane, fp_in) != nplane)
	  message (prog, "an error occured while reading", ERROR);
	if (swab) dbrev (ntot, datain+j*ntot, 1, datain+j*ntot, 1);
      }

      if (force) { /* -- We can just copy in physical space. */
	for (k = 0; k < nrep; k++) { 
	  for (j = 0; j < nzin; j++) {
	    if (fwrite (datain+j*ntot, sizeof(double),nplane, fp_out) != nplane)
	      message (prog, "an error occured while writing", ERROR);
	  }
	}
      } else {	   /* -- Have to go to Fourier space for padding. */
	/* -- Use dDFTr_netlib since it doesn't require nplane to be even. */
	dDFTr_netlib (datain,  nzin,  ntot, FORWARD);
	pack         (datain,  nzin,  dataout, nzout, nrep, ntot);
	dDFTr_netlib (dataout, nzout, ntot, INVERSE);

	for (j = 0; j < nzout; j++)
	  if (fwrite (dataout+j*ntot, sizeof (double),nplane, fp_out) != nplane)
	    message (prog, "an error occured while writing", ERROR);
      }
    }
  } 

  freeDvector (datain,  0);
  freeDvector (dataout, 0);
  
  return EXIT_SUCCESS;
}


static void getargs (int    argc ,
		     char** argv ,
		     FILE** fp_in,
		     int*   nrep ,
		     int*   force)
/* ------------------------------------------------------------------------- *
 * Parse command line arguments.
 * ------------------------------------------------------------------------- */
{
  char c, fname[FILENAME_MAX];
  char usage[] = "repeatz [-h] [-n <rep>] [-f] [input[.fld]\n";

  while (--argc && **++argv == '-')
    switch (c = *++argv[0]) {
    case 'h':
      fputs (usage, stderr);
      exit  (EXIT_SUCCESS);
      break;
    case 'f':
      *force = 1;
      break;
    case 'n':
      if (*++argv[0]) *nrep = atoi (*argv);
      else           {*nrep = atoi (*++argv); argc--;}
      break;
    default:
      fprintf (stderr, "%s: unknown option -- %c\n", prog, c);
      break;
    }
  
  if (argc == 1)
    if ((*fp_in = fopen(*argv, "r")) == (FILE*) NULL) {
      sprintf(fname, "%s.fld", *argv);
      if ((*fp_in = fopen(fname, "r")) == (FILE*) NULL) {
	fprintf(stderr, "%s: unable to open input file -- %s or %s\n",
		prog, *argv, fname);
	exit (EXIT_FAILURE);
      }
    }
}


static int roundup (const int nzdes)
/* ------------------------------------------------------------------------- *
 * Roundup number of z planes to suit Fourier transformation.
 * ------------------------------------------------------------------------- */
{
  int n, nz = nzdes, ip, iq, ir, ipqr2;

  if (nz != 1) {
    do {
      n = nz;
      prf235 (&n, &ip, &iq, &ir, &ipqr2);
      nz += (n) ? 0 : 2;
    } while (n == 0);
  }

  return nz;
}


static void pack (const double* datain ,  
		  const int     nzin   ,  
		  double*       dataout, 
		  const int     nzout  , 
		  const int     nrep   ,
		  const int     psize  )
/* ------------------------------------------------------------------------- *
 * Place planes of datain in the appropriate planes of dataout
 * (Fourier space).
 * ------------------------------------------------------------------------- */
{
  const int nmode = nzin  >> 1;
  const int msize = psize << 1;
  int       i;

  dcopy (psize, datain, 1, dataout, 1);
  if (nzin > 1) dcopy (psize, datain + psize, 1, dataout + psize, 1);

  for (i = 1; i < nmode; i++)
    dcopy (msize, datain + i * msize, 1, dataout + i * nrep * msize, 1);
}
