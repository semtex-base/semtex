/*****************************************************************************
 * xplane: from a 3D data file, extract a 2D plane of data on named
 * plane and output 2D data file.
 *
 * Copyright (c) 2000+, Hugh M Blackburn
 *
 * Usage
 * -----
 * xplane [-h] [-n plane] [-2] [input[.fld]
 *
 * Synopsis
 * --------
 * Field must be binary format.  By default, output plane 1 (i.e. the
 * first plane). Optionally (-2) output two consecutive planes, i.e. a
 * Fourier mode (presuming the input has first been Fourier
 * transformed); in this case the requested plane number is the first
 * of the two desired plane indices.  Plane indexing used for command
 * line is 1-based.
 *****************************************************************************/

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

#include <cfemlib.h>
#include <cfemdef.h>
#include <cveclib.h>

static void getargs (int, char**, FILE**, int*, int*);

static char prog[] = "xplane";


int main (int    argc,
	  char** argv)
/* ------------------------------------------------------------------------- *
 * Wrapper.
 *
 * NB: internal plane index is one less than set on command line.
 * ------------------------------------------------------------------------- */
{
  char   buf[STR_MAX], fields[STR_MAX];
  int    i, j, n, np, nz, nel, pindex = 0, twoplanes = 0;
  int    nfields, nplane, npts, ntot, nreal, swab;
  FILE   *fp_in = stdin, *fp_out = stdout;
  double **data;
  char   session[STR_MAX];
  int    step;
  double time, timestep, kinvis, beta;

  getargs (argc, argv, &fp_in, &pindex, &twoplanes);

  while (fgets (buf, STR_MAX, fp_in)) {

    if (!strstr (buf, "Session"))
      message (prog, "input is not a field file", ERROR);
    sscanf (buf, "%s", session);
    
    fgets (buf, STR_MAX, fp_in); /* -- Creation date (discard). */
    
    fgets (buf, STR_MAX, fp_in);
    if (sscanf (buf, "%d%*s%d%d", &np, &nz, &nel) != 3)
      message (prog, "unable to read the file size", ERROR);

    if (pindex >= nz) {
      sprintf (fields, "requested plane (%1d) not present (nz = %1d)",
	       pindex + 1, nz);
      message (prog, fields, ERROR);
    }

    fgets  (buf, STR_MAX, fp_in); sscanf (buf, "%d",  &step);
    fgets  (buf, STR_MAX, fp_in); sscanf (buf, "%lf", &time);
    fgets  (buf, STR_MAX, fp_in); sscanf (buf, "%lf", &timestep);
    fgets  (buf, STR_MAX, fp_in); sscanf (buf, "%lf", &kinvis);
    fgets  (buf, STR_MAX, fp_in); sscanf (buf, "%lf", &beta);
    fgets  (buf, STR_MAX, fp_in); sscanf (buf, "%s",  fields);

    nfields = strlen (fields);

    fgets (buf, STR_MAX, fp_in);
    
    if (!strstr(buf, "binary"))
      message (prog, "input file not binary format", ERROR);
    if (!strstr (buf, "endian"))
      message (prog, "input field file in unknown binary format", WARNING);
    else
      swab = needbyteswap (buf);

    semheaderstr (buf, session, NULL, np, np, (twoplanes) ? 2 : 1, nel,
		  step, time, timestep, kinvis, beta, fields, "binary");
    
    fputs (buf, fp_out);
    
    /* -- Set sizes, allocate storage. */

    nplane = np * np * nel;
    nreal  = (twoplanes) ? nplane + nplane : nplane;
    npts   = nz * nplane;
    ntot   = nfields * npts;
    data   = dmatrix (0, nfields - 1, 0, npts - 1);
    
    /* -- Read in all data fields. */

    for (i = 0; i < nfields; i++) {
      for (j = 0; j < nz; j++) {
	if (fread(data[i]+j*nplane,sizeof(double),nplane,fp_in) != nplane)
	  message (prog, "an error occured while reading", ERROR);
	if (swab) dbrev (nplane, data[i]+j*nplane, 1, data[i]+j*nplane, 1);
      }
    }

    /* -- Write out nominated plane(s). Note planes are consecutive. */

    for (i = 0; i < nfields; i++)
      if (fwrite(data[i]+pindex*nplane,sizeof(double),nreal,fp_out) != nreal)
	message (prog, "an error occured while writing", ERROR);
    
    freeDmatrix (data,  0, 0);
  } 
  
  return EXIT_SUCCESS;
}


static void getargs (int    argc     ,
		     char** argv     ,
		     FILE** fp_in    ,
		     int*   plane    ,
		     int*   twoplanes)
/* ------------------------------------------------------------------------- *
 * Parse command line arguments.
 * ------------------------------------------------------------------------- */
{
  char c, fname[FILENAME_MAX];
  char usage[] = "xplane [-h] [-n plane] [-2] [input[.fld]\n";

  while (--argc && (*++argv)[0] == '-')
    switch (c = *++argv[0]) {
    case 'h':
      fputs (usage, stderr);
      exit  (EXIT_SUCCESS);
      break;
    case 'n':
      if (*++argv[0]) *plane = atoi (*argv);
      else {*plane = atoi (*++argv); argc--;}
      (*plane)--;
      break;
    case '2':
      *twoplanes = 1;
      break;
    default:
      fprintf (stderr, "%s: unknown option -- %c\n", prog, c);
      break;
    }
  
  if (argc == 1)
    if ((*fp_in = fopen(*argv, "r")) == (FILE*) NULL) {
      sprintf(fname, "%s.fld", *argv);
      if ((*fp_in = fopen(fname, "r")) == (FILE*) NULL) {
	fprintf(stderr, "%s: unable to open input file -- %s or %s\n",
		prog, *argv, fname);
	exit (EXIT_FAILURE);
      }
    }
}
