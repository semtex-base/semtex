#ifndef ASSEMBLYMAP_H
#define ASSEMBLYMAP_H

class AssemblyMap
// ===========================================================================
// This class is a holder for Field Element-boundary numbering
// schemes.  Different Fields can potentially have unique
// AssemblyMaps, or they may share them with other Fields, according
// to distribution of essential BCs.
//
// The numbering system is associated with the Geometry class, but
// augments it by allowing for connectivity and boundary conditions.
//
// _Bmask and _emask provide a hierarchy of descriptions of element-
// boundary-node essential boundary condition masks:
//
// 1. _bmask is a vector of int, 4*(np-1)*nel (i.e. nbndry) in length,
//    which describe the CCW traverse of element-boundary nodes.
//    Values set to 1 indicate that the node has an imposed
//    (essential) BC, values set to 0 indicate that the node
//    either has a natural or no BC.
//
// 2. _emask is the next level of the hierarchy, a vector of ints,
//    nel long.  Values are set to 1 if the corresponding element has
//    any bmask values set to true, otherwise set to 0.
//
// (On first acquaintance it might seem more logical to use vectors of
// bool type for these masks, but one cannot obtain an unique byte
// address of an individual bool in a vector, which turns out to be an
// issue.  We could perhaps have used a short int, instead, but that
// would break our veclib conventions, where int (same as int) is
// the smallest integer type considered.)
//
// _Nglobal specifies the number of global nodes, while _nsolve ( <=
// _nglobal) specifies the number of global nodes which have _bmask
// values of 0, i.e. where the nodal values will be solved for instead
// of set explicitly.
//
// _Nglobal and _nsolve also effectively provide a top level in the
// _bmask/_emask hierarchy, since if their difference is non-zero then
// at least one _bmask value (and _emask value) will be 1.
// 
// _Btog gives global node numbers (i.e. assembly map) to
// Element-boundary nodes, same length as _bmask (i.e. _nbndry).  The
// optimization level describes the scheme used to build _btog.
//
// An AssemblyMap may be uniquely identified by the entries in _btog,
// or equivalently the entries of _bmask, together with optimization
// level/global elliptic problem solution strategy.
// ===========================================================================
{
public:
  AssemblyMap (const int, const int, const int,
	       const vector<int>&, const vector<int>&,
	       const char, const int);
 ~AssemblyMap () { };

  bool         willMatch (const vector<int>&)     const;
  void         addTag    (const char, const int);
  bool         matchTag  (const char, const int)  const;
  void         printTags (char*)                  const;

  int        nGlobal () const { return _nglobal; }
  int        nSolve  () const { return _nsolve;  }
  int        nBand   () const { return _nbandw;  }

  const int* bmask () const { return &_bmask[0];         }
  const int* emask () const { return &_emask[0];         }
  const int* btog  () const { return &_btog[0];          }  // Assembly map.
  int        fmask () const { return _nglobal - _nsolve; }
  
private:
  int _optlev ;	  // Optimization level used for btog.
  int _nglobal;	  // Number of unique globally-numbered nodes.
  int _nbndry ;	  // Number of element-edge nodes.
  int _nsolve ;	  // Number of non-masked global nodes.
  int _nbandw ;	  // Bandwidth of btog (includes diagonal).
  int _np     ;	  // Number of nodes on an element edge.
  int _nel    ;	  // Number of elements;

  vector<int> _bmask;	  // 1 for essential-BC nodes, 0 otherwise.
  vector<int> _emask;	  // 1 if associated Element has any essential set.
  vector<int> _btog ;	  // Assembly mapping for all element-boundary nodes.

  vector<pair<char, int>* > _tag; // Field name and mode tag pairs for *this.

  int sortGid         (vector<int>&, const vector<int>&);
  int buildAdjncySC   (vector<int>&, vector<int>&, const int = 0) const;
  int globalBandwidth () const;
  void  connectivSC   (vector<vector<int> >&, const int*,
		       const int*, const int) const;
  int bandwidthSC     (const int*, const int*, const int) const;
  void  RCMnumbering  ();
};

#endif
