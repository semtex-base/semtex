#ifndef EDGE_H
#define EDGE_H

class Edge
// ===========================================================================
// Element-edge data and operators.
// ===========================================================================
{
public:
  Edge (const char*, const Element*, const int); 

  int  bOff  () const { return _elmt -> ID() * Geometry::nExtElmt(); }
  int  dOff  () const { return _doffset; }
  int  dSkip () const { return _dskip; }

  const double* nx () const { return _nx; }
  const double* ny () const { return _ny; }

  void   get      (const double*,double*)                                const;
  void   geometry (double*,double*,double* = 0,double* = 0,double* = 0)  const;
  bool   isCurved () const { return _curved; }

  void   sideGrad (const double* u, double* ux, double* uy, double* wk) const 
                       { _elmt -> sideGrad (_side, u + _eoffset, ux, uy, wk); }
  void   curlCurl (const int,
		   const double*,const double*,const double*,
		   const double*,const double*,const double*,
		   double*,double*,double*,double*,double*)              const;

  void   mulY     (double*)                                              const;
  void   divY     (double*)                                              const;
  void   sideEvaluate (const char*, double*)                             const;

  bool   inGroup      (const char* grp) const { return !(strcmp (grp,_group)); }
  void   addForGroup  (const char*, const double,  double*)              const;
  void   setForGroup  (const char*, const double,  double*)              const;
  void   dotInForGroup(const char*, const Vector&, double*)              const;

  double vectorFlux (const char*,const double*,const double*,double*)    const;
  double scalarFlux (const char*,const double*,double*)                  const;
  double torqueFlux (const char*,const double*,double*)                  const;

  Vector normTraction (const char*,const double*,double*)                const;
  Vector tangTraction (const char*,const double*,const double*,double*)  const;

  void traction (const int,const double,
		 const double*,const double*,const double*,const double*,
		 const double*,const double*,const double*,const double*,
		 double*,double*,double*,double*,double*,double*,double*)const;

protected:
  int            _np     ;	// Matches Geometry::nP().
  char*          _group  ;	// Group string (if any).

  const Element* _elmt   ;	// Corresponding element.
  int            _side   ;	// Corresponding side.

  int            _eoffset;      // Offset of corresponding element in Field.
  int            _doffset;	// Offset in Field data plane (matches BLAS).
  int            _dskip  ;	// Skip   in Field data plane (matches BLAS).

  double*        _x      ;	// Node locations.
  double*        _y      ;	//
  double*        _nx     ;	// Unit outward normal components at nodes.
  double*        _ny     ;	// 
  double*        _area   ;	// Weighted multiplier for parametric mapping.

  bool           _curved ;      // True if the edge is curved.
};

#endif
