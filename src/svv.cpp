///////////////////////////////////////////////////////////////////////////////
// svv.cpp: provide SVV-stabilized differentiation operator matrices/constants.
//
// The operating convention is similar to what the equivalent femlib
// routines provide: we only return pointers if the input pointers are
// non-null.
//
// Tokens SVV_EPSN and SVV_MN (where 0<SVV_MN<N_P)    (SEM directions) and
//        SVV_EPSZ and SVV_MZ (where 0<SVV_MZ<N_Z/2)  (Fourier direction)
// should be predefined: the default values of -1 for the SVV_M* parameters
// leaves SVV switched off. Typically SVV_EPS* is defined as a multiple of
// KINVIS (say 5*KINVIS).
//
// Equivalences with symbols used in [2]:
// 
// SVV_EPSN <==> \epsilon_{zr}, SVV_MN <==> M_zr
// SVV_EPSZ <==> \epsilon_\phi, SVV_MZ <==> M_\phi
//
// 1.  C Xu & R Pasquetti (2004), 'Stabilized spectral element
//     computations of high Reynolds number incompressible flows', JCP
//     196, 680-704.
//
// 2.  K Koal, J Stiller & HM Blackburn (2012),
//     'Adapting the spectral vanishing viscosity method for
//     large-eddy simulations in cylindrical configurations', JCP 231,
//     3389--3405.
//
// Copyright (c) 2004+, Hugh M Blackburn
///////////////////////////////////////////////////////////////////////////////

#include <map>

#include <sem.h>
    
namespace SVV {

typedef struct { double* dv; double* dt; } vector_pair;

const double* coeffs (const int np)
// ---------------------------------------------------------------------------
// Return from internal storage a pointer to an array Q of SVV filter
// weights for the x and y (non-Fourier) directions. We use the tokens
// SVV_MN and SVV_EPSN (see [1]) to define this. Specifically, we
// create
//
//   S = 1 + eps_N/nu * Q.
//
// If the relevant vector of weights doesn't exist, create it
// first.
// ---------------------------------------------------------------------------
{
  char                        routine[] = "svv::coeffs";
  static map<int, double*>    cmap;
  map<int, double*>::iterator c = cmap.find (np);

  if (c == cmap.end()) {
    const double eps = Femlib:: value ("SVV_EPSN");
    const int    mN  = Femlib:: value ("SVV_MN");
    const double nu  = Femlib:: value ("KINVIS");

    const int    N = np - 1;
    double*      svvcoeff = new double [np];
    int          i;

    cmap[np] = svvcoeff;
    Veclib::zero (np, svvcoeff, 1);

    if ((mN >= 0) && (mN < N) && (eps > EPSDP)) {
      for (i = mN+1; i < np; i++) svvcoeff[i] = exp (-sqr ((N-i)/(1.0*mN-i)));
      for (i = 0; i < np; i++)    svvcoeff[i] = 1.0 + eps/nu * svvcoeff[i];
    }
    else
      for (i = 0; i < np; i++)    svvcoeff[i] = 1.0;
  }

  return cmap[np];
}


const double* coeffs_z (const int numModes)
// ---------------------------------------------------------------------------
// Return from internal storage a pointer to an array Q of SVV filter
// weights for the z (Fourier) direction. We use tokens SVV_MZ and
// SVV_EPZ (see [1]) to define this. Specifically, we create
//
//   S = 1 + eps_N/nu * Q.
//
// If the relevant vector of weights doesn't exist, create it first.
// ---------------------------------------------------------------------------
{
  char                        routine[] = "svv::coeffs_z";
  static map<int, double*>    czmap;
  map<int, double*>::iterator c = czmap.find (numModes);

  if (c == czmap.end()) {
    const double eps  = Femlib:: value ("SVV_EPSZ");
    const int    mN   = Femlib:: value ("SVV_MZ");
    const double nu   = Femlib:: value ("KINVIS");

    const int    N    = Geometry::nMode() - 1;
    const int    base = Geometry::baseMode();

    double*      svvcoeff = new double [numModes];
    int          i;

    czmap[numModes] = svvcoeff;
    Veclib::zero (numModes, svvcoeff, 1);

    if ((mN >= 0) && (mN < N) && (eps > EPSDP)) {
      for (i = base; i < base + numModes; i++) 
        if (i > mN) 
	   svvcoeff[i-base] = exp (-sqr ((N-i)/(1.0*mN - i))) ;
      for (i = 0; i < numModes; i++)  svvcoeff[i] = 1.0 + eps/nu * svvcoeff[i];
    }
    else
      for (i = 0; i < numModes; i++)    svvcoeff[i] = 1.0;
  }

  return czmap[numModes];
}


void operators (const int      np ,
		const double** SDV,
		const double** SDT)
// ---------------------------------------------------------------------------
// Return from internal storage pointers to (flat) arrays that provide
// the SVV-stabilized derivative operator SDV and its transpose SDT.
// Again, if the operators do not exist, we create them and leave on
// internal static storage.
//
//   SDV = [M^{-1}][diag(1+SVV_EPSN/KINVIS*Q)]^1/2[M][DV],
//
// where [DV] is the standard operator, and see [1].
// ---------------------------------------------------------------------------
{
  static map<int, vector_pair>     dmap;
  map <int, vector_pair>::iterator d = dmap.find (np);

  if (d == dmap.end()) {
    double*        dv = new double [sqr (np)];
    double*        dt = new double [sqr (np)];
    const double*  S  = SVV::coeffs (np);
    vector<double> sqrtS (np);
    int            i, j;
    const double   *DV;  	// -- Lagrange interpolant operator matrix.
    const double   *MF;		// -- Forward polynomial transform matrix.
    const double   *MI;		// -- Inverse polynomial transform matrix.

    // -- Gather up the various matrices.

    for (i = 0; i < np; i++) sqrtS[i] = sqrt (S[i]);
    Femlib::quadrature (0, 0, &DV, 0, np, 'L', JAC_ALFA, JAC_BETA);

#if 1  // -- Legendre polynomial transform.
    Femlib::legTran (np, &MF, 0, &MI, 0, 0, 0);
#else  // -- Modal polynomial transform.
    Femlib::modTran (np, &MF, 0, &MI, 0, 0, 0);
#endif

    // -- Multiply them.

    for (i = 0; i < np; i++)
      Veclib::smul (np, sqrtS[i], MF + i*np, 1, dv + i*np, 1);
    Blas::mxm (MI, np, dv, np, dt, np);
    Blas::mxm (dt, np, DV, np, dv, np);

    // -- Make the transpose.

    for (i = 0; i < np; i++)
      for (j = 0; j < np; j++)
	dt[Veclib::row_major (j, i, np)] = dv[Veclib::row_major (i, j, np)];

    // -- Set the map storage.

    dmap[np].dv = dv;
    dmap[np].dt = dt;
  }

  if (SDV) *SDV = const_cast<const double*> (dmap[np].dv);
  if (SDT) *SDT = const_cast<const double*> (dmap[np].dt);
}
}

#if 0
// -- Self-contained testing.
//g++ svv.cpp -I. -I../semtex/include -L../semtex/lib/Darwin -L/sw/lib -lfem -lvec -framework Accelerate -lg2c
int main (int    argc,
	  char** argv)
{
  const int     np = 6;
  int           i, j;
  const double* c;
  const double* S;
  const double* I;

  Femlib::initialize(&argc, &argv);
  Femlib::ivalue ("SVV_MN",   3);
  Femlib:: value ("SVV_EPSN", 0.2);

  c = SVV::coeffs(np);

  for (i = 0; i < np; i++) cout << c[i] << "  ";
  cout << endl;

  Femlib::quadrature (0, 0, &S, 0, np, 'L', JAC_ALFA, JAC_BETA);

  cout << "-- The original matrix D" << endl;
  for (i = 0; i < np; i++) {
    for (j = 0; j < np; j++) {
      cout << S[Veclib::row_major (i, j, np)] << "  ";
    }
    cout << endl;
  }

  SVV::operators (np, &S, &I);

  cout << "-- With SVV modification" << endl;
  for (i = 0; i < np; i++) {
    for (j = 0; j < np; j++) {
      cout << S[Veclib::row_major (i, j, np)] << "  ";
    }
    cout << endl;
  }

  cout << "-- Transpose" << endl;
  for (i = 0; i < np; i++) {
    for (j = 0; j < np; j++) {
      cout << I[Veclib::row_major (i, j, np)] << "  ";
    }
    cout << endl;
  }

  return EXIT_SUCCESS;
}
#endif
