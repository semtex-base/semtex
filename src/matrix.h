#ifndef MATRIX_H
#define MATRIX_H


class MatrixSys
// ===========================================================================
// System of global and local Helmholtz matrices and partitions.
// Matrix factorisations are Cholesky, use LAPACK storage schemes.
// ===========================================================================
{
friend class Field;
//friend ostream& operator << (ostream&, MatrixSys&);
//friend istream& operator >> (istream&, MatrixSys&);
public:
  MatrixSys  (const double, const double, const int, const vector<Element*>&,
	      const BoundarySys*, const AssemblyMap*, const SolverKind);
 ~MatrixSys  ();
  bool match (const double, const double, const AssemblyMap*,
	      const SolverKind) const;

private:
  double  _HelmholtzConstant;	// Same for all modes.
  double  _FourierConstant  ;	// Varies with mode number.
 
  const vector<Boundary*>& _BC;	// Internal copy of Boundary conditions.
  const AssemblyMap*       _AM;	// Internal copy of assembly information.

  int        _nel     ;		// Number of elemental matrices.
  int        _nglobal ;		// Number of unique element-boundary nodes.
  int        _singular;		// If system is potentially singular.
  int        _nsolve  ;		// System-specific number of global unknowns.
  SolverKind _method;		// Flag specifies direct or iterative solver.

  // -- For _method == DIRECT:

  int      _nband ;		// Bandwidth of global matrix (incl. diagonal).
  int      _npack ;		// Number of doubles for global matrix.
  double*  _H     ;		// (Factored) packed global Helmholtz matrix.
  double** _hbi   ;		// Element external-internal coupling matrices.
  double** _hii   ;		// (Factored) internal-internal matrices.
  int*     _bipack;		// Size of hbi for each element.
  int*     _iipack;		// Size of hii for each element.

  // -- For _method == JACPCG:

  int    _npts;		// Total number of unique meshpoints.
  double*  _PC  ;		// Diagonal preconditioner matrix.
};

ostream& operator << (ostream&, MatrixSys&);
istream& operator >> (istream&, MatrixSys&);


class ModalMatrixSys
// ===========================================================================
// A way of organising MatrixSys*'s by Fourier mode.
// ===========================================================================
{
public:
  ModalMatrixSys (const double, const double, const int, const int,
		  const vector<Element*>&, const BoundarySys*, const NumberSys*,
		  const SolverKind);
 ~ModalMatrixSys ();

  const MatrixSys* operator [] (const int i) const { return _Msys[i]; }

private:
  //  char*              _fields;	// Character field tags for this system.
  vector<MatrixSys*> _Msys  ;	// One MatrixSys for each Fourier mode.
};

#endif
