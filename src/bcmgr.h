#ifndef BCMGR_H
#define BCMGR_H

typedef struct bctriple { char group; int elmt; int side; } BCtriple;

class BCmgr
// ===========================================================================
// This is a factory / retrieval service for classes derived from
// Condition, and maintains GROUP descriptors.  In addition, it reads
// and returns NumberSys objects from session.num.  Also, it contains
// code for maintenance and evaluation of computed BC types.
//
// REFERENCES
// ----------
// [1] Karniadakis, Israeli & Orszag, JCP 97:414-443 (1991)
// [2] Blackburn & Sherwin, JCP 197:759-778 (2004)
// [3] Dong, JCP 302:300-328 (2015)
// [4] Dong, Karniadakis & Chryssostomidis, JCP 261:83-105 (2014)
// [5] Liu, Xie & Dong, IJHFF 151:119355 (2020)
// ===========================================================================
{
public:
  BCmgr (FEML*, vector<Element*>&);

  const char*        field        () const { return _fields; }
  const char*        groupInfo    (const char) const;
  const Condition*   getCondition (const char, const char, const int = 0);
  vector<BCtriple*>& getBCedges   () { return _elmtbc; }
  int                nBCedges     () const { return _elmtbc.size(); }
  int                nWall        (); // Why not const: OSX compiler bug?
  int                nAxis        ();
  int                nMatching    (const char*);

  class CondRecd {
  public: 
    char       grp  ;
    char       fld  ;
    Condition* bcn  ;
    char*      value;
  };

  // -- Routines for maintaining and evaluating computed BCs.

  // -- Chicken & egg: buildComputedBCs can't be in class constructor
  //    because we need a Field to do it.  Right?

  void buildComputedBCs (const Field*, const bool = false);

  void maintainFourier  (const int, const Field*, const AuxField**,
			 const AuxField**, const int, const int,
			 const bool = true);  
  void maintainPhysical (const Field*, const vector<AuxField*>&,
			 const int, const int);
  void evaluateCNBCp    (const int, const int, const int, double*);
  void evaluateCMBCp    (const Field*, const int, const int, 
			 const int, double*);
  void evaluateCMBCu    (const Field*, const int, const int, 
			 const int, const char, double*);
  void evaluateCMBCc    (const Field*, const int, const int,
			 const int, double*);
  void accelerate       (const Vector&, const Field*);

private:
  char*              _fields  ; // String containing field names.
  vector<char>       _group   ; // Single-character group tags.
  vector<char*>      _descript; // Group name strings.
  vector<CondRecd*>  _cond    ; // Conditions in storage.
  vector<BCtriple*>  _elmtbc  ; // Group tags for each element-side BC.
  bool               _axis    ; // Session file declared an axis BC group.
  bool               _open    ; // Session file declared an open BC group.

  void buildnum  (const char*, vector<Element*>&);
  void buildsurf (FEML*, vector<Element*>&);

  // -- Storage of past-time values needed for computed BCs:

  int       _nLine;     // Same as for Field storage.
  int       _nEdge;     // Number of edges with BCs attached.
  int       _nZ;        // Same as for Field storage.
  int       _nP;        // Geometry::nP(), number of points on element edge.
  int       _nTime;     // N_TIME, time stepping order.

  double*** _u;         // (Physical) x velocity component.
  double*** _v;         // (Physical) y velocity component.
  double*** _w;         // (Physical) z velocity component.
  double*** _c;         // (Physical) scalar.  

  double*** _uhat;      // (Fourier)  x velocity component.
  double*** _vhat;      // (Fourier)  y velocity component.
  double*** _what;      // (Fourier)  z velocity component.
  double*** _chat;      // (Fourier)  scalar.    

  double*** _un;	// (Fourier)  normal velocity u.n for d(u.n)/dt.
  double*** _divu;	// (Fourier)  KINVIS * div(u).
  double*** _gradu;	// (Fourier)  KINVIS * (normal gradient of velocity).n.
  double*** _hopbc;	// (Fourier)  normal component of [N(u)+f-curlCurl(u)].
  double*** _ndudt;     // (Fourier)  normal component of (partial) du/dt.

  double*   _work;      // Computational workspace (scratch).
  double*   _Theta;     // Inflow switch function.
  double*   _u2;	// (Physical) 2 * kinetic energy (u^2+v^2+w^2)*.
  double*   _unp;       // (Physical) u*.n.
  double*   _Enux;	// Energy flux traction, x component. See Ref [3].
  double*   _Enuy;	// Energy flux traction, y component.
  double*   _H;		// Scalar "energy" fluxion.  See Ref [5].

  bool      _toggle;    // Toggle switch for Fourier transform of KE.
  bool      _scalar;	// Problem has a scalar as well as velocity.  
};

#endif
