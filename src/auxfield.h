#ifndef AUXFIELD_H
#define AUXFIELD_H


class AuxField
//  ==========================================================================
/// Physical field storage class, no BCs.
///
/// An AuxField has a storage area for data, physical space mesh, and a
/// matching list of elements which can access this storage area on an
/// element-wise basis if required.  Functions to operate on fields as
/// a whole are provided.  AuxFields have no boundary conditions, no
/// global numbering system, and no solution routines.
///
/// For 2D problems, the data storage is organized by 2D Elements.
///
/// For 3D problems, Fourier expansions are used in the 3rd direction,
/// and each Fourier mode can be thought of as a separate 2D problem
/// (with real and imaginary parts, or planes, of 2D data).  The data
/// are then organized plane-by-plane, with each plane being a 2D
/// AuxField; if in physical space there are nz planes of data, then
/// there are nz/2 Fourier modes.  Data for the Nyquist mode are stored
/// as the imaginary part of the zeroth Fourier mode, but are kept zero
/// and never evolve.  The planes always point to the same storage
/// locations within the data area.
///
/// The data are transformed to physical space for storage in restart
/// files.
//  ==========================================================================
{
friend istream& operator >> (istream&, AuxField&);
friend ostream& operator << (ostream&, AuxField&);
friend class    Field;
friend class    BCmgr;

public:
  
  AuxField (double*, const int, vector<Element*>&, const char = 0);

  char          name     ()      const { return _name; }
  void          setName  (const char name) { _name = name; }
  void          describe (char*) const;
  const double* data     ()      const { return _data; }

  AuxField& operator  = (const double);
  AuxField& operator += (const double);
  AuxField& operator -= (const double);
  AuxField& operator *= (const double);
  AuxField& operator /= (const double);

  AuxField& operator  = (const AuxField&);
  AuxField& operator  - (const AuxField&);
  AuxField& operator += (const AuxField&);
  AuxField& operator -= (const AuxField&);
  AuxField& operator *= (const AuxField&);
  AuxField& operator /= (const AuxField&);

  AuxField& operator  = (const char*);
  AuxField& axpy        (const double, const AuxField&);

  AuxField& extractMode      (const AuxField&, const int);
  AuxField& innerProduct     (const vector<AuxField*>&,
			      const vector<AuxField*>&,
			      const int = 0);
  AuxField& innerProductMode (const vector<AuxField*>&,
			      const vector<AuxField*>&,
			      const int = 0);
  
  AuxField& times        (const AuxField&, const AuxField&);
  AuxField& divide       (const AuxField&, const AuxField&);  
  AuxField& timesPlus    (const AuxField&, const AuxField&);
  AuxField& timesMinus   (const AuxField&, const AuxField&);
  AuxField& crossProductPlus (const int, const vector<double>&,
                              const vector<AuxField*>&);
  AuxField& crossXPlus  (const int, const vector<double>&);
  AuxField& vvmvt       (const AuxField&, const AuxField&, const AuxField&);
  AuxField& mag         (const vector<AuxField*>&);

  AuxField& transform   (const int);
  AuxField& transform32 (const int, double*);
  AuxField& addToPlane  (const int, const double);
  AuxField& addToPlane  (const int, const double*);
  AuxField& getPlane    (const int, double*);
  AuxField& setPlane    (const int, const double*);
  AuxField& setPlane    (const int, const double);

  AuxField& gradient (const int);
  AuxField& sqroot   ();
  AuxField& mulY     ();
  AuxField& divY     ();
  AuxField& mulX     ();
  AuxField& sgn      ();
  AuxField& exp      ();
  AuxField& abs      ();
  AuxField& pow      (const double);
  AuxField& clipUp   (const double = 0.0);
  AuxField& zeroNaN  ();

  void gradient (const int, const int, double*, const int) const;
  void mulY     (const int, double*)                       const;
  void divY     (const int, double*)                       const;
  void mulX     (const int, double*)                       const;

  void   errors      (const Mesh*, const char*);
  void   lengthScale (double*)                const;
  double norm_inf    ()                       const;
  double mode_L2     (const int)              const;
  double integral    ()                       const;
  double integral    (const int)              const;
  Vector centroid    (const int)              const;
  double CFL         (const int, int& el)   const;
  double area        ()                       const;

  double probe (const Element*, const double, const double, const int   )const;
  double probe (const Element*, const double, const double, const double)const;

  AuxField& reverse     ();
  AuxField& zeroNyquist ();

  AuxField& perturb     (const double, const int = -1);
  AuxField& projStab    (const double, AuxField&);
  AuxField& smooth      (const int, const int*, const double*);

  static void swapData  (AuxField*, AuxField*);
  static void couple    (AuxField*, AuxField*, const int);

protected:

  char              _name ;	//!< Identification tag.  '\0' by default.
  vector<Element*>& _elmt ;	//!< Quadrilateral elements.
  int               _nz   ;	//!< Number of data planes (per process).
  int               _size ;	//!< _nz * Geometry::planeSize().
  double*           _data ;	//!< 2/3D data area, element x element x plane.
  double**          _plane;	//!< Pointer into data for each 2D frame.

private:

};


void readField  (istream&, vector<AuxField*>&);
void writeField (ostream&, const char*, const int, const double,
		 vector<AuxField*>&);

#endif
