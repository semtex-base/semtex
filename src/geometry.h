#ifndef GEOMETRY_H
#define GEOMETRY_H

#include <cfemdef.h>

class Geometry
// ===========================================================================
// Details of the logical/storage (as opposed to geometric)
// representation used for scalar fields.  Static functions make
// information globally accessible - this is effectively a singleton
// class, with no constructor.
//
// In all cases, 2D quad elements are employed, with a possible
// extension by Fourier expansions in the third dimension.  While the
// representation implied by this class is not necessarily (if
// typically) conforming, the same order of interpolation is used in
// each element.  Equal-order interpolation is used in each direction
// on faces of quads.
//
// With the introduction of concurrent execution, the concept of
// geometry has been extended to include the processor ID, number of
// processors, number of data planes per processor, etc.  And with the
// possibility of 2D partitioning, we allow a distinction between the
// number of elements in the partition and those in the whole 2D mesh.
//
// Copyright (c) 1994+, Hugh M Blackburn
// ===========================================================================
{
public:
  enum CoordSys { Cartesian, Cylindrical };

  static void set (const int, const int, const int, const CoordSys);

  static CoordSys system      () { return _csys;                 }  
  static bool     cylindrical () { return _csys == Geometry::Cylindrical; }

  static int  nP        () { return _np;                   }
  static int  nZ        () { return _nz;                   }
  static int  nElmt     () { return _nel;                  }
  static int  nTotElmt  () { return _np * _np;             }
  static int  nExtElmt  () { return 4 * (_np - 1);         }
  static int  nIntElmt  () { return (_np - 2) * (_np - 2); }
  static int  nMode     () { return (_nz + 1) >> 1;        }
  static int  nDim      () { return _ndim;                 }
  static int  nPlane    () { return _nel * nTotElmt();     }
  static int  nBnode    () { return _nel * nExtElmt();     }
  static int  nInode    () { return _nel * nIntElmt();     }
  static int  nTot      () { return _nz  * nPlane();       }
  static int  planeSize () { return _psize;                }
  static int  nTotal    () { return _nz * _psize;          }

  static int  nProc     () { return _nproc;                }
  static int  procID    () { return _pid;                  }
  static int  nZProc    () { return _nzp;                  }
  static int  nZ32      () { return (_nproc > 1) ? _nzp : (3 * _nz) >> 1; }
  static int  nTotProc  () { return _nzp * _psize;         }
  static int  nModeProc () { return nMode() / _nproc;      }
  static int  baseMode  () { return _pid * nModeProc();    }
  static int  basePlane () { return _pid * _nzp;           }
  static int  nBlock    () { return _psize / _nproc;       }

private:
  static int      _nproc ;	// Number of processors.
  static int      _pid   ;	// ID for this processor, starting at 0.
  static int      _ndim  ;	// Number of space dimensions.
  static int      _np    ;	// Number of points along element edge.
  static int      _nz    ;	// Number of planes (total).
  static int      _nzp   ;	// Number of planes per processor.
  static int      _nel   ;	// Number of elements.
  static int      _psize ;	// nPlane rounded up to suit restrictions.
  static CoordSys _csys  ;	// Coordinate system (Cartesian/cylindrical).

};
#endif
