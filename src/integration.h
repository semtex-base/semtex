#ifndef INTEGRATION_H
#define INTEGRATION_H

class Integration
// ===========================================================================
// Return coefficients for time-integration schemes.
// ===========================================================================
{
public:
  static const int OrderMax;

  static void AdamsBashforth (const int, double*);
  static void AdamsMoulton   (const int, double*);
  static void StifflyStable  (const int, double*);
  static void Extrapolation  (const int, double*);
};

#endif
