#ifndef FAMILY_H
#define FAMILY_H

namespace Family {
  void abandon (double**);
  void adopt   (const int, double**);
}

#endif
