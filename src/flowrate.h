#ifndef FLOWRATE_H
#define FLOWRATE_H


class Flowrate
// ===========================================================================
// Routines to measure and set volumetric flow rates using Stokes
// Green's functions.
//
// This part of the project is TBD.
// ===========================================================================
{
public:
  Flowrate (Domain*, FEML*);
  ~Flowrate () { }

  double getQ () const;
  double setQ (AuxField*, AuxField*) const;

protected:
  Domain*           _src;
  vector<Edge*>     _curve;
  vector<AuxField*> _green;
  double            _gamma;
  double            _refQ ;

};

#endif
