#ifndef HISTORY_H
#define HISTORY_H


class HistoryPoint
// ===========================================================================
// Class used to provide x,y,z history information.
// ===========================================================================
{
public:
  HistoryPoint (const int id, const Element* e, const double r, 
		const double s, const double x, const double y, const double z):
    _id (id), _E (e), _r (r), _s (s), _x (x), _y (y), _z (z) { }

  int                 ID      () const { return _id; } 
  void                  extract (vector<AuxField*>&, double*) const;
  static const Element* locate  (const double, const double,
				 vector<Element*>&, double&, double&);

private:
  const int      _id;		// Numeric identifier.
  const Element* _E ;		// Pointer to element.
  const double   _r ;		// Canonical-space r-location.
  const double   _s ;		// Canonical-space s-location.
  const double   _x ;		// x location.
  const double   _y ;		// y location.
  const double   _z ;		// Location in homogeneous direction.
};

#endif
