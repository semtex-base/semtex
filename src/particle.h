#ifndef PARTICLE_H
#define PARTICLE_H

class FluidParticle
// ===========================================================================
// Class used to locate and integrate positions of massless particles.
// ===========================================================================
{
public:
  FluidParticle (Domain*, const int, Point&);
  void            integrate (); 
  int             ID        () const { return _id;     }
  double          ctime     () const { return _ctime;  }
  const  Element* inMesh    () const { return _E;      }
  const  Point&   location  () const { return _p;      } 
  static int      IDMax     ()       { return _ID_MAX; }

private:
  const int    _id   ;	// Numeric tag.
  const double   _ctime;	// Time of initialisation.
  int          _step ;	// Number of integration steps.
  const Element* _E    ;        // Pointer to the element particle is in.
  double         _r    ;        // Corresponding "r" location within element.
  double         _s    ;	// likewise for "s".
  Point          _p    ;	// Physical space location.
  double*        _u    ;	// Multilevel "x" velocity storage.
  double*        _v    ;	// Multilevel "y" velocity storage.
  double*        _w    ;	// Multilevel "z" velocity storage.

  static Domain* _Dom    ;	// Velocity fields and class functions.
  static int     _NCOM   ;	// Number of velocity components.
  static int     _NEL    ;	// Number of elements in mesh.
  static int     _NZ     ;	// Number of z planes.
  static int     _TORD   ;	// Order of N--S timestepping.
  static int     _ID_MAX ;	// Highest issued id.
  static double* _P_coeff;	// Integration (predictor) coefficients.
  static double* _C_coeff;	// Integration (corrector) coefficients.
  static double* _Work   ;	// Work area for point location routines.
  static double  _DT     ;	// Time step.
  static double  _Lz     ;	// Periodic length.
};

#endif
