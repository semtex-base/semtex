#ifndef MESSAGE_H
#define MESSAGE_H

#include <cfemdef.h>

namespace Message {
  void init      (int* argc, char*** argv, int& nproc, int& iproc);
  void stop      ();
  void sync      ();
  
  void send      (double* data, const int N, const int tgt);
  void send      (int*    data, const int N, const int tgt);
  void recv      (double* data, const int N, const int src);
  void recv      (int*    data, const int N, const int src);

  void grid      (const int& npart2d, int& ipart2d,
		  int& npartz,        int& ipartz);
  
  void exchange  (double* data, const int nZ, const int nP, const int sign);
  void exchange  (int*    data, const int nZ, const int nP, const int sign);
}
#endif



