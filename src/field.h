#ifndef FIELD_H
#define FIELD_H

class Field : public AuxField
//  ==========================================================================
/// Field adds boundary conditions and global numbering to AuxField.
/// With the boundary conditions comes knowledge of which sections of
/// the boundary hold essential (known) nodes, and which contain
/// natural (unknown) nodes.
///
/// A Field holds global node numbers and solve masks for element
/// boundaries: where domain boundary value is given by an essential
/// BC, the solve mask value is 0, for all other nodes the value is 1.
///
/// Helmholtz problem solution
/// --------------------------
/// Field solve routines provide solution to the discrete form of the
/// Helmholtz equation
/// \f[
///                 \nabla^2 u - \lambda^2 u = f
/// \f]
///
/// on domain \f$\Omega\f$, subject to essential BCs \f$u=g\f$ on
/// \f$\Gamma_g\f$ and natural BCs \f$\partial u/\partial n=h\f$ on
/// \f$\Gamma_h\f$, where the boundary \f$\Gamma\f$ of \f$\Omega\f$ is
/// the union of (non-overlapping) \f$\Gamma_g\f$ and \f$\Gamma_h\f$
/// and \f$\boldmath{n}\f$ is the unit outward normal vector on
/// \f$\Gamma\f$.  \f$\lambda^2\f$ is called the Helmholtz constant
/// below.
///
/// The Galerkin form, using integration by parts with weighting functions w
/// which are zero on \f$\Gamma_g\f$, is
/// \f[
///            (\nabla u, \nabla w) + \lambda^2 (u, w) = - (f, w) + <h, w>
/// \f]
/// where
/// + \f$(a, b)=\int a.b~d\Omega\f$ is an integration over the domain and
/// + \f$<a, b>=\int a.b~d\Gamma\f$ is an integration over the domain boundary.
///
/// The discrete (finite element) equivalent is to solve
/// \f[
///                   K.u + \lambda^2 M.u = - M.f + <h, w>
/// \f]
/// or
/// \f[
///                         H.u = - M.f + <h, w>
/// \f]
/// where \f$K, M\f$ and \f$H\f$ are respectively (assembled)
/// "stiffness", "mass" and Helmholtz matrices.
///
//  Some complications arise from dealing with essential boundary
//  conditions, since typically the elemental matrices K^e, M^e which
//  are assembled to form K and M do not account for the boundary
//  requirements on w.  There are a number of ways of dealing with this
//  issue: one approach is to partition H as it is formed (here F =
//  -M.f + <h, w>):
// 
//    +--------+-------------+ /  \     /  \
//    |        |             | |  |     |  |
//    |   Hp   |     Hc      | |u |     |F |   u : nodal values for solution.
//    |        |(constraint) | | s|     | s|    s
//    |        |             | |  |     |  |       (n_solve values)
//    +--------+-------------+ +--+     +--+
//    |        | H_ess: this | |  |  =  |  |
//    |        | partition   | |  |     |  |
//    |    T   | relates to  | |u |     |F |   u : are given essential BCs.
//    |  Hc    | essential   | | g|     | g|    g
//    |        | BC nodes    | |  |     |  |       (n_global - n_solve values)
//    |        | and is not  | |  |     |  |
//    |        | assembled.  | |  |     |  |
//    +--------+-------------+ \  /     \  /
//
//  Partition out the sections of the matrix corresponding to the known
//  nodal values (essential BCs), and solve instead the constrained
//  problem
//
//    +--------+               /  \     /  \     +-------------+ /  \
//    |        |               |  |     |  |     |             | |  |
//    |   Hp   |               |u |     |F |     |     Hc      | |  |
//    |        |               | s|  =  | s|  -  |             | |u |
//    |        |               |  |     |  |     |             | | g|
//    +--------+               \  /     \  /     +-------------+ |  |.
//                                                               |  |
//                                                               \  /
//  Here n_global is the number of nodes that receive global node
//  numbers, typically those on the mesh edges.  N_solve is the number
//  of these nodes that have values that must be solved for,
//  i.e. n_global minus the number of global nodes situated on
//  essential-type boundaries.
// 
//  The action of Hc on u_g can be formed by a loop over the elements
//  which touch the essential boundaries and does not require storage
//  of the partition Hc.  M.f can also be formed on an
//  element-by-element basis and is cheap for nodal spectral elements
//  since M is diagonal.  Both tasks are performed by the
//  Field::constrain routine.
//
/// Field names
/// -----------
/// The (one character) names of field variables are significant, and have
/// the following reserved meanings:
/// 
/// + u:  First velocity component.           (Cylindrical: axial     velocity.)
/// + v:  Second velocity component.          (Cylindrical: radial    velocity.)
/// + w:  Third velocity component.           (Cylindrical: azimuthal velocity.)
/// + p:  Pressure divided by density.
/// + c:  Scalar for transport or elliptic problems.
///
//  ==========================================================================
{
friend class BCmgr;
public:
  Field  (double*, BoundarySys*, NumberSys*, const int,
	  vector<Element*>&, const char);
 ~Field  () { }
 
  Field& operator = (const AuxField& z) {AuxField::operator=(z); return *this;}
  Field& operator = (const double&   z) {AuxField::operator=(z); return *this;}
  Field& operator = (const char*     z) {AuxField::operator=(z); return *this;}

  Field& solve  (AuxField*, const ModalMatrixSys*);

  void evaluateBoundaries    (const Field*, const int, const bool = true);
  void evaluateM0Boundaries  (const Field*, const int);
  void addToM0Boundaries     (const double, const char*);
  void bTransform            (const int);

  void overwriteForGroup      (const char*, const AuxField*, AuxField*);

  static double scalarFlux    (const Field*);
  static Vector normTraction  (const Field*);
  static Vector tangTraction  (const Field*, const Field*, const Field* = 0);
  static void   normTractionV (double*, double*, const Field*);
  static void   tangTractionV (double*, double*, double*, const Field*,
			       const Field*, const Field* = 0);
  static void   traction      (double*, double*, double*,
			       const int, const int, const Field*,
			       const Field*, const Field*, const Field* = 0);
  static void   coupleBCs     (Field*, Field*, const int);
  static double modeConstant  (const char, const int, const double);

private:
  int           _nbound;	//!<  Number of boundary edges.
  int           _nline ;	//!<  Length of one boundary line.
  double*       _sheet ;	//!<  Wrap-around storage for data boundary.
  double**      _line  ;	//!<  Single plane's worth of sheet.
  BoundarySys*  _bsys  ;	//!<  Boundary system information.
  NumberSys*    _nsys  ;        //!<  Assembly mapping information.

  void getEssential      (const double*, double*,
			  const vector<Boundary*>&, const AssemblyMap*) const;
  void setEssential      (const double*, double*, const AssemblyMap*);

  void local2global      (const double*, double*, const AssemblyMap*)   const;
  void global2local      (const double*, double*, const AssemblyMap*)   const;
  void local2globalSum   (const double*, double*, const AssemblyMap*)   const;

  void constrain         (double*, const double, const double,
			  const double*, const AssemblyMap*, double*)   const;
  void buildRHS          (double*,const double*, double*, double*, 
			  const double**, const int, const int,
			  const vector<Boundary*>&,
			  const AssemblyMap*, double*)                  const;
  void HelmholtzOperator (const double*, double*, const double,
			  const double, const int, double*)           const;
};

#endif
