#ifndef SVV_H
#define SVV_H

#include <cfemdef.h>

// ===========================================================================
//
// ===========================================================================

namespace SVV {

  const double* coeffs    (const int);
  const double* coeffs_z  (const int);

  void          operators (const int, const double**, const double**);
}

#endif
