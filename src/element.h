#ifndef ELEMENT_H
#define ELEMENT_H

class Element
//  ==========================================================================
// Virtual 2D quadrilateral element class, equal order in each direction.
//
//         s                                y ^     +4
//         ^                                  |    / \
//     4   |                                  |  1+   +3
//     +------+                               |    \  |
//     |   |  |      <== Logical  space       |     \ |
//     |   +--|-> r      Physical space ==>   |      \|
//     |      |                               |       +2
//     +------+                               +-----------> x
//     1      2
//
// Master element coordinates: -1 < r,s < 1; edge traverses CCW.
//
// Elements in this class will use Gauss--Lobatto--Legendre
// meshes, integration, and nodal basis functions.
//
// All 2D storage is row-major. NB: starting at bottom-left corner of
// element and working across and up (instead of across and down).
// ===========================================================================
{
public:
  Element (const int,const int,const Mesh*);
  ~Element();
  
  int ID () const { return _id; }

  // -- Elemental Helmholtz matrix constructor, operator.

  void HelmholtzSC   (const double,const double,double*,double*,
		      double*,double*,double*,int*)                      const;
  void HelmholtzDiag (const double,const double,double*,double*)         const;
  void HelmholtzKern (const double,const double,
		      double*,double*,double*,double*)                   const;
  void HelmholtzOp   (const double,const double,double*,double*,double*) const;

  // -- Local/global projectors.

  void local2global      (const double*,const int*,double*,double*)    const;
  void local2globalSum   (const double*,const int*,double*,double*)    const;
  void local2globalSumSC (double*,const int*,double*,const double*,
			  double*)                                     const;
  void global2local      (double*,const int*,const double*,
			  const double*)                               const;
  void global2localSC    (const double*,const int*,double*,double*,
			  const double*,const double*,double*)         const;

  // -- Project from one interpolation order to another.

  void project (const int,const double*,const int,double*,double*)     const;
  
  // -- Element-boundary operators.

  void bndryDsSum  (const int*,const double*,double*)                  const;
  void bndryMask   (const int*,double*,const double*,const int*)       const;
  void bndryInsert (const int*,const double*,double*)                  const;

  // -- Element-side operators.

  void sideGeom  (const int,double*,double*,double*,double*,double*)   const;
  void sideEval  (const int,double*,const char*)                       const;
  void sideGrad  (const int,const double*,double*,double*,double*)     const;
  void sideGet   (const int,const double*,double*)                     const;
  void sideGetY  (const int,double*)                                   const;
  void sideMulY  (const int,const double*,double*)                     const;
  void sideDivY  (const int,const double*,double*)                     const;
  void sideDivY2 (const int,const double*,double*)                     const;

  // -- Element-operator functions.

  void grad  (double*,double*,double*)             const;
  void gradX (const double*,const double*,double*) const;
  void gradY (const double*,const double*,double*) const;

  void divY (double*) const;
  void mulY (double*) const;
  void mulX (double*) const;
  void crossXPlus (const int,const double,const vector<double>&,double*) const;
  
  void evaluate (const char*,double*) const;

  double integral (const char*,  double*) const;
  double integral (const double*,double*) const;
  double momentX  (const char*,  double*) const;
  double momentY  (const char*,  double*) const;
  double momentX  (const double*,double*) const;
  double momentY  (const double*,double*) const;
  double area     ()                      const;
  void   weight   (double*)               const;

  void   lengthScale (double*)                              const;
  double CFL         (const double*,const double*,double*)  const;

  double norm_inf (const double*) const;
  double norm_L2  (const double*) const;
  double norm_H1  (const double*) const;

  // -- Probe functions.

  bool   locate (const double,const double,double&,double&,
		 double*,const bool = false)                      const;
  double probe  (const double,const double,const double*,double*) const;

  // -- Debugging/informational routines.

  void printMesh  ()              const;
  void printBndry (const double*) const;
  void printMatSC (const double*,const double*,const double*)            const;
  void Helmholtz  (const double,const double,double*,double*,double*)    const;

protected:

  const int     _id   ;		// Element identifier.
  const int     _np   ;		// Number of points on an edge.
  const int     _npnp ;		// Total number = np * np.
  const int     _next ;		// Number of points on periphery.
  const int     _nint ;		// Number of internal points.
  const bool    _cyl  ;         // Cylindrical coordinate problem.

  const double* _zr   ;		// Master element mesh points on [-1, 1], r.
  const double* _zs   ;		// Master element mesh points on [-1, 1], s.
  const double* _wr   ;		// Master element quadrature weights, r.
  const double* _ws   ;		// Master element quadrature weights, s.
  const double* _DVr  ;		// Master element derivative operator, r.
  const double* _DTr  ;		// Transpose.
  const double* _DVs  ;		// Master element derivative operator, s.
  const double* _DTs  ;		// Transpose.
  const double* _SDVr ;		// As above, but with SVV modifications.
  const double* _SDTr ;
  const double* _SDVs ;
  const double* _SDTs ;

  int*          _emap ;		// Indices of edges in nodal matrices.
  int*          _pmap ;		// Inversion of emap (pmap[emap[i]] = i).

  double*       _xmesh;		// Physical space mesh.
  double*       _ymesh;		// 2D row-major store.

  double*       _drdx ;		// Partial derivatives (r, s) --> (x, y),
  double*       _dsdx ;		//   evaluated at quadrature points.
  double*       _drdy ;		//   (2D row-major storage.)
  double*       _dsdy ;		//

  double*       _delta;		// Local length scale.

  double*       _Q1   ;		// Geometric factor 1 at quadrature points.
  double*       _Q2   ;		// Geometric factor 2 at quadrature points.
  double*       _Q3   ;		// Geometric factor 3 at quadrature points.
  double*       _Q4   ;		// Geometric factor 4 at quadrature points.
  double*       _Q8   ;	        // Like _Q4 but without possible factor of y.

  // -- Geometric and quadrature-specific internal functions.

  void mapping      ();
  void HelmholtzRow (const double, const double, const int,
		     const int, double*, double*) const;

  // -- BLAS-conforming edge offsets & skips to produce
  //    CCW element-edge traverses.

  void terminal (const int side, int& start, int& skip) const
  { switch (side) {
  case 0: start = 0;             skip  = 1;    break;
  case 1: start = _np - 1;       skip  = _np;  break;
  case 2: start = _np*(_np - 1); skip  = -1;   break;
  case 3: start = 0;             skip  = -_np; break;
  } }
};

#endif
