#ifndef MESH_H
#define MESH_H

///////////////////////////////////////////////////////////////////////////////
// mesh: header file for Mesh and related classes.
///////////////////////////////////////////////////////////////////////////////

#include <vector>
#include <string>

#include <cfemdef.h>
#include <feml.h>

class Curve;

class Mesh
// ===========================================================================
// Mesh class is used as a storage for inter-element connectivities,
// BC tags, element-corner vertex locations and curved boundaries.
// It plays no direct part in the eventual solution of the discrete
// equations.  Presently, meshes are 2D only.
// 
// Mesh provides seven externally-visible facilities: 
// (1) Return number of elements in mesh.
// (2) Fill a vector with indices of elements on current 2D partition.
// (3) Generation of (BC-naive) element-boundary assembly mapping.
// (4) Generation of element-boundary Essential BC mask vector.
// (5) Generation of element mesh knot points.
// (6) Print up NEKTON-style .rea information (backwards compatibility).
// (7) Return x--y extent of mesh, based on traverse of element vertices.
// (8) Return true if an element touches the z axis (cylindrical coords).
//
// (3--5) can be independently computed for arbitrary polynomial orders.
// ===========================================================================
{
public:
  class Node;
  class Elmt;
  class Side;

  Mesh (FEML*, const bool = true);

  void  partMap (const int, const int, vector<int>&) const;

  int nEl () const { return _elmtTable.size(); }

  void  buildLiftMask    (const int, const char, const int, int*) const;
  int buildAssemblyMap (const int, int*) const;
  
  void  meshElmt    (const int, const int, const double*, const double*,
		     double*, double*) const;
  void  printNek    () const;
  void  extent      (Point&, Point&) const;
  void  assemble    (const bool = false);

  void  buildDualGraph (vector<int>&, vector<int>&, const int = 0) const;

  static void showGlobalID (Mesh&);
  static void showAssembly (Mesh&);

  class Node {
  public:
    int   ID;
    int   gID;
    Point loc;
    Node* periodic;
  };

  class Elmt {
    friend class Mesh;
  private:
    vector<Node*> node;
    vector<Side*> side;
  public:
    int ID;
    int nNodes   () const  { return node.size(); }
    Node* ccwNode  (int i) { return node[(i         +1) % nNodes()]; }
    Node* cwNode   (int i) { return node[(i+nNodes()-1) % nNodes()]; }
    Point centroid () const;
  };
  
  class Side {
    friend class Mesh;
  private:
    Side (int id, Node* n1, Node* n2) :
      ID(id), startNode(n1), endNode(n2), mateElmt(0) {
      gID.resize (0); mateSide = 0;
      axial = (n1 -> loc.y == 0.0) && (n2 -> loc.y == 0.0);
    }
    Side () {}
  public:
    int         ID;
    bool          axial;
    vector<int> gID;
    Node*         startNode;
    Node*         endNode;
    Elmt*         thisElmt;
    Elmt*         mateElmt;	// -- Doubles as a flag for union, if set/NULL:
    union { Side* mateSide; char group; };
    void connect (const int, int&);
  };

private:
  FEML&          _feml;
  vector<Node*>  _nodeTable;
  vector<Elmt*>  _elmtTable;
  vector<Curve*> _curveTable;

  void surfaces ();
  void curves   ();
  bool _checked;   // -- Connectivity checking was/not applied by constructor.

  void checkAssembly ();
  void chooseNode    (Node*, Node*);
  void fixPeriodic   ();

  void describeGrp (char, char*)                          const;
  void describeBC  (char, char, char*)                    const;
  bool matchBC     (const char, const char, const char)   const;

  void meshSide (const int, const int, const int,
		 const double*, Point*)                   const;
};


class Curve
// ===========================================================================
// Base class for curved edges.
// ===========================================================================
{
public:
  virtual ~Curve () { }

  virtual void compute  (const int, const double*, Point*) const = 0;
  virtual void printNek ()                                   const = 0;

  bool ismatch (const int e, const int s) const {
    return (e == curveSide -> thisElmt -> ID && s == curveSide -> ID);
  }

protected:
  Mesh::Side* curveSide;
};


class CircularArc : public Curve
// ===========================================================================
// Prototype curved edge class.   To make new kinds, retain the same
// interface structure and put new instances in Mesh::curves.
// ===========================================================================
{
public:
  CircularArc (const int, Mesh::Side*, const double);
  virtual void compute  (const int, const double*, Point*) const;
  virtual void printNek () const;

private:
  Point  centre;
  double radius;
  double semiangle;
  int  convexity;
};


class spline2D
// ===========================================================================
// Definition of spline2D struct used by Spline class.
// ===========================================================================
{
public:
  char*          name  ;	// name of file containing knot points
  vector<double> x     ;	// knot x-coordinates
  vector<double> y     ;	// knot y-coordinates
  vector<double> sx    ;	// spline x-coefficients
  vector<double> sy    ;	// spline y-coefficients
  vector<double> arclen;        // arclength along curve at knot locations
  int          pos   ;	// index of last confirmed position
};


class Spline : public Curve
// ===========================================================================
// A 2D splined curve defined in a named file.
// ===========================================================================
{
public:
  Spline (const int, Mesh::Side*, const char*);
  virtual void compute  (const int, const double*, Point*) const;
  virtual void printNek () const;

private:
  char*     _name;
  spline2D* _geom;
  double    _startarc;
  double    _endarc;

  spline2D* getGeom  (const char*);
  double    getAngl  (const double&);
  int     closest  (const Point&);
  double    arcCoord ();
};

#endif
