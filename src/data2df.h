#ifndef DATA2DF_H
#define DATA2DF_H

///////////////////////////////////////////////////////////////////////////////
// Define simple routines to handle quad spectral element x Fourier
// data (class Data2DF), plus simple header data I/O class (class
// Header) both with public data.
///////////////////////////////////////////////////////////////////////////////


class Data2DF
// ============================================================================
// Canonical field class, each np X np element is defined on [-1,1] X [-1, 1].
// Data are arranged element-ordered in 2D planes to create a 3D scalar field.
// ============================================================================
{
friend istream& operator >> (istream&, Data2DF&);
friend ostream& operator << (ostream&, Data2DF&);

public:
  Data2DF  (const int nP, const int nZ, const int nEl,
	    const char Name='\0');
  ~Data2DF () { delete [] _data; delete [] _plane; }

  char getName () { return _name; }
  Data2DF& reverse ();

  Data2DF& operator  =   (const double);
  Data2DF& operator *=   (const double);
  Data2DF& operator  =   (const Data2DF&);
  Data2DF& operator +=   (const Data2DF&);
  Data2DF& operator -=   (const Data2DF&);
  Data2DF& operator *=   (const Data2DF&);

  Data2DF& DFT1D         (const int);
  Data2DF& DPT2D         (const int, const char);

  Data2DF& filter1D      (const double, const int);
  Data2DF& filter2D      (const double, const int);

  Data2DF& F_conjugate   (const bool);
  Data2DF& F_symmetrize  (const bool);
  Data2DF& F_shift       (const double alpha, const bool);

  Data2DF& reflect2D     (vector<int>&, vector<int>&);

#if 0 // -- Sorry: wanted the convenience of public data.
protected:
#endif
  const char _name;
  const int  _np, _nz, _nel, _np2;
  int        _nplane, _ntot;
  double*    _data;
  double**   _plane;
};


class Header
// ===========================================================================
// Nekton/Prism/Semtex-compatible header struct + I/O routines +
// public data descriptors.  No array data storage.
// ===========================================================================
{
public:
  Header();
  

  Header (const char* session,
	  const char* created,
	  int         nR     ,
	  int         nS     ,
	  int         nZ     ,
	  int         nEL    ,
	  int         runstep,
	  double      runtime,
	  double      deltat ,
	  double      kinvis ,
	  double      wavenum,
	  const char* fields ,
	  const char* wformat);

 ~Header() { delete [] sess; delete [] sesd; delete [] flds; delete [] frmt; }

  bool  swab    () const;
  int nFields () const { return strlen (flds); }

  char*  sess;
  char*  sesd;
  int    nr  ;
  int    ns  ;
  int    nz  ;
  int    nel ;
  int    step;
  double time;
  double dt  ;
  double visc;
  double beta;
  char*  flds;
  char*  frmt;
};
istream& operator >> (istream&, Header&);
ostream& operator << (ostream&, Header&);

#endif
