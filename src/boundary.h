#ifndef BOUNDARY_H
#define BOUNDARY_H

class Boundary : public Edge
// ===========================================================================
// Scalar field element-wise boundary class.  These evaluate and apply
// a (boundary) Condition to a Field on a particular side (Edge) of an
// Element that lies on the periphery of a computational Domain.
// ===========================================================================
{
public:
  Boundary (const int id, const char* group, const Condition* bcond,
	    const Element* elmt, const int side):
            Edge (group, elmt, side), _id (id), _bcond (bcond) { }

  int   ID        () const { return _id; }
  void  print     () const;

  void  evaluate  (const Field*,const int,const int,const bool,
		   double*)                                              const;

  // -- Impose essential BCs:
  void  set       (const double*,const int*,double*)                   const;

  // -- Apply natural BCs:
  void  sum       (const double*,const int*,double*,double*)           const;

  // -- Apply mixed BCs:
  void  augmentSC (const int,const int,const int*,double*,double*) const;
  void  augmentOp (const int*,const double*,double*)                   const;
  void  augmentDg (const int*,double*)                                 const;

private:
  int              _id   ;	// Ident number.
  const Condition* _bcond;	// Boundary condition.
};

#endif
